<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductTVCCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_transactions', function (Blueprint $table) {
            $table->string('product_tvc_code')->nullable()->comment('Mã product trên Tavico');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_transactions', function (Blueprint $table) {
            $table->dropColumn('product_tvc_code');
        });
    }
}
