<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gb_id')->unique();
            $table->integer('gb_status')->default(0)->comment('Trạng thái nhóm');
            $table->integer('gb_title')->nullable();
            $table->string('gb_code')->nullable();
            $table->text('gb_description')->nullable();
            $table->integer('reference_id')->nullable()->comment('Sàn trên CRM - tavico');
            $table->integer('level_id')->nullable();
            $table->timestamp('gb_created_time')->nullable();
            $table->timestamp('gb_updated_time')->nullable();
            $table->integer('gb_updated_user')->nullable();
            $table->integer('gb_created_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_user_groups');
    }
}
