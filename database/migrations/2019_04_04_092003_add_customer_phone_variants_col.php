<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerPhoneVariantsCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_customers', function (Blueprint $table) {
            $table->json('phone_variants')->nullable()->comment('Số điện thoại khác');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_customers', function (Blueprint $table) {
            $table->dropColumn('phone_variants');
        });
    }
}
