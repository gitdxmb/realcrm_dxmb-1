<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillRequiredMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->integer('bill_required_money')->after('bill_total_money')->nullable()->comment('Số tiền yêu cầu cọc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->dropColumn('bill_required_money');
        });
    }
}
