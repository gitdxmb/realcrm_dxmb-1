<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApartmentGrid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_categories', function (Blueprint $table) {
            $table->boolean('apartment_grid')->nullable()->default(true)->after('type')->comment('true - Chung cư; false - LKBT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_categories', function (Blueprint $table) {
            $table->dropColumn('apartment_grid');
        });
    }
}
