<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->comment('ID bài post');
            $table->text('comment')->nullable()->comment('Nội dung bình luận văn bản');
            $table->tinyInteger('option')->nullable()->comment('Lựa chọn vote / Tình trạng check-in');
            $table->integer('created_by')->comment('Người tạo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_feedbacks');
    }
}
