<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerStaffmappingInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_customer_mappings', function (Blueprint $table) {
            $table->string('c_title')->nullable()->comment('Tên khách hàng');
            $table->string('c_phone')->nullable()->comment('SĐT khách hàng');
            $table->text('c_address')->nullable()->comment('ĐC khách hàng');
            $table->text('c_note')->nullable()->comment('Ghi chú khách hàng');
            $table->integer('c_rating')->default(0)->comment('Điểm tiềm năng khách hàng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_customer_mappings', function (Blueprint $table) {
            $table->dropColumn('c_title');
            $table->dropColumn('c_phone');
            $table->dropColumn('c_address');
            $table->dropColumn('c_note');
            $table->dropColumn('c_rating');
        });
    }
}
