<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BoUserJwt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->string('remember_jwt')->nullable()->comment('JWT đăng nhập từ app');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->dropColumn('remember_jwt');
        });
    }
}
