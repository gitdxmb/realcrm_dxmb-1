<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditBOBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            if ($table->getColumns('bill_customer_verified_time')) {
                $table->dropColumn("bill_customer_verified_time");
            }
            if ($table->getColumns('staff_verified_staff_id')) {
                $table->dropColumn("staff_verified_staff_id");
            }
            if ($table->getColumns('product_verified_staff_id')) {
                $table->dropColumn("product_verified_staff_id");
            }
            if ($table->getColumns('bill_staff_verified_time')) {
                $table->dropColumn("bill_staff_verified_time");
            }
            $table->json("bill_approval_log")->nullable()->comment("Log trạng thái duyệt thay đổi qua thời gian, do NVKD, KH, QLS duyệt, kèm ghi chú nếu có");
            $table->integer("book_request_id")->nullable()->comment("ID yêu cầu đặt chỗ. null/0 → hóa đơn đặt mua không qua đặt chỗ");
            $table->json("media")->nullable()->comment("DS tập tin đính kèm");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->dropColumn("bill_approval_log");
            $table->dropColumn("book_request_id");
            $table->dropColumn("media");
            $table->timestamp("bill_customer_verified_time");
            $table->integer("staff_verified_staff_id");
            $table->integer("product_verified_staff_id");
            $table->timestamp("bill_staff_verified_time");
        });
    }
}
