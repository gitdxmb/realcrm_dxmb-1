<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->float('value')->comment('Số lượng giao dịch');
            $table->integer('department')->comment('Mã sàn, phòng ban');
            $table->integer('building')->comment('Mã tòa nhà')->nullable();
            $table->integer('project')->comment('Mã dự án');
            $table->integer('created_by')->comment('Người tạo');
            $table->date('report_date')->comment('Ngày báo cáo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
