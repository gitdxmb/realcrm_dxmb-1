<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_customers', function (Blueprint $table) {
            $table->string('cb_id_passport')->nullable();
            $table->date('cb_issue_date')->nullable();
            $table->string('cb_issue_place')->nullable();
            $table->string('cb_permanent_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_customers', function (Blueprint $table) {
            $table->dropColumn(['cb_id_passport', 'cb_issue_date', 'cb_issue_place','cb_permanent_address']);
        });
    }
}
