<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ub_id')->unique();
            $table->integer('ub_status')->default(0);
            $table->string('ub_title')->nullable();
            $table->json('group_ids')->nullable();
            $table->integer('level_id')->nullable();
            $table->string('ub_account_name')->nullable();
            $table->string('ub_email')->nullable();
            $table->text('ub_token')->nullable();
            $table->timestamp('ub_last_logged_time')->nullable();
            $table->timestamp('ub_created_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_users');
    }
}
