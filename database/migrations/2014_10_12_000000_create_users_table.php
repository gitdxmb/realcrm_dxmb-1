<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unique();
            $table->integer('user_status')->default(0);
            $table->string('user_title')->nullable();
            $table->string('user_fullname');
            $table->integer('user_sex')->nullable();
            $table->integer('job_id')->nullable();
            $table->json('user_tag_ids')->nullable();
            $table->string('user_id_number')->nullable();
            $table->timestamp('user_id_date_issue')->nullable();
            $table->string('user_id_place_issue')->nullable();
            $table->string('user_telephone_number')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_addr')->nullable();
            $table->string('user_addr_in_id')->nullable();
            $table->string('user_location')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('user_company_location')->nullable();
            $table->double('user_sale_sum')->nullable();
            $table->double('user_buy_sum')->nullable();
            $table->timestamp('user_created_time')->nullable();
            $table->timestamp('user_updated_time')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->text('user_log_edit')->nullable();
            $table->text('user_log_transaction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
