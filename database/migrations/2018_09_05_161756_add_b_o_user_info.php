<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBOUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->integer('user_id')->nullable();
            $table->string('ub_staff_code')->nullable()->comment('Mã nhân viên DXMB');
            $table->string('ub_tvc_code')->nullable()->comment('Mã nhân viên Tavico');
            $table->string('ub_avatar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('ub_staff_code');
            $table->dropColumn('ub_tvc_code');
            $table->dropColumn('ub_avatar');
        });
    }
}
