<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pm_id')->unique();
            $table->tinyInteger('pm_status')->default(1);
            $table->string('pm_title')->nullable();
            $table->string('pm_code')->nullable();
            $table->text('pm_note')->nullable();
            $table->timestamp('pm_created_time')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->timestamp('pm_updated_time')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->text('pm_log_edit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
