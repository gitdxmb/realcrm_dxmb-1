<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_transactions', function (Blueprint $table) {
            $table->double('trans_deposit')->nullable('Số tiền yêu cầu khách cọc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_transactions', function (Blueprint $table) {
            $table->dropColumn('trans_deposit');
        });
    }
}
