<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BOCustomerSaveFCM extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_customers', function (Blueprint $table) {
            $table->string('device_token')->nullable()->comment('Token thiết bị để gửi FCM');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_customers', function (Blueprint $table) {
            $table->dropColumn('device_token');
        });
    }
}
