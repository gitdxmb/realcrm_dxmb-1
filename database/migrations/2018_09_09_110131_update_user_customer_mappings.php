<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserCustomerMappings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_customer_mappings', function (Blueprint $table) {
            $table->string('id_passport')->nullable();
            $table->date('issue_date')->nullable();
            $table->string('issue_place')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_customer_mappings', function (Blueprint $table) {
            $table->dropColumn(['id_passport', 'issue_date', 'issue_place','permanent_address', 'email']);
        });
    }
}
