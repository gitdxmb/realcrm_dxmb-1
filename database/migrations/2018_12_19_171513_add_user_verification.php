<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserVerification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->boolean('is_verified')->nullable()->default(false)->comment('Tình trạng xác thực email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_users', function (Blueprint $table) {
            $table->dropColumn('is_verified');
        });
    }
}
