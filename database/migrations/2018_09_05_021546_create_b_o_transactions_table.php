<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trans_id')->unique();
            $table->integer('trans_status');
            $table->string('trans_code')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('trans_title')->nullable();
            $table->text('trans_note')->nullable();
            $table->integer('staff_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->json('product_ids')->nullable();
            $table->double('trans_money')->nullable();
            $table->integer('bill_id')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->timestamp('trans_created_time')->nullable();
            $table->timestamp('trans_updated_time')->nullable();
            $table->integer('updated_user_id');
            $table->text('trans_log_edit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_transactions');
    }
}
