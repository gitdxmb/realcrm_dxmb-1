<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBONotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nb_title');
            $table->text('nb_body');
            $table->tinyInteger('nb_type')->comment('0-Căn hộ,1-Tin nội bộ, 2-Diễn đàn, 3-Giao dịch, 4-Hợp đồng');
            $table->integer('object_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_notifications');
    }
}
