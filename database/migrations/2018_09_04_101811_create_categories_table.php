<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unique();
            $table->integer('category_status')->default(0)->comment('1- xóa; 0 - ẩn; 1 - available');
            $table->string('category_code')->nullable()->comment('mã danh mục');
            $table->string('category_reference')->nullable()->comment('code để nối đến hệ thống khác (ví dụ tavico)');
            $table->integer('parent_id')->default(0)->comment('id danh mục cha');
            $table->integer('level_id')->comment('id cấp của danh mục');
            $table->string('category_title')->comment('tiêu đề');
            $table->text('category_short_description')->nullable()->comment('mô tả ngắn (dạng mô tả ngắn dưới tên – nếu có)');
            $table->text('category_description')->nullable()->comment('mô tả dài');
            $table->string('category_image_thumb')->nullable()->comment('ảnh đại diện');
            $table->text('category_media')->nullable()->comment('file');
            $table->json('extra_ids')->nullable()->comment('thêm (nối với bảng extra)');
            $table->json('permission_group_ids')->nullable()->comment('id phân quyền nhóm');
            $table->json('permission_staff_ids')->nullable()->comment('id phân quyền nhân viên');
            $table->integer('updated_user_id')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->timestamp('user_ created _time')->nullable();
            $table->timestamp('user_updated_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
