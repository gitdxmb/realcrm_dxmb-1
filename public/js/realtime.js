$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    Pusher.logToConsole = true;
    var pusher = new Pusher('8C3B1E6357C7B46D', {
        wsHost: "45.124.95.45",
        wsPort: "8080",
        'cluster': 'mt1',
        'appId': '1992',
        enabledTransports: ['ws', 'flash']
    });
    var channel = pusher.subscribe('apartment'); //
    channel.bind('change-status-apartment', function (data) {
        console.log(data);
        var apartmentId = data.apartmentId;
        var apartmentStatus = data.statusDisplay;
        console.log(apartmentStatus['color']);
        // console.log(apartmentStatus[status]['color']);
        // console.log(apartmentStatus[status]['icon-web']);
        $('#'+apartmentId).css({
            "background-color" : apartmentStatus['background-color'],
            "color" : apartmentStatus['color']
        });
        $('#icon-'+apartmentId).removeAttr('class').attr('class',apartmentStatus['icon-web']);
    });

});