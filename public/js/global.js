var GLOBAL_JS = {
    go_on_business: 'Phiếu công tác',
    tbl: $('#tbl').val(),
    /**
     * Auth: Dienct
     * Des: delete record
     * Since: 31/12/2015
     * */
    b_fValidateEmpty: function (e) {
        var t = /^ *$/;
        if (e == "" || t.test(e)) {
            return true;
        }
        return false;
    },
    b_fCheckEmail: function (e) {
        var t = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return t.test(e)
    },
    b_fCheckEmailDXMB: function (sz_Email) {
        var sz_check = sz_Email.substr(sz_Email.indexOf("@") + 1);
        if (sz_check != 'dxmb.vn')
            return false;
        else
            return true;
    },
    b_fCheckMinLength: function (e, the_i_Length) {
        if (e.length < the_i_Length) {
            return false;
        }
        return true;
    },
    b_fCheckMaxLength: function (e, the_i_Length) {
        if (e.length > the_i_Length) {
            return false;
        }
        return true;
    },
    b_fCheckConfirmPwd: function (e, t) {
        if (e == t) {
            return true;
        }
        return false;
    },
    v_fDelRow: function (id, type, field) {
        var sz_confirm = type == 1 ? "Bạn có muốn cho vào thùng rác?" : "Xóa vĩnh viễn tài khoản trên Izi. Bạn có muốn tiếp tục?";
        if (confirm(sz_confirm)) {
            var o_data = {
                id: id,
                type: type,
                field: field,
                func: 'delete-row',
                tbl: GLOBAL_JS.sz_Tbl,
            };
            $.ajax({
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: o_data,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                }
            });
        }
    },
    /**
     * Auth: Dienct
     * Des: recover record
     * Since: 31/12/2015
     * */
    v_fRecoverRow: function (id, field) {

        var o_data = {
            id: id,
            field: field,
            func: 'recover-row',
            tbl: GLOBAL_JS.sz_Tbl,
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {

                location.reload();
            }
        });
    },
    v_fUpdateStatus: function (id, type) {
        var sz_confirm = type == 1 ? "Bạn có muốn cho vào thùng rác?" : "Bạn có muốn xóa đơn này?";
        if (confirm(sz_confirm)) {
            var o_data = {
                id: id,
                type: type,
                func: 'update-status',
                tbl: GLOBAL_JS.sz_Tbl,
            };
            $.ajax({
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: o_data,
                dataType: 'json',
                success: function (data) {

                    location.reload();
                }
            });
        }
    },
    /**
     * Auth: Dienct
     * Des:
     * Since: 02/003/2017
     * */
    v_fSubmitProjectValidate: function () {
        var sz_name = $('#name').val();
        if (GLOBAL_JS.b_fValidateEmpty(sz_name)) {
            $('.required_name').remove();
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần nhập tên</strong></p>');
            $('.alert-danger').removeClass('hide');
            $('#name').focus();
            return false;
        }
        if ($('.alert-danger').text() != '')
            return false;
        $('.submit').click();
    },
    /**
     * Auth: Dienct
     * Des:
     * Since: 02/003/2017
     * */
    v_fSubmitUserInBoValidate: function () {

        var pwd = $('#password').val()
        var re_pwd = $('#re-password').val();

        if (pwd != re_pwd) {
            $('.required_name').remove();
            $('.alert-danger').append('<p><strong class="required_name">bạn cần nhập đúng password</strong></p>');
            $('.alert-danger').removeClass('hide');
            $('#name').focus();
            return false;
        }
        if ($('.alert-danger').text() != '')
            return false;
        $('.submit').click();
    },
    v_fSubmitPayMent: function () {
        var bill = $('#bill_id').val();
        var staff = $('#request_staff_id').val();
        var customer_id = $('#customer_id').val();
        if (bill == '' || staff == '' || customer_id == '') {
            $('.required_name').remove();
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn đủ dữ liệu</strong></p>');
            $('.alert-danger').removeClass('hide');
            $('#name').focus();
            return false;
        }

        if (GLOBAL_JS.b_fValidateEmpty(bill)) {
            $('.required_name').remove();
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần nhập tên</strong></p>');
            $('.alert-danger').removeClass('hide');
            $('#name').focus();
            return false;
        }
        if ($('.alert-danger').text() != '')
            return false;
        $('.submit').click();
    },

    v_fConfirmPayment: function (type, id, bill_code) {

        var money = $("#pb_response_money").val();
        var pb_bank_number_id = $("#pb_bank_number_id").val();
        var method_id = $("#method_id").val();
        if (type == '1' && money == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần nhập số tiền</strong></p>');
            $('.alert-danger').removeClass('hide');
            return;
        }


        var o_data = {
            id: id,
            bill_code: bill_code,
            type: type,
            money: money,
            pb_bank_number_id: pb_bank_number_id,
            method_id: method_id,
            func: 'confirm-payment'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                alert(data.success);
                window.location.replace(GLOBAL_JS.sz_CurrentHost + "/payment");
            }
        });
    },


    /**
     * Auth: Dienct
     * Des:
     * Since: 02/003/2017
     * */

    v_fGetObjectIdRole: function () {
        var key = $("#ro_key").val();
        if (key != 'project') {
            $('#ro_object_ids').html('');
            return;
        } else {
            var o_data = {
                key: key,
                func: 'get-objectId',
            };
            $.ajax({
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: o_data,
                dataType: 'json',
                success: function (data) {
                    $('#ro_object_ids').html(data.html);
                    $('#ro_object_ids').removeClass('hide');
                }
            });
        }
    },
    /**
     * Auth: HuyNN
     * Des:
     * Since: 02/003/2017
     * */
    v_fInfoBill: function () {
        var billId = $("#bill_id").val();
        var o_data = {
            bill_id: billId,
            func: 'get-bill-info',
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                if (data.success == 1) {
                    $('#customer_id').html(data.htmlCus);
                    $('#customer_id').removeClass('js-select2');
                    $('#customer_id').addClass('js-select2');
                    $('#request_staff_id').html(data.htmlStaff);
                    $('#request_staff_id').removeClass('js-select2');
                    $('#request_staff_id').addClass('js-select2');
                    $('#bill_total_money').remove();
                    $('#fileupload').append(data.htmlTotalMoney);
                    $('#pb_note2').val(data.htmlnote2);
                    $('#productCode').html(data.htmlProductCode);
                    $('#buildingCode').html(data.htmlBuildingCode);
                    $('#projectName').html(data.htmlProjectName);
                }
            }
        });
    },

    /**
     * Auth: Dienct
     * Des:
     * Since: 02/003/2017
     * */
    v_fSubmitSlideValidate: function () {
        var sz_name = $('#link').val();
        if (GLOBAL_JS.b_fValidateEmpty(sz_name)) {
            $('.required_name').remove();
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần nhập tên</strong></p>');
            $('.alert-danger').removeClass('hide');
            $('#name').focus();
            return false;
        }
        if ($('.alert-danger').text() != '')
            return false;
        $('.submit').click();
    },
    /**
     * Auth: Dienct
     * Des:
     * Since: 16/003/2017
     * */
    v_fSubmitUser: function () {
        //$chech 0- insert 1-edit
        var $chech = $('#id').val();
        var sz_pass = $('#password').val();
        var sz_repass = $('#re_password').val();
        $('.alert-danger').empty();
        if ($chech == 0) {
            if (sz_pass == '' || sz_pass != sz_repass) {
                $('.alert-danger').append('<p><strong class="required_name">Bạn cần kiểm tra lại password</strong></p>');
                $('.alert-danger').removeClass('hide');
            }
        } else {
            if (sz_pass != '' && sz_pass != sz_repass) {
                $('.alert-danger').append('<p><strong class="required_name">Bạn cần kiểm tra lại password</strong></p>');
                $('.alert-danger').removeClass('hide');
            }
        }
        if ($('.alert-danger').text() != '')
            return false;
        $('.submit').click();
    },

    /**
     * Auth: Dienct
     * Des: Check validate then submit form if 0 error
     * Since: 06/003/2017
     * */
    v_fSubmitJobValidate: function () {
        var sz_projects = $('#projects').val();
        var sz_supplier = $('#supplier').val();
        var sz_channel = $('#channel').val();
        var sz_job_type = $('#job_type').val();
        var sz_branch = $('#branch').val();
        $('.alert-danger').empty();

        if (sz_projects == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn dự án</strong></p>');
            $('.alert-danger').removeClass('hide');
        }
        if (sz_supplier == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn nhà cung cấp</strong></p>');
            $('.alert-danger').removeClass('hide');
        }

        if (sz_channel == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn kênh</strong></p>');
            $('.alert-danger').removeClass('hide');
        }

        if (sz_job_type == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn loại tác vụ</strong></p></br>');
            $('.alert-danger').removeClass('hide');
        }

        if (sz_branch == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn chi nhanh</strong></p></br>');
            $('.alert-danger').removeClass('hide');
        }

        if ($('.alert-danger').text() != '')
            return false;
        $('.submit').click();
    },
    /**
     * Auth: Dienct
     * Des: Submit search All module
     * Since: 06/03/2017
     * */
    v_fSearchSubmitAll: function () {
        $('.submit').click();
    },
    /**
     * Auth: Dienct
     * Des: Transfer Customer
     * Since: 06/09/2018
     * */
    v_fTransferData: function () {
        var staff = $("#staff_id").val();
        var chkArray = [];

        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        $(".chk_item:checked").each(function () {
            chkArray.push($(this).val());
        });
        if (staff == '') {
            alert("Bạn cần chọn nhân viên !");
            return;
        }
        if (chkArray.length == 0) {
            alert("Bạn cần chọn khách hàng !");
            return;
        }
        var o_data = {
            staff: staff,
            chkArray: chkArray,
            func: 'transfer-data'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                alert(data.msg);
                location.reload();
            }
        });
    },
    /*
     * @Auth: Dienct
     * @Login TVC     
     * **/
    v_fLoginTVC: function () {
        var username = $("#inputEmail").val();
        var pwd = $("#inputPassword").val();

        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        if (username == '' || pwd == '') {
            alert('Kiểm tra thông tin đăng nhập');
            return;
        }

        var o_data = {
            username: username,
            pwd: pwd,
            func: 'login-tvc'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                if (data.result == 1) {
                    window.location.replace(GLOBAL_JS.sz_CurrentHost + "/customer");
                } else {
                    alert(data.success);
                    window.location.replace(GLOBAL_JS.sz_CurrentHost + "/sign-in");
                }
            }
        });
    },


    /**
     * Auth: Dienct
     * Des: Transfer Customer
     * Since: 06/09/2018
     * */
    v_fTransferDataTMP: function () {
        var staff = $("#staff_id").val();
        var chkArray = [];

        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        $(".chk_item:checked").each(function () {
            chkArray.push($(this).val());
        });
        if (staff == '') {
            alert("Bạn cần chọn nhân viên !");
            return;
        }
        if (chkArray.length == 0) {
            alert("Bạn cần chọn khách hàng !");
            return;
        }

        var o_data = {
            staff: staff,
            chkArray: chkArray,
            func: 'transfer-data-tmp'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                alert(data.msg);
                location.reload();
            }
        });
    },
    /**
     * Auth: Dienct
     * Des: Submit search job statistics
     * Since: 06/03/2017
     * */
    v_fSearchSubmitStatistics: function () {

        var sz_filter_by = $('#filter_by').val();
        $('.alert-danger').empty();
        if (sz_filter_by == '') {
            $('.alert-danger').append('<p><strong class="required_name">Bạn cần chọn loại lọc</strong></p></br>');
            $('.alert-danger').removeClass('hide');
        }
        if ($('.alert-danger').text() != '') return false;

        $('.submit').click();
    },
    pad: function (num) {
        var str = num.toString().split('.');
        if (str[0].length >= 4) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        return (str.join('.'));
    },
    /**
     * Auth: Dienct
     * Des: chekc all role
     * Since: 18/03/2017
     * */
    v_fCheckAllRoleGroup: function (the_sz_Id) {
        if ($('#' + the_sz_Id).is(':checked')) {
            $('.' + the_sz_Id).prop('checked', true);
        } else
            $('.' + the_sz_Id).prop('checked', false);
    },
    v_fCustomerInfomation: function () {
        var name = $("#name").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        if (name == "" || phone == "" || email == "") {
            alert('Cần nhập đầy đủ thông tin!');
            return;
        } else {
            var o_data = {
                name: name,
                phone: phone,
                email: email,
                func: 'add-customer-info',
            };
            $.ajax({
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: o_data,
                dataType: 'json',
                success: function (data) {
                    alert("Chúng tôi sẽ sớm liên hệ");
                    location.reload();
                }
            });

        }
    },
    v_fToggleLeftSide: function () {
        if ($('#side-menu').hasClass('hide')) {
            $('#side-menu').removeClass('hide');
            $('#page-wrapper').css('margin', '0 0 0 250px');
        } else {
            $('#side-menu').addClass('hide');
            $('#page-wrapper').css('margin', '0 0 0 0');
        }

    },

    v_fLoadBuilding: function (me) {
        var projectId = me.value;
        var htmlOp = '<option value=""><span class="text-center">Chọn tòa</span></option>';
        var o_data = {
            projectId: projectId,
            func: 'loadBuildingByProjectId'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    htmlOp += '<option value="' + item.cb_id + '">' + item.cb_title + ' (' + item.cb_code + ' )</option>';
                });
                $('#building').html(htmlOp);
            }
        });
    },
    v_fLoadApartment: function (me) {
        var buildingId = me.value;
        var htmlOp = '<option value=""><span class="text-center">Chọn căn hộ</span></option>';
        var o_data = {
            buildingId: buildingId,
            func: 'loadApartmentByBuildingId'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    htmlOp += '<option value="' + item.pb_id + '">' + item.pb_code + ' </option>';
                });
                $('#apartment').html(htmlOp);
            }
        });
    },

    v_fLoadBillByApartment: function (me) {
        var apartmentId = me.value;
        var typeOfPayment = $("input[id='typeOfPayment']:checked").val();
        GLOBAL_JS.loadBill(apartmentId, typeOfPayment);
    },

    v_fLoadBillByTypePayment: function (me) {
        var apartmentId = $('#apartment').val();
        var typeOfPayment = me.value;
        GLOBAL_JS.loadBill(apartmentId, typeOfPayment);
    },

    loadBill: function (apartmentId, typeOfPayment) {
        var htmlOp = '<option value=""><span class="text-center">Chọn Bill</span></option>';
        var o_data = {
            apartmentId: apartmentId,
            typeOfPayment: typeOfPayment,
            func: 'loadBillByApartmentId'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    htmlOp += '<option value="' + item.id + '">' + item.bill_code + ' </option>';
                });
                $('#bill_id').html(htmlOp);
                $('#pb_note2').val('');
                $('#customer_id').val(null).trigger("change");
                $('#request_staff_id').val(null).trigger("change");
            }
        });
    },

    pad: function (num) {
        var str = num.toString().split('.');
        if (str[0].length >= 4) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        return (str.join('.'));
    },
};

$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var t = $(location).attr("href");
    GLOBAL_JS.sz_CurrentHost = t.split("/")[0] + "//" + t.split("/")[2];
    GLOBAL_JS.sz_Tbl = $('#tbl').val();

    if ($('.tinymce').length) {
        function applyMCE() {
            tinyMCE.init({
                mode: "textareas",
                editor_selector: "tinymce",
                theme: "modern",
                width: '100%',
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor responsivefilemanager code fullscreen"
                ],
                toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code fullscreen | fontsizeselect",
                fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
                image_advtab: true,
                relative_urls: false,
                remove_script_host: false,

                forced_root_block: "",
                force_br_newlines: false,
                force_p_newlines: true,
                entity_encoding: "raw",
                //paste_word_valid_elements: "b,strong,i,em,h1,h2,u,p,ol,ul,li,a[href],span,color,font-size,font-color,font-family,mark",
                paste_retain_style_properties: "all",
                formats: {
                    bold: {inline: 'b'},
                    italic: {inline: 'i'}
                },
                external_filemanager_path: "/plugin/filemanager/filemanager/",
                filemanager_title: "Responsive Filemanager",
                external_plugins: {"filemanager": "/plugin/filemanager/filemanager/plugin.min.js"}
            });
        }
        applyMCE();
    }


    /////Định dạng ô money trong phần tính lãi vay////
    $(".formatMoney").keyup(function () {
        var num = $(this).val();
        if (/\D/g.test(num)) num = num.replace(/\D/g, '');
        var i = 0, strLength = num.length;
        for (i; i < strLength; i++) {
            num = num.replace(' ', '');
        }
        $(this).val(GLOBAL_JS.pad(num));
    });

    if ($('.backend').length) {
        // it exists
        $('.js-select2').select2();
        $("#checkboxALL").click(function(){
            if($("#checkboxALL").is(':checked') ){
                $("#ro_object_ids > option").prop("selected","selected");
                $("#ro_object_ids").trigger("change");
            }else{
                $("#ro_object_ids > option").removeAttr("selected");
                 $("#ro_object_ids").trigger("change");
             }
        });
        
        
        $('#check_all').click(function (event) {
            if (this.checked) {
                $('.chk_item').each(function () {
                    this.checked = true;
                    $(this).closest("tr").addClass("tr_check");
                });
            } else {
                $('.chk_item').each(function () {
                    this.checked = false;
                    $(this).closest("tr").removeClass("tr_check");
                });
            }
        });
    }

    $('.showPayment').click(function () {

        var billcode = $(this).attr('data-bill-code');
        var o_data = {
            billcode: billcode,
            func: 'loadPayments'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                var html = '';
                $.each(data, function (index, item) {
                    var stt = index + 1;
                    html += '<tr >\n' +
                        '                                    <td>' + stt + '</td>\n' +
                        '                                    <td>' + item.bill_code + '</td>\n' +
                        '                                    <td>' + item.request_staff.name + '</td>\n' +
                        '                                    <td><p><strong>Họ tên: ' + item.customer.cb_name + '</strong></p> <p><strong>SĐT: ' + item.customer.cb_phone + '</strong></p> <p><strong>Địa chỉ: ' + item.customer.cb_permanent_address + '</strong></p> <p><strong>Email: ' + item.customer.cb_email + '</strong></p></td>\n' +
                        '                                    <td>' + item.pb_response_money + '</td>\n' +
                        '                                    <td>' + item.payment_method.pm_title + '</td>\n' +
                        '                                    <td>' + item.request_time + '</td>\n' +
                        '                                </tr>';
                });
                $('#popupPayment #payment').html(html);
                $("#popupPayment").modal()
            }
        });
    });

    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="feedback_options[]" value="" required><a href="javascript:void(0);" class="remove_button"><img src="' + GLOBAL_JS.sz_CurrentHost + '/remove-icon.png"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function () {
        //Check maximum number of input fields
        if (x < maxField) {
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });

    $('#selectTypeCate').change(function () {
        var type = $(this).val();
        ///Nếu là dự án///
        if (type == 1) {
            $('#investor').prop('disabled', false);
            $('.apartmentGrid').prop('disabled', false);
            $('#project').prop('disabled', true).val(null).trigger("change");
            $('.type').prop('disabled', true).removeAttr('checked');
            $('.active_booking').prop('disabled', true).removeAttr('checked');
            $('#active_send_mail').closest('div.form-group').addClass('hide');
            $('#active_send_mail').prop('disabled', true);
            $('.datetimepicker').data("DateTimePicker").disable().clear();
        } else {
            $('#investor').prop('disabled', true).val(null).trigger("change");
            $('.apartmentGrid').prop('disabled', true).removeAttr('checked');
            $('#project').prop('disabled', false);
            $('.type').prop('disabled', false);
            $('.active_booking').prop('disabled', false);
            $('#active_send_mail').closest('div.form-group').removeClass('hide');
            $('#active_send_mail').prop('disabled', false);
            $('.datetimepicker').data("DateTimePicker").enable();

        }
    });

    var currentMethodPayment = $("#methodPayment option:selected").attr('code');
    if (currentMethodPayment == 'CK' || currentMethodPayment == 'MB' || currentMethodPayment == 'CKCDT' || currentMethodPayment == 'QT') {
        $('#bankCode').prop('disabled', false).prop('required', true);
    } else {
        $('#bankCode').prop('disabled', true).val(null).trigger("change");
    }

    $('#methodPayment').change(function () {
        var method = $('option:selected', this).attr('code');
        if (method == 'CK' || method == 'MB' || method == 'CKCDT' || method == 'QT') {
            $('#bankCode').prop('disabled', false).prop('required', true);
        } else {
            $('#bankCode').prop('disabled', true).val(null).trigger("change");
            $('#pb_bank_number_id').val('');
        }
    });

    $('#bankCode').change(function () {
        var idBank = $(this).val();
        var o_data = {
            idBank: idBank,
            func: 'loadBankInfo'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: o_data,
            dataType: 'json',
            success: function (data) {
                if (data.result) {
                    $('#pb_bank_number_id').val(data.bank.bank_number);
                }
            }
        });
    });
    $('#typeTemplate').change(function () {
        var isDefault = $(this).attr('is_default');
        var type = $(this).val();
        var is_apartment = $('#is_apartment').val();
        var o_data = {
            isDefault: isDefault,
            type: type,
            is_apartment: is_apartment,
            func: 'checkTemplateDefaultEmail'
        };
        checkTemplateDefaultEmail(o_data);
    });

    $('#is_apartment').change(function () {
        var isDefault = $('#typeTemplate').attr('is_default');
        var type = $('#typeTemplate').val();
        var is_apartment = $(this).val();
        var o_data = {
            isDefault: isDefault,
            type: type,
            is_apartment: is_apartment,
            func: 'checkTemplateDefaultEmail'
        };
        checkTemplateDefaultEmail(o_data);
    });

    function checkTemplateDefaultEmail(data){
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.result == 0) {
                    alert('Đã tồn tại Template mặc định thuộc loại này rồi!');
                    $('#typeTemplate').prop('selectedIndex', 0).focus();
                    $('#is_apartment').prop('selectedIndex', 0).focus();
                }
            }
        });
    };

    $(".showDetailApartment").click(function () {
        var productId = $(this).attr('id');
        if (productId != '') {
            var o_data = {
                productId: productId,
                func: 'showDetailApartment'
            };
            $.ajax({
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: o_data,
                dataType: 'json',
                success: function (data) {
                    var apart = data.apartment;
                    $('#pb_code').val(apart.pb_code);
                    $('#pb_used_s').val(apart.pb_used_s);
                    $('#pb_built_up_s').val(apart.pb_built_up_s);
                    $('#pb_door_direction').val(apart.pb_door_direction);
                    $('#pb_balcony_direction').val(apart.pb_balcony_direction);
                    $('#view').val(apart.view);
                    $('#bedroom').val(apart.bedroom);
                    $('#price_used_s').val(apart.price_used_s);
                    $('#pb_price_per_s').val(apart.pb_price_per_s);
                    $('#price_min').val(apart.price_min);
                    $('#price_max').val(apart.price_max);
                    $('#price_maintenance').val(apart.price_maintenance);
                    $('#pb_price_total').val(apart.pb_price_total);
                    $('#number_lock').val(apart.number_lock);
                    $('#pb_required_money').val(apart.pb_required_money);
                    $('#note').val(apart.note);
                    $('#stage option').each(function () {
                        if ($(this).val() == apart.stage) {
                            $(this).prop("selected", true);
                        }
                    });
                    $('#book_type option').each(function () {
                        if ($(this).val() == apart.book_type) {
                            $(this).prop("selected", true);
                        }
                    });
                    $('#status option').each(function () {
                        if ($(this).val() == apart.status_id) {
                            $(this).prop("selected", true);
                        }
                    });
                    $('#status option').each(function () {
                        if ($(this).val() == apart.status_id) {
                            $(this).prop("selected", true);
                        }
                    });
                    $('#pb_status option').each(function () {
                        if ($(this).val() == apart.pb_status) {
                            $(this).prop("selected", true);
                        }
                    });
                    $('#updateApartment').attr('data-id', productId);
                    if (apart.disabledOptionStatus == true) {
                        $('#status').prop('disabled', true);
                    } else {
                        $('#status').prop('disabled', false);
                    }

                    // if(apart.showTrans.length > 0){
                    //     var html = '<tr>';
                    //     $.each(apart.showTrans, function (i, item) {
                    //         $.each(item, function (key, val) {
                    //             html += '<td>'+ val +'</td>';
                    //         });
                    //         html += '</tr>';
                    //     });
                    //     $('#listTransLog').html(html);
                    //     $('#tblTransLog').show();
                    // }else{
                    //     $('#tblTransLog').hide();
                    // }
                    // $('#titleTransLog').text(apart.titleTransLog);
                    if(apart.showBtnUpdate){
                        $('#updateApartment').removeClass('hide');
                    }else $('#updateApartment').addClass('hide');
                    $("#detailApartmentPopup").modal();
                }
            });
        }
    });

    $('.old_procedure').click(function() {
        if ($(this).is(':checked')) {
            if (!confirm("Sau khi chuyển đổi sang Quy trình cũ sẽ không thể hoàn tác lại. Bạn có chắc chắn?")) {
                return false;
            }
        }
    });
    $("#updateApartment").click(function () {
        var productId = $(this).attr('data-id');
        var stage = $('#stage').val();
        var status = $('#status').val();
        var pb_status = $('#pb_status').val();
        var book_type = $('#book_type').val();
        var pb_used_s = $('#pb_used_s').val();
        var pb_built_up_s = $('#pb_built_up_s').val();
        var pb_door_direction = $('#pb_door_direction').val();
        var pb_balcony_direction = $('#pb_balcony_direction').val();
        var view = $('#view').val();
        var price_used_s = $('#price_used_s').val();
        var pb_price_per_s = $('#pb_price_per_s').val();
        var price_min = $('#price_min').val();
        var price_max = $('#price_max').val();
        var price_maintenance = $('#price_maintenance').val();
        var pb_price_total = $('#pb_price_total').val();
        var number_lock = $('#number_lock').val();
        var pb_required_money = $('#pb_required_money').val();
        var note = $('#note').val();
        var o_data = {
            productId: productId,
            stage: stage,
            status: status,
            pb_status: pb_status,
            pb_used_s: pb_used_s,
            pb_built_up_s: pb_built_up_s,
            pb_door_direction: pb_door_direction,
            pb_balcony_direction: pb_balcony_direction,
            view: view,
            book_type: book_type,
            price_used_s: price_used_s,
            pb_price_per_s: pb_price_per_s,
            price_min: price_min,
            price_max: price_max,
            price_maintenance: price_maintenance,
            pb_price_total: pb_price_total,
            number_lock: number_lock,
            pb_required_money: pb_required_money,
            note: note,
            func: 'updateDetailApartment'
        };
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: {
                func: 'updateDetailApartment',
                data: o_data
            },
            dataType: 'json',
            success: function (data) {
                alert(data.alert);
                if (data.result) {
                    $('#closeModalApartment').click();
                }
            }
        });
    });

    $('.datetimepicker').datetimepicker({
        format: 'DD-MM-YYYY HH:mm',
        minDate: new Date(),
    });

    $('#table_id').DataTable({
        paging: true,
        pageLength: 15,
    });

    // Setup - add a text input to each footer cell
    $('#table_id1 tfoot th').each( function () {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="'+title+'" />');
    });
    $('table tfoot th:eq(0)').find("input[type=text]").remove();
    $('table tfoot th:eq(4)').find("input[type=text]").remove();
    // DataTable
    var table = $('#table_id1').DataTable({
        paging: true,
        pageLength: 10
    });
    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        });
    });

    $('.creatDatatable').each(function() {
        var that = this;
        $(that).DataTable({
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "order": [[ 1, "asc" ]],
            "columnDefs": [
                { "orderable": false, "targets": [0,-1] }
            ],
            "language": {
                "processing": "Đang xử lý...",
                "lengthMenu": "Xem _MENU_ mục",
                "zeroRecords": "Không tìm thấy kết quả phù hợp",
                "info": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "infoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "infoFiltered": "(được lọc từ _MAX_ mục)",
                "infoPostFix": "",
                "search": "Tìm:",
                "url": "",
                "paginate": {
                    "first": "Đầu",
                    "previous": "Trước",
                    "next": "Tiếp",
                    "last": "Cuối"
                }
            },
            ajax: {
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: {
                    func: 'getParamDatatable',
                    table: $(that).attr('id')
                }
            },
        });
    });



    $("#listTransaction_filter").css("display","none");
    $('.search-input-text').on( 'keyup click', function () {
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );
    $('.search-input-select').on('change', function () {
        var i =$(this).attr('data-column');
        var v =$(this).val();
        dataTable.columns(i).search(v).draw();
    } );

    var dataTable =  $('#listTransaction').DataTable( {
        "processing": true,
        "serverSide": true,
        "destroy": true,
        ajax:{
            url : GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: "POST",
            data: {
                func: 'test'
            }
        }
    });

    $("#paymentDeny").click(function () {
        if (confirm('Từ chối phiếu thu này?')) {
            var paymentId = $(this).attr('data-id');
            $.ajax({
                url: GLOBAL_JS.sz_CurrentHost + '/ajax',
                type: 'POST',
                data: {
                    func: 'paymentDeny',
                    paymentId: paymentId
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.alert);
                    window.location.href = GLOBAL_JS.sz_CurrentHost + '/payment';
                }
            });
        }
    });

    $(document).on('click', '.changeStatusBill', function () {
        event.preventDefault();
        if (confirm('Hành động không thể hoàn tác, bạn có muốn tiếp tục?')) {
            var billId = $(this).attr('bill-id');
            var href = $(this).attr('href');
            if($('#'+billId).is(':checked')){
                href += '&old_procedure=1';
            }
            window.location = href ;
        }
    });

    $(document).on('click', '.showImgBill', function () {
        var billId = $(this).attr('bill-id');
        var showType = $(this).attr('show-type');
        $.ajax({
            url: GLOBAL_JS.sz_CurrentHost + '/ajax',
            type: 'POST',
            data: {
                func: 'showImgBill',
                billId: billId,
                showType: showType
            },
            dataType: 'json',
            success: function (data) {
                if(data.length > 0){
                    var html = '';
                    $.each(data, function (i, img) {
                        var active = i == 0 ? 'active' : '';
                        html += '<div class="item '+ active + '"> <img class="img-responsive" src="' + img + '"></div>';
                    });
                    $('div.carousel-inner').html(html);
                    $(".popupBillImg").removeClass('hide');
                }else{
                    $(".popupBillImg").addClass('hide')
                }
                $(".popupBillImg").modal();
            }
        });
    });

    // $(document).on('click', '.showDetailPayment', function () {
    //     var paymentId = $(this).attr('payment-id');
    //     $.ajax({
    //         url: GLOBAL_JS.sz_CurrentHost + '/ajax',
    //         type: 'POST',
    //         data: {
    //             func: 'showDetailPayment',
    //             paymentId: paymentId
    //         },
    //         dataType: 'json',
    //         success: function (data) {
    //             $("#detailPaymentPopup").modal();
    //         }
    //     });
    // });

    $('.test').click(function () {
        $(".bs-example-modal-lg").modal()
    })
});
