/*
 * Copyright (c) 2019. By CongPeter-PCN@DXMB
 */

$(function () {
    // Click "Congratulations!" to play animation
    let particles = ['.blob', '.star'],
        $congratsSection = $('#congrats'),
        $title = $('#title');

    $(function() {
        init({
            numberOfStars: 120,
            numberOfBlobs: 50
        });

        fancyPopIn();
    });

    $congratsSection.click(fancyPopIn);

    function fancyPopIn() {
        reset();
        animateText();

        for (let i = 0, l = particles.length; i < l; i++) {
            animateParticles(particles[i]);
        }
    }

    function animateText() {
        TweenMax.from($title, 0.65, {
            scale: 0.4,
            opacity: 0,
            rotation: 15,
            ease: Back.easeOut.config(5),
        });
    }

    function animateParticles(selector) {
        let xSeed = _.random(600, 800);
        let ySeed = _.random(150, 400);

        $.each($(selector), function(i) {
            let $particle = $(this);
            let speed = _.random(1, 4);
            let rotation = _.random(20, 100);
            let scale = _.random(0.8, 1.5);
            let x = _.random(-xSeed, xSeed);
            let y = _.random(-ySeed, ySeed);

            TweenMax.to($particle, speed, {
                x: x,
                y: y,
                ease: Power1.easeOut,
                opacity: 0,
                rotation: rotation,
                scale: scale,
                onStartParams: [$particle],
                onStart: function($element) {
                    $element.css('display', 'block');
                },
                onCompleteParams: [$particle],
                onComplete: function($element) {
                    $element.css('display', 'none');
                }
            });
        });
    }

    function reset() {
        for (let i = 0, l = particles.length; i < l; i++) {
            $.each($(particles[i]), function() {
                TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
            });
        }

        TweenMax.set($title, { scale: 1, opacity: 1, rotation: 0 });
    }

    function init(properties) {
        for (let i = 0; i < properties.numberOfStars; i++) {
            $congratsSection.append('<div class="particle star fa fa-star ' + i + '"></div>');
        }

        for (let i = 0; i < properties.numberOfBlobs; i++) {
            $congratsSection.append('<div class="particle blob ' + i + '"></div>');
        }
    }
});
