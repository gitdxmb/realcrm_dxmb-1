<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $email;
    protected $name;
    protected $url;

    /**
     * VerifyEmail constructor.
     * @param $name
     * @param $url
     */
    public function __construct($name, $url)
    {
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Yêu cầu xác thực tài khoản Booking Online')
            ->from('noreply@dxmb.vn')
            ->view('mail.email-verification')
            ->with([
                'name' => $this->name,
                'url'   => $this->url
            ]);
    }
}
