<?php

namespace App\Mail;

use App\BOBill;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StatusBillCustomer extends Mailable
{
    use Queueable, SerializesModels;
    public $bill;
    protected $productCode;
    protected $projectName;
    protected $statusBill;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(BOBill $bill, $productCode, $projectName, $statusBill)
    {
        $this->productCode = $productCode;
        $this->projectName = $projectName;
        $this->statusBill = $statusBill;
        $this->bill = $bill;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->statusBill == BOBill::STATUSES['STATUS_DEPOSITED']){
            $title = 'Xác nhận đặt mua thành công từ Đất Xanh Miền Bắc';
        }else if($this->statusBill == BOBill::STATUSES['STATUS_SUCCESS']){
            $title = 'Chúc mừng Quý khách ký HĐMB thành công';
        }else if($this->statusBill == BOBill::STATUSES['STATUS_BOOKED']){
            $title = 'Xác nhận đặt chỗ thành công từ Đất Xanh Miền Bắc';
        }else if($this->statusBill == BOBill::STATUSES['STATUS_CANCELED']){
            if($this->bill->type == BOBill::TYPE_DEPOSIT){
                $title = 'Xác nhận thanh lý đặt mua từ Đất Xanh Miền Bắc';
            }else{
                $title = 'Xác nhận thanh lý đặt chỗ từ Đất Xanh Miền Bắc';
            }
        }
        $data = [
            'productCode' => $this->productCode,
            'projectName' => $this->projectName,
            'statusBill' => $this->statusBill,
            'typeBill' => $this->bill->type
        ];
        return $this->subject($title)->view('mail.status-bill-customer', $data);
    }
}
