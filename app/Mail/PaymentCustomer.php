<?php

namespace App\Mail;

use App\BOBill;
use App\BOCategory;
use App\BOPayment;
use App\BOProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $payment;
    protected $apartment;
    protected $building;
    protected $project;
    protected $bill;

    /**
     * PaymentCustomer constructor.
     * @param BOPayment $payment
     * @param BOProduct $apartment
     * @param BOCategory $building
     * @param BOCategory $project
     * @param BOBill|null $bill
     */
    public function __construct(BOPayment $payment, BOProduct $apartment, BOCategory $building, BOCategory $project, BOBill $bill)
    {
        $this->payment = $payment;
        $this->apartment = $apartment;
        $this->building = $building;
        $this->project = $project;
        $this->bill = $bill;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->payment->pb_status == 1 ? 'Xác nhận chuyển tiền thành công từ Đất Xanh Miền Bắc' : 'Xác nhận thông tin chuyển tiền từ Đất Xanh Miền Bắc';
        $data = [
            'customer' => $this->bill->bill_customer_info->name,
            'apartment_code' => $this->apartment->pb_code,
            'project' => $this->project->cb_title,
            'request_money' => $this->payment->pb_request_money? number_format($this->payment->pb_request_money) : 0,
            'response_money' => $this->payment->pb_response_money? number_format($this->payment->pb_response_money) : 0,
            'status' => $this->payment->pb_status
        ];
        return $this->subject($title)->view('mail.customer-payment', $data);
    }
}
