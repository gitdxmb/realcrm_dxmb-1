<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $content, $bill, $customer, $status;
    protected $attachment;

    /**
     * BillCreated constructor.
     * @param $content
     * @param $bill
     * @param $status
     * @param $customer
     * @param null $attachment
     */
    public function __construct($content, $bill, $status, $customer, $attachment = null)
    {
        $this->content = $content;
        $this->bill = $bill;
        $this->customer = $customer;
        $this->attachment = $attachment;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->attachment) $this->attach($this->attachment);
        return $this->subject('Yêu cầu xác nhận đơn hàng từ Đất Xanh Miền Bắc: HD'.$this->bill)->view('mail.customer-bill',[
            'url' => env('APP_URL', 'http://bo.dxmb.vn')
        ]);
    }
}
