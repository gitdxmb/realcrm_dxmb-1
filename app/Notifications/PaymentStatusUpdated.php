<?php

namespace App\Notifications;

use App\BOPayment;
use App\BOUser;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentStatusUpdated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $payment;
    protected $api_gate;

    /**
     * PaymentStatusUpdated constructor.
     * @param BOPayment $payment
     */
    public function __construct(BOPayment $payment, $api_gate = false)
    {
        $this->payment = $payment;
        $this->api_gate = $api_gate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'payment' => $this->payment->{BOPayment::ID_KEY},
            'by'   => BOUser::getCurrentUID($this->api_gate),
            'status' => $this->payment->pb_status,
        ];
    }
}
