<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetRequest extends Notification implements ShouldQueue
{
    use Queueable;
    protected $token;
    protected $email;
    protected $name;

    /**
     * PasswordResetRequest constructor.
     * @param $token
     * @param $email
     * @param $name
     */
    public function __construct($token, $email, $name)
    {
        $this->token = $token;
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/api/password/find/'.$this->token);
        $notifiable->email = $this->email;

        return (new MailMessage)
            ->from('noreply@dxmb.vn')
            ->greeting("Chào {$this->name},")
            ->subject('[DXMB] Yêu cầu khôi phục mật khẩu | Hệ thống Booking Online - Đất Xanh Miền Bắc')
            ->line('Bạn đã yêu cầu khôi phục mật khẩu trên '.url('/').' với email '.$this->email.'.')
            ->line('Để hoàn tất việc lấy lại mật khẩu, vui lòng nhấn vào nút dưới đây:')
            ->action('Khôi phục mật khẩu', url($url))
            ->line('Lưu ý: Yêu cầu khôi phục sẽ tự động hết hạn sau 20 phút!')
            ->line('Nếu không phải bạn thực hiện, vui lòng KHÔNG nhấn vào đường dẫn trên.');
    }

}
