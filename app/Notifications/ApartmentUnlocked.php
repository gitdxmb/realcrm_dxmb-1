<?php

namespace App\Notifications;

use App\BOProduct;
use App\Http\Controllers\API\AuthController;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApartmentUnlocked extends Notification implements  ShouldQueue
{
    use Queueable;
    protected $apartment;

    /**
     * ApartmentUnlocked constructor.
     * @param BOProduct $apartment
     */
    public function __construct(BOProduct $apartment)
    {
        $this->apartment = $apartment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'apartment' => $this->apartment->{BOProduct::ID_KEY},
            'by'        => AuthController::getCurrentUID()
        ];
    }
}
