<?php

namespace App\Notifications;

use App\BOTransaction;
use App\Http\Controllers\API\AuthController;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransactionCustomerAdded extends Notification implements ShouldQueue
{
    use Queueable;
    protected $transaction;

    /**
     * TransactionCustomerAdded constructor.
     * @param BOTransaction $transaction
     */
    public function __construct(BOTransaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'transaction' => $this->transaction->{BOTransaction::ID_KEY},
            'by'          => AuthController::getCurrentUID()
        ];
    }
}
