<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetSuccess extends Notification implements ShouldQueue
{
    use Queueable;
    protected $email;
    protected $name;

    /**
     * PasswordResetSuccess constructor.
     * @param $email
     * @param $name
     */
    public function __construct($email, $name)
    {
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->email = $this->email;
        return (new MailMessage)
            ->from('noreply@dxmb.vn')
            ->greeting("Chào {$this->name},")
            ->subject('[DXMB] Khôi phục mật khẩu thành công | Hệ thống Booking Online - Đất Xanh Miền Bắc')
            ->line('Bạn đã khôi phục thành công mật khẩu trên '.url('/'))
            ->line('Ngay bây giờ, bạn có thể đăng nhập tài khoản với mật khẩu mới!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
