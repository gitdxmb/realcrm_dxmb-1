<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Util;
class Poll extends Model
{
    public $timestamps = false;
    const ID_KEY = 'cn_id';



    /**
     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditPoll($id) {
        
        //data news
        $a_DataUpdate = array();
        $a_DataUpdate['title'] = Input::get('title');
        $a_DataUpdate['short_description'] = Input::get('short_description');
        $a_DataUpdate['description'] = Input::get('description');
        
        $a_images = Input::get('images');
        $a_filename = Input::get('filename');
        $a_deleteUrl = Input::get('deleteUrl');
        if(is_array($a_images) && count($a_images) > 0){
            foreach ($a_images as $key => $val){
                $aryIMG['img'] = $val;
                $aryIMG['filename'] = $a_filename[$key];
                $aryIMG['deleteUrl'] = $a_deleteUrl[$key];
                $aryReturn[] = $aryIMG;
            }
        }
        
        if(isset($aryReturn)){
            $a_DataUpdate['media'] = json_encode($aryReturn);
        }
        $a_DataUpdate['type'] = Input::get('type');
        $a_DataUpdate['category'] = Input::get('category');
        $a_DataUpdate['poll_option'] = Input::get('poll_option');
        $a_DataUpdate['receive_user'] = Input::get('receive_user');
        $a_DataUpdate['receive_groups'] = Input::get('receive_groups');
        
        // Respone-info
        $respone_info['title'] = Input::get('title');
        $respone_info['type'] = Input::get('type');
        if (is_numeric($id) == true && $id != 0) {
            DB::table('respone_info')->where('id', $id)->update($a_DataUpdate);
        } else{
            $respone_info['pk_id'] = time();
            $respone_info = DB::table('respone_info')->insert($a_DataUpdate);
        }
        

        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('news')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['cn_id'] = time();
            $a_DataUpdate['created_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('news')->insert($a_DataUpdate);
        }
    }

    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getPollById($id) {

        $a_Data = DB::table('news')->where('id', $id)->first();        
        $a_Data->media  = json_decode($a_Data->media);
        return $a_Data;
    }

    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('news')->select('*');
        $a_data = $o_Db->where('cn_status', '!=', -1);
        $a_search = array();
        //search

        $sz_cn_title = Input::get('cn_title', '');
        if ($sz_cn_title != '') {
            $a_search['cn_title'] = $sz_cn_title;
            $a_data = $o_Db->where('cn_title', 'like', '%' . $sz_cn_title . '%');
        }
        
        $a_data = $o_Db->orderBy('updated_time', 'desc')->paginate(20);

        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    /*
     * @auth: Dienct
     * @Des: get all Payment Method
     * @Since: 06/09/2018
     * 
     * **/
   public static function getAllPoll(){
       $o_Db = DB::table('news')->select('*')->where('cn_status', '!=', -1)->get();
       return $o_Db;
   }
}
