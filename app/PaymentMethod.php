<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Util;
class PaymentMethod extends Model
{
    public $timestamps = false;
    const ID_KEY = 'pm_id';



    /**
     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditpaymentMethod($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['pm_title'] = Input::get('pm_title');

        $a_DataUpdate['pm_code'] = Input::get('pm_code');
        $a_DataUpdate['pm_status'] = Input::get('pm_status') == 'on' ? 1 : 0;
        $a_DataUpdate['pm_note'] = Input::get('pm_note');

        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['pm_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('payment_methods')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['pm_id'] = time();
            $a_DataUpdate['pm_created_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['pm_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('payment_methods')->insert($a_DataUpdate);
        }
    }

    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getpaymentMethodById($id) {

        $a_Data = DB::table('payment_methods')->where('id', $id)->first();

        if ($a_Data) {
            $a_Data->pm_created_time = Util::sz_DateTimeFormat($a_Data->pm_created_time);
            $a_Data->pm_updated_time = Util::sz_DateTimeFormat($a_Data->pm_updated_time);
        }

        return $a_Data;
    }

    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('payment_methods')->select('*');
        $a_data = $o_Db->where('pm_status', '!=', -1);
        $a_search = array();
        //search

        $sz_pm_title = Input::get('pm_title', '');
        if ($sz_pm_title != '') {
            $a_search['pm_title'] = $sz_pm_title;
            $a_data = $o_Db->where('pm_title', 'like', '%' . $sz_pm_title . '%');
        }
        
        $a_data = $o_Db->orderBy('pm_updated_time', 'desc')->paginate(30);

        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    /*
     * @auth: Dienct
     * @Des: get all Payment Method
     * @Since: 06/09/2018
     * 
     * **/
   public static function getAllPaymentMethod(){
       $o_Db = DB::table('payment_methods')->select('*')->where('pm_status', '!=', -1)->get();
       return $o_Db;
   }
}
