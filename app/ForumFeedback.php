<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumFeedback extends Model
{
    public function created_by() {
        return $this->belongsTo(BOUser::class, 'created_by', BOUser::ID_KEY);
    }

    public function replies() {
        return $this->hasMany(self::class, 'reply_to', 'id');
    }

    public function getCreatedAtAttribute($value) {
        return $value? Util::timeAgo($value) : $value;
    }
}
