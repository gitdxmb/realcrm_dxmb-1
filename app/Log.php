<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $timestamps = true;
    protected $fillable = ['bo_content', 'bo_object', 'bo_user', 'bo_object_type', 'bo_type'];

    const LOG_OBJECTS = [
        'GLOBAL' => 0,
        'BO_PRODUCT' => 1,
        'BO_CATEGORY' => 2,
        'BO_TRANSACTION' => 3,
        'BO_BILL' => 4,
        'BO_CUSTOMER' => 5,
        'BO_USER' => 6,
        'BO_PAYMENT' => 7,
        'BO_USER_GROUP' => 8,
        'FORUM' => 9,
        'POST' => 10,
    ];

    const LOG_TYPES = [
        'INFO' => 0,
        'WARNING' => 1,
        'ERROR' => 2
    ];

}
