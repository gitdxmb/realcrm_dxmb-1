<?php

namespace App;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\BOProductController;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
class BOTransaction extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    const ID_KEY = "trans_id";
    /** @var int Seconds */
    const LIMIT_TIME = 1800;
    /** @var array */
    const STATUSES = [
        "R-LCK" => "Y/C lock",
        "R-EXT" => "Y/C Gia hạn",
        "A-LCK" => "Đã Lock", // Tự động
        "M-LCK" => "Đã Lock", // Được duyệt
        "R-CANCEL" => "Y/C hủy",
        "A-CANCEL" => "Bung lock",
        "M-CANCEL" => "Đã hủy",
        "R-BLL" => "Tạo bill",
        "C-BLL" => "Hủy bill"
    ];
    const STATUS_CODES = [
        "REQUEST_LOCK" => "R-LCK",
        "REQUEST_EXTEND" => "R-EXT",
        "LOCKED_AUTO" => "A-LCK", // Tự động
        "LOCKED_MANUAL" => "M-LCK", // Được duyệt
        "REQUEST_CANCEL" => "R-CANCEL",
        "CANCELED_AUTO" => "A-CANCEL",
        "CANCELED_MANUAL" => "M-CANCEL",
        "BILL_REQUEST" => "R-BLL",
        "BILL_CANCEL" => "C-BLL"
    ];
    const STATUSES_COLOR = [
        "R-LCK" => "<span class='alert alert-warning'> Y/C lock </span>",
        "R-EXT" => "<span class='alert alert-warning'>Gia hạn lock </span>",
        "A-LCK" => "<span class='alert alert-warning'>Đã Lock </span>", // Tự động
        "M-LCK" => "<span class='alert alert-warning'>Đã Lock </span>", // Được duyệt
        "R-CANCEL" => "Y/C hủy",
        "A-CANCEL" => "<span class='alert alert-danger'>Bung lock </span>",
        "M-CANCEL" => "<span class='alert alert-danger'>Đã hủy </span>",
        "R-BLL" => "<span class='alert alert-success'>Tạo bill </span>",
        "C-BLL" => "<span class='alert alert-danger'>Hủy bill </span>"
    ];
    const STATUS_COLORS = [
        "R-LCK" => "#b2a40c",
        "R-EXT" => "#b2a40c",
        "A-LCK" => "#1974d6", // Tự động
        "M-LCK" => "#1974d6", // Được duyệt
        "R-CANCEL" => "#724347",
        "A-CANCEL" => "#4D1217",
        "M-CANCEL" => "#4D1217",
        "R-BLL" => "#0a870a",
        "C-BLL" => "#0a870a"
    ];

    /**Disable multiple products
     * @var array
     */
    protected $hidden = ['product_ids'];

    protected $casts = [
        'product_ids' => 'array',
        'trans_created_time' => 'datetime:d-m-Y H:i',
        'trans_status_log' => 'array'
    ];

    protected $dates = ['trans_created_time', 'trans_updated_time'];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value)
    {
        $id = time();
        switch ($key) {
            case "trans_id":
                $this->attributes[$key] = $value?? $id;
                break;
            case "product_ids":
                $this->attributes[$key] = is_array($value)? json_encode($value) : null;
                break;
            case "trans_status_log":
                $this->attributes[$key] = is_array($value)? json_encode($value) : null;
                break;
            case "trans_updated_time":
                $this->attributes[$key] = now();
                break;
            case "staff_code":
            case "created_user_id":
            case "updated_user_id":
                $this->attributes[$key] = $value?? BOUser::getCurrentUID();
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    //************************* Relationships ***********************
    //*********************************************************************
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff() {
        return $this->belongsTo(BOUser::class, "staff_code", BOUser::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function created_by() {
        return $this->belongsTo(BOUser::class, "created_user_id", BOUser::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updated_by() {
        return $this->belongsTo(BOUser::class, "updated_user_id", BOUser::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product() {
        return $this->belongsTo(BOProduct::class, "product_id", BOProduct::ID_KEY);
    }

    /**
     * @param $value
     * @return string
     */
    public function getTransDepositAttribute($value) {
        return $value? number_format($value) : $value;
    }

    /**
     * @param $status_code
     * @param int $uid
     * @return array
     */
    public static function generateStatusLog($status_code, $uid = 0, $api_gate = true) {
        if (!in_array($status_code, self::STATUS_CODES)) return [];
        return [
            "VALUE" => $status_code,
            "TIME"  => time(),
            "CREATED_BY" => $uid>0? $uid : AuthController::getCurrentUID($api_gate),
            "NOTE"  => self::STATUSES[$status_code]
        ];
    }
    /**
     * @param $status_code
     * @param int $uid
     * @return array
     */
    public static function generateStatusLogWeb($status_code) {
        if (!in_array($status_code, self::STATUS_CODES)) return [];
        return [
            "VALUE" => $status_code,
            "TIME"  => time(),
            "CREATED_BY" => Auth::guard('loginfrontend')->user()->ub_id,
            "NOTE"  => self::STATUSES[$status_code]
        ];
    }

    /**
     * @Auth:HuyNN
     * @Des: get all record transactons table
     * @Since: 05/10/2018
     */
    public static function getAllSearch() {
//        $a_search = array();
//
//        $a_WhereTrans = array();
//        $trans_code = Input::get('trans_code', '');
//        if ($trans_code != '') {
//            $a_search['trans_code'] = $trans_code;
//            $a_WhereTrans['trans_code'] = $trans_code;
//        }
//
//        $project = Input::get('project', '');
//        $a_WhereProject = array();
//        if ($project != '') {
//            $a_search['project'] = $project;
//            $a_WhereProject['cb_id'] = $project;
//        }
//        $building = Input::get('building', '');
//        $a_WhereBuilding = array();
//        if ($building != '') {
//            $a_search['building'] = $building;
//            $a_WhereBuilding['cb_id'] = $building;
//        }

        $arrProjects = BOProductController::getProjectsByUserRoleWeb('approve');
        $arrApartment = BOProductController::getApartmentsByProject($arrProjects);
        $a_data = BOTransaction::select('*')->whereIn('product_id',$arrApartment)->with(
            [
                "staff" => function($staff) {
                    return $staff->select(["ub_id", "ub_account_name AS account", "ub_title AS name", "group_ids"])
                        ->with(["group" => function($group) {
                            return $group->select(['gb_id', 'gb_title AS name']);
                        }]);
                },
                "product" => function($product)  {
                    return $product->select([
                        "pb_id",
                        "pb_title AS apartment", "pb_code AS code",
                        "category_id",
                        "pb_required_money AS required_money"
                    ])->with([ "category" => function($category) {
                        return $category->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])->where("cb_level", 2)
                            ->with(["sibling" => function($project){
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])->where("cb_level", 1);
                            }]);
                    }]);
                }
            ]
        )->orderBy('trans_created_time','desc')->get();

        $a_return = array('a_data' => $a_data);

//        $a_data = array();
//        $o_Db = DB::table('b_o_transactions')->select('*');
//
//
//        //search
//
//        $sz_bank_title = Input::get('bank_title', '');
//        if ($sz_bank_title != '') {
//            $a_search['bank_title'] = $sz_bank_title;
//            $a_data = $o_Db->where('bank_title', 'like', '%' . $sz_bank_title . '%');
//        }
//        $a_data = $o_Db->orderBy('trans_created_time', 'desc')->paginate(30);

//        return $a_return;
        return $a_return;
    }
}
