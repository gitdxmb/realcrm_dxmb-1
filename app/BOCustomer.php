<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use DB;
use Illuminate\Support\Facades\Input;
use App\BOUser;

class BOCustomer extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public $timestamps = true;
    const ID_KEY = 'cb_id';
    const CUSTOMER_SOURCES = [
        'marketing' => 1,
        'staff'     => 2,
        'others'    => 2
    ];

    public function getIssueDateAttribute($date)
    {
        return $date? Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y') : null;
    }

    public function getPhoneVariantsAttribute($value) {
        return $value? json_decode($value) : [];
    }

    public function setAttribute($key, $value){
        switch ($key) {
            case 'phone_variants':
                $value = $value? json_encode($value) : null;
                break;
        }
        $this->attributes[$key] = $value;
    }

    /**
     * @param $phone
     * @param null $by
     * @param bool $api_gate
     * @return array
     */
    public static function generatePhoneVariant($phone, $by=null, $api_gate=true) {
        return [
            'VALUE' => trim($phone),
            'BY'    => $by?? BOUser::getCurrentUID($api_gate),
            'TIME'  => time()
        ];
    }

    /**

     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditCustomer($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['cb_name'] = Input::get('cb_name');
        
        $a_DataUpdate['cb_phone'] = Input::get('cb_phone');
        if($id == 0){
            $o_checkIssetCus = DB::table('b_o_customers')->where('cb_phone', Input::get('cb_phone'))->first();
            if ($o_checkIssetCus) return ['success' => false, 'msg' => 'Trùng SĐT'];
        }

        $a_DataUpdate['source_id'] = Input::get('source_id');
        $a_DataUpdate['cb_status'] = Input::get('cb_status') == 'on' ? 1 : 0;
        $a_DataUpdate['cb_email'] = Input::get('cb_email');
        $a_DataUpdate['cb_login'] = 0;        
        if (is_numeric($id) == true && $id != 0) {
            $o_Customer = DB::table('b_o_customers')->where('id', $id)->first();
            $a_staff = $o_Customer->cb_staff_id != '' ? explode(',',$o_Customer->cb_staff_id) : array();

            if (in_array(Input::get('cb_staff_id'), $a_staff)){
                $fail = 1;
            }else{

                $a_staff[] = Input::get('cb_staff_id');
                $str_staff = implode(',',$a_staff);
                $a_DataUpdate['cb_staff_id'] = $str_staff;
            }
            $a_DataUpdate['updated_at'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_customers')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['cb_id'] = time();
            $a_DataUpdate['created_at'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['updated_at'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_customers')->insert($a_DataUpdate);
        }
        return ['success' => true, 'msg' => 'Thành Công'];

    }
    
    public function AddEditCustomerMapping($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['c_title'] = Input::get('c_title');
        $a_DataUpdate['c_phone'] = Input::get('c_phone');
        $o_checkIssetCus = DB::table('user_customer_mappings')->where('id', $id)->update($a_DataUpdate);
    }
    
    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getCustomerById($id) {
        
        $a_Data = DB::table('b_o_customers')->where('id', $id)->first();
        
        if ($a_Data){
            $a_Data->created_at = Util::sz_DateTimeFormat($a_Data->created_at);
            $a_Data->updated_at = Util::sz_DateTimeFormat($a_Data->updated_at);
        }

        return $a_Data;
    }
    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('user_customer_mappings')->select('*');
        $a_data = $o_Db->where('id_user', Auth::guard('loginfrontend')->user()->ub_id);
        $a_search = array();
        //search
        
        $sz_cb_name = Input::get('cb_name','');
        if($sz_cb_name != '') {
            $a_search['cb_name'] = $sz_cb_name;
            $a_data = $o_Db->where('c_title', 'like', '%'.$sz_cb_name.'%');
        }

        $a_data = $o_Db->orderBy('id', 'desc')->paginate(30);
        
        foreach ($a_data as $key => &$val){
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    
    public function getAllOfficalSearch($showContact=true) {
        $a_data = array();
        $o_Db = DB::table('b_o_customers')->select('*');
        $a_data = $o_Db->where('cb_status', '!=', -1);
        $a_search = array();
        //search
        
        $sz_cb_name = Input::get('cb_name','');
        if($sz_cb_name != '') {
            $a_search['cb_name'] = $sz_cb_name;
            $a_data = $o_Db->where('cb_name', 'like', '%'.$sz_cb_name.'%');
        }
        
        $i_source_id = Input::get('source_id','');
        if($i_source_id != '') {
            $a_search['source_id'] = $i_source_id;            
            $a_data = $o_Db->where('source_id', $i_source_id);
        }
        $a_data = $o_Db->orderBy('updated_at', 'desc')->paginate(30);
                
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
            // Ẩn thông tin liên hệ
            if($showContact){
                $val->cb_phone = mb_substr($val->cb_phone, 0, 3). '****' . mb_substr($val->cb_phone,  -3);
                $val->cb_email =  mb_substr($val->cb_email, 0, 2) . '****@' . str_after($val->cb_email, '@');
            }
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    // get file
    public function getAllFileSearch(){
        
        $a_data = array();
        $o_Db = DB::table('b_o_customer_files')->select('*');
//        $a_data = $o_Db->where('tc_created_by', Auth::guard('loginfrontend')->user()->ub_id);
        $a_search = array();
        //search
        
        $sz_tc_name = Input::get('tc_name','');
        if($sz_tc_name != '') {
            $a_search['tc_name'] = $sz_tc_name;
            $a_data = $o_Db->where('tc_name', 'like', '%'.$sz_tc_name.'%');
        }
        
//        $i_source_id = Input::get('source_id','');
//        if($i_source_id != '') {
//            $a_search['source_id'] = $i_source_id;
//            $a_data = $o_Db->where('source_id', $i_source_id);
//        }
        $a_data = $o_Db->orderBy('id', 'desc')->paginate(30);
                
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
            $val->projectname = isset(DB::table('b_o_categories')->where('cb_id', $val->pj_id)->first()->cb_title) ? DB::table('b_o_categories')->where('cb_id', $val->pj_id)->first()->cb_title : '';
            $val->created_by_staff = isset(DB::table('b_o_users')->where('ub_id', $val->cf_created_by)->first()->ub_account_tvc) ? DB::table('b_o_users')->where('ub_id', $val->cf_created_by)->first()->ub_account_tvc : '';
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
        
        
    }
    
    
    
    
    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearchCusRecent() {
        $o_BOUser = new BOUser();
        $a_data = array();
        $o_Db = DB::table('tmp_customer_data')->select('*');
        if(Input::get('id') != ''){
            $a_data = $o_Db->where('cf_id', Input::get('id'));
        }
        
        $a_search = array();
        //search
        
        $sz_tc_name = Input::get('tc_name','');
        if($sz_tc_name != '') {
            $a_search['tc_name'] = $sz_tc_name;
            $a_data = $o_Db->where('tc_name', 'like', '%'.$sz_tc_name.'%');
        }
        
        $i_source_id = Input::get('source_id','');
        if($i_source_id != '') {
            $a_search['source_id'] = $i_source_id;
            $a_data = $o_Db->where('source_id', $i_source_id);
        }
        $a_data = $o_Db->orderBy('tc_file_name', 'desc')->paginate(30);
                
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
            $val->PersonNameTransfer = isset(DB::table('b_o_users')->where('ub_id', $val->tc_staff_ids)->first()->ub_account_tvc) ? DB::table('b_o_users')->where('ub_id', $val->tc_staff_ids)->first()->ub_account_tvc : '';
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }

    /**
     * @param $value
     */
    public function setTcCreatedByAttribute($value) {
        $this->attributes['tc_created_by'] = $value?? BOUser::getCurrentUID();
    }

    /**
     * @param $value
     */
    public function setCbStaffIdAttribute($value) {
        $this->attributes['cb_staff_id'] = $value?? BOUser::getCurrentUID();
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cb_password',
    ];

    protected $casts = [
        'created_at' => 'datetime:H:i d/m/Y',
        'updated_at' => 'datetime:H:i d/m/Y',
    ];

    /** Custom Password Field
     * @return mixed|string
     */
    public function getAuthPassword() {
        return $this->cb_password;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
