<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Util;

class BORoleGroup extends Model
{
    const ID_key = 'rg_id';

    protected $casts = [
        'rg_staff_ids' => 'array'
    ];

    /**

     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditRoleGroup($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['rg_title'] = Input::get('rg_title');
        
        $a_DataUpdate['rg_description'] = Input::get('rg_description');
        $userInGroup = array();
        if(count(Input::get('rg_staff_ids')) >0){
            foreach(Input::get('rg_staff_ids') as $val){
                $userInGroup[$val] = $val;
            }
        }
        $a_DataUpdate['rg_staff_ids'] = json_encode($userInGroup);
        $a_DataUpdate['rg_status'] = Input::get('rg_status') == 'on' ? 1 : 0;
        
        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['rg_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_role_groups')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['rg_id'] = time();
            $a_DataUpdate['rg_created_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['rg_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_role_groups')->insert($a_DataUpdate);
        }
    }
    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getRoleGroupById($id) {
        
        $a_Data = DB::table('b_o_role_groups')->where('id', $id)->first();
        
        if ($a_Data){
            $a_Data->rg_created_time = Util::sz_DateTimeFormat($a_Data->rg_created_time);
            $a_Data->rg_updated_time = Util::sz_DateTimeFormat($a_Data->rg_updated_time);
        }

        return $a_Data;
    }
    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('b_o_role_groups')->select('*');
        $a_data = $o_Db->where('rg_status', '!=', -1);
        $a_search = array();
        //search
        
        $sz_rg_title = Input::get('rg_title','');
        if($sz_rg_title != '') {
            $a_search['rg_title'] = $sz_rg_title;
            $a_data = $o_Db->where('rg_title', 'like', '%'.$sz_rg_title.'%');
        }
        
        $sz_rg_staff = Input::get('rg_staff_id','');
        if($sz_rg_staff != '') {
            $a_search['rg_staff'] = $sz_rg_staff;
            $a_data = $o_Db->where('rg_staff_ids', 'like', '%'.$sz_rg_staff.'%');
        }
                
        $a_data = $o_Db->orderBy('rg_updated_time', 'desc')->paginate(30);
                
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    
    /*
     * @auth: Dienct
     * @Since: 09/09/2018
     * @Des: Get all Role Group
     * 
     * ***/
    public static function getAllRoleGroups(){        
        return DB::table('b_o_role_groups')->select('rg_id', 'rg_title')->where('rg_status', 1)->get();
    }
    
}
