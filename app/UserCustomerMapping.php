<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserCustomerMapping extends Model
{
    public $timestamps = true;
    protected $casts = [
        'created_at' => 'datetime:H:i d/m/Y',
        'updated_at' => 'datetime:H:i d/m/Y',
        'c_images' => 'array'
    ];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case 'c_images':
                $this->attributes[$key] = $value? json_encode($value) : null;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getIssueDateAttribute($value)
    {
        return $value? Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y') : null;
    }


    public function bo_user() {
        return $this->belongsTo(BOUser::class, "id_user", BOUser::ID_KEY);
    }

    public function bo_customer() {
        return $this->belongsTo(BOCustomer::class, "id_customer", BOCustomer::ID_KEY);
    }
}
