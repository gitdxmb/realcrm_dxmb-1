<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BankInfo;
use App\PaymentMethod;
use App\BOUser;

class BankInfoController extends Controller
{
    private $o_Bank_infos;
    private $o_PayMentMethod;
    public function __construct() {
        $this->o_Bank_infos = new BankInfo();
    }
    public function getAllBankInfo(){
        $a_Data = $this->o_Bank_infos->getAllSearch();
        
        $Data_view['a_BankInfo'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];
        

        return view('bankInfo.index',$Data_view);
        
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit customer
     */
    public function addEditBankInfo(){
        $a_DataView = array();
        $a_DataView['paymentMethod'] = PaymentMethod::getAllPaymentMethod();
        
        $bankId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $bankId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            
            $this->o_Bank_infos->AddEditBank($bankId);
                return redirect('bank')->with('status', 'Cập nhật thành công!');
        }
        
        $a_DataView['a_BankData'] = $bankId != 0 ? $this->o_Bank_infos->getBankInfoById($bankId) : array();

        return view('bankInfo.edit', $a_DataView );
    }
    
    /**
     * @auth: Dienct
     * @since: 21/09/2018
     * @des: sync all Banks
     */
    public function syncAPIBankTVC(){
        
        
        $dataBank = [
            "reportcode" => "apiBNK01",
            "queryFilters" => array(
                array("name" => "dtb", "value" => "DXMB"),
                array("name" => "nadcode", "value" => "O00000000001")
            )
        ];
        $bodyGetBanks = array (
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$dataBank],
            'type' => 'rpc',
            'tid' => 1,
        );
        $DataReturn =  API\APIController::TVC_POST_DECODE($bodyGetBanks,'','PDT.BO@CNNA');
        if (!$DataReturn['data']) {
            echo 'Không tồn tại dữ liệu';
            return;
        }
        
        $a_Banks = isset($DataReturn['data']) ? $DataReturn['data'] : null;
        if(isset($a_Banks) && count($a_Banks)>0){
            $k = time();
            $successUpdate = 0;
            $successInsert = 0;
            
            foreach($a_Banks as $a_Bank){
                    $dataSave = array();
                    $dataSave['bank_title'] = $a_Bank['bankname'];
                    $dataSave['bank_code'] = $a_Bank['bankcode'];
                    $dataSave['bank_holder'] = $a_Bank['bankaccountname'];
                    $dataSave['bank_number'] = $a_Bank['bankaccountnumber'];
                    $dataSave['bank_status'] = 1;
                    // check update or insert
                    $checkBank = DB::table('bank_infos')->where('bank_code', $a_Bank['bankcode'])->first();
                    if(isset($checkBank)){
                        // update
                        DB::table('bank_infos')->where('bank_code', $a_Bank['bankcode'])->update($dataSave);
                        $successUpdate += 1;
                        $dataSave['bank_updated_time'] = date("Y-m-d H:i:s", time());
                    }else{
                        $dataSave['bank_id'] = (int) $k += 1;
                        $dataSave['bank_created_time'] = date("Y-m-d H:i:s", time()); 
                        $dataSave['bank_updated_time'] = date("Y-m-d H:i:s", time()); 
                        //insert
                        DB::table('bank_infos')->insert($dataSave);
                        $successInsert +=1;
                    }
                unset($dataSave);
            }
            echo "Insert thành công {$successInsert} bản ghi và update thành công {$successUpdate} bản ghi";
            // Update last sync TVC time
            
        }else{
            echo "Không có bản ghi nào cập nhật";
        }
        
        
    }
    
    
}
