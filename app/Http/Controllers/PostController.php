<?php

namespace App\Http\Controllers;

use App\BOCategory;
use App\BOUser;
use App\BOUserGroup;
use App\Notification;
use App\Notifications\PostCreated;
use App\PostFeedback;
use App\PostTarget;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function index(Request $request)
    {
        /** @var $filter */
        $filter = [
            'keyword' => $request->input('keyword', null),
            'category' => $request->input('category') >= 0 ? $request->input('category') : null,
            'priority' => $request->input('priority') >= 0 ? $request->input('priority') : null,
            'project' => $request->input('project', null),
            'user' => $request->input('user', null),
        ];
        /** @var $data */
        $data = Post::orderBy('updated_at', 'DESC');
        /** @var $where */
        $where = BOUser::is_admin(false) ? [] : ['created_by' => BOUser::getCurrentUID(false)];
        if ($filter['keyword'] && trim($filter['keyword']) != '') {
            $where[] = ['title', 'LIKE', '%' . trim($filter['keyword']) . '%'];
        }
        if ($filter['category'] >= 0 && trim($filter['category']) != '') {
            $where[] = ['category', '=', (int)$filter['category']];
        }
        if ($filter['project'] && trim($filter['project']) != '') {
            $data = $data->whereJsonContains('bo_category_ids->value', (int)$filter['project']);
        }
        if ($filter['priority'] >= 0 && trim($filter['priority']) != '') {
            $where[] = ['priority', '=', (int)$filter['priority']];
        }
        if ($filter['user'] && BOUser::is_admin(false)) {
            $where[] = ['created_by', '=', (int) $filter['user']];
        }
        $data = $data
            ->where($where)
            ->with(['author:id,ub_title,' . BOUser::ID_KEY])
            ->withCount('post_feedbacks')
            ->paginate(15);
//        return $data;

        /** @var $projects */
        $projects = BOCategory::select([BOCategory::ID_KEY . ' AS id', 'cb_title AS name', 'cb_code AS code'])
            ->where('cb_status', '!=', env('STATUS_DELETE', -1))
            ->where('cb_level', 1)
            ->get();
        return view('post.index', [
            'data' => $data->appends(Input::except('page')),
            'projects' => $projects,
            'filter' => $filter,
            'is_admin'  => BOUser::is_admin(false)
        ]);
    }

    /** todo: Get Post Form - add / update
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function postForm($id = 0)
    {
        $current_account = Auth::guard('loginfrontend')->user()->ub_account_tvc;
        if (!isset($_COOKIE['current_user'])) {
            setcookie("current_user", $current_account, time() + 7 * 24 * 3600, "/");
        }
        if (!is_dir(public_path() . '/uploads/post/')) {
            mkdir(public_path() . '/uploads/post/');
        }
        if (!is_dir(public_path() . '/uploads/post/' . $current_account . '/')) {
            mkdir(public_path() . '/uploads/post/' . $current_account . '/');
        }

        /** @var $data */
        $data = ['item' => null];
        if ($id > 0) {
            /** @var $item */
            $item = Post::select(['*', 'content as raw_content'])->with('post_target')->find($id);
            if (!$item) return abort(404);
            $data['item'] = $item;
            /** @var $notifications */
            $notifications = Notification::select(['notifiable_type', 'notifiable_id'])
                ->where([
                    'type' => (string)PostCreated::class,
                    'data->post' => (int)$id
                ])->get();
            /** @var $target_type */
            $target_type = 'user';
            if ($notifications && count($notifications) > 0) {
                $target_type = array_first($notifications)->notifiable_type == BOUserGroup::class ? 'group' : 'user';
            }
            /** @var $targets */
            $targets = array_pluck($notifications, 'notifiable_id');
            $data['target_type'] = $target_type;
            $data['targets'] = $targets;
//            return $item;
        }
//        return $data;
        $data['projects'] = BOCategory::getAllProject();
        $data['users'] = BOUser::select([BOUser::ID_KEY, 'id', 'ub_title AS name', 'ub_account_tvc AS account'])
            ->where('ub_status', env('STATUS_ACTIVE', 1))->orderBy('ub_title', 'ASC')
            ->get();
        $data['groups'] = BOUserGroup::select([BOUserGroup::ID_KEY, 'id', 'gb_title AS title'])
            ->where('gb_status', env('STATUS_ACTIVE', 1))
            ->where('reference_code', '!=', 'ALL')
            ->get();
        $data['group_all'] = BOUserGroup::select(['id', BOUserGroup::ID_KEY, 'gb_title AS title'])
            ->where('reference_code', 'ALL')
            ->first();
        if (isset($data['target_type']) && $data['group_all']) {
            if ($data['target_type'] == 'group' && in_array($data['group_all']->id, $data['targets'])) {
                $data['target_type'] = 'all';
            }
        }
//        return $data;
        return view('post.addedit', $data);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function submitForm(Request $request, $id = 0)
    {
        $request->validate([
            'title' => 'string|required',
            'description' => 'string|required|max:500',
            'content' => 'string|required',
            'type' => 'required',
            'priority' => 'required',
            'target_departments' => 'required',
            'target_type' => 'required|string',
        ]);
        /** @var $projects */
        $projects = $request->input('projects', null);
//        return $request->all();
        if ($id > 0) {
            $item = Post::findOrFail($id);
        } else {
            $item = new Post();
            $item->created_by = BOUser::getCurrentUID(false);
        }
        $item->title = trim($request->input('title'));
        $item->description = trim($request->input('description'));
        $item->content = trim($request->input('content'));
        $item->type = (int)$request->input('type', 0);
        $item->category = (int)$request->input('category', 0);
        $item->priority = (int)$request->input('priority', 0);
        $item->feedback_type = (int)$request->input('feedback_type', 0);
        /** @var $feedback_options */
        $feedback_options = $request->input('feedback_options', null);
        if ($feedback_options) {
            foreach ($feedback_options as $key => &$feedback_option) {
                if (!$feedback_option || trim($feedback_option) == '') unset($feedback_options[$key]);
            }
        }
        $item->feedback_options = $feedback_options;
        $item->bo_category_ids = $projects ? array_values($projects) : null;
        $item->video = $request->input('video', null);
        $item->status = env('STATUS_ACTIVE', 1);
        $item->tags = $request->input('tags', null);
        if ($id == 0) {
            $item->creator_department = BOUser::getCurrent('group_ids', false);
        }
        $item->updated_by = BOUser::getCurrentUID(false);
        /** todo: handle images */
        $images = $request->input('images', null);
        if (is_array($images)) {
            foreach ($images as $key => &$image) {
                if (!$image) {
                    unset($images[$key]);
                } else {
                    $image = str_after($image, url(''));
                }
            }
            $item->pictures = $images;
        }
        $item->image = is_array($images) ? str_after(array_first($images), url('')) : null;
        if ($item->save()) {
            /** todo: Remove Old targets */
            if ($id > 0) {
                PostTarget::where('post_id', $id)->delete();
            }
            /** @var $post_target */
            $post_target = new PostTarget();
            $post_target->post_id = $item->id;
            $target_departments = $request->input('target_departments', []);
            $post_target->department_ids = in_array('all', $target_departments) ? ['all'] : array_unique($target_departments);
            $post_target->save();

            /** todo: Handle Notification */
            $notify = !$id || $id == 0;
            if ($id > 0 && $request->input('renotify', false)) {
                /** todo: Remove Old notifications */
                Notification::where([
                    'type' => (string)PostCreated::class,
                    'data->post' => (int)$id
                ])->delete();
                $notify = true;
            }
            if ($notify) {
                $target_type = $request->input('target_type', 'all');
                switch ($target_type) {
                    case 'all':
                        $targets = 'all';
                        $target_type = BOUserGroup::class;
                        break;
                    case 'department':
                        $targets = $request->input('notified_departments', []);
                        $target_type = BOUserGroup::class;
                        break;
                    case 'user':
                        $targets = $request->input('notified_users', []);
                        $target_type = BOUser::class;
                        break;
                    default:
                        $target_type = BOUser::class;
                        $targets = [];
                }
//                return $targets;
                NotificationController::notifyPostCreated($item, $targets, $target_type, true, false);
            }
            session()->flash('message', 'Cập nhật nội dung thành công. TB: ' . ($notify ? 'on' : 'off'));
            return response()->redirectTo(route('post-list') . '?category=' . $item->category);
        } else {
            return abort(500, 'Xảy ra lỗi khi cập nhật nội dung!');
        }
    }

    /** Ajax delete post
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function delete($id = 0)
    {
        if (!\Illuminate\Support\Facades\Request::ajax()) return abort(500, 'Yêu cầu không hợp lệ!');
        if ($id > 0) {
            $post = Post::where('status', '!=', -1)->find($id);
            if (!$post) return self::jsonError('Bài viết không tồn tại!');
            $post->status = -1;
            if ($post->save()) return self::jsonSuccess('Xóa bài viết thành công!');
        }
        return self::jsonError('Xảy ra lỗi khi xóa bài viết!');
    }

    /** Ajax get post feedbacks
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getFeedbacks($id = 0)
    {
        if ($id > 0 && \Illuminate\Support\Facades\Request::ajax()) {
            $post = Post::select(['id', 'title', 'feedback_options', 'feedback_type', 'type'])
                ->where('status', '!=', env('STATUS_DELETE', -1))
                ->find($id);
            if (!$post) return self::jsonError('Bài viết không tồn tại!');
            $info = [];
            $post_feedbacks = PostFeedback::where('post_id', $id)->orderBy('updated_at', 'DESC');
            if ($post->feedback_type == 1) {
                $post_feedbacks = $post_feedbacks->whereNotNull('option')->get();
                $grouped_feedbacks = collect($post_feedbacks)->groupBy('option')->toArray();
            } else {
                $post_feedbacks = $post_feedbacks
                    ->whereNull('option')
                    ->with('created_by:ub_account_tvc,ub_avatar,ub_title,' . BOUser::ID_KEY)
                    ->get();
                $grouped_feedbacks = [];
            }
            $post->post_feedbacks = $post_feedbacks;
            $info['grouped_feedbacks'] = $grouped_feedbacks;
            return self::jsonSuccess($post, 'Thành công', $info);
        }
        return self::jsonError('Yêu cầu không hợp lệ!');
    }

    /**
     * @auth: HuyNN
     * @since: 12/12/2018
     * @Des: add, edit Post
     */
    public function addedit()
    {
        $a_DataView = array();
        $PostId = (int)Input::get('id', 0);
        if ($PostId != 0) {
            $post = Post::find($PostId);
        } else {
            $post = new Post();
        }

        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $data = Input::get();
            $post->title = $data['title'];
            $post->description = $data['description'];
            $post->content = $data['content'];
            $post->type = $data['type'];
            $post->priority = $data['priority'];
            $post->save();
            return redirect('post')->with('status', 'Cập nhật thành công!');
        }
        $a_DataView['i_id'] = $PostId;
        $a_DataView['post'] = $post;
        $a_Project = BOCategory::getAllProject();
        $a_DataView['a_Project'] = $a_Project;
        return view('post.addedit', $a_DataView);
    }
}
