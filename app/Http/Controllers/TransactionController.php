<?php

namespace App\Http\Controllers;

use App\BOCategory;
use App\BOProduct;
use App\Http\Controllers\API\BOProductController;
use App\Http\Controllers\API\BOTransactionController;
use Illuminate\Http\Request;
use DB;
use App\BOTransaction;
use Illuminate\Support\Facades\Auth;
class TransactionController extends Controller
{
    public function __construct() {

    }
    public function getAllTransaction(Request $request){
        $a_Data = BOTransaction::getAllSearch();
        $a_DataView['a_AllTransaction'] = $a_Data['a_data'];
//        $Data_view['a_search'] = $a_Data['a_search'];
        $a_DataView['a_Project'] = BOCategory::getAllProject();
        return view('transaction.index',$a_DataView);
    }

    public function confirm(Request $request){
        $request = $request->input();
        $transaction = BOTransaction::where([
            BOTransaction::ID_KEY => $request['trans_id']
        ])->first();
        if($transaction){
            if(isset(BOTransaction::STATUS_CODES[$request['status_code']])){
                $userInfo = Auth::guard('loginfrontend')->user();
                $current_log = $transaction->trans_status_log;
                $current_log[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES[$request['status_code']],$userInfo->ub_id);
                $transaction->trans_status_log = $current_log;
                $transaction->trans_code = BOTransaction::STATUS_CODES[$request['status_code']];
                $transaction->trans_title = BOTransaction::STATUSES[BOTransaction::STATUS_CODES[$request['status_code']]];
                $transaction->save();
                return redirect('transaction')->with('status', 'Cập nhật thành công!');
            }else{
                return redirect('transaction')->with('status', 'Trạng thái không hợp lệ!');
            }
        }else{
            return redirect('transaction')->with('status', 'Không tồn tại Yêu cầu Lock này!');
        }
    }
    public function changeStatus(Request $request){
        $request = $request->input();
        $status = $request['code'];
        $trans_id = $request['trans_id'];
        if(!$status || !$trans_id){
            return redirect('transaction')->with('status', 'Kiểm tra lại dữ liệu!');
            return false;
        }
        if (!in_array($status, array(BOTransaction::STATUS_CODES['LOCKED_MANUAL'],BOTransaction::STATUS_CODES['CANCELED_MANUAL']))) {
            return redirect('transaction')->with('status', 'Trạng thái duyệt không chính xác!');
            return false;
        }
        /** @var $query */
        $query = BOTransaction::where(BOTransaction::ID_KEY, $trans_id)
            ->with(['created_by:ub_id,ub_title,ub_tvc_code,ub_account_tvc', 'product:pb_code,'.BOProduct::ID_KEY])
            ->first();

        if(!$query){
            return redirect('transaction')->with('status', 'Không tìm thấy yêu cầu!');
            return false;
        }
        if (!$query->product){
            return redirect('transaction')->with('status', 'Sản phẩm không tồn tại!');
            return false;
        }
        $release = false;
        //** todo: Pusher on apartment status changed */
        switch ($status) {
            case BOTransaction::STATUS_CODES['CANCELED_MANUAL']:
                /** @var $apartment_release */
                $release = BOProductController::releaseApartment($query->product_id, true, $query);
                break;
            case BOTransaction::STATUS_CODES['LOCKED_MANUAL']:
                if ($query->product) {
                    /** Lock & notify */
                    BOProductController::lockApartment($query->product->{BOProduct::ID_KEY}, true, true, $query->created_user_id);
                    /** todo: Set auto unlock timer */
                    BOTransactionController::autoCancelTransaction($trans_id);
                }
                $query->auto_cancel_time = time() + BOTransaction::LIMIT_TIME;
                break;
            default:
        }
        $current_log = $query->trans_status_log;
        $current_log[] = BOTransaction::generateStatusLogWeb($status);
        $query->trans_code = $status;
        $query->trans_title = BOTransaction::STATUSES[$status];
        $query->updated_user_id = null;
        $query->trans_updated_time = null;
        $query->trans_status_log = $current_log;
        if($query->save()){
            return redirect('transaction')->with('status', 'Cập nhật thành công!');
        }else{
            return redirect('transaction')->with('status', 'Cập nhật không thành công!');
        }
    }
}
