<?php

namespace App\Http\Controllers;

use App\BOCategory;
use App\BOProduct;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\BOProductController;
use App\UserCustomerMapping;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BOPayment;
use App\BOBill;
use App\PaymentMethod;
use App\BankInfo;
use App\BOUser;
use Illuminate\Support\Facades\Auth;
class PaymentController extends Controller
{
    private $BOPayment;
    public function __construct() {
        $this->BOPayment = new BOPayment();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function index(Request $request) {
        /** @var $filter */
        $filter = [
            'page'   => $request->input('page', 1),
            'code'   => $request->input('code', null),
            'bill_code'   => $request->input('bill_code', null),
            'product'   => $request->input('product', null),
            'status'   => $request->input('status')!=''? (int) $request->input('status') : null,
        ];
        $size = 10;
        /** @var $apartments */
        $apartments = [];
        $is_admin = BOUser::is_admin(false);

        /** todo: Get Bill in Paid-Once status */
        $filtering_bills = BOBill::select('bill_code')
            ->where('status_id', '!=', BOBill::STATUSES['STATUS_PAID_ONCE']);
        if (!$is_admin) {
            $projects_involved = BOProductController::getProjectsByUserRole(self::ROLE_GROUPS['ACCOUNTANT'], false);
            $apartments = BOProductController::getApartmentsByProject($projects_involved);
            $filtering_bills = $filtering_bills->whereIn('product_id', $apartments);
        }
        $filtering_bills = $filtering_bills->get();
        if ($filtering_bills) {
            $filtering_bills = $filtering_bills->pluck('bill_code')->toArray();
        }

        /** @var $data */
        $data = BOPayment::select('*');

        /** todo: Filter by: Code, Status, Bill code, Product ID */
        if ($filter['code']) $data = $data->where('pb_code', trim($filter['code']));
        if ($filter['status']!==null) $data = $data->where('pb_status', $filter['status']);
        if ($filter['bill_code']) $data = $data->where('bill_code', trim($filter['bill_code']));
        if ($filter['product']) {
            $bills_by_product = BOBill::select('bill_code')
                                ->where('status_id', '!=', BOBill::STATUSES['STATUS_PAID_ONCE'])
                                ->where('product_id', (int) $filter['product']);
            if (!$is_admin) $bills_by_product = $bills_by_product->whereIn('product_id', $apartments);
            $bills_by_product = $bills_by_product->get();
            $data = $data->whereIn('bill_code', $bills_by_product->pluck('bill_code')->toArray());
        } else {
            $data = $data->whereIn('bill_code', $filtering_bills);
        }
        /** Finalize $data */
        $data = $data
            ->with([
                'bill' => function($bill) {
                    $bill->select([
                        BOBill::ID_KEY,
                        'bill_code',
                        'bill_total_money AS total_money',
                        'bill_customer_info',
                        'product_id',
                        'contract_reference_code',
                        'contract_reference_code_F1',
                        'status_id'
                    ]);
                },
                'requestStaff:ub_title,ub_account_tvc,'.BOUser::ID_KEY,
                'responseStaff:ub_title,ub_account_tvc,'.BOUser::ID_KEY,
            ])
            ->orderBy('updated_at', 'DESC')
            ->paginate($size);
        /** @var $products */
        $products = [];
        foreach ($data as &$datum) {
            if ($datum->bill) {
                $products[] = $datum->bill->product_id;
                if(file_exists(public_path('/pdf/hop-dong-'.$datum->bill->bill_id.'.pdf'))){
                    $datum->pdf = '/pdf/hop-dong-'.$datum->bill->bill_id.'.pdf';
                }
            }
        }
        /** @var $apartments */
        $apartments = BOProduct::getAllBasic($products, BOProduct::ID_KEY);

        return view('payment.index', [
            'data' => $data->appends(Input::except('page')),
            'apartments' => $apartments,
            'bills' => $filtering_bills,
            'filter' => $filter
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function showForm($id) {
        if (!$id) abort(302, 'Yêu cầu không hợp lệ!');
        /** @var $payment_methods */
        $payment_methods = PaymentMethod::getAllPaymentMethod();
        /** @var $bank_info */
        $bank_info = BankInfo::where('bank_status', '!=', -1)->orderBy('bank_title','asc')->get();
        /** @var $data */
        $data = $this->BOPayment->getPaymentById($id);
        return view('payment.form', [
            'payment_methods' => $payment_methods,
            'bank_info' => $bank_info,
            'data'  => $data,
            'is_accountant' => AuthController::is_accountant(false)
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function submitForm($id) {
        if (!$id) abort(302, 'Yêu cầu không hợp lệ!');
        if (!AuthController::is_accountant(false)) abort(401, 'Bạn không có quyền thực hiện thao tác này!');
        self::savePayment($id);
        session()->flash('message', 'Cập nhật thành công!');
        return redirect('payment');
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit Payment
     *      */
//    public function addEditPayment(){
//        $a_DataView = array();
//        $a_DataView['a_PaymentMethod'] = PaymentMethod::getAllPaymentMethod();
//        $a_DataView['a_Bank'] = BankInfo::where('bank_status', '!=', -1)->orderBy('bank_title','asc')->get();
//
//        $PaymentId = (int) Input::get('id', 0);
//        $a_DataView['i_id'] = $PaymentId;
//        $checksubmit = Input::get('submit');
//        if (isset($checksubmit) && $checksubmit != "") {
////            if(self::checkLoginTvcWeb()){
////                self::ProcessPayMent($PaymentId);
////            }
//            self::ProcessPayMent($PaymentId);
//            return redirect('payment')->with('status', 'Cập nhật thành công!');
//        }
//
//        $a_DataView['PaymentData'] = $PaymentId != 0 ? $this->BOPayment->getPaymentById($PaymentId) : array();
//        $a_DataView['is_accountant'] = AuthController::is_accountant(false);
//        return view('payment.edit', $a_DataView );
//    }

    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit Payment
     *      */
    public function addNewPayment(){
        $a_DataView = array();
        $a_DataView['a_PaymentMethod'] = PaymentMethod::getAllPaymentMethod();
        $a_DataView['a_Bank'] = BankInfo::where('bank_status', '!=', -1)->orderBy('bank_title','asc')->get();
        $PaymentId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $PaymentId;

        $projects = BOCategory::where('cb_level' , 1)->get();
        $a_DataView['projects'] = $projects;

        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
//            if(self::checkLoginTvcWeb()){
//                self::ProcessPayMent($PaymentId);
//            }
            self::savePayment($PaymentId);
            return redirect('payment')->with('status', 'Cập nhật thành công!');
        }
        $a_DataView['PaymentData'] = $PaymentId != 0 ? $this->BOPayment->getPaymentById($PaymentId) : array();
        return view('payment.new', $a_DataView );
    }


    public function savePayment($id) {
        $bill_id = Input::get('bill_id');
        $bill = BOBill::find($bill_id);
        if($id != 0){
            $payment = BOPayment::find($id);
            $typeOfPayment = $payment->type_of_payment;
        }else{
            $payment = new BOPayment();
            $payment->pb_id = time();
            $payment->pb_code = 'OBP'. $payment->pb_id;
            $payment->bill_code = $bill->bill_code;
            $payment->request_time = now();
            $payment->type = $bill->type != 1 ? 2 : 1;
            $payment->customer_id = Input::get('customer_id');
            $payment->request_staff_id = Input::get('request_staff_id');
            $payment->pb_request_money = (int) str_replace(' ','',Input::get('pb_request_money')) ;
            $payment->request_time = now();
            $typeOfPayment = Input::get('typeOfPayment');
            $payment->type_of_payment = $typeOfPayment;
        }
        $payment->pb_response_money = (int) str_replace(' ','',Input::get('pb_response_money'));
        $payment->response_staff_id = Auth::guard('loginfrontend')->user()->ub_id; // ke toan thu tien
        $payment->pb_note = Input::get('pb_note');
        $payment->pb_note2 = Input::get('pb_note2');
        $payment->method_id = Input::get('method_id');
        $payment->bank_id = Input::get('bank_id') ? Input::get('bank_id') : null;
        $payment->pb_bank_number_id = Input::get('pb_bank_number_id');
        $payment->pb_status = 1;
        $payment->reference_code = Input::get('reference_code');
        $payment->pb_response_time = now();
        if($payment->save() && $typeOfPayment == 'C'){
            if(in_array($bill->status_id,[BOBill::STATUSES['STATUS_PAYING'],BOBill::STATUSES['STATUS_PAID_ONCE']])){
                // check thu du tien hay chua
                $a_allPaymentByBillCode = BOPayment::where('bill_code', $bill->bill_code)->get();
                $totalResponMoney = 0;
                foreach($a_allPaymentByBillCode as $val){
                    $totalResponMoney += (int) $val->pb_response_money;
                }

                if($totalResponMoney >= (int) $bill->bill_required_money){
                    if($bill->type != BOBill::TYPE_DEPOSIT){
                        DB::table('b_o_bills')->where('bill_code', $bill->bill_code)->update(['status_id'=>BOBill::STATUSES['STATUS_BOOKED']]);
                        BOProductController::changeStatus($bill->product_id, BOProduct::STATUSES['DCH'], true);
                    }else{
                        DB::table('b_o_bills')->where('bill_code', $bill->bill_code)->update(['status_id'=>BOBill::STATUSES['STATUS_PAID']]);
                    }
                }else{
                    DB::table('b_o_bills')->where('bill_code', $bill->bill_code)->update(['status_id'=>BOBill::STATUSES['STATUS_PAYING']]);
                }
            }
            //Send mail///
            MailController::customerPayment($payment);
            /// Push Noti///
            /** Push fcm & store */
            \App\Http\Controllers\NotificationController::notifyPaymentStatusUpdated($payment, $bill);
        }

        //$bill = BOBill::where('bill_code',Input::get('bill_code'))->first();
//        $type = $bill->type == 1 ? BOPayment::TYPE_PAYMENT_TAVICO['DCO'] : BOPayment::TYPE_PAYMENT_TAVICO['DCHO'];
        //$referenceCode = self::createPaymentTvc($a_DataInsert,$type,$bill);
//        if($referenceCode != '')
//            DB::table('b_o_payments')->where('id', $insertId)->update(array('reference_code' => $referenceCode));
    }
    
    /**
     * @auth: HuyNN
     * @since: 05/09/2018
     * @Des: add, edit Payment Tavico
     *      */
    public static function createPaymentTvc($data,$type,$bill){
        $product = BOProduct::where([BOProduct::ID_KEY => $bill->product_id])->first();
        $customerMap = UserCustomerMapping::where(["id_customer" =>$bill->customer_id])->first();
        $staff = BOUser::where([BOUser::ID_KEY => $bill->staff_id])->first();
        switch ($type) {
            case BOPayment::TYPE_PAYMENT_TAVICO['DCHO']:
                $revenuetype = 'TTDCH';
                $d_c = 'C';
                $anal_r0 = '';
                $anal_r2 = $customerMap->potential_reference_code;
                $anal_r3 = $bill->bill_reference_code;
                $anal_r6 = 'Z';
                $customer = '';
                break;
            case BOPayment::TYPE_PAYMENT_TAVICO['DCO']:
                $revenuetype = 'TTDCO';
                $d_c = 'C';
                $anal_r0 = $bill->contract_reference_code;
                $anal_r2 = $customerMap->potential_reference_code;
                $anal_r3 = $bill->bill_reference_code;
                $anal_r6 = 'Z';
                $customer = $customerMap->customer_reference_code;
                break;
            case BOPayment::TYPE_PAYMENT_TAVICO['CANCEL_DCHO']:
                $revenuetype = 'TTDCH';
                $d_c = 'D';
                $anal_r0 = '';
                $anal_r2 = $customerMap->potential_reference_code;
                $anal_r3 = $bill->bill_reference_code;
                $anal_r6 = 'Z';
                $customer = '';
                break;
            default:
        }

        $DataPayment =
            array (
                'revenuetype' => $revenuetype, /// Loại thu
                'transdate' => date('Y-m-d',time()),
                'transmonth' => '',
                'transref' => '+',
                'property' => $product->pb_code, //Mã căn hộ
                'customer' => $customer,
                'bankcode' => '',
                'd_c' => $d_c, // C- Thu, D -Chi
                'amount' => $data['pb_response_money']? str_replace(',','',$data['pb_response_money']): "",
                'anal_r0' => $anal_r0, // Mã giao dịch
                'anal_r1' => $staff->ub_tvc_code, ///
                'anal_r2' => $anal_r2, //Mã KHTN
                'anal_r3' => $anal_r3, //Mã Phiếu đặt chỗ
                'anal_r6' => $anal_r6, //Mã KH-NCC
                'extreference1' => $customerMap->c_title, // Ng nộp
                'extreference2' => '', //Số bảng kê
                'extdescription1' => $data['pb_note']?? "", //Diễn giải
                'extdescription2' => $data['pb_note']?? "", // Ghi chú
                'extdescription3' => Auth::guard('loginfrontend')->user()->ub_title, //Người thu tiền
            );

        $body = array(
            'action' => 'FrmPmTransaction',
            'method' => 'roughEntry',
            'data' => [$DataPayment],
            'type' => 'rpc',
            'tid' => 1,
        );

        $res = self::TVC_POST_DECODE($body);
        $referenceCode = '';
        if($res['success'] == true){
            $referenceCode = $res['data'][0]['transref'];
        }
        return $referenceCode;
    }


    /**
     * @auth: HuyNN
     * @since: 10/12/2018
     * @Des: print Payment
     *      */
    public function print(){
        $paymentId = (int) Input::get('pb_id', 0);
        $data = BOPayment::where([BOPayment::ID_KEY => (int) $paymentId])
            ->with([
                'bill:bill_id,bill_title,bill_reference_code,bill_product_ids',
                'customer:cb_id,cb_name,cb_phone,cb_permanent_address',
                'paymentMethod:pm_id,pm_title',
                'requestStaff:ub_id,ub_title',
            ])
            ->first();
        if($data){
            $a_DataView['payment'] = $data;
            return view('payment.print', $a_DataView );
        }else{
            return redirect('payment')->with('status', 'Không tồn tại phiếu thu này!');
        }
    }
}
