<?php

namespace App\Http\Controllers;

use App\BOCategory;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BOProduct;
use App\Util;
use App\Product;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\BOUser;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\LogController;
use Illuminate\Support\Carbon;
class ProductController extends Controller
{
    private $BOProduct;
    public function __construct() {
        $this->BOProduct = new BOProduct();
        $this->BOCategory = new BOCategory();
    }

    /**
     * @auth: HuyNN
     * @since:
     * @des: get all apartment
     */
    public function getAllApartment(){
        $count = 0;
        $a_search = array();
        $pid = (int) Input::get('project', 0);
        if($pid != 0){
            $a_Building = $this->BOCategory->getAllBuildingByProject($pid);
            $a_DataView['a_Building'] = $a_Building;
            $a_search['project'] = $pid;
        }
        $bid = (int) Input::get('building', 0);
        if($bid != 0){
            $a_search['building'] = $bid;
        }
        $a_Apartment = $this->BOProduct->getAllApartment($bid);
        $count = $a_Apartment->count();
        $o_Building = BOCategory::where(BOCategory::ID_KEY, $bid)->first();
        $a_Project = BOCategory::getAllProject();
        $a_numberOfType = array();
        $a_numberOfFloor = array();
        $arrApartment = array();
        if(count($a_Apartment) > 0){
            foreach ($a_Apartment as $o_Apartment){
                $numberTypeApart = (string) substr($o_Apartment->pb_code, -3);
                $firstCharTypeApart = substr($numberTypeApart,0,1);
                // Sua boi CCN
                //if($firstCharTypeApart == "0"){
                //    $numberTypeApart = substr($numberTypeApart, 1);
                //}
                $pb_door_direction = $o_Apartment->pb_door_direction;
                $pb_balcony_direction = $o_Apartment->pb_balcony_direction;
                if(!isset($a_numberOfType[$numberTypeApart])){
                    $a_numberOfType[$numberTypeApart] = array(
                        'pb_door_direction' => '',
                        'pb_balcony_direction' => ''
                    );
                }
                $a_numberOfType[$numberTypeApart]['pb_door_direction'] = $pb_door_direction != '' ? $pb_door_direction : $a_numberOfType[$numberTypeApart]['pb_door_direction'];
                $a_numberOfType[$numberTypeApart]['pb_balcony_direction'] = $pb_balcony_direction != '' ? $pb_balcony_direction : $a_numberOfType[$numberTypeApart]['pb_balcony_direction'];

                $numberFloor = (string) substr($o_Apartment->pb_code, -6, 3);
                $firstCharFloor = substr($numberFloor,0,1);
                // Sua boi CCN 
                //if($firstCharFloor == "0"){
                //   $numberFloor = substr($numberFloor, 1);
                //}
                if(!in_array($numberFloor,$a_numberOfFloor)){
                    $a_numberOfFloor[] = $numberFloor;
                }

                $arrApartment[$numberFloor.$numberTypeApart] = array(
                    'status' => $o_Apartment->status_id,
                    'price' => $o_Apartment->pb_price_total,
                    'id' => $o_Apartment->pb_id
                );
            }
            ksort($a_numberOfType);
            asort($a_numberOfFloor);
        }

        $a_DataView['a_numberOfType'] = $a_numberOfType;
        $a_DataView['a_numberOfFloor'] = $a_numberOfFloor;
        $a_DataView['o_Building'] = $o_Building;
        $a_DataView['a_Project'] = $a_Project;
        $a_DataView['a_search'] = $a_search;
        $a_DataView['arrApartment'] = $arrApartment;
        $a_DataView['statusDisplay'] = BOProduct::STATUS_DISPLAY;
        $a_DataView['count'] = $count;
        return view('product.index', $a_DataView );

    }
    
    
    /**
     * @auth: Dienct
     * @since: 21/09/2018
     * @des: sync all Product
     */
    public function syncAPIProductTVC(){
        set_time_limit(5500);
        //login
        $login_info =  [
            "id"=> 1,
            "username"=> "HUYNN@DXMB",
            "password"=> "123456",
            "appLog"=> "Y",
            "securityKey"=> "DD19-C2DA-B6E6-DB45-8DB3-E0EF-F1B7-9DEC-488B-913C"
          ];
        $bodyLogin = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $dataLogin = API\APIController::TVC_POST($bodyLogin);
        
        // get last sync time
        $firstProduct = DB::table('b_o_products')->first();        
        $dateF = isset($firstProduct->last_sync_tvc) && $firstProduct->last_sync_tvc != '' ? Util::sz_DateTimeGetAPI2222($firstProduct->last_sync_tvc) : '';        
        $dateT = date("Y-m-d H:i:s", time());
        
        // get Data Project TVC
        $dataGetProject = [
            "reportcode" => "apiPPT01",            
            "queryFilters" => array(
                array("name" => "project", "value" => "VOP"),
                array("name" => "dtb", "value" => "DXMB"),
                array("name" => "block", "value" => ""),
                array("name" => "property", "value" => ""),
                array("name" => "datef", "value" => $dateF),
                array("name" => "datet", "value" => $dateT)
            )
        ];
        $bodyGetProject = array (
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$dataGetProject],
            'type' => 'rpc',
            'tid' => 1,
        );
        $DataReturn =  API\APIController::TVC_POST($bodyGetProject);
        $a_Projects = isset($DataReturn) ? json_decode($DataReturn->original['data'])->result->data : null;        
        
        // update Project
        if(isset($a_Projects) && count($a_Projects)>0){
            $successUpdate = 0;
            $successInsert = 0;
            // get All Project
            $k = time() + 55000;
            $o_building = DB::table('b_o_categories')->where('cb_status', 1)->where('cb_level', 2)->get();
            foreach ($o_building as $val){
                if(strlen($val->cb_code) == 6){
                    $a_Building[$val->cb_code] = $val->cb_id;
                }
            }
            
            foreach($a_Projects as $a_Project){
                if(strlen($a_Project->property) == 12){
                    $dataSave = array();
                    $dataSave['status_id'] = isset(BOProduct::STATUSES[$a_Project->status]) ? BOProduct::STATUSES[$a_Project->status] : -2 ;
                    $dataSave['pb_code'] =  $a_Project->property;
                    $dataSave['category_id'] =  isset($a_Building[$a_Project->block]) ? $a_Building[$a_Project->block] : null;
                    $dataSave['pb_used_s'] =  $a_Project->value1;
                    $dataSave['pb_built_up_s'] =  $a_Project->area;
                    // check update or insert
                    $checkCat = DB::table('b_o_products')->where('pb_code', $a_Project->property)->first();

                    if(isset($checkCat)){
                        // update
                        DB::table('b_o_products')->where('pb_code', $a_Project->property)->update($dataSave);
                        $successUpdate += 1;
                    }else{
                        $dataSave['pb_id'] = (int) $k += 1;
                        //insert
                        DB::table('b_o_products')->insert($dataSave);
                        $successInsert +=1;
                    }
                }
                unset($dataSave);
            }
            DB::table('b_o_products')->update(['last_sync_tvc' => date("Y-m-d H:i", time())]);
            echo "Insert thành công {$successInsert} bản ghi và update thành công {$successUpdate} bản ghi";
            // Update last sync TVC time
            
        }else{
            echo "Không có bản ghi nào cập nhật";
        }
    }

    /**
     * @auth: HuyNN
     * @since: 15/10/2018
     * @des: import apartment
     */
    
    public function importApartment(){
//        $a_Res = array();
        if (Input::hasFile('excel')) {
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = strtolower(Input::file('excel')->getClientOriginalExtension());
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $res = $this->importExcelApartment($sz_FileDir);
            }else{
                $res = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }
            $a_DataView['res'] = $res;
        }
        $this->BOCategory->getAllCategoriesByParentID(0, $a_project);
        $a_DataView['a_projects'] = $a_project;
        return view('product.import',$a_DataView);
    }

    public function importExcelApartment($dirFile) {
        /** @var $results */
        $results = [];

        set_time_limit(600);
        $buildingId = Input::get('building');
        $buildingObject = BOCategory::where([BOCategory::ID_KEY => $buildingId])->first();
        $projectObject = BOCategory::where(['id' => $buildingObject->parent_id])->first();

        Excel::selectSheetsByIndex(0)->load($dirFile, function($reader) use (&$results) {
            $reader->formatDates(true, 'Y-m-d');
            $results = $reader->toArray();
        });
        $userInfo = Auth::guard('loginfrontend')->user();
        self::checkLoginTvcWeb(); // login tvc
        // get all price booking id                    
        // get user config
        $cookie = strtoupper($userInfo->ub_account_tvc);
        $user = BOUser::where('ub_account_tvc', $cookie)->first();
        $login_info = [
            "id" => 1,
            "username" => $cookie,
            "password" => self::decrypte(session('encryptPass'), self::ENCRYPTION_KEY),
            "appLog" => "Y",
            "securityKey" => $user->security_code
        ];
        $login_body = array(
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $do_login = self::TVC_POST_DECODE($login_body, '', $cookie);
        if (!$do_login) {
            LogController::logBill('[TVC] Không thể đăng nhập tài khoản hệ thống ' . $cookie . '!', 'error', null);
            return 'Phiên đăng nhập hết hạn, vui lòng đăng nhập lại tài khoản '.$cookie . ' trên App Đất Xanh Miền Bắc';
        }
        $checkProject = API\APIController::TVC_CHECK_EXIST_PROJECT($projectObject->reference_code, $cookie);
        $checkBuilding = API\APIController::TVC_CHECK_EXIST_BUILDING($buildingObject->reference_code, $cookie);
        if ($checkProject == 0) {
            $dataProject = [
                'project' => $projectObject->reference_code,
                'name' => $projectObject->cb_title
            ];
            API\APIController::TVC_ADD_PROJECT($dataProject, $cookie, false);
        }
        if ($checkBuilding == 0) {
            $dataBuilding = [
                'project' => $projectObject->reference_code,
                'block' => $buildingObject->reference_code,
                'name' => $buildingObject->cb_title
            ];
            API\APIController::TVC_ADD_BUILDING($dataBuilding, $cookie, false);
        }
        $aryInsertProduct = $aryUpdateProduct = $aryInsertPriceBooking = $aryUpdatePriceBooking = array();
        $updateCount = 0;
        /** @var $logs */
        $logs = [];
        $logs[] = LogController::logProduct('[Product_Import] Chuẩn bị import '.count($results).' căn hộ!', 'info', null, false, false);
        foreach ($results as $index => $row) {
            if ($row['bds'] != '' && substr($row['bds'], 0, 6) == $buildingObject->reference_code && strlen($row['bds']) == 12) {
                $apartment = BOProduct::where(['pb_code' => $row['bds']])->first();
                if (!$apartment) {
                    $update_price = true;
                    $apartment = new BOProduct();
                    $apartment->pb_code = trim($row['bds']);
                    $apartment->pb_id = null;
//                    $apartment->pb_id = BOProduct::orderBy('id','desc')->first()->pb_id + 1;
                } else {
                    $update_price = $apartment->status_id == BOProduct::STATUSES['MBA'];
                }
                $apartment->number_lock = $row['so_nguoi_lock'] != '' ? $row['so_nguoi_lock'] : 1;
                $apartment->ub_updated_time = date('Y-m-d H:i:s', time());
                $apartment->updated_user_id = Auth::guard('loginfrontend')->user()->ub_id;

                if ($update_price) {
                    $apartment->category_id = $buildingId;
                    $apartment->status_id = $row['tinh_trang_can_ho'] != '' ? BOProduct::STATUSES[trim($row['tinh_trang_can_ho'])] : BOProduct::STATUSES['MBA'];
                    $apartment->price_used_s = $row['gia_thong_thuy'] != '' ? (float) trim($row['gia_thong_thuy']) : Null;
                    $apartment->pb_price_per_s = $row['gia_tim_tuong'] != '' ? (float) trim($row['gia_tim_tuong']) : Null;
                    $apartment->price_min = $row['gia_san_min'] != '' ? (float) trim($row['gia_san_min']) : Null;
                    $apartment->price_max = $row['gia_tran_max'] != '' ? (float) trim($row['gia_tran_max']) : Null;
                    $apartment->pb_price_total = $row['tong_gia_ban_bao_gom_vat_ko_phi_bao_tri'] != '' ? (float) trim($row['tong_gia_ban_bao_gom_vat_ko_phi_bao_tri']) : Null;
                    $apartment->pb_required_money = $row['so_tien_yeu_cau'] != '' ? (float) trim($row['so_tien_yeu_cau']) : 50000000;
                    $apartment->pb_code_real = $row['so_bds_thuc_te'];
                    $apartment->pb_used_s = $row['dt_thong_thuy'] == '' ? 0 : (float) trim($row['dt_thong_thuy']);
                    $apartment->pb_built_up_s = $row['dt_tim_tuong'] == '' ? 0 : (float) trim($row['dt_tim_tuong']);
                    $apartment->bedroom = $row['phong_ngu'];
                    $apartment->price_maintenance = $row['phi_bao_tri'] != '' ? (float) trim($row['phi_bao_tri']) : Null;
                    $apartment->view = $row['view'];
                    $apartment->corner = $row['goc'] == 0 || $row['goc'] == '' ? 0 : 1;
                    $apartment->payment_methods = trim($row['pttt']);
                    $apartment->note = $row['ghi_chu'];
                    $apartment->stage = $row['giai_doan'] != '' ? $row['giai_doan'] : 1;
                    $apartment->pb_door_direction = $row['huong'];
                    $apartment->book_type = $row['loai_dat_cho'] !== '' ? (int) $row['loai_dat_cho'] : Null;
                    $apartment->pb_status = $row['trang_thai'] != '' ? (int) $row['trang_thai'] : 1;
                }
                $debug = [
                    'pb_status' => $apartment->pb_status,
                    'status_id' => $apartment->status_id,
                    'code'  => $apartment->pb_code,
                    'id'    => $apartment->pb_id
                ];
                if ($apartment->save()){
                    $updateCount += 1;
                    $logs[] = LogController::logProduct('[Product_Import] Cập nhật căn hộ BO thành công: '. json_encode($debug), 'info', $apartment->{BOProduct::ID_KEY}, false, false);
                } else {
                    $logs[] = LogController::logProduct('[Product_Import] Cập nhật căn hộ BO không thành công: '. json_encode($debug), 'error', null, false, false);
                }
                /** @var $dataTVC */
                $dataTVC = [
                    'property' => $apartment->pb_code,
                    "propertynum" => $apartment->pb_code_real?? "",
                    "block" => substr($row['bds'], 0, 6),
                    "area" => $apartment->pb_built_up_s?? 0,
                    "value1" => $apartment->pb_used_s?? 0
                ];
                if ($update_price) $dataTVC['status'] = $row['tinh_trang_can_ho'] != '' ? trim($row['tinh_trang_can_ho']) : 'MBA';
                // check Product
                $checkProduct = API\APIController::TVC_CHECK_EXIST_PRODUCT($row['bds'], $cookie, false);
                if (!$checkProduct||$checkProduct===0) {
                    $aryInsertProduct[] = $dataTVC;
                } else {
                    $aryUpdateProduct[] = $dataTVC;
                }

                // array update price
                $dataPrice = API\APIController::TVC_LIST_PRICEBOOKING($apartment->pb_code, $cookie);
                if ($dataPrice && count($dataPrice) > 0) {
                    foreach ($dataPrice as $val) {
                        $aryUpdatePrice['pricebookid'] = $val['pricebookid'];
                        $aryUpdatePrice['status'] = 'C';
                        $aryUpdatePriceBooking[] = $aryUpdatePrice;
                    }
                }

                // add price booking id
                $aryInsertPriceBooking[] = [
                    "pricebookid" => "",
                    "pymtterm" => "z",
                    "status" => "W",
                    "property" => $apartment->pb_code,
                    "startdate" => "",
                    "enddate" => "",
                    "notes" => "",
                    "propertyvalue" => $apartment->pb_price_total,
                    "value0" => $row['gia_ban_chua_vat_de_tinh_hoa_hong']? (float) trim($row['gia_ban_chua_vat_de_tinh_hoa_hong']) : "",
                    "value1" => $row['thue_vat'],
                    "value2" => $apartment->price_maintenance,
                    "value3" => $row['gia_tri_quyen_sdd'],
                    "value4" => "",
                    "value5" => $row['don_gia_chua_vat']? (float) trim($row['don_gia_chua_vat']) : "",
                    "value6" => $apartment->pb_price_per_s,
                    "value7" => $apartment->pb_required_money,
                    "value8" => $apartment->price_min,
                    "value9" => $apartment->price_max,
                ];
            }
        }
        LogController::saveLogs($logs);

        // insert, update product
        if ($aryInsertProduct && count($aryInsertProduct) > 0) {
            Log::debug('[Product_Import] Chuẩn bị thêm căn hộ Tavico. TK: '.$cookie, $aryInsertProduct);
            API\APIController::TVC_UPDATE_PRODUCT($aryInsertProduct, 'add', $cookie, false);
        }
        if ($aryUpdateProduct && count($aryUpdateProduct) > 0) {
            Log::debug('[Product_Import] Chuẩn bị cập nhật căn hộ Tavico. TK: '.$cookie, $aryUpdateProduct);
            API\APIController::TVC_UPDATE_PRODUCT($aryUpdateProduct, 'upd', $cookie, false);
        }
        if ($aryUpdatePriceBooking && count($aryUpdatePriceBooking) > 0) {
            Log::debug('[Product_Import] Chuẩn bị cập nhật bảng giá Tavico. TK: '.$cookie, $aryUpdatePriceBooking);
            API\APIController::TVC_UPD_PRICEBOOKING($aryUpdatePriceBooking, $cookie, false);
        }
        if ($aryInsertPriceBooking && count($aryInsertPriceBooking) > 0) {
            Log::debug('[Product_Import] Chuẩn bị thêm bảng giá Tavico. TK: '.$cookie, $aryInsertPriceBooking);
            API\APIController::TVC_ADD_PRICEBOOKING($aryInsertPriceBooking, $cookie, false);
        }


        // END _____________________________________  END

        $res = "Cập nhật thành công $updateCount/".count($results)." căn hộ!";

        /** Log the import action */
        Log::critical('[ImportApartment] '.$res, [
            'by' => $userInfo->{BOUser::ID_KEY}?? null,
            'cookie' => $cookie,
            'file_name'  => $dirFile
        ]);
        return $res;
    }

    // Sync All Product
    
    public function syncAllProduct(){
        $a_DataView = array();
        $a_DataView['a_Projects'] = BOCategory::where('parent_id', 0)->get();
        $OtherCRM = Input::get('otherCRM');
        $config_account = config('cmconst.account_CRM');
        $a_DataView['otherCRM'] = $config_account;        
        
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            self::processTranferProduct();
            return redirect('syncProduct')->with('status', 'Cập nhật thành công!');
        }

        return view('product.syncProduct', $a_DataView );
        
    }
    
    public function processTranferProduct(){
        set_time_limit(10000);
        $project = Input::get('product_code');
        $projectObject = BOCategory::where(['reference_code' => $project])->first();
        $OtherCRM = Input::get('otherCRM');
        $config_account = config('cmconst.account_CRM');
        // Login DXMB
        $user = BOUser::where('ub_account_tvc', $config_account['DXMB']['username'])->first();
        $login_info = [
                "id" => 1,
                "username" => $config_account['DXMB']['username'],
                "password" => $config_account['DXMB']['pwd'],
                "appLog" => "Y",
                "securityKey" => $user->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $config_account['DXMB']['username']);
            if (!$do_login) {
                LogController::logBill('[TVC] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account['DXMB']['username'] . ' thất bại!', 'error', null);
                return false;
            }                    
        $aryAllBuilding = API\APIController::TVC_LIST_BUILDING($project,$config_account['DXMB']['username']);
        $aryAllProduct = API\APIController::TVC_LIST_PRODUCT($project,$config_account['DXMB']['username']);
        $dataPriceDXMB = API\APIController::TVC_LIST_PRICEBOOKING("", $config_account['DXMB']['username'], $project);       
        
        $cookie = null;
        foreach ($OtherCRM as $val) {
            // get user config
            $cookie = $config_account[$val]['username'];
            $usercrm = BOUser::where('ub_account_tvc', $config_account[$val]['username'])->first();            
            $login_info = [
                "id" => 1,
                "username" => $config_account[$val]['username'],
                "password" => $config_account[$val]['pwd'],
                "appLog" => "Y",
                "securityKey" => $usercrm->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $config_account[$val]['username']);
            if (!$do_login) {
                LogController::logBill('[TVC] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account[$val]['username'] . ' thất bại!', 'error', null);
                return false;
            }

            $checkProject = API\APIController::TVC_CHECK_EXIST_PROJECT($project, $cookie);            
            if ($checkProject == 0) {
                $dataProject = [
                    'project' => $projectObject->reference_code,
                    'name' => $projectObject->cb_title
                ];
                API\APIController::TVC_ADD_PROJECT($dataProject, $cookie);
            }
            
            foreach($aryAllBuilding as $key => $valBuilding){
                $checkBuilding = API\APIController::TVC_CHECK_EXIST_BUILDING($valBuilding['block'], $cookie);
                if ($checkBuilding == 0) {
                    $dataBuilding = [
                        'project' => $projectObject->reference_code,
                        'block' => $valBuilding['block'],
                        'name' => $valBuilding['name'],
                    ];
                    API\APIController::TVC_ADD_BUILDING($dataBuilding, $cookie);
                }
            }
            
            $aryInsertProduct = $aryUpdateProduct = $aryInsertPriceBooking =  $aryUpdatePriceBooking = array();
                        
            foreach ($aryAllProduct as $index => $valProduct) {
                if ($valProduct['property'] != '') {
                    // data save TVC
                    
                    $dataTVC = [
                        'property' => $valProduct['property'],
                        'status' => $valProduct['status'],
                        "propertynum" => $valProduct['propertynum'],
                        "block" => $valProduct['block'],
                        "area" => $valProduct['area'],
                        "value1" => $valProduct['value1'],
                    ];
                    // check Product
                    $checkProduct = API\APIController::TVC_CHECK_EXIST_PRODUCT($valProduct['property'], $cookie);
                    if($checkProduct == 0){
                        $aryInsertProduct[] = $dataTVC ;
                    }else{
                        $aryUpdateProduct[] = $dataTVC ;
                    }
                    
                    
                    // array update price
                    if($index == 0){
                        $dataPrice = API\APIController::TVC_LIST_PRICEBOOKING("", $cookie, $project);
                        if ($dataPrice && count($dataPrice) > 0) {
                            foreach ($dataPrice as $valPrice) {
                                $aryUpdatePrice['pricebookid'] = $valPrice['pricebookid'];
                                $aryUpdatePrice['status'] = 'C';
                                $aryUpdatePriceBooking[] = $aryUpdatePrice;
                            }
                        }

                        // add price booking id
                        if (isset($dataPriceDXMB) && count($dataPriceDXMB) > 0) {
                            foreach ($dataPriceDXMB as $valPriceNew) {
                                $aryInsertPriceBooking[] = [
                                    "pricebookid" => "",
                                    "pymtterm" => "z",
                                    "status" => "W",
                                    "property" => $valPriceNew['property'],
                                    "startdate" => $valPriceNew['startdate'],
                                    "enddate" => $valPriceNew['enddate'],
                                    "notes" => "",
                                    "propertyvalue" => $valPriceNew['propertyvalue'],
                                    "value0" => $valPriceNew['value0'],
                                    "value1" => $valPriceNew['value1'],
                                    "value2" => $valPriceNew['value2'],
                                    "value3" => $valPriceNew['value3'],
                                    "value4" => $valPriceNew['value4'],
                                    "value5" => $valPriceNew['value5'],
                                    "value6" => $valPriceNew['value6'],
                                    "value7" => $valPriceNew['value7'],
                                    "value8" => $valPriceNew['value8'],
                                    "value9" => $valPriceNew['value9'],
                                ];
                            }
                        }
                    }
                }
            }
            
            // insert, update product
            if ($aryInsertProduct && count($aryInsertProduct) > 0) {
                API\APIController::TVC_UPDATE_PRODUCT($aryInsertProduct,'add', $cookie);
            }
            if ($aryUpdateProduct && count($aryUpdateProduct) > 0) {
                API\APIController::TVC_UPDATE_PRODUCT($aryUpdateProduct,'upd', $cookie);
            }            
            if ($aryUpdatePriceBooking && count($aryUpdatePriceBooking) > 0) {
                API\APIController::TVC_UPD_PRICEBOOKING($aryUpdatePriceBooking, $cookie);
            }
            if ($aryInsertPriceBooking && count($aryInsertPriceBooking) > 0) {
                API\APIController::TVC_ADD_PRICEBOOKING($aryInsertPriceBooking, $cookie);
            }
                
        }
        
        
    }
    public static function checkLoginTvcWeb1($retry_login = true, $tvcAcc, $typePwd){
        $usercrm = BOUser::where('ub_account_tvc', $tvcAcc)->first();
        $data = [
            "action" => "ConnectDB",
            "method" => "checkSession",
            "data"   => [[]],
            "type"   => "rpc",
            "tid"    => rand(1,99)
        ];
        $response = self::TVC_POST_DECODE($data, '', $tvcAcc);
        if ($response) {
            if ($response['success']) return true;
        }
        if (!$retry_login) return false;
        /** @var $login_info */
        $login_info = [
            "id" => 1,
            "username" => $tvcAcc,
            "password" => $typePwd,
            "appLog" => "Y",
            "securityKey" => $usercrm->security_code
        ];
        $body = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => time(),
        );
        $retry = self::TVC_POST_DECODE($body,'',$tvcAcc);
        $success = $retry&&$retry['success']&&$retry['data'];
        return $success;
    }
    
    public function syncPriceList($project = ""){
        set_time_limit(15000);
        $config_account = config('cmconst.account_CRM');
        
        $cookie_account = $config_account['DXMB']['username'];
        $user = BOUser::where('ub_account_tvc', $cookie_account)->first();
        
        $login_info = [
            "id" => 1,
            "username" => $cookie_account,
            "password" => $config_account['DXMB']['pwd'],
            "appLog" => "Y",
            "securityKey" => $user->security_code
        ];
        $login_body = array(
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $do_login = self::TVC_POST_DECODE($login_body, '', $cookie_account);
        if (!$do_login) {
            LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Không thể đăng nhập tài khoản hệ thống: ' . $cookie_account, 'error', $log_transaction);
            return false;
        }
        $dataPrice = API\APIController::TVC_LIST_PRICEBOOKING('', $cookie_account, $project);
        $countsuccess = 0; 
        foreach($dataPrice as $key => $val){
            $dataUpdate = [
                'pb_required_money' => $val['value7'],
                'pb_price_total' => $val['propertyvalue'],                
                'price_maintenance' => $val['value2'],
                'price_min' => $val['value8'],
                'price_max' => $val['value9'],
            ];
            
            $res = BOProduct::where('pb_code', $val['property'])->update($dataUpdate);
            if($res == 1) $countsuccess += 1 ;
            
        }
        return "update thành công {$countsuccess} căn hộ";

    }
    
    public function crawPrice(){
        set_time_limit(15000);
        $a_DataView = array();
        $a_DataView['a_Projects'] = BOCategory::where('parent_id', 0)->get();
        
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $project = Input::get('product_code');
            
            self::syncPriceList($project);
            return redirect('apartment')->with('status', 'Cập nhật thành công!');
        }

        return view('product.crawPrice', $a_DataView );
    }
    
    public function crawProductFromTvcToBO(){
        set_time_limit(15000);
        // Login
        
        $userInfo = Auth::guard('loginfrontend')->user();
//        self::checkLoginTvcWeb(); // locin tvc
        // get user config
        $cookie = strtoupper($userInfo->ub_account_tvc);
        $user = BOUser::where('ub_account_tvc', $cookie)->first();
        $login_info = [
            "id" => 1,
            "username" => $cookie,
            "password" => self::decrypte(session('encryptPass'), self::ENCRYPTION_KEY),
            "appLog" => "Y",
            "securityKey" => $user->security_code
        ];
        $login_body = array(
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $do_login = self::TVC_POST_DECODE($login_body, '', $cookie);
        if (!$do_login) {
            LogController::logBill('[TVC] Không thể đăng nhập tài khoản hệ thống ' . $cookie . ' thất bại!', 'error', null);
            return false;
        }
        // get project
        $dataProject = array();
        $dataProject = API\APIController::TVC_LIST_PROJECT('', $cookie);
        
        $a_DataView = array();
        $a_DataView['a_Projects'] = $dataProject;
                
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $project = Input::get('product_code');
            return redirect("processcrawProduct?project={$project}&&cookie={$cookie}"); 
        }
        return view('product.crawProduct', $a_DataView );
    }
    
    public function ProcessCrawProduct(){
        set_time_limit(15000);
        $project = Input::get('project');
        $cookie = Input::get('cookie');
        
        self::checkLoginTvcWeb();
        $projectName = $project != "__" ? str_after($project, '__') : '';
        $projectCode = $project != "__" ? str_before($project, '__') : '';
        
        // get All building, product, price.
        $aryBuilding = API\APIController::TVC_LIST_BUILDING($projectCode, $cookie);
        $aryProduct = API\APIController::TVC_LIST_PRODUCT($projectCode, $cookie);
        $aryPrice = API\APIController::TVC_LIST_PRICEBOOKING('', $cookie,$projectCode);
        
        $o_Project = BOCategory::where('cb_code', $projectCode)->first();
        if (!$o_Project) {
            $o_Project = new BOCategory();
            $o_Project->cb_id = time();
            $o_Project->parent_id = 0;
            $o_Project->cb_code = $projectCode;
            $o_Project->reference_code = $projectCode;
            $o_Project->created_user_id = Auth::guard('loginfrontend')->user()->ub_id;
        }
        $o_Project->updated_user_id = Auth::guard('loginfrontend')->user()->ub_id;
        $o_Project->cb_title = $projectName;
        $o_Project->ub_updated_time = date('Y-m-d H:i:s', time());
        $o_Project->save();
        $idProject = $o_Project->id;
        // insert building        
        if(is_array($aryBuilding)&&count($aryBuilding) > 0){
            foreach ($aryBuilding as $key => $val) {
                $o_Building = BOCategory::where('cb_code', $val['block'])->first();
                if (!$o_Building) {
                    $o_Building = new BOCategory();
                    $o_Building->cb_id = time()+ 20 + $key;
                    $o_Building->parent_id = $idProject;
                    $o_Building->cb_level = 2;
                    $o_Building->cb_code = $val['block'];
                    $o_Building->reference_code = $val['block'];
                    $o_Building->created_user_id = Auth::guard('loginfrontend')->user()->ub_id;
                }
                $o_Building->updated_user_id = Auth::guard('loginfrontend')->user()->ub_id;
                $o_Building->cb_title = $val['name'];
                $o_Building->ub_updated_time = date('Y-m-d H:i:s', time());
                $o_Building->save();
            }
        } else {
            die('Not array!');
        }
        
        $o_buildingData = DB::table('b_o_categories')->where('parent_id', $idProject)->get();
        if(count($o_buildingData) > 0){
            foreach ($o_buildingData as $val){
                if(strlen($val->cb_code) == 6){
                    $a_Building[$val->cb_code] = $val->cb_id;
                }
            }
        }
        // Product
        if(is_array($aryProduct)&&count($aryProduct) > 0){
            foreach ($aryProduct as $keyProduct => $valProduct) {
                if(strlen($valProduct['property']) == 12){
                    $o_Product = BOProduct::where('pb_code', $valProduct['property'])->first();
                    if (!$o_Product) {
                        $o_Product = new BOProduct();
                        $o_Product->pb_id = BOProduct::orderBy('id','desc')->first()->pb_id + 1;
                        $o_Product->pb_code = $valProduct['property'];
                        $o_Product->category_id = isset($a_Building[$valProduct['block']]) ? $a_Building[$valProduct['block']] : null;
                        $o_Product->pb_used_s = $valProduct['value1'];
                        $o_Product->pb_built_up_s = $valProduct['area'];
                        $o_Product->ub_updated_time = date('Y-m-d H:i:s', time());
                        $o_Product->updated_user_id = Auth::guard('loginfrontend')->user()->ub_id;
                        
                    }
                    $o_Product->status_id = isset(BOProduct::STATUSES[$valProduct['status']]) ? BOProduct::STATUSES[$valProduct['status']] : -2 ;
                    $o_Product->save();
                }
            }
        }
        $countsuccess = 0;
        $str = "";
        if(is_array($aryPrice)&&count($aryPrice) > 0 ){
            foreach($aryPrice as $keyPrice => $valPrice){
                $dataUpdate = [
                    'pb_required_money' => $valPrice['value7'],
                    'pb_price_total' => $valPrice['propertyvalue'],                
                    'price_maintenance' => $valPrice['value2'],
                    'price_min' => $valPrice['value8'],
                    'price_max' => $valPrice['value9'],
                    'ub_updated_time' => date('Y-m-d H:i:s', time()),
                    'updated_user_id' => Auth::guard('loginfrontend')->user()->ub_id
                ];
                $res = DB::table('b_o_products')->where('pb_code', $valPrice['property'])->update($dataUpdate);
                if($res == 1) $countsuccess += 1 ;
            }
        }
        $str .= "update thành công {$countsuccess} căn hộ";
        echo $str;
    }

    public function updatecategoryId(){
        
    $arrayProduct = BOProduct::where('category_id', null)->get();
    $sc = 0;
    foreach ($arrayProduct as $key => $val){
        
        if(strlen($val['pb_code']) == 12){            
            $buildingObject = BOCategory::where('cb_code', substr($val['pb_code'], 0, 6))->first();
            if($buildingObject){
                BOProduct::where('pb_code', $val['pb_code'])->update(['category_id'=> $buildingObject->cb_id]);
                $sc +=1;
            }
        }
    }
        echo "cap nhat duoc {$sc} can ho";
    

    }

    public function exportApartment(Request $request){
        $request = $request->input();
        if(!isset($request['project']) || !isset($request['building'])){
            return redirect('apartment')->with('status', 'Kiểm tra lại dữ liệu!');
        }
        $a_Apartment = BOProduct::where('category_id',$request['building'])->get();
        if(!$a_Apartment){
            return redirect('apartment')->with('status', 'Không có dữ liệu Export!');
        }
        try{
            Excel::create('export_bang_hang', function($excel) use($a_Apartment) {
                // Set the title
                $excel->setDescription('export_bang_hang');
                $excel->sheet('sheet1', function($sheet) use($a_Apartment) {
                    $a_dataExport = array();
                    foreach ($a_Apartment as $key => $o_apartment) {
                        $a_apartment1['BĐS'] = $o_apartment->pb_code;
                        $a_apartment1['Số BĐS thực tế'] = $o_apartment->pb_code_real;
                        $a_apartment1['PTTT'] = '';
                        $a_apartment1['Ngày bắt đầu'] = '';
                        $a_apartment1['Ngày kết thúc'] = '';
                        $a_apartment1['Tình trạng căn hộ'] = array_search($o_apartment->status_id,BOProduct::STATUSES);
                        $a_apartment1['Trạng thái'] = $o_apartment->pb_status;
                        $a_apartment1['Tầng'] = '';
                        $a_apartment1['Phòng ngủ'] = $o_apartment->bedroom;
                        $a_apartment1['Hướng'] = $o_apartment->pb_door_direction;
                        $a_apartment1['View'] = $o_apartment->view;
                        $a_apartment1['Góc'] = $o_apartment->corner;
                        $a_apartment1['DT thông thủy'] = (string) $o_apartment->pb_used_s;
                        $a_apartment1['DT tim tường'] = (string) $o_apartment->pb_built_up_s;
                        $a_apartment1['Giá thông thủy'] = $o_apartment->price_used_s;
                        $a_apartment1['Giá tim tường'] = $o_apartment->pb_price_per_s;
                        $a_apartment1['Tổng Giá bán (bao gồm VAT ko phí bảo trì)'] = $o_apartment->pb_price_total;
                        $a_apartment1['Giá bán chưa VAT (để tính hoa hồng)'] = '';
                        $a_apartment1['Giá tính thuế VAT'] = '';
                        $a_apartment1['Thuế VAT'] = '';
                        $a_apartment1['Giá trị quyền SDĐ'] = '';
                        $a_apartment1['Phí bảo trì'] = $o_apartment->price_maintenance;
                        $a_apartment1['Đơn giá chưa VAT'] = '';
                        $a_apartment1['Đơn giá có VAT'] = '';
                        $a_apartment1['Giá sàn (MIN)'] = $o_apartment->price_min;
                        $a_apartment1['Giá trần (MAX)'] = $o_apartment->price_max;
                        $a_apartment1['Giai đoạn'] = $o_apartment->stage;
                        $a_apartment1['Loại đặt chỗ'] = $o_apartment->book_type;
                        $a_apartment1['Số tiền yêu cầu'] = $o_apartment->pb_required_money;
                        $a_apartment1['Số người lock'] = $o_apartment->number_lock;
                        $a_apartment1['Ghi chú'] = $o_apartment->note;
                        $a_dataExport[] = $a_apartment1;
                        $row = $key + 2;
                        $sheet->getStyle('A'.$row.':AE'.$row)->getNumberFormat()->setFormatCode('#,##0');
                    }
                    if(isset($a_dataExport)){
                        $sheet->fromArray($a_dataExport);
                    }
                    $sheet->cells('A1:AE1', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setFont(array(
                            'bold' => true
                        ));
                    });
                });
            })->download('xlsx');
        }catch (\Exception $e){
            echo $e->getMessage();
        }
    }
}
