<?php

namespace App\Http\Controllers;
use App\BOCategory;
use App\BOInvestor;
use App\Http\Controllers\API\APIController;
use Illuminate\Http\Request;
use DB;
use App\Util;
use Illuminate\Support\Facades\Auth;
class CategoryController extends Controller
{
    /**
     * BOBillController constructor.
     */
    private $productController;
    public function __construct()
    {
        $this->productController = new ProductController();
    }

    /**
     * @param Request $request
     * @param $uid
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request) {
        $investor = BOInvestor::all();
        $arrInvestor = array();
        if(count($investor)){
            foreach ($investor as $investor){
                $arrInvestor[$investor->id] = array(
                    'code' => $investor->code,
                    'name' => $investor->name,
                );
            }
        }
        $a_DataView['arrInvestor'] = $arrInvestor;

        $projects = BOCategory::where('cb_level' , 1)->get();
        $arrProject = array();
        if(count($projects)){
            foreach ($projects as $project){
                $arrProject[$project->id] = array(
                    'title' => $project->cb_title,
                    'code' => $project->cb_code
                );
            }
        }
        $a_DataView['arrProject'] = $arrProject;

        $a_search = $request->input();
        $where = array();
        if(isset($a_search['search_project']) && $a_search['search_project'] != ''){
            $where = array('id' => $a_search['search_project']);
            $a_Building = BOCategory::where(['cb_level' => 2, 'parent_id' => $a_search['search_project']])->get();
            $a_DataView['a_Building'] = $a_Building;
        }
        if(isset($a_search['search_building']) && $a_search['search_building'] != ''){
            $where = array('cb_id' => $a_search['search_building']);
        }
        $cats = BOCategory::where($where)->orderBy('ub_updated_time','desc')->paginate(30);
        $a_DataView['a_search'] = $a_search;
        $a_DataView['cats'] = $cats;
        return view('category.index',$a_DataView);
    }

    public function addEdit(Request $request) {
        $investors = BOInvestor::all();
        $a_DataView['investors'] = $investors;

        $projects = BOCategory::where('cb_level' ,1)->get();
        $a_DataView['projects'] = $projects;

        $catId = $request->input('id', 0);
        $submit = $request->input('submit');
        $a_DataView['catId'] = $catId;
        if($catId != 0){
            $cat = BOCategory::find($catId);
            if($cat) $a_DataView['cat'] = $cat;
        }else{
            $cat = new BOCategory();
            $cat->cb_id = time();
        }
        if (isset($submit) && $submit != "") {
            $arrData = $request->input();
            $cat->parent_id = isset($arrData['project']) ?  $arrData['project'] : 0;
            $cat->cb_code = $arrData['reference_code'];
            $cat->reference_code = $arrData['reference_code'];
            $cat->cb_title = $arrData['cb_title'];

            $cat->investor_id = isset($arrData['investor']) ?  $arrData['investor'] : null;
            if(isset($arrData['apartmentGrid'])){
                $cat->apartment_grid = $arrData['apartmentGrid'] == 1 ? true : false;
            }
            if(isset($arrData['type'])){
                $cat->type = $arrData['type'];
            }
            $cat->active_booking = isset($arrData['active_booking']) ? (int) $arrData['active_booking'] : Null;
            $cat->cb_level = $arrData['level'];
            $cat->created_user_id = $catId == 0 ? Auth::guard('loginfrontend')->user()->ub_id : $cat->created_user_id;
            $cat->updated_user_id = Auth::guard('loginfrontend')->user()->ub_id;
            $cat->ub_updated_time = now();
            $cat->ub_created_time = $catId == 0 ? now() : $cat->ub_created_time;
            $cat->enable_list_price = isset($arrData['enableListPrice']) ? strtotime($arrData['enableListPrice']) : Null;
            $cat->send_mail = isset($arrData['send_mail']) ? 1 : ($arrData['level'] == 2 ? 0 : Null);

            if($cat->save()){
                /** Login Tavico */
                $login_info = [
                    'id' => 1,
                    'username' => 'DT_APITEST7@DXMBT',
                    'password' => 'user',
                    'appLog' => 'Y',
                    'securityKey' => 'EAB5-93AF-DF7E-CB91-8D45-4A40-45AB-C376-E6AA-DA7E',
                ];
                $body = array (
                    'action' => 'ConnectDB',
                    'method' => 'getConfig',
                    'data' => [$login_info],
                    'type' => 'rpc',
                    'tid' => time(),
                );
                $tvc_response = APIController::TVC_POST_DECODE($body, '', 'DT_APITEST7@DXMBT');
                if ($tvc_response && $tvc_response['success']) {
                    if($cat->cb_level == 1){
                        $investor = BOInvestor::find($cat->investor_id);
                        /** Validate Project Before Add */
                        $dataAddProjectValidate = [
                            'PPJ',
                            'C',
                            [
                                [
                                    'project' => $cat->reference_code,
                                    'name' => $cat->cb_title,
                                    'status' => 'W',
                                    'address' => '',
                                    'notes' => '',
                                    'anal_ppj4' => $investor ? $investor->code : '',
                                    'anal_ppj6' => '',
                                    'anal_ppj7' => '',
                                    'anal_ppj8' => '',
                                    'details' =>
                                        [
                                            [
                                                'name' => $investor ? $investor->name : '',
                                                'investor' => $investor ? $investor->code : '',
                                                'project' => $cat->reference_code,
                                            ],
                                        ],
                                ],
                            ],
                        ];
                        $body = array (
                            'action' => 'FrmFdProcess',
                            'method' => 'getValidation',
                            'data' => $dataAddProjectValidate,
                            'type' => 'rpc',
                            'tid' => 65,
                        );
                        $tvc_response = APIController::TVC_POST_DECODE($body, '', 'DT_APITEST7@DXMBT');
                        if ($tvc_response && $tvc_response['success']) {
                            /** Add-Update Project */
                            $dataAddProject = [
                                [
                                    [
                                        'project' => $cat->reference_code,
                                        'name' => $cat->cb_title,
                                        'status' => 'W',
                                        'address' => '',
                                        'notes' => '',
                                        'anal_ppj4' => $investor ? $investor->code : '',
                                        'anal_ppj6' => '',
                                        'anal_ppj7' => '',
                                        'anal_ppj8' => '',
                                        'details' =>
                                            [
                                                [
                                                    'name' => $investor ? $investor->name : '',
                                                    'investor' => $investor ? $investor->code : '',
                                                    'project' => $cat->reference_code,
                                                ],
                                            ],
                                    ],
                                ],
                            ];
                            $method = $catId == 0 ? 'add' : 'upd';
                            $body = array (
                                'action' => 'FrmPmProject',
                                'method' => $method,
                                'data' => $dataAddProject,
                                'type' => 'rpc',
                                'tid' => 69,
                            );

                            $tvc_response = APIController::TVC_POST_DECODE($body, '', 'DT_APITEST7@DXMBT');
                        }
                    }else{
                        $project = BOCategory::find($cat->parent_id);
                        /** Validate Building Before Add */
                        $dataAddBuildingValidate = [
                            'BLK',
                            'C',
                            [
                                [
                                    'project' => $project->reference_code,
                                    'block' => $cat->reference_code,
                                    'name' => $cat->cb_title,
                                    'status' => 'W',
                                    'address' => '',
                                    'anal_blk4' => '',
                                    'anal_blk5' => '',
                                    'anal_blk6' => '',
                                    'anal_blk7' => '',
                                    'anal_blk8' => ''
                                ],
                            ],
                        ];
                        $body = array (
                            'action' => 'FrmFdProcess',
                            'method' => 'getValidation',
                            'data' => $dataAddBuildingValidate,
                            'type' => 'rpc',
                            'tid' => 65,
                        );
                        $tvc_response = APIController::TVC_POST_DECODE($body, '', 'DT_APITEST7@DXMBT');
                        if ($tvc_response && $tvc_response['success']) {
                            /** Add - Update Building */
                            $dataAddBuilding = [
                                [
                                    [
                                        'project' => $project->reference_code,
                                        'block' => $cat->reference_code,
                                        'name' => $cat->cb_title,
                                        'status' => 'W',
                                        'address' => '',
                                        'anal_blk4' => '',
                                        'anal_blk5' => '',
                                        'anal_blk6' => '',
                                        'anal_blk7' => '',
                                        'anal_blk8' => ''
                                    ],
                                ],
                            ];
                            $method = $catId == 0 ? 'add' : 'upd';
                            $body = array (
                                'action' => 'FrmPmBlock',
                                'method' => $method,
                                'data' => $dataAddBuilding,
                                'type' => 'rpc',
                                'tid' => 69,
                            );
                            $tvc_response = APIController::TVC_POST_DECODE($body, '', 'DT_APITEST7@DXMBT');
                        }
                    }
                }
                if($cat->cb_level == 1){
                    /** TODO: ADD MONGO */
                    $investor = BOInvestor::find($cat->investor_id);
                    APIController::DXMB_POST('/api/app/addProjectFromBo', null, [
                        "investor" => $investor ? $investor->name : '',
                        "tvcCode" => $cat->cb_code,
                        "name" => $cat->cb_title,
                        "action" => $catId == 0 ? 'add' : 'upd'
                    ]);
                }
                return redirect('category')->with('status', 'Cập nhật thành công!');
            }
        }
        $a_DataView['cat'] = $cat;
        return view('category.addedit',$a_DataView);
    }


    /**
     * @auth: Dienct
     * @since: 21/09/2018
     * @des: sync all project
     */
    public function syncAPIProjectTVC(){
        //login
        $login_info =  [
            "id"=> 1,
            "username"=> "HUYNN@DXMB",
            "password"=> "123456",
            "appLog"=> "Y",
            "securityKey"=> "DD19-C2DA-B6E6-DB45-8DB3-E0EF-F1B7-9DEC-488B-913C"
          ];
        $bodyLogin = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $dataLogin = API\APIController::TVC_POST($bodyLogin);
        
        // get last sync time
        $firstCat = DB::table('b_o_categories')->where('cb_level', 1)->first();
        
        $dateF = isset($firstCat->last_sync_tvc) && $firstCat->last_sync_tvc != '' ? Util::sz_DateTimeGetAPI($firstCat->last_sync_tvc) : '';
        $dateT = date("Y-m-d H:i", time());
        
        // get Data Project TVC
        $dataGetProject = [
            "reportcode" => "apiPPJ01",            
            "queryFilters" => array(
                array("name" => "dtb", "value" => "DXMB"),
                array("name" => "anal_ppj4", "value" => ""),
                array("name" => "datef", "value" => ""),
                array("name" => "datet", "value" => ""),
                array("name" => "project", "value" => "")
            )
        ];
        $bodyGetProject = array (
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$dataGetProject],
            'type' => 'rpc',
            'tid' => 1,
        );
        $DataReturn =  API\APIController::TVC_POST($bodyGetProject);
        
        $a_Project = json_decode($DataReturn->original['data'])->result->data;
                        
                        
        // update Project
        if(isset($a_Project) && count($a_Project)>0){
            $k = time();
            $successUpdate = 0;
            $successInsert = 0;
            foreach($a_Project as $valProject){
                $dataSave = array();
                $dataSave['cb_title'] = $valProject->name;
                $dataSave['cb_code'] = $valProject->project;
                $dataSave['reference_code'] = $valProject->project;
                $dataSave['cb_level'] = 1;
                $dataSave['ub_updated_time'] = date("Y-m-d H:i:s", time());
                
                
                // check update or insert
                $checkCat = DB::table('b_o_categories')->where('reference_code', $valProject->project)->first();
                
                if(isset($checkCat)){
                    // update
                    DB::table('b_o_categories')->where('reference_code', $valProject->project)->update($dataSave);
                    $successUpdate += 1;
                }else{
                    $dataSave['ub_created_time'] = date("Y-m-d H:i:s", time());
                    $dataSave['cb_id'] = (int) $k += 1;
                    //insert
                    DB::table('b_o_categories')->insert($dataSave);
                    $successInsert +=1;
                }
                unset($dataSave);
            }
            // Update last sync TVC time
            DB::table('b_o_categories')->where('cb_level', 1)->update(['last_sync_tvc' => date("Y-m-d H:i", time())]);
            echo "Insert thành công {$successInsert} bản ghi và update thành công {$successUpdate} bản ghi";
        }
//        $this->syncAPIBuildingTVC();
    }
    
    /**
     * @auth: Dienct
     * @since: 21/09/2018
     * @des: sync all building
     */
    public function syncAPIBuildingTVC(){
        set_time_limit(5500);
        $login_info =  [
            "id"=> 1,
            "username"=> "HUYNN@DXMB",
            "password"=> "123456",
            "appLog"=> "Y",
            "securityKey"=> "DD19-C2DA-B6E6-DB45-8DB3-E0EF-F1B7-9DEC-488B-913C"
          ];
        $bodyLogin = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $dataLogin = API\APIController::TVC_POST($bodyLogin);
        // get last sync time
        $firstCat = DB::table('b_o_categories')->where('cb_level', 2)->first();
        
        
        // get Data Project TVC
        $dataGetBuilding = [
            "reportcode" => "apiBLK01",             
            "queryFilters" => array(
                array("name" => "project", "value" => ""),
                array("name" => "dtb", "value" => "DXMB"),
                array("name" => "block", "value" => ""),
                array("name" => "datef", "value" => ""),
                array("name" => "datet", "value" => "")
            )
        ];
        $bodyGetBuilding = array (
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$dataGetBuilding],
            'type' => 'rpc',
            'tid' => 1,
        );
        $DataReturn =  API\APIController::TVC_POST($bodyGetBuilding);
       
        $a_Buildings = json_decode($DataReturn->original['data'])->result->data;
                
        // update Project
        if(isset($a_Buildings) && count($a_Buildings)>0){
            $k = time() + 300;
            $successUpdate = 0;
            $successInsert = 0;
            // get All Project
            $o_Project = DB::table('b_o_categories')->where('cb_status', 1)->get();
            foreach ($o_Project as $val){
                if(strlen($val->cb_code) == 3){
                    $a_Project[$val->cb_code] = $val->id;
                }
            }
            
            foreach($a_Buildings as $a_Building){
                if(strlen($a_Building->block) == 6){
                    $dataSave = array();
                    $keyProject = substr($a_Building->block, 0, -3);
                    $dataSave['parent_id'] = isset($a_Project[$keyProject]) ? $a_Project[$keyProject] : null;
                    $dataSave['cb_code'] = $a_Building->block;
                    $dataSave['reference_code'] = $a_Building->block;
                    $dataSave['cb_title'] = $a_Building->name;
                    $dataSave['cb_level'] = 2;
                    $dataSave['ub_updated_time'] = date('Y-m-d H:i:s', time());
                    
                    // check update or insert
                    $checkCat = DB::table('b_o_categories')->where('reference_code', $a_Building->block)->first();

                    if(isset($checkCat)){
                        // update
                        DB::table('b_o_categories')->where('reference_code', $a_Building->block)->update($dataSave);
                        $successUpdate += 1;
                    }else{
                        $dataSave['ub_created_time'] = date("Y-m-d H:i:s", time());
                        $dataSave['cb_id'] = (int) $k += 1;
                        //insert
                        DB::table('b_o_categories')->insert($dataSave);
                        $successInsert +=1;
                    }
                }
                unset($dataSave);
            }
            DB::table('b_o_categories')->where('cb_level', 2)->update(['last_sync_tvc' => date("Y-m-d H:i", time())]);
            echo "Insert thành công {$successInsert} bản ghi và update thành công {$successUpdate} bản ghi";
            // Update last sync TVC time
            
        }
//        $this->productController->syncAPIProductTVC();
    }
    
    


}
