<?php
namespace App\Http\Controllers\API;

use App\BOBill;
use App\BOPayment;
use App\BOProduct;
use App\BOUser;
use App\BOTransaction;
use App\BOUserGroup;
use App\Http\Controllers\MailController;
use App\Http\Controllers\NotificationController;
use App\PaymentMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\UserCustomerMapping;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class BOBillController extends Controller
{

    /**
     * BOBillController constructor.
     * @param Request $request
     */


    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5", null);
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt");
            $this->middleware("CheckStoredJWT");
        }
        $this->middleware('CheckTVCLogin')->only('changeStatus');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        $options = $request->all();
        $page = isset($options['page'])? (int) $options['page'] : 1;
        $page_size = isset($options['size'])? (int) $options['size'] : env("PAGE_SIZE", 15);
        $filter_status = $request->input('status', null);
        $keyword = $request->input('keyword', null);
        $offset = ($page-1)*$page_size;

        /** @var $data */
        $data = BOBill::select("*")->where("bill_status", "!=", env("STATUS_DELETE", -1));
        $role = 'sale';
        $show_filter = true;
        // **todo: Check Role to filter list
        if (AuthController::is_cs_staff()) {
            $view_statuses = [
                BOBill::STATUSES['STATUS_PAID'], BOBill::STATUSES['STATUS_PAYING'],
                BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT'], BOBill::STATUSES['STATUS_BOOKED'],
                BOBill::STATUSES['STATUS_DEPOSITED'], BOBill::STATUSES['STATUS_SUCCESS'],
                BOBill::STATUSES['STATUS_FINALIZED']
            ];
//            $data = $data->whereIn('status_id', $view_statuses);
            $role = 'CS Staff';
            /** @var $projects_involved */
            $projects_involved = BOProductController::getProjectsByUserRole(self::ROLE_GROUPS['CUSTOMER_SERVICE_STAFF']);
            /** @var $apartments */
            $apartments = BOProductController::getApartmentsByProject($projects_involved);
            $data = $data->whereIn('product_id', $apartments);
        } elseif (AuthController::is_accountant()) {
            $data = $data->where('status_id', '>=', BOBill::STATUSES['STATUS_PAYING']);
            $role = 'Accountant';
            $show_filter = false;
        } elseif (AuthController::is_manager()) {
            /** Manager filter */
            $group = BOUser::getCurrent('group_ids');
            $group_staff = BOUserController::getStaffByGroups($group);
            $data = $data->whereIn('staff_id', $group_staff);
            $role = 'Manager';
        } elseif (AuthController::is_product_manager()) {
            /** @var $projects_involved */
            $projects_involved = BOProductController::getProjectsByUserRole(self::ROLE_GROUPS['PRODUCT_MANAGER']);
            /** @var $apartments */
            $apartments = BOProductController::getApartmentsByProject($projects_involved);
            /** @var $staff */
            $staff = BOUserController::getStaffByGroups(BOUser::getCurrent('group_ids'));
            /** @var $data - filtered by managed apartments */
            $data = $data->where(function ($query) use ($apartments, $staff) {
                $query->whereIn('product_id', $apartments)->orWhereIn('staff_id', $staff);
            });
            $role = 'Product Manager';
        } else {
            /** @var $data - filtered by Current Sale Staff */
            $data = $data->where('staff_id', AuthController::getCurrentUID());
        }
        /** Filter status query */
        if ($filter_status!==null) $data = $data->where('status_id', $filter_status);
        if ($keyword) {
            $filter_products = BOProduct::where("pb_code", 'LIKE', "%$keyword%")->pluck(BOProduct::ID_KEY)->toArray();
            $data = $data->where(function ($query) use ($keyword, $filter_products) {
                $query->where('bill_code', 'LIKE', "%$keyword%")->orWhereIn('product_id', $filter_products);
            });
        }
        $data = $data
            ->has('product')
            ->with([
                'staff' => function($staff) {
                    $staff->select([
                        'ub_id',
                        'ub_title AS name',
                        'ub_account_name AS account',
                        'ub_email AS email',
                        'ub_avatar AS avatar'
                    ]);
                },
                'customers' => function($customers) {
                    $customers->select(['id_user', 'id_customer', 'c_title AS name', 'c_phone AS phone', 'email','id_passport'])
                                ->where('id_user', '>', 0);
                },
                'product' => function($apartment) {
                    return $apartment->select([
                        "pb_id",
                        "pb_title AS apartment", "pb_code AS code",
                        "category_id",
                        "pb_required_money AS money"
                    ])
                        ->with([ "category" => function($building) {
                            return $building->select([
                                "id", "cb_id", "parent_id", "cb_title AS building"
                            ])
                            ->where("cb_level", 2)
                            ->with(["sibling" => function($project) {
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])
                                    ->where("cb_level", 1);
                            }]);
                    }]);
                }
            ])
            ->orderBy('bill_updated_time', 'DESC')
            ->skip($offset)->take($page_size)
            ->get();
        return self::jsonSuccess($data, "Thành công!", [
            'statuses' => BOBill::STATUS_TEXTS,
            'status_colors' => BOBill::STATUS_COLORS,
            'role' => $role,
            'show_filter' => $show_filter
            ]
        );
    }

    /**
     * @param int $status_id
     * @param int $type
     * @param int $product_id
     * @param null|int $product_stage
     * @return array
     */
    private static function _getActionButtonsByRole($status_id, $type, $product_id, $product_stage = null) {
        /** @var $product_managers */
        $product_managers = BOProductController::getProductManagersByApartment($product_id);
        /** @var $is_responsible_pm */
        $is_responsible_pm = AuthController::is_product_manager()&&$product_managers&&in_array(AuthController::getCurrentUID(), $product_managers);
        switch ($status_id) {
            case BOBill::STATUSES['STATUS_CANCEL_REQUEST']:
                /** NVKD đã gửi yc huỷ hđ */
                if ($type == BOBill::TYPE_DEPOSIT) {
                    if ($is_responsible_pm) {
                        return [
                            APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_APPROVED'], 'Xác nhận huỷ cọc', false)
                        ];
                    }
                } else {
                    if ($is_responsible_pm) {
                        return [
                            APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Xác nhận huỷ chỗ', false)
                        ];
                    }
                }
                break;
            case BOBill::STATUSES['STATUS_CANCEL_APPROVED']:
                if (AuthController::is_cs_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Xác nhận huỷ cọc', false)
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_REQUEST']:
                /** NVKD đã gửi YC tạo BILL */
                if (AuthController::is_manager()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER'], 'Từ chối HĐ', false),
                    ];
                } elseif ($is_responsible_pm) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_APPROVED'], 'Đồng ý Y/C', true, 'confirm'),
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_DISAPPROVED'], 'Từ chối HĐ', false),
                    ];
                } elseif(AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Huỷ hợp đồng', false)
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_APPROVED']:
                /** TKKD đã duyệt */
                if (AuthController::is_manager()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER'], 'Xác nhận HĐ', true),
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER'], 'Từ chối HĐ', false),
                    ];
                } elseif (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Huỷ HĐ', false),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']:
                /** Giám đốc sàn đã duyệt */
                if (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Huỷ HĐ', false),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER']:
                /** Giám đốc sàn từ chối */
                if (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Xác nhận huỷ', false),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_PAID_ONCE']:
                /**  NVKD đã thanh toán 1 lần */
                //** todo: Customer confirmed && TVC contract not generated */
                if ($is_responsible_pm) {
                    return [
                        APIController::generateAppButtons(
                            BOBill::STATUSES['STATUS_PAYING'],
                            'Tạo giao dịch '. BOBill::TYPE_TEXTS[$type] .' Tavico',
                            true
                        )
                    ];
                } elseif (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_REQUEST'], 'YC huỷ HĐ', false),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_PAYING']:
                /** TKKD đã chuyển HĐ sang TT đang thanh toán */
                if (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_REQUEST'], 'YC huỷ HĐ', false),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_PAID']:
                /** HĐ đã thanh toán đủ */
                /** todo: Customer service staff Confirm */
                if (AuthController::is_cs_staff()) {
                    $new_status = $type==BOBill::TYPE_DEPOSIT? BOBill::STATUSES['STATUS_DEPOSITED'] : BOBill::STATUSES['STATUS_BOOKED'];
                    return [
                        APIController::generateAppButtons($new_status, 'Xác nhận '.BOBill::TYPE_TEXTS[$type], true)
                    ];
                } elseif (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_REQUEST'], 'YC huỷ HĐ', false),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER']:
                /** todo: sale staff handles */
                if (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Hủy HĐ', false),
                        APIController::generateAppButtons('RENEW_TRANSACTION', 'Tạo giao dịch mới', true)
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_BOOKED']:
                /** Đã đặt chỗ */
                if (AuthController::is_sale_staff()) {
                    $buttons = [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_REQUEST'], 'Y/C hủy chỗ', false),
                    ];
                    if ($product_stage == BOProduct::STAGE_DEPOSIT) {
                        $buttons[] = APIController::generateAppButtons(BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST'], 'Y/C Chuyển cọc', true);
                    }
                    return $buttons;
                }
                break;
            case BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']:
                /** Đã gửi yc chuyển cọc */
                if ($is_responsible_pm) {
                    if ($type==BOBill::TYPE_BOOK_TO_DEPOSIT) {
                       return [
                            APIController::generateAppButtons(BOBill::STATUSES['STATUS_DEPOSITED'], 'Xác nhận Chuyển cọc', true)
                        ];
                    } elseif ($type==BOBill::TYPE_BOOK_ONLY) {
                        return [
                            APIController::generateAppButtons(BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT'], 'Xác nhận Chuyển cọc', true),
                        ];
                    }
                }
                break;
            case BOBill::STATUSES['STATUS_DEPOSITED']:
                /** Đã cọc */
                if (AuthController::is_sale_staff()) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_REQUEST'], 'Y/C thanh lý', false),
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_FINALIZED'], 'Y/C giao dịch thành công', true, 'finalize'),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_FINALIZED']:
                /** Đã gửi yc xác nhận hoàn thành */
                if ($is_responsible_pm) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_SUCCESS'], 'Xác nhận hoàn thành', true)
                    ];
                }
                break;
            default:
                return [];
        }
        return [];
    }

    /**
     * @param int $status_id
     * @param int $type
     * @param int $product_id
     * @param null|int $product_stage
     * @return array
     */
    public static function _getActionButtonsByRoleWeb($status_id, $type, $product_id, $product_stage = null) {
        switch ($status_id) {
            case BOBill::STATUSES['STATUS_CANCEL_REQUEST']:
                /** NVKD đã gửi yc huỷ hđ */
                if ($type == BOBill::TYPE_DEPOSIT) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCEL_APPROVED'], 'Xác nhận huỷ cọc', false)
                    ];
                } else {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_CANCELED'], 'Xác nhận huỷ chỗ', false)
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_REQUEST']:
                /** NVKD đã gửi YC tạo BILL */
                return [
                    APIController::generateAppButtons(BOBill::STATUSES['STATUS_APPROVED'], 'Đồng ý Y/C', true),
                    APIController::generateAppButtons(BOBill::STATUSES['STATUS_DISAPPROVED'], 'Từ chối HĐ', false),
                ];
                break;
            case BOBill::STATUSES['STATUS_PAID_ONCE']:
                /**  NVKD đã thanh toán 1 lần */
                //** todo: Customer confirmed && TVC contract not generated */
                return [
                    APIController::generateAppButtons(
                        BOBill::STATUSES['STATUS_PAYING'],
                        'Tạo giao dịch '. BOBill::TYPE_TEXTS[$type] .' Tavico',
                        true
                    )
                ];
                break;
            case BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']:
                /** Đã gửi yc chuyển cọc */
                if ($type==BOBill::TYPE_BOOK_TO_DEPOSIT) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_DEPOSITED'], 'Xác nhận Chuyển cọc', true)
                    ];
                } elseif ($type==BOBill::TYPE_BOOK_ONLY) {
                    return [
                        APIController::generateAppButtons(BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT'], 'Xác nhận Chuyển cọc', true),
                    ];
                }
                break;
            case BOBill::STATUSES['STATUS_FINALIZED']:
                /** Đã gửi yc xác nhận hoàn thành */
                return [
                    APIController::generateAppButtons(BOBill::STATUSES['STATUS_SUCCESS'], 'Xác nhận hoàn thành', true)
                ];
                break;
            default:
                return [];
        }
        return [];
    }

    /**
     * @param int $customer_id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listByCustomer($customer_id = 0) {
        if ($customer_id > 0) {
            $data = BOBill::where([
                'staff_id'  => AuthController::getCurrentUID(),
                'customer_id'   => $customer_id,
                'bill_status'   => env('STATUS_ACTIVE', 1)
            ])
                ->with([
                    'product' => function($apartment) {
                        return $apartment->select([
                            "pb_id",
                            "pb_title AS apartment", "pb_code AS code",
                            "category_id",
                            "pb_required_money AS money"
                        ])
                            ->with([ "category" => function($building) {
                                return $building->select([
                                    "id", "cb_id", "parent_id", "cb_title AS building"
                                ])
                                    ->where("cb_level", 2)
                                    ->with(["sibling" => function($project) {
                                        return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])
                                            ->where("cb_level", 1);
                                    }]);
                            }]);
                    }
                ])
                ->get();
            if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
            return self::jsonSuccess($data, 'Success', $customer_id);
        } else {
            return self::jsonError('Yêu cầu không hợp lệ!');
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID hóa đơn");
        }

        /** @var $data */
        $data = BOBill::select([
            '*',
            'bill_required_money AS required_money',
            'bill_total_money AS total_money',
        ])->where([BOBill::ID_KEY => (int) $id])
        ->with([
            'staff' => function($staff) {
                $staff->select([
                    'ub_id',
                    'ub_title AS name',
                    'ub_account_name AS account',
                    'ub_email AS email',
                    'ub_avatar AS avatar',
                    'group_ids'
                ])->with(["group" => function($group) {
                    return $group->select([BOUserGroup::ID_KEY, 'gb_title AS name']);
                }]);
            },
            'product' => function($apartment) {
                return $apartment->select([
                    "pb_id",
                    "pb_title AS apartment", "pb_code AS code",
                    "category_id", "stage",
                    "pb_required_money AS required_money"
                ])
                    ->with([ "category" => function($building) {
                        return $building->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])
                            ->where("cb_level", 2)
                            ->with(["sibling" => function($project) {
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])
                                    ->where("cb_level", 1);
                            }]);
                    }]);
            },
            'transaction:trans_id,trans_deposit',
            'payments' => function($payments) {
                $payments->select(['bill_code', 'pb_response_money'])->where('pb_status', BOPayment::STATUS['APPROVED']);
            }
        ])->first();
        if (!$data) return self::jsonError('Không tìm thấy hóa đơn', ['item' => $id]);

        /** @var $paid */
        $paid = 0;
        foreach ($data->payments as $payment) {
            $paid += (int) $payment->pb_response_money;
        }
        /** @var $unpaid */
        $unpaid = ($data->bill_required_money? (int) $data->bill_required_money : 0) - $paid;
        /** @var $pdf */
        $pdf = null;
        if (!AuthController::is_accountant()&&file_exists(public_path('pdf/hop-dong-'.$id.'.pdf'))) $pdf = url('pdf/hop-dong-'.$id.'.pdf');
        /** Format Currency */
        $data->bill_required_money = $data->bill_required_money? number_format($data->bill_required_money) : null;
        $data->bill_total_money = $data->bill_total_money? number_format($data->bill_total_money) : null;
        /** @var $info */
        $info = [
            'add_payment' => false,
            'statuses' => BOBill::STATUS_TEXTS,
            'status_colors' => BOBill::STATUS_COLORS,
            'paid'   => number_format($paid),
            'unpaid' => number_format($unpaid),
            'pdf'   => $pdf
        ];
        $bill_customer = $data->bill_customer_info;
        if (!AuthController::is_sale_staff() && $bill_customer) {
            $bill_customer->phone = substr($bill_customer->phone, 0, 3) . '****' . substr($bill_customer->phone,  -3);
            $bill_customer->email = mb_substr($bill_customer->email, 0, 2) . '****@' . str_after($bill_customer->email, '@');
            $data->bill_customer_info = $bill_customer;
        }
        /** @var $product */
        $product = $data->product;
        $info['buttons'] = self::_getActionButtonsByRole(
            $data->status_id,
            $data->type,
            $product? $product->{BOProduct::ID_KEY}:null,
            $product? $product->stage : null
        );
        $info['allowSkipping'] = (bool) $data->status_id == BOBill::STATUSES['STATUS_REQUEST']
                                    && AuthController::is_product_manager()
                                    && count($info['buttons'])>0
                                    && $data->type==BOBill::TYPE_DEPOSIT;
        /** @var $remindable_statuses */
        $remindable_statuses = [
            BOBill::STATUSES['STATUS_REQUEST'],
            BOBill::STATUSES['STATUS_APPROVED'],
            BOBill::STATUSES['STATUS_PAID_ONCE'],
            BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST'],
            BOBill::STATUSES['STATUS_CANCEL_REQUEST'],
            BOBill::STATUSES['STATUS_FINALIZED'],
        ];
        $info['allowReminding'] = AuthController::is_sale_staff() && in_array($data->status_id, $remindable_statuses);

        return self::jsonSuccess($data, 'Thành công', $info);
    }

    /***
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function submitForm(Request $request, $id = 0) {
        if ($id>0) {
            $query = BOBill::where([BOBill::ID_KEY => $id])->first();
            if (!$query) {
                return self::jsonError('Không tìm thấy hóa đơn', ['item' => $id]);
            }
        } else {
            $query = new BOBill;
            $query->bill_created_time = now();
        }
        $form_data = $request->all();
        $query->{BOBill::ID_KEY} = $id>0? $id : null;
        if ($form_data["status"]) {
            $query->bill_status = (int)$form_data["status"];
        }
        $query->bill_title = $form_data["title"]?? null;
        $query->bill_product_ids = $form_data["products"]?? null;
        $query->product_id = isset($form_data["product_id"])? $form_data["product_id"] : null;
        $query->transaction_id = isset($form_data["transaction_id"])? (int) $form_data["transaction_id"] : null;
        $query->bill_code = $form_data["code"]?? null;
        $query->customer_id = isset($form_data["customer"])? (int)$form_data["customer"] : 0;
        $query->sale_program_id = $form_data["sale_program"]?? null;
        $query->bill_total_money = $form_data["total"]? (double) $form_data["total"] :  null;
        $query->media = $form_data["media"]?? null;

        return $query->save()? self::jsonSuccess($query, $id? 'Cập nhật thành công!' : 'Thêm dữ liệu thành công!') : self::jsonError("Lưu không thành công!", $query);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request, $id) {
        $status = $request->input("status", null);
        $disallowed_status = [
            BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER'],
            BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER']
        ];
        /** @var $logs */
        $logs = [];
        if ($id > 0 || !$status || in_array((int) $status, $disallowed_status)) {
            if (!in_array($status, BOBill::STATUSES) && $status!='RENEW_TRANSACTION') {
                return self::jsonError('Trạng thái không hợp lệ',
                    ['new_status' => $status, 'available_statuses' => BOBill::STATUSES]
                );
            }
            /** @var $query */
            $query = BOBill::where([BOBill::ID_KEY => $id])
                ->with([
                'staff' => function($staff) {
                    $staff->select([
                        'ub_id',
                        'ub_title AS name',
                        'ub_account_name AS account',
                        'ub_email AS email',
                        'ub_avatar AS avatar',
                        'signature',
                        'ub_tvc_code',
                        "group_ids",
                        'ub_account_tvc'
                    ])
                    ->with(["group" => function($group) {
                        return $group->select(["gb_title"]);
                    }]);
                },
                'transaction' => function($transaction) {
                    $transaction->select([
                        'trans_id', 'trans_deposit AS deposit_amount', 'staff_code'
                    ]);
                },
                'product' => function($apartment) {
                    return $apartment->select([
                        "pb_id",
                        "pb_title AS apartment", "pb_code AS code",
                        "category_id", "pb_required_money",
                        "pb_required_money AS required_money",
                        "pb_used_s", "pb_built_up_s", "pb_code_real", "pb_price_total",
                        "price_used_s","pb_price_per_s",
                        "stage", "book_type"
                    ]);
                },
            ])->first();
            if (!$query) {
                return self::jsonError('Không tìm thấy hợp đồng', ['item' => $id]);
            }
            $query->bill_updated_time = now();
            $msg = "Cập nhật thành công";
            /** @var $branch_product_manager */
            $branch_product_manager = BOUser::getCurrent('group_ids');
            /** @var $staff */
            $staff = $query->staff;
            $branch_check = $staff && $staff->group_ids == $branch_product_manager ? 1 : 0;
            $branch = $staff? str_after($staff->ub_account_tvc, '@') : null;
            
            // check CRM
            $crm_product_manager = BOUser::getCurrent('ub_account_tvc') ? str_after(BOUser::getCurrent('ub_account_tvc'), '@') : null;
            $crm_check = $crm_product_manager && $branch == $crm_product_manager ? 1 : 0; // 1 la cung san, 0 la khac san
            $cmConfigF1AccountCode = config('cmconst.account_CRM_F1');
            $cmConfigAccount = config('cmconst.account_CRM');
            
            //** Handle new status */
            switch ((int) $status) {
                case BOBill::STATUSES['STATUS_CANCEL_REQUEST']:
                    if (!AuthController::is_sale_staff()) return self::jsonError('Thao tác chỉ dành cho NVKD!');
                    break;
                case BOBill::STATUSES['STATUS_PAYING']:
                    if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    /** Save tvc contract */
                    if($query->type == BOBill::TYPE_DEPOSIT) {
                        $tvc_booking_status = 'XNDCO';
                        // ghi giao dich xuông F1
                        if($crm_check == 0){
                            $logs[] = LogController::logBill('[STATUS_PAYING] Khởi tạo GD trên F1 Tavico!', 'info', $id, false);
                            if (!$query->contract_reference_code_F1) {
                                $tvc_contract_F1 = APIController::TVC_CONTRACT_CREATE_F1($query, 0, $crm_product_manager, $cmConfigF1AccountCode[$crm_product_manager][$branch]);
                                if (!$tvc_contract_F1['success'] || !$tvc_contract_F1['data']) {
                                    $logs[] = LogController::logBill('[STATUS_PAYING] Không thể tạo GD trên F1 Tavico!', 'error', $id, false);
                                    return self::jsonError($tvc_contract_F1['msg'] ?? 'Không thể tạo HĐ F1 trên Tavico', $tvc_contract_F1, $logs);
                                }
                                $query->contract_reference_code_F1 = $tvc_contract_F1['data'];
                            } else {
                                $logs[] = LogController::logBill('[STATUS_PAYING] Đã tồn tại GD trên F1 Tavico: '.$query->contract_reference_code_F1, 'info', $id, false);
                            }
                        }
                        if (!$query->contract_reference_code) {
                            $logs[] = LogController::logBill('[STATUS_PAYING] Khởi tạo GD trên F2 Tavico!', 'info', $id, false);
                            /** @var $tvc_contract */
                            $tvc_contract = APIController::TVC_CONTRACT_CREATE($query, $branch_check, $branch);
                            if (!$tvc_contract['success']) return self::jsonError($tvc_contract['msg'] ?? 'Không thể tạo HĐ trên Tavico', $tvc_contract);
                            $query->contract_reference_code = $tvc_contract['data'] ?? null;
                        } else {
                            $logs[] = LogController::logBill('[STATUS_PAYING] Đã tồn tại GD trên F2 Tavico: '.$query->contract_reference_code, 'info', $id, false);
                        }
                    }else{
                        $tvc_booking_status = 'XNDCH';
                    }
                    APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, $tvc_booking_status, $id, $branch_check, $branch);
                    if($crm_check == 0){
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, $tvc_booking_status, $id, 0, $crm_product_manager);
                    }
                    /** todo: Update apartment status & trigger pusher */
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['CDDCO'], true);
                    break;
                case BOBill::STATUSES['STATUS_DISAPPROVED']:
                    /** TKKD từ chối HĐ */
                    if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    if ($query->bill_reference_code){
                        APIController::TVC_CANCEL_BOOKING_REQUEST($query->bill_reference_code, $id, true, $branch);
                        if($crm_check == 0){
                            APIController::TVC_CANCEL_BOOKING_REQUEST($query->bill_reference_code_F1, $id, true, $crm_product_manager);                            
                        }
                    } 
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['MBA'], true);
                    /** @var  $transaction */
                    $transaction = BOTransaction::where(BOTransaction::ID_KEY, '=', $query->transaction_id)->first();
                    if ($transaction) {
                        $current_log = $transaction->trans_status_log;
                        $current_log[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['BILL_CANCEL']);
                        $transaction->trans_code = BOTransaction::STATUS_CODES['BILL_CANCEL'];
                        $transaction->save();
                    }
                    break;
                case BOBill::STATUSES['STATUS_CANCEL_APPROVED']:
                    //** todo: TKKD duyệt Huỷ Cọc, chờ DVKH duyệt */
                    if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    if ($query->type!=BOBill::TYPE_DEPOSIT) return self::jsonError('Thao tác không hợp lệ với HĐ đặt chỗ!');
                    break;
                case BOBill::STATUSES['STATUS_CANCELED']:
                    /** NVKD huỷ - TKKD, DVKH duyệt huỷ */
                    $current_status = $query->status_id;
                    $valid = false;
                    //** todo: 1. NVKD tự huỷ HĐ ***** */
                    if (AuthController::is_sale_staff()) {
                        $allowed_statuses = [
                            BOBill::STATUSES['STATUS_REQUEST'],
                            BOBill::STATUSES['STATUS_APPROVED'],
                            BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER'],
                            BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER'],
                            BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER'],
                        ];
                        if (!in_array($current_status, $allowed_statuses)) return self::jsonError('Thao tác không hợp lệ!');
                        $valid = true;
                        // Chuyển phiếu về X-cancel.
                        APIController::TVC_CANCEL_BOOKING_REQUEST($query->bill_reference_code, $id);
                        if($crm_check == 0){
                            APIController::TVC_CANCEL_BOOKING_REQUEST($query->bill_reference_code_F1, $id, true, $crm_product_manager);
                        }
                    } elseif (AuthController::is_product_manager()) {
                        //** todo: 2. TKKD duyệt huỷ chỗ ****** */
                        if ($current_status != BOBill::STATUSES['STATUS_CANCEL_REQUEST'] || $query->type == BOBill::TYPE_DEPOSIT) return self::jsonError('Thao tác không hợp lệ!');
                        $valid = true;
                        if ($query->bill_reference_code) {
                            // chuyển tình trạng phiếu đặt chỗ
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'XNHDCH', $id, $branch_check, $branch);
                            if($crm_check == 0){
                                APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, 'XNHDCH', $id, 0, $crm_product_manager);
                            }
                        }
                        ///Send mail///
                        MailController::customerStatusBill($query,BOBill::STATUSES['STATUS_CANCELED']);
                    } elseif (AuthController::is_cs_staff()) {
                        //** todo: 3. DVKH duyệt huỷ cọc **** */
                        if ($current_status != BOBill::STATUSES['STATUS_CANCEL_APPROVED'] || $query->type != BOBill::TYPE_DEPOSIT) return self::jsonError('Thao tác không hợp lệ!');
                        $valid = true;
                        if($query->contract_reference_code && $query->contract_reference_code != ''){
//                            APIController::TVC_CS_CANCEL_CONTRACT($query->contract_reference_code, $id, $branch_check, $branch, $query->product->code);
//                            if($crm_check == 0){
//                                APIController::TVC_CS_CANCEL_CONTRACT($query->contract_reference_code_F1, $id, 0, $crm_product_manager, $query->product->code);
//                            }
                        }else{
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'YCDCO', $id, $branch_check, $branch);
                            if($crm_check == 0){
                                APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, 'YCDCO', $id, 0, $crm_product_manager);
                            }
                        }
                        
                        ///Send mail///
                        MailController::customerStatusBill($query,BOBill::STATUSES['STATUS_CANCELED']);
                    }
                    /** todo: Update BO Transaction **** */
                    if ($valid) {
                        /** @var  $transaction */
                        $transaction = BOTransaction::where(BOTransaction::ID_KEY, '=', $query->transaction_id)->first();
                        if ($transaction) {
                            $current_log = $transaction->trans_status_log;
                            $current_log[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['BILL_CANCEL']);
                            $transaction->trans_code = BOTransaction::STATUS_CODES['BILL_CANCEL'];
                            $transaction->save();
                        }
                        /** Release Apartment */
                        BOProductController::releaseApartment($query->product_id, true, $query->staff_id);
                    }
                    break;
                case BOBill::STATUSES['STATUS_APPROVED']:
                    /** TKKD duyệt yêu cầu HĐ */
                    if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    /** todo: F1 Customer & Bill Request */
                    if($crm_check == 0){
                        $logs[] = LogController::logBill('[STATUS_APPROVED] Ghi nhận phiếu sang F1', 'info', $id, false);
                        /** @var $tvc_potential_code_F1 */
                        $tvc_potential_code_F1 = APIController::TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE($query->bill_customer_info->phone);
                        if (!$tvc_potential_code_F1) {
                            $logs[] = LogController::logBill('[STATUS_APPROVED] Chưa có khách hàng tiềm năng F1. Đang đồng bộ...'.$query->bill_customer_info->phone, 'info', $id, false);
                            $tvc_add_potential_customer_F1 = APIController::TVC_ADD_POTENTIAL_CUSTOMER_BK($query->bill_customer_info->name, $query->bill_customer_info->phone, $cmConfigF1AccountCode[$crm_product_manager][$branch], $cmConfigAccount[$crm_product_manager]['username']);
                            if (!$tvc_add_potential_customer_F1) {
                                $msg = 'Xảy ra lỗi khi thêm khách hàng F1 trên Tavico. Hãy đăng nhập lại!';
                                $logs[] = LogController::logBill('[F1-PotentialCustomer] '.$msg, 'warning', $id, false);
                                return self::jsonError($msg, [], $logs);
                            } else {
                                $tvc_potential_code_F1 = $tvc_add_potential_customer_F1["prospect"];
                                $msg = 'Đã thêm Khách hàng F1 tiềm năng trên Tavico: '.$tvc_potential_code_F1;
                                $logs[] = LogController::logBill('[F1-PotentialCustomer]' . $msg, 'info', $id, false);
                            }
                        }
                        // Update P.Customer F1
                        UserCustomerMapping::where([
                            'id_user'       => $query->staff_id,
                            'id_customer'   => $query->customer_id
                        ])->update(['potential_reference_code_F1' => $tvc_potential_code_F1]);
                        if (!$query->bill_reference_code_F1) {
                            /** Add F1 Bill request */
                            $logs[] = LogController::logBill('[F1-Bill Init] Đang khởi tạo phiếu F1 trên Tavico. KHTN: ' . $tvc_potential_code_F1, 'info', $id, false);
                            $tvc_add_bill_F1 = APIController::TVC_BILL_CREATE(
                                $query->product ? $query->product->code : null,
                                $tvc_potential_code_F1,
                                $query->product->stage,
                                $cmConfigF1AccountCode[$crm_product_manager][$branch],
                                ""
                            );
                            if (!$tvc_add_bill_F1 || !$tvc_add_bill_F1['systemno']) {
                                $logs[] = LogController::logTransaction('[F1-Bill] Tạo phiếu Tavico F1 không thành công!', 'warning', $query->transaction->trans_id, false);
                                return self::jsonError('Xảy ra lỗi khi tạo phiếu yêu cầu xuống F1 Tavico', [], $logs);
                            } else {
                                $query->bill_reference_code_F1 = $tvc_add_bill_F1['systemno'];
                                $logs[] = LogController::logTransaction('[F1-Bill] Tạo phiếu Tavico F1 thành công: ' . $query->bill_reference_code_F1, 'info', $query->transaction->trans_id, false);
                            };
                        } else {
                            $logs[] = LogController::logBill('[F1-Bill Init] Đã tồn tại phiếu F1 trên Tavico: '.$query->bill_reference_code_F1.'. KHTN: ' . $tvc_potential_code_F1, 'info', $id, false);
                        }
                    } else {
                        $logs[] = LogController::logBill('[STATUS_APPROVED] TKKD duyệt HĐ F2!', 'info', $id, false);
                    }

                    /** todo: TKKD bỏ qua các bước duyệt */
                    if ($request->input('stepsAborted', null) === true) {
                        $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] TKKD duyệt HĐ theo quy trình cũ!', 'info', $id, false);
                        /** Save tvc contract */
                        if($query->type == BOBill::TYPE_DEPOSIT) {
                            /** todo: F1 Contract */
                            if($crm_check == 0){
                                $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Ghi GD F1 theo quy trình cũ!', 'info', $id, false);
                                if (!$query->contract_reference_code_F1) {
                                    $tvc_contract_F1 = APIController::TVC_CONTRACT_CREATE_F1($query, 0, $crm_product_manager, $cmConfigF1AccountCode[$crm_product_manager][$branch]);
                                    if (!$tvc_contract_F1['success'] || !$tvc_contract_F1['data']) {
                                        /** Save F1 bill request */
                                        $query->save();
                                        return self::jsonError($tvc_contract_F1['msg'] ?? 'Không thể tạo GD F1 trên Tavico', $tvc_contract_F1);
                                    }
                                    $query->contract_reference_code_F1 = $tvc_contract_F1['data'] ?? null;
                                } else {
                                    $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] F1 đã tồn tại giao dịch Tavico! '.$query->contract_reference_code_F1, 'info', $id, false);
                                }
                            }
                            /** @var $tvc_booking_status */
                            $tvc_booking_status = 'XNDCO';
                            if (!$query->contract_reference_code) {
                                /** @var $tvc_contract */
                                $tvc_contract = APIController::TVC_CONTRACT_CREATE($query, $branch_check, $branch);
                                if (!$tvc_contract['success'] || !$tvc_contract['data']) {
                                    /** Save F1 contract */
                                    $query->save();
                                    $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Không thể tạo GD trên F2 Tavico', 'error', $id, false);
                                    return self::jsonError($tvc_contract['msg'] ?? 'Không thể tạo GD trên F2 Tavico', $tvc_contract, $logs);
                                }
                                $query->contract_reference_code = $tvc_contract['data'];
                            }
                        }else{
                            $tvc_booking_status = 'XNDCH';
                        }
                        /**  UPDATE BOOKING TVC F1 */
                        if($crm_check == 0){
                            if (!$query->bill_reference_code_F1) {
                                $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Chưa có Phiếu Tavico F1 để cập nhật!', 'error', $id, false);
                                return self::jsonError('Không tồn tại phiếu F1 Tavico cho HĐ: '.$query->bill_code, [], $logs);
                            }
                            $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Cập nhật trạng thái Phiếu F1 Tavico: '.$query->bill_reference_code_F1, 'info', $id, false);
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, $tvc_booking_status, $id, 0, $crm_product_manager);
                        }
                        /**  UPDATE BOOKING TVC F2 */
                        if (!$query->bill_reference_code) {
                            $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Chưa có Phiếu Tavico F2 để cập nhật!', 'error', $id, false);
                            return self::jsonError('Không tồn tại phiếu Tavico cho HĐ: '.$query->bill_code, [], $logs);
                        }
                        $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Cập nhật trạng thái Phiếu F2 Tavico: '.$query->bill_reference_code, 'info', $id, false);
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, $tvc_booking_status, $id, $branch_check, $branch);
                        /** todo: Update apartment status & trigger pusher */
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['CDDCO'], true);
                        if ($query->transaction) {
                            $payment = new BOPayment();
                            $payment->{BOPayment::ID_KEY} = null;
                            $payment->pb_status = BOPayment::STATUS['PENDING'];
                            $payment->pb_code = null;
                            $payment->reference_code = null;
                            $payment->bill_code = $query->bill_code;
                            $payment->pb_note = "Y/C thanh toán cho HĐ theo quy trình cũ";
                            $payment->customer_id = $query->customer_id;
                            $payment->request_staff_id = $query->staff_id;
                            $payment->request_time = now();
                            $payment->pb_request_money = (double) $query->transaction->deposit_amount;
                            $payment->method_id = 0;
                            $payment->pb_bank_number_id = null;
                            $payment->type = $query->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
                            if ($payment->save()) {
                                $logs[] = LogController::logPayment('[BO] Đã tạo Y/C thanh toán cho HĐ theo quy trình cũ: '.$query->bill_code, 'info', $payment->{BOPayment::ID_KEY}, false);
                            } else {
                                $logs[] = LogController::logPayment('[BO] Xảy ra lỗi khi tạo Y/C thanh toán cho HĐ theo quy trình cũ: '.$query->bill_code, 'warning', null, false);
                            }
                        }
                        $status = BOBill::STATUSES['STATUS_PAYING'];
                        $query->bill_note = ($query->bill_note?? '') . ' - HĐ được duyệt theo quy trình cũ';
                    }
                    break;
                case BOBill::STATUSES['STATUS_BOOKED']:
                    /** Đặt chỗ */
                    if (!AuthController::is_cs_staff()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    /** todo: Update apartment status & trigger pusher */
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['DCH'], true);
                    ///Send mail///
                    MailController::customerStatusBill($query,BOBill::STATUSES['STATUS_BOOKED']);
                    break;

                case BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']:
                    /** Y/C Chuyển cọc */
                    if (!AuthController::is_sale_staff()) return self::jsonError('Thao tác chỉ dành cho NVKD!');
                    if ($query->type==BOBill::TYPE_DEPOSIT) return self::jsonError('Yêu cầu không hợp lệ! HĐ không thuộc loại đặt chỗ!');
                    if (!$query->product || $query->product->stage!=BOProduct::STAGE_DEPOSIT) return self::jsonError('Căn hộ chưa cho phép đặt cọc!');
                    break;

                case BOBill::STATUSES['STATUS_DEPOSITED']:
                    /** Xác nhận Đặt cọc || Chuyển chỗ lên cọc trực tiếp */
                    if ($query->type == BOBill::TYPE_BOOK_ONLY) return self::jsonError('HĐ đặt chỗ không thể chuyển cọc trực tiếp!');
                    if (!$query->product || $query->product->stage!=BOProduct::STAGE_DEPOSIT) return self::jsonError('Căn hộ chưa cho phép đặt cọc!');
                    if ($query->status_id == BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']) {
                        /** TKKD xác nhận chuyển cọc trực tiếp */
                        if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                        // chuyển tình trạng phieus XNDCH => XNDCO, và tạo giao dịch.
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'XNDCO', $id, $branch_check, $branch);
                        $tvc_contract = APIController::TVC_CONTRACT_CREATE($query, $branch_check, $branch);
                        if (!$tvc_contract['success']) return self::jsonError($tvc_contract['msg']?? 'Không thể tạo HĐ trên Tavico', $tvc_contract);
                        $query->type = BOBill::TYPE_DEPOSIT;
                        $query->contract_reference_code = $tvc_contract['data'];
                        // ghi giao dich xuông F1
                        if($crm_check == 0){
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, 'XNDCO', $id, 0, $crm_product_manager);
                            $tvc_contract_F1 = APIController::TVC_CONTRACT_CREATE_F1($query, 0, $crm_product_manager, $cmConfigF1AccountCode[$crm_product_manager][$branch]);
                            if (!$tvc_contract_F1['success']) return self::jsonError($tvc_contract_F1['msg']?? 'Không thể tạo HĐ F1 trên Tavico', $tvc_contract_F1);
                            $query->contract_reference_code_F1 = $tvc_contract_F1['data']?? null;
                        }

//                        $oldRequiredMoney = $query->bill_required_money;
//                        $newRequiredMoney = $query->product? $query->product->pb_required_money : 0;
//                        ///Nếu số tiền đặt chỗ = số tiền đặt cọc mới thì sẽ update payment cũ trên tavico///
//                        if($oldRequiredMoney == $newRequiredMoney && $newRequiredMoney != 0){
//                            ///Update Tavico Payment///
//                            PaymentController::createPaymentTvc($payment->toArray(),BOPayment::TYPE_PAYMENT_TAVICO['CANCEL_DCHO'],$query);
//                        }

                        // check thu du tien hay chua
                        $payments = BOPayment::where('bill_code', $query->bill_code)->get();
                        $paid = 0;
                        foreach($payments as $item){
                            $paid += (int) $item->pb_response_money;
                        }
                        $compareMoney = $paid > (int)$query->bill_required_money ? $paid : (int)$query->bill_required_money;
                        $status =  $compareMoney < (int)$query->product->pb_required_money ? BOBill::STATUSES['STATUS_PAYING'] : BOBill::STATUSES['STATUS_PAID'];
                        $current_required_money = $query->bill_required_money;
                        $query->bill_required_money = $query->product? $query->product->pb_required_money : null;
                        $edit_log = [
                            self::generateEditLog('type', $query->type, BOBill::TYPE_DEPOSIT),
                            self::generateEditLog('bill_required_money', $current_required_money, $query->bill_required_money),
                        ];
                        $query->bill_log_edit = is_array($query->bill_log_edit)? array_merge($query->bill_log_edit, $edit_log) : $edit_log;
                        ///Send mail///
                        MailController::customerContract($query, BOBill::STATUSES['STATUS_DEPOSITED']);

                        /** todo: Update apartment status & trigger pusher */
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['CDDCO'], true);
                    } elseif ($query->status_id == BOBill::STATUSES['STATUS_PAID']) {
                        /** DVKH xác nhận HĐ đặt cọc */
                        if (!AuthController::is_cs_staff()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                        APIController::TVC_CONTRACT_DEPOSITED($query->contract_reference_code, null, $branch_check, $branch, $query->product->code);
                        if($crm_check == 0){
                            APIController::TVC_CONTRACT_DEPOSITED($query->contract_reference_code_F1, null, 0, $crm_product_manager, $query->product->code);
                        }
                        ///Send mail///
                        MailController::customerStatusBill($query, BOBill::STATUSES['STATUS_DEPOSITED']);

                        /** todo: Update apartment status & trigger pusher */
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['DCO'], true);
                    } else {
                        return self::jsonError('Yêu cầu không hợp lệ!');
                    }

                    break;
                case BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']:
                    if (!AuthController::is_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    if ($query->status_id !== BOBill::STATUSES['STATUS_APPROVED']) {
                        return self::jsonError('HĐ chưa được Thư ký bảng hàng duyệt!');
                    }
                    MailController::customerContract($query, BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']);
                    break;
                case BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT']:
                    /** TKKD Xác nhận Huỷ chỗ - cọc mới */
                    if ($query->type != BOBill::TYPE_BOOK_ONLY || $query->status_id!=BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']) return self::jsonError('Tình trạng hợp đồng không hợp lệ!');
                    if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    if (!$query->product || $query->product->stage!=BOProduct::STAGE_DEPOSIT) return self::jsonError('Căn hộ chưa cho phép đặt cọc!');

                    /*******************************************
                     * 1. TVC UPDATE OLD BILL & CREATE NEW ONE
                     *******************************************/
                    // chuyển phiếu cũ thành XNHDCH, Tao phieu moi XNDCO, va tao giao dich
                    APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'XNHDCH', $id, $branch_check, $branch);
                    if($crm_check == 0){
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, 'XNHDCH', $id, 0, $crm_product_manager);
                    }
                    // tạo phiêu yêu cầu mới
                    $customer = UserCustomerMapping::where([
                        'id_user'       => $query->staff_id,
                        'id_customer'   => $query->customer_id
                    ])->first();
                    if (!$customer) return self::jsonError('Không tìm thấy thông tin khách hàng!');
                    /** @var $tvc_potential_code */
                    $tvc_potential_code = $customer->potential_reference_code;
                    $tvc_potential_code_F1 = $customer->potential_reference_code_F1;
                    /** @var $product_code */
                    $product_code = $query->product? $query->product->code : null;
                    if (!$product_code) return self::jsonError('Không tìm thấy thông tin căn hộ!');
                    if (!$staff || !$staff->ub_tvc_code) return self::jsonError('Không tìm thấy thông tin nhân viên!');
                    /** @var $note */
                    $note = "Hủy chỗ, tạo phiếu cọc mới";
                    /** @var $tvc_add_bill */
                    $tvc_add_bill = APIController::TVC_BILL_CREATE($product_code, $tvc_potential_code, 3, $staff->ub_tvc_code, $note, null, $cmConfigAccount[$branch]['username']);
                    if (!$tvc_add_bill) {
                        return self::jsonError('Xảy ra lỗi khi tạo GD trên Tavico!');
                    }
                    if($crm_check == 0){
                        $tvc_add_bill_F1 = APIController::TVC_BILL_CREATE($query->product? $query->product->code : null, $tvc_potential_code_F1, 3, $cmConfigF1AccountCode[$crm_product_manager][$branch], "");
                        if (!$tvc_add_bill_F1) {
                            $logs[] = LogController::logTransaction('[addTransactionCustomer] Tạo phiếu Tavico F1 không thành công!', 'warning', $query->transaction->trans_id, false);
                            $tvc_add_bill_F1 = [ 'systemno' => null];
                        } else {
                            $logs[] = LogController::logTransaction('[addTransactionCustomer] Tạo phiếu Tavico F1 thành công!', 'info', $query->transaction->trans_id, false);
                        };                        
                    }

                    /*************************
                     * 2. BO ClONE NEW BILL
                     **************************/
                    $new_bill_id = time();
                    $new_bill = $query->replicate();
                    $new_bill->{BOBill::ID_KEY} = $new_bill_id;
                    $new_bill->bill_code = BOBill::generateBillCode(BOBill::TYPE_DEPOSIT, $query->product);
                    $new_bill->bill_title = null;
                    /** @var $paid */
                    $paid = 0;
                    /** check Payment status */
                    if ($query->product) {
                        $new_bill->bill_total_money = $query->product->pb_price_total;
                        $new_bill->bill_required_money = $query->product->pb_required_money;
                        /** @var $payments */
                        $paid += BOPayment::where(['bill_code' => $query->bill_code, 'pb_status' => BOPayment::STATUS['APPROVED']])
                                    ->where('pb_response_money', '>', 0)
                                    ->sum('pb_response_money');
                        $new_bill->status_id = $new_bill->bill_required_money && $new_bill->bill_required_money <= $paid?
                                                BOBill::STATUSES['STATUS_PAID'] : BOBill::STATUSES['STATUS_PAYING'];
                    } else {
                        $new_bill->status_id = BOBill::STATUSES['STATUS_PAYING'];
                    }
                    $new_bill->bill_created_time = now()->addSeconds(2);
                    $new_bill->bill_updated_time = now()->addSeconds(2);
                    $new_bill->type = BOBill::TYPE_DEPOSIT;

                    ///Save Log Manager approve///
                    $logsQuery = $query->bill_approval_log;
                    $bill_approval_log = array();
                    foreach ($logsQuery as $log){
                        if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
                            $bill_approval_log[] = $log;
                        }
                    }
                    $bill_approval_log[] = BOBill::generateApprovalLog(BOBill::STATUSES['STATUS_PAID_ONCE']);

                    $new_bill->bill_approval_log = $bill_approval_log;
//                    unset($new_bill->total_money);
//                    unset($new_bill->required_money);
                    /** @var $tvc_contract */
                    $new_bill->bill_reference_code = $tvc_add_bill&&$tvc_add_bill['systemno']? $tvc_add_bill['systemno'] : null;
                    $tvc_contract = APIController::TVC_CONTRACT_CREATE($new_bill, $branch_check, $branch);
                    if (!$tvc_contract['success']) return self::jsonError($tvc_contract['msg']?? 'Không thể tạo HĐ trên Tavico', $tvc_contract);
                    if($crm_check == 0){
                        $new_bill->bill_reference_code_F1 = $tvc_add_bill_F1&&$tvc_add_bill_F1['systemno']? $tvc_add_bill_F1['systemno'] : null;
                        $tvc_contract_F1 = APIController::TVC_CONTRACT_CREATE_F1($new_bill, 0, $crm_product_manager, $cmConfigF1AccountCode[$crm_product_manager][$branch]);
                        if (!$tvc_contract_F1['success']) return self::jsonError($tvc_contract_F1['msg']?? 'Không thể tạo HĐ F1 trên Tavico', $tvc_contract_F1);
                        $new_bill->contract_reference_code_F1 = $tvc_contract_F1['data']?? null;
                        
                    }
                        
                    $new_bill->contract_reference_code = $tvc_contract['data']?? null;
                    
                    if ($new_bill->save()) {
                        LogController::logBill('Chuyển HĐ chỗ sang cọc mới thành công: '.$new_bill->bill_code, 'info', $new_bill_id, true);
                        $edit_log = [self::generateEditLog('type', $query->type,BOBill::TYPE_DEPOSIT)];
                        $query->bill_log_edit = is_array($query->bill_log_edit)? array_merge($query->bill_log_edit, $edit_log) : $edit_log;
                        /** Generate first payment */
                        $payment = new BOPayment();
                        $payment->pb_id = null;
                        $payment->pb_status = env('STATUS_ACTIVE', 1);
                        $payment->pb_code = null;
                        $payment->bill_code = $new_bill->bill_code;
                        $payment->pb_note = "HĐ chỗ sang cọc mới: TT lần đầu tự động";
                        $payment->customer_id = $new_bill->customer_id;
                        $payment->request_staff_id = $new_bill->staff_id;
                        $payment->request_time = now();
                        $payment->pb_request_money = $paid;
                        $payment->pb_response_money = $paid;
                        $payment->response_staff_id = AuthController::getCurrentUID();
                        $payment->pb_response_time = now();
                        $payment->type = $new_bill->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
                        if (!$payment->save()) {
                            LogController::logPayment('Không thể tạo thanh toán tự động cho HĐ chuyển chỗ sang cọc mới: '.$query->bill_code, 'warning');
                        }
                        ///Create Tavico Payment///
                        // BOPaymentController::createPaymentTvc($payment->toArray(),BOPayment::TYPE_PAYMENT_TAVICO['CANCEL_DCHO'],$query);
                        // $referenceCode = BOPaymentController::createPaymentTvc($payment->toArray(),BOPayment::TYPE_PAYMENT_TAVICO['DCO'],$new_bill);
                        // $payment->reference_code = $referenceCode;
                        /** Tạo phiếu chi **/
                        $spending = new BOPayment();
                        $spending->pb_id = $payment->pb_id+1;
                        $spending->pb_status = env('STATUS_ACTIVE', 1);
                        $spending->pb_code = null;
                        $spending->bill_code = $query->bill_code;
                        $spending->pb_note = "HĐ chỗ sang cọc mới: Tạo phiếu chi tự động";
                        $spending->customer_id = $query->customer_id;
                        $spending->request_staff_id = $query->staff_id;
                        $spending->request_time = now();
                        $spending->pb_request_money = $paid;
                        $spending->pb_response_money = $paid;
                        $spending->response_staff_id = AuthController::getCurrentUID();
                        $spending->pb_response_time = now();
                        $spending->type = $new_bill->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
                        $spending->type_of_payment = 'D' ;
                        if (!$spending->save()) {
                            LogController::logPayment('Không thể tạo phiếu chi tự động cho HĐ chuyển chỗ sang cọc mới: '.$query->bill_code, 'warning');
                        }

                        $msg = 'Đã chuyển chỗ sang cọc, hợp đồng mới: ' . $new_bill->bill_code;
                        //Send mail///
                        MailController::customerContract($new_bill, BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT']);
                    } else {
                        LogController::logBill('Không thể chuyển HĐ chỗ sang cọc mới: '. $query->bill_code, 'error', $id, true);
                        return self::jsonError("Không thể chuyển HĐ chỗ sang cọc mới", $query);
                    }
                    break;

                case BOBill::STATUSES['STATUS_FINALIZED']:
                    if (!AuthController::is_sale_staff()) return self::jsonError('Chỉ NVKD mới có quyền thực hiện thao tác này!');
                    /** @var $attachments */
                    $attachments = $request->input('attachments', []);
                    if (!$attachments || count($attachments)==0) return self::jsonError('Vui lòng đính kèm tập tin!');
                    foreach ($attachments as &$attachment) {
                        $image = str_replace(' ', '+', $attachment);
                        $image = str_after($image, 'data:image/jpeg;base64,');
                        $imageName = time() . str_random(2) . '.jpg';
                        $root_path = public_path('uploads' . '/' . $imageName);
                        File::put($root_path, base64_decode($image));
                        $attachment = '/uploads/' . $imageName;
                        if (file_exists(public_path($attachment))) {
                            $optimizerChain = OptimizerChainFactory::create();
                            $optimizerChain->optimize(public_path($attachment));
                        }
                    }
                    $query->media = $attachments;
                    break;

                case BOBill::STATUSES['STATUS_SUCCESS']:
                    /** TKKD xác nhận hoàn thành HĐ */
                    if (!AuthController::is_product_manager()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
//                    APIController::TVC_PRODUCT_MANAGER_UPDATE_CONTRACT_STATUS_CDGDTC($query->contract_reference_code, $query->product->code, $branch_check, $id, $branch);
                    $query->bill_completed_time = now();
                    ///Send mail///
                    MailController::customerStatusBill($query, BOBill::STATUSES['STATUS_SUCCESS']);
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['HDO'], true);
                    break;

                case 'RENEW_TRANSACTION':
                    /** NVKD tạo lại giao dịch bị khách hàng huỷ */
                    if (!AuthController::is_sale_staff()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
                    /* Cancel old Bill*/
                    $status = BOBill::STATUSES['STATUS_CANCELED'];

                    /* clone new transaction */
                    $transaction = BOTransaction::where(BOTransaction::ID_KEY, '=', $query->transaction_id)->first();
                    $new_transaction = $transaction->replicate();
                    $new_transaction->{BOTransaction::ID_KEY} = time();
                    $new_transaction->trans_status = env('STATUS_ACTIVE', 1);
                    $new_transaction->trans_code = BOTransaction::STATUS_CODES['LOCKED_AUTO'];
                    $new_transaction->trans_title = 'Yêu cầu lock lại';
                    $new_transaction->updated_user_id = AuthController::getCurrentUID();
                    $new_transaction->trans_created_time = now();
                    $new_transaction->trans_updated_time = now();

                    $transaction_logs = $transaction->trans_status_log?? [];
                    $new_transaction_logs = $transaction_logs;
                    $new_transaction_logs[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['LOCKED_AUTO']);
                    $new_transaction->trans_status_log = $new_transaction_logs;

                    /* cancel old transaction */
                    $transaction_logs[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['CANCELED_MANUAL']);
                    $transaction->trans_code = BOTransaction::STATUS_CODES['CANCELED_MANUAL'];
                    $transaction->trans_title = BOTransaction::STATUSES[BOTransaction::STATUS_CODES['CANCELED_MANUAL']];
                    $transaction->updated_user_id = null;
                    $transaction->trans_updated_time = null;
                    $transaction->trans_status_log = $transaction_logs;

                    if ($transaction->save() && $new_transaction->save()) {
                        $msg = "Thành công, đã khởi tạo giao dịch mới!";
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['LOCKED'], true);
                    } else {
                        $msg = "Thao tác giao dịch không thành công! Đã cập nhật hợp đồng.";
                    }
                    break;

                default:
            }
            $current_log = $query->bill_approval_log;
            $current_log[] = BOBill::generateApprovalLog((int) $status);
            $query->status_id = (int) $status;
            $query->bill_approval_log = $current_log;

            if ($query->save()) {
                NotificationController::notifyBillStatusUpdated($query);
                return self::jsonSuccess($query, $msg, ['new_status' => $status], $logs);
            } else {
                return self::jsonError("Không thành công", $query, $logs);
            }

        } else {
            return self::jsonError('Dữ liệu không hợp lệ', ['item' => $id]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse e
     */
    protected function getLog($id) {
        /** @var $bill */
        $bill = BOBill::select([
            BOBill::ID_KEY,
            'bill_code',
            'staff_id',
            'customer_id',
            'status_id',
            'bill_created_time',
            'bill_completed_time',
            'bill_approval_log',
            'bill_updated_time',
            'bill_log_edit'
        ])
        ->where(BOBill::ID_KEY, (int) $id)
        ->with([
            'payments' => function($payments) {
                $payments->select('*')->with([
                    'paymentMethod:pm_id,pm_title',
                    'responseStaff' => function($accountant) {
                        $accountant->select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids'])->with('group:gb_id,gb_title');
                    }
                ])->orderBy('request_time', 'DESC');
            },
            'official_customer:cb_id,cb_name'
        ])
        ->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!', ['item' => $id]);
        $users = [$bill->staff_id];
        $info = ['statuses' => BOBill::STATUS_TEXTS];
        //** todo: Format Approval Logs */
        $approval_logs = $bill->bill_approval_log;
        foreach ($approval_logs as &$item) {
            $item['TIME'] = $item['TIME']? date('H:i d-m-Y', $item['TIME']) : null;
            $users[] = $item['CREATED_BY'];
            $item['VALUE'] = isset($item['VALUE'])? (string) $item['VALUE'] : null;
        }
        $bill->bill_approval_log = array_reverse($approval_logs);

        //** todo: Collect Customer info + Users involved in Bill Logs */
        $info['users'] = BOUser::select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids', 'ub_staff_code AS staff_code', 'ub_account_tvc AS tvc_account'])
                        ->whereIn(BOUser::ID_KEY, $users)
                        ->with('group:gb_id,gb_title')
                        ->get()->keyBy(BOUser::ID_KEY);
        if ($bill->official_customer) {
            $info['users'][$bill->official_customer->cb_id] = ['name' => $bill->official_customer->cb_name];
        }
        return self::jsonSuccess($bill, 'Thành công', $info);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function InitPayment($id = 0) {
        if (!$id) return self::jsonError('Không tìm thấy ID hợp đồng');
        /** @var $bill */
        $bill = BOBill::select(['*', 'bill_total_money AS total'])->where([
            BOBill::ID_KEY => (int) $id,
            'staff_id' => AuthController::getCurrentUID()
        ])->with([
            'product' => function($product) {
                $product->select(['pb_id', 'pb_code AS code']);
            },
            'transaction:trans_id,trans_deposit',
            'payments' => function($payments) {
                $payments->select(['bill_code', 'pb_response_money AS amount'])->where('pb_status', BOPayment::STATUS['APPROVED']);
            }
        ])->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng');
        switch ($bill->status_id) {
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER']:
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER']:
            case BOBill::STATUSES['STATUS_DISAPPROVED']:
            case BOBill::STATUSES['STATUS_CANCELED']:
                return self::jsonError('Không thể thanh toán với hợp đồng đã huỷ!');
                break;
//            case BOBill::STATUSES['STATUS_BOOKED']:
//            case BOBill::STATUSES['STATUS_DEPOSITED']:
//                return self::jsonError('Hợp đồng đã đặt chỗ/cọc thành công. Không thể thanh toán!');
//                break;
            case BOBill::STATUSES['STATUS_FINALIZED']:
            case BOBill::STATUSES['STATUS_SUCCESS']:
                return self::jsonError('Hợp đồng đã kết thúc. Không thể thanh toán!');
                break;
            case BOBill::STATUSES['STATUS_REQUEST']:
                return self::jsonError('Hợp đồng chưa có hiệu lực. Không thể thanh toán!');
                break;
            default:
                /** @var $paid */
                $paid = 0;
                foreach ($bill->payments as $payment) {
                    $paid += (int) $payment->amount;
                }
                /** @var $unpaid */
                $unpaid = ($bill->total? (int) $bill->total : 0) - $paid;
                /** @var $payment_methods */
                $payment_methods = PaymentMethod::select(['pm_id AS id', 'pm_title AS title', 'pm_note AS note'])
                                    ->where('pm_status', env('STATUS_ACTIVE', 1))->get();
                return self::jsonSuccess($bill, 'Thành công', [
                    'methods' => $payment_methods,
                    'paid' => number_format($paid),
                    'unpaid' => number_format($unpaid),
                    'first_payment' => false
                ]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getReport() {
        /** @var $fillter_status */
        $fillter_status = [BOBill::STATUSES['STATUS_DEPOSITED'], BOBill::STATUSES['STATUS_FINALIZED'], BOBill::STATUSES['STATUS_SUCCESS']];
        /** @var $bills */
        $bills = BOBill::select([BOBill::ID_KEY, 'bill_code', 'bill_required_money', 'bill_approval_log', 'transaction_id'])
                ->whereIn('status_id', $fillter_status)
                ->where('bill_status', '!=', env('STATUS_DELETE', -1))
                ->whereNotNull('bill_approval_log')
                ->whereNotNull('transaction_id')
                ->with('transaction:trans_created_time,'.BOTransaction::ID_KEY)
                ->get();
        /** @var $report_rows */
        $report_rows = [];
        $time = 0;
        foreach ($bills as $k => &$bill) {
            /** @var $log */
            $log = array_values(array_where($bill->bill_approval_log, function ($value, $key) {
                return $value['VALUE'] == BOBill::STATUSES['STATUS_DEPOSITED'];
            }));
            /** @var $completed_time */
            $completed_time = $log&&array_first($log)['TIME']? array_first($log)['TIME'] : null;
            if ($completed_time) {
                $start_time = strtotime($bill->transaction->trans_created_time);
                $bill->duration = round(($completed_time - $start_time)/60);
                $time += $bill->duration;
            } else {
                unset($bills[$k]);
            }
        }
        $bills = array_values(array_sort($bills, function ($value) {
           return (int) $value['duration'];
        }));
        $report_rows['MIN_DURATION'] = array_first($bills)['duration'] . ' phút';
        $report_rows['MAX_DURATION'] = array_last($bills)['duration'] . ' phút';
        $report_rows['AVG_DURATION'] = $time / count($bills) . ' phút';
        $report_rows['BILL_COUNT']  = count($bills);
        return self::jsonSuccess($report_rows, 'Done', $bills);
    }
}
