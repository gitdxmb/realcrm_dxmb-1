<?php

namespace App\Http\Controllers\API;

use App\BOCategory;
use App\BOCustomerDiary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOCustomerDiaryController extends Controller
{
    /**
     * BOCustomerDiaryController constructor.
     */
    public function __construct()
    {
        $this->middleware("laravel.jwt");
        $this->middleware("CheckStoredJWT");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        /** @var $data */
        $data = BOCustomerDiary::select([
            BOCustomerDiary::ID_KEY . ' AS id',
            'cd_description AS description',
            'cd_rating AS rate',
            'cd_time AS date',
            'cd_category',
            'created_at'
        ])
            ->where([
                'cd_status' => env('STATUS_ACTIVE', 1),
                'cd_user_id'    => AuthController::getCurrentUID(),
            ])
            ->orderBy('cd_time', 'DESC')
            ->with('project:cb_title,cb_code,'.BOCategory::ID_KEY);
        /** @var $customer */
        $customer = $request->input('customer', null);
        if ($customer) $data = $data->where('cd_customer_id', $customer);
        $data = $data->get();
        if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
        return self::jsonSuccess($data, 'Done', compact('customer'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function submitForm(Request $request, $id = 0) {
        $request->validate([
            'date' => 'string|required'
        ]);
        if ($id > 0) {
            /** @var $data */
            $data = BOCustomerDiary::where(BOCustomerDiary::ID_KEY, $id)
            ->where('cd_user_id', AuthController::getCurrentUID())->first();
            if (!$data) return self::jsonError('Không tồn tại dữ liệu!');
        } else {
            $request->validate([
                'customer' => 'integer|required'
            ]);
            $data = new BOCustomerDiary();
            $data->created_at = now();
            $data->cd_customer_id = (int) $request->input('customer');
            $data->cd_user_id = AuthController::getCurrentUID();
            $data->cd_status = env('STATUS_ACTIVE', 1);
            $data->{BOCustomerDiary::ID_KEY} = null;
        }
        /** @var $note */
        $note = $request->input('note', null);
        /** @var $rate */
        $rate = $request->input('rate', null);
        /** @var $project */
        $project = $request->input('project', null);
        /** @var $date */
        $date = $request->input('date');
        $date = $date? Carbon::createFromFormat('d-m-Y', $date) : today();
        $data->cd_time = $date;
        $data->cd_description = $note;
        $data->cd_rating = $rate? (int) $rate : null;
        $data->cd_category = $project? (int) $project : null;
        if ($data->save()) return self::jsonSuccess('Cập nhật dữ liệu thành công!');
        return self::jsonError('Yêu cầu không hợp lệ!');
    }
}
