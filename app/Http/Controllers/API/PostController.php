<?php

namespace App\Http\Controllers\API;

use App\BOUser;
use App\BOUserGroup;
use App\Post;
use App\PostFeedback;
use App\PostTarget;
use App\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct() {
//        $this->middleware("laravel.jwt");
//        $this->middleware("CheckStoredJWT");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        if (env('MAINTENANCE_MODE', false)) return self::jsonError('Máy chủ đang bảo trì, chúng tôi sẽ trở lại sớm!');
        $page = $request->input('page', 1);
        $page_size = $request->input('size', env("PAGE_SIZE", 15));
        $offset = ($page-1)*$page_size;
        $keyword = $request->input('keyword', null);
        /** @var $category: 0|1 */
        $category = $request->input('category', null);
        /** @var $priority */
        $priority = $request->input('priority', null);
        /** @var $department */
        $department = $request->input('department', null);
        /** @var $project */
        $project = $request->input('project', null);
        /** @var $tag */
        $tag = $request->input('tag', null);
        /** @var $resize */
        $custom_resize = $request->input('resize', null);

        /** @var $get_bo */
        $get_bo = true;

        if ($get_bo) {
            /** @var $data */
            $data = Post::select(["id", "title", "description", "type", "priority", "image", "pictures", "bo_category_ids", "tags",
                "feedback_type", "creator_department", "feedback_options", "video", "created_by", "created_at", "updated_by"])
                ->where("status", env("STATUS_ACTIVE", 1))
                ->with(['creator_department' => function ($group) {
                    $group->select([BOUserGroup::ID_KEY, 'gb_title AS name']);
                }])
                ->withCount('post_feedbacks')
                ->orderBy('created_at', 'DESC');
            if ($category!==null && $category >= 0) {
                $data = $data->where('category', (int)$category);
            }
            if ($priority!==null && $priority>=0) $data = $data->where('priority', (int) $priority);
            if ($department > 0) $data = $data->where('creator_department', $department);
            if ($tag) $data = $data->where('tags', 'LIKE', "%$tag%");
            if ($project > 0) $data = $data->whereJsonContains('bo_category_ids->value', (int) $project);
            /** @var $pins */
            $pins = null;
            if ($keyword && trim($keyword) != '') {
                $data = $data->where('title', 'LIKE', '%' . trim($keyword) . '%');
            } else {
                if ($priority===null) {
                    /** @var $pins */
                    $pins = clone($data);
                    $pins = $pins->where('priority', 2)->get();
                    $data = $data->where('priority', '<', 2);
                }
            }
            $data = $data->skip($offset)->take($page_size)->get();
            return self::jsonSuccess($data, 'OK', ['pins' => $pins, 'category' => $category, 'keyword' => $keyword]);
        } else {
            if ($category!==1) {
                $dxmb_posts = DXMBController::getHighlightedNews($page, $page_size, "", $keyword);
            } else {
                $dxmb_posts = DXMBController::getInternalNews($page, $page_size, "", $keyword);
            }
            if (!$dxmb_posts || !isset($dxmb_posts['data'])) return self::jsonError('Không tìm thấy dữ liệu!', $dxmb_posts);
    //        /** @var $posts */
            $posts = array_values($dxmb_posts['data']);
            foreach ($posts as $key => &$post) {
    //            $post['detail'] = Util::escapeHTMLforWebView($post['detail']);
                $post['title'] = $post['name'];
                $post['description'] = $post['shortDetail']?? '';
                if ($key == 0) {
                    $post['priority'] = 1;
                    $resize = $custom_resize?? '600x400';
                } else {
                    $post['priority'] = 0;
                    $resize = $custom_resize?? '100x100';
                }
                $post['type'] = 0;
                $post['feedback_type'] = 0;
                $post['feedback_options'] = null;
                $post['video'] = null;
                $post['image'] = isset($post['avatar'])&&$post['avatar']!=''? ('https://datxanhmienbac.com.vn/cache/'.$resize.'-1' . $post['avatar']) : null;
                $post['created_at'] = $post['createTime'];
                $post['creator_department'] = isset($post['create_by'])? ['name' => $post['create_by']]
                    : (isset($post['user_departmentName'])? ['name' => $post['user_departmentName']] : null);
                /** Unset Mongo Array Keys */
                unset($post['detail']);
                unset($post['shortDetail']);
                unset($post['name']);
                unset($post['createTime']);
                unset($post['create_by']);
                unset($post['user_departmentName']);
                unset($post['avatar']);
                unset($post['attachFile']);
                unset($post['permalink']);
            }
//        return $posts;
            return self::jsonSuccess($posts, 'OK', ['pins' => null, 'category' => $category]);
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id=null) {
        if (!$id) return self::jsonError('Yêu cầu không hợp lệ! Chưa nhập ID.');
        $get_raw = $request->input('get_raw', false);

        /** @var $data */
        if (!preg_match('/\D+/', $id) && $id > 0) {
            $data = Post::select($get_raw? ['*', 'content AS raw_content'] : '*')
                ->where('id', $id)->with('creator_department:gb_title,' . BOUserGroup::ID_KEY)
                ->withCount('post_feedbacks')
                ->first();
            if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
        } else {
            /** @var $dxmb_post */
            $dxmb_post = DXMBController::getNewsDetails($id);
            if (!$dxmb_post || !$dxmb_post['success']) return self::jsonError('Không tìm thấy dữ liệu!');
            /** @var $post */
            $post = array_first($dxmb_post['detail']);
            /** @var $data */
            $data = [
                'id' => $id,
                'title' => $post['name'],
                'description' => '',
                'content' => $get_raw? $post['detail'] : Util::escapeHTMLforWebView($post['detail']),
                'type'  => 0,
                'priority' => 0,
                'image' => isset($post['avatar'])&&$post['avatar']!=''? ('https://datxanhmienbac.com.vn' . $post['avatar']) : null,
                'creator_department'  => ['gb_title' => $post['create_by']],
                'feedback_type' => 0,
                'feedback_options'  => null,
                'video' => null,
                'created_at' => $post['createTime']
            ];
        }
        /** @var $info */
        $info = [
            'quote_background'  => Util::AVATAR_RANDOM_BG[(int) substr($id, -1)]
        ];
        return self::jsonSuccess($data, 'Thành công', $info);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listComments(Request $request, $id) {
        $post = Post::find($id);
        if (!$post) return self::jsonError('Bài viết không tồn tại hoặc đã bị xóa!');
        $page = $request->input('page', 1);
        $size = $request->input('size', 15);
        $offset = ($page - 1) * $size;
        /** @var $data */
        $data = PostFeedback::where('post_id', $id)
                ->orderBy('created_at', 'DESC')
                ->skip($offset)
                ->take($size)
                ->with(['created_by' => function($query) {
                    $query->select([BOUser::ID_KEY, 'ub_title AS name', 'ub_avatar AS avatar', 'group_ids', 'ub_account_tvc AS account'])
                        ->with('group:gb_title,'.BOUserGroup::ID_KEY);
                }])
                ->get();
        if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
        return self::jsonSuccess($data, 'Done', $request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendComment(Request $request, $id) {
        $request->validate([
            'comment' => 'string|required'
        ]);
        $post = Post::find($id);
        if (!$post) return self::jsonError('Bài viết không tồn tại hoặc đã bị xóa!');
        $query = new PostFeedback();
        $query->post_id = $id;
        $query->comment = trim($request->input('comment'));
        $query->created_by = AuthController::getCurrentUID();
        if (!$query->save()) return self::jsonError('Bình luận không thành công');
        return self::jsonSuccess($query);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function massAssign(Request $request) {
        $request->validate([
            'data' => 'required|array',
            'fields' => 'required|array'
        ]);
        $fields = $request->input('fields');
        $required_fields = ['title', 'description', 'content', 'image', 'categories',
                            'creator_department', 'created_at', 'created_by',
                            'target_departments',
                        ];
        $valid = true;
        foreach ($required_fields as $required_field) {
            if (!in_array($required_field, $fields)) {
                $valid = false;
                continue;
            }
        }
        if (!$valid) return self::jsonError('Cấu trúc dữ liệu không hợp lệ!', compact('required_fields'));
        $count = 0;
        $target_count = 0;
        /** @var $data */
        $data = $request->input('data');
        /** @var $targets */
        $targets = [];
        $now = now();
        $ids = [];
        foreach ($data as $datum) {
            $datum = (object) $datum;
            $id = Post::insertGetId([
                'title' => $datum->title,
                'description' => $datum->description,
                'content' => $datum->content,
                'type' => 0,
                'category' => str_contains(strtolower($datum->title), ['giao dịch', 'báo cáo', 'thống kê'])? 1 : 0,
                'priority' => 0,
                'image' => $datum->image,
                'bo_category_ids' => $datum->categories? json_encode(['value' => $datum->categories]) : null,
                'creator_department' => $datum->creator_department,
                'created_at' => $datum->created_at,
                'updated_at' => $now,
                'created_by' => $datum->created_by,
                'status' => env('STATUS_ACTIVE', 1),
            ]);
            if ($id > 0) {
                $count += 1;
                $ids[] = $id;
                if (count($datum->target_departments) > 0) {
                    $targets[$id] = [
                        'post_id' => $id,
                        'department_ids' => $datum->target_departments,
                        'created_at' => $datum->created_by,
                        'updated_at' => $now
                    ];
                    $query = PostTarget::insertGetId([
                        'post_id' => $id,
                        'department_ids' => json_encode(['value' => $datum->target_departments]),
                        'created_at' => $datum->created_at,
                        'updated_at' => $now
                    ]);
                    if ($query) $target_count += 1;
                }
            }
        }
        return self::jsonSuccess([
            'successes' => $count,
            'failures'  => count($data) - $count,
            'total'     => count($data),
            'target_count' => count($targets),
            'target_sent'  => $target_count,
            'posts' => $ids,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listTags(Request $request) {
        /** @var $amount */
        $amount = $request->input('amount', 10);
        /** @var $data */
        $data = Post::select('tags')->whereNotNull('tags')->take($amount)->get();
        if (!$data) return self::jsonError('Không thể truy xuất thẻ bài viết');
        if (count($data) === 0) return self::jsonSuccess([]);
        $data = join(',', $data->pluck('tags')->toArray());
        $data = explode(',', $data);
        return self::jsonSuccess(array_unique($data), 'Done', compact('amount'));
    }
}
