<?php

namespace App\Http\Controllers\API;

use App\BOBill;
use App\BOTransaction;
use App\BOUser;
use App\BOUserGroup;
use App\Notification;
use App\SMSLog;
use App\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use SoapClient;

class NotificationController extends Controller
{
    /**
     * NotificationController constructor.
     */
    public function __construct() {
        $this->middleware("laravel.jwt")->except('testFCM');
        $this->middleware("CheckStoredJWT")->except('testFCM');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        $objects = $request->input('types', []);
        $page_size = (int) $request->input('size', env('PAGE_SIZE'));
        $page = (int) $request->input('page', 1);
        $offset = ($page-1)*$page_size;
        /** @var $types */
        $types = [];
        foreach ($objects as $object) {
            $object = (int) $object;
            if (isset(Notification::OBJECTS[$object])) {
                $types = array_merge($types, Notification::OBJECTS[$object]);
            }
        }

        /** @var $data */
        $data = Notification::where(function ($query) {
                return $query
                    ->where([
                        'notifiable_id' => BOUser::getCurrent('id'),
                        'notifiable_type' => (string) BOUser::class
                    ])
                    ->orWhere([
                        ['notifiable_id', '=', BOUserGroup::findGroupOneForAll()],
                        ['notifiable_type', '=', (string) BOUserGroup::class]
                    ])
                    ->orWhere([
                        ['notifiable_id', '=', BOUser::getCurrent('group_ids')],
                        ['notifiable_type', '=', (string) BOUserGroup::class]
                    ]);
            })
            ->whereIn('type', $types)
            ->orderBy('created_at', 'DESC')
            ->skip($offset)->take($page_size)
            ->get();
        return self::jsonSuccess($data, 'Thành công', [compact('types')]);
    }

    /**
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($notification = '') {
        if ($notification == '') return self::jsonError('Yêu cầu không hợp lệ!');
        /** @var $item */
        $item = Notification::find($notification);
        if (!$item) return self::jsonError('Không tìm thấy thông báo!', compact('notification'));
        if (!$item->read_at) {
            $item->read_at = now();
            $item->save();
        }
        /** @var $data */
        $data = Notification::getAppDataByType($item->type, $item->data);
        return self::jsonSuccess($data, 'Thành công', $item);
    }

    /**
     * @param string $content
     * @param string $target
     * @param bool $api_gate
     * @return bool
     */
    public static function sendSMS($content = '', $target='', $api_gate=true) {
        if (!$content) {
            LogController::logCustomer('[sendSMS] Chưa có nội dung gửi tới SĐT: '.$target, 'error', null, true, $api_gate);
            return false;
        }
        if (!$target) return false;
        $target = Util::formatPhoneVN((string) $target);
        $payload = [
            "destination"   => $target,
            "sender"        => env('BRAND_NAME'),
            "keyword"       => "DXMB",
            "outContent"    => trim($content),
            "chargingFlag"  => "0",
            "moSeqNo"       => "0",
            "contentType"   => "0",
            "localTime"     => now()->format("YmdHij").'000',
            "userName"      => env('BRAND_USER'),
            "password"      => env('BRAND_PASSWORD')
        ];
        /** @var $client */
        $client = new SoapClient(env('BRAND_WSDL'));
        /** @var $objResponse */
        $objResponse = $client->sendMT($payload);
        /** Log SMS */
        Log::debug('[sendSMS] Đã gửi sms tới '.$target, $payload);
        $sms_log = new SMSLog();
        $sms_log->target = $target;
        $sms_log->content = $content;
        $sms_log->response = json_encode($objResponse);
        $sms_log->save();
        LogController::logCustomer('[sendSMS] Đã gửi sms tới '.$target, 'notice', null, true, $api_gate);
        if ($objResponse && starts_with($objResponse->return, '00')) return true;
        return false;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function remindApproval(Request $request) {
        $request->validate([
            'type' => 'string|required',
            'id' => 'integer|required',
        ]);
        if (!AuthController::is_sale_staff()) return self::jsonError('Thao tác chỉ dành cho NVKD!');
        /** @var $id */
        $id = $request->input('id');
        $model = BOTransaction::class;
        switch ($request->input('type')) {
            case 'transaction':
                $item = BOTransaction::where([
                    BOTransaction::ID_KEY => $id,
                    'staff_code'    => BOUser::getCurrentUID()
                ])->first();
                $allowed_statuses = [
                    BOTransaction::STATUS_CODES['REQUEST_LOCK'],
                    BOTransaction::STATUS_CODES['REQUEST_EXTEND'],
                    BOTransaction::STATUS_CODES['REQUEST_CANCEL'],
                ];
                break;
            case 'bill':
                $item = BOBill::where([
                    BOBill::ID_KEY  => $id,
                    'staff_id'      => BOUser::getCurrentUID()
                ])->first();
                $model = BOBill::class;
                $allowed_statuses = [
                    BOBill::STATUSES['STATUS_REQUEST'],
                    BOBill::STATUSES['STATUS_APPROVED'],
                    BOBill::STATUSES['STATUS_PAID_ONCE'],
                    BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST'],
                    BOBill::STATUSES['STATUS_CANCEL_REQUEST'],
                    BOBill::STATUSES['STATUS_FINALIZED'],
                ];
                break;
            default:
                return self::jsonError('Yêu cầu không hợp lệ!');
        }
        if (!$item) return self::jsonError('Không tìm thấy yêu cầu');
        /** todo: Validate Item status */
        switch ($request->input('type')) {
            case 'transaction':
                if (!in_array($item->trans_code, $allowed_statuses)) {
                    return self::jsonError('Tình trạng yêu cầu lock không hợp lệ!');
                }
                break;
            case 'bill':
                if (!in_array($item->status_id, $allowed_statuses)) {
                    return self::jsonError('Tình trạng hợp đồng không hợp lệ!');
                }
                break;
        }
        if (\App\Http\Controllers\NotificationController::notifyApprovalReminded($item, $model)) {
            return self::jsonSuccess($request->all(), 'Gửi nhắc duyệt tới TKKD/GĐS thành công!');
        }
        return self::jsonError('Xảy ra lỗi khi gửi nhắc duyệt tới TKKD/GĐS!', $request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function testFCM(Request $request) {
        $tokens = $request->input('tokens', []);
        $results = APIController::pushFCM(array_wrap($tokens), 'Test thông báo', 'Đất Xanh Miền Bắc', [
            'bo-notification' => true,
            'screen' => 'ApartmentDetails',
            'params' => [
                'id' => 1542480635
            ]
        ]);
        return self::jsonSuccess($tokens, 'Done', $results);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function testSMS(Request $request) {
        $request->validate([
            'content'   => 'string|required',
            'target'   => 'string|required',
        ]);
        $target = $request->input('target');
//        $target = Util::formatPhoneVN($target);
        $content = $request->input('content', '');
        $payload = [
            "destination"   => $target,
            "sender"        => "DATXANHMB",
            "keyword"       => "DXMB",
            "outContent"    => trim($content),
            "chargingFlag"  => "0",
            "moSeqNo"       => "0",
            "contentType"   => "0",
            "localTime"     => now()->format("YmdHij").'000',
            "userName"      => "DATXANHMB",
            "password"      => "dxmb123"
        ];
        /** @var $client */
        $client = new SoapClient(self::VNPay_SMS_Brand);
        /** @var $objResponse */
        $objResponse = $client->sendMT($payload);
        /** Log SMS */
        Log::debug('[sendSMS] Đã gửi sms tới '.$target, $payload);
        $sms_log = new SMSLog();
        $sms_log->target = $target;
        $sms_log->content = $content;
        $sms_log->response = json_encode($objResponse);
        $sms_log->save();
        LogController::logCustomer('[sendSMSTest] Đã gửi sms tới '.$target, 'notice', null, true, true);
        if ($objResponse && starts_with($objResponse->return, '00')) return self::jsonSuccess(compact('content'));
        return self::jsonError($objResponse);
    }
}
