<?php

namespace App\Http\Controllers\API\Customer;

use App\BOUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOUserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function StaffLookup(Request $request) {
        $staff_code = $request->input('staff_code', null);
        if (!$staff_code) {
            return self::jsonError('Chưa nhập mã nhân viên');
        }
        $staff = BOUser::where('ub_staff_code', $staff_code)
            ->orWhere('ub_tvc_code', $staff_code)
            ->orWhere('ub_account_tvc', $staff_code)->get()->first();
        return $staff? self::jsonSuccess($staff) : self::jsonError('Không tìm thấy nhân viên');
    }
}
