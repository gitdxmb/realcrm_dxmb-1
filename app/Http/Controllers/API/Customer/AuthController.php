<?php

namespace App\Http\Controllers\API\Customer;

use App\BOCustomer;
use App\Http\Controllers\API\APIController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $credentials = $request->only('account', 'password');
        if (!$credentials['account']) {
            return self::jsonError('Thông tin đăng nhập chưa đầy đủ');
        }
        $token = Auth::guard('api-customer')->setTTL(3600 * 24 * 7)->attempt([
            'cb_account' => $credentials['account'],
            'password' => $credentials['password']
        ]);
        $info = null;
        if ($token) {
            $info = BOCustomer::where([
                'cb_account' => $credentials['account']
            ])->first();
        }

        return $token? self::jsonSuccess(['jwt' => $token], 'Đăng nhập thành công', $info)
            : self::jsonError('Thông tin đăng nhập không chính xác!', $credentials);
    }

    /***
     *
     */
    /**
     * Get the authenticated customer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkLogin()
    {
        if (Auth::check()) {
            return response()->json(['success'=>true, 'loggedIn' => true, 'data' => Auth::user()]);
        } else {
            return response()->json(['success'=>true, 'loggedIn' => false]);
        }
    }

    /**
     * @param string $field
     * @return null
     */
    public static function getCurrentUser($field = '') {
        $customer = Auth::guard('api-customer')->user();
        if ($field) {
            return $customer->{$field}?? null;
        }
        return $customer?? null;
    }

    /**
     * @return null
     */
    public static function getCurrentUID() {
        $customer = Auth::guard('api-customer')->user();
        return $customer? $customer->{BOCustomer::ID_KEY} : null;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFCMToken(Request $request)
    {
        $token = $request->input('token', null);
        $query = BOCustomer::where([BOCustomer::ID_KEY => self::getCurrentUID()])->update(['device_token' => $token]);
        return $query? self::jsonSuccess(['token' => $token], 'Cập nhật FCM token thành công')
                : self::jsonError('Không thành công', [$token]);
    }

    /** Customer Notifications
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function notifications(Request $request) {
        $page = (int) $request->input('page', 1);
        $type = $request->input('type', null);
        $offset = ($page-1)*env('PAGE_SIZE', 25);


        $data = [];

        return $data? self::jsonSuccess($data, "Thành công!", $request->all())
            : self::jsonError('Không thành công!', $request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request) {
        $request->validate([
            'password' => 'required',
            'new_password' => 'required|min:6',
        ]);
        /** @var $user */
        $user = Auth::guard('api-customer')->attempt([
            'cb_account' => self::getCurrentUser('cb_account'),
            'password' => $request->input('password')
        ]);
        if (!$user) return self::jsonError('Thông tin tài khoản không chính xác!', $request->toArray());
        /** @var $update */
        $update = BOCustomer::where('cb_account', self::getCurrentUser('cb_account'))
                            ->update(['cb_password' => bcrypt(trim($request->input('new_password')))]);
        if (!$update) return self::jsonError('Xảy ra lỗi khi cập nhật mật khẩu!');
        return self::jsonSuccess([], 'Đổi mật khẩu thành công!');
    }
}
