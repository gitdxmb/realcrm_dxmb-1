<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DXMBController extends Controller
{
    private const HOST = 'https://datxanhmienbac.com.vn';
    private const JWT = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU5ZWQ2NDE4ZWQ1MDNhNzcwOTdiMjQzNiIsImVtYWlsIjoiY2hpbmhuY0BkeG1iLnZuIiwidXNlcm5hbWUiOiJjaGluaG5jIn0.GBkBT4r4rjkgi_l550TWHPauTZNoxtSOz8HxuKTeXJk';

    /**
     * @param int $page
     * @param int $size
     * @param string $department
     * @param string $keyword
     * @return mixed|null
     */
    public static function getInternalNews($page = 1, $size = 10, $department = "", $keyword = "") {
        if ($keyword!="") {
            return self::DXMB_POST('/api/app/findInternalByTitle', ['kw' => trim($keyword)]);
        }
        $filter = [
            'page' => $page,
            'limit' => $size,
            'department_id' => $department
        ];
        return self::DXMB_POST('/api/app/getNewinternals', $filter);
    }

    /**
     * @param int $page
     * @param int $size
     * @param string $department
     * @param string $keyword
     * @return mixed|null
     */
    public static function getHighlightedNews($page = 1, $size = 10, $department = "", $keyword = "") {
        if ($keyword!="") {
            return self::DXMB_POST('/api/app/findInternalByTitle', ['kw' => trim($keyword)]);
        }
        $filter = [
            'page' => $page,
            'limit' => $size,
            'department_id' => $department
        ];
        return self::DXMB_POST('/api/app/getHighlights', $filter);
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public static function getNewsDetails($id) {
        return self::DXMB_POST('/api/app/getDetailNewsInternal', compact('id'));
    }

    /**
     * @param Request $request
     * @return mixed|null
     */
    protected static function testAPI(Request $request) {
        $endpoint = $request->input('endpoint');
        $data = $request->input('data');
        $response = self::DXMB_POST($endpoint, $data);
        return $response? self::jsonSuccess($response, 'Done', $data) : self::jsonError($response);
    }
    /**
     * @param $endpoint
     * @param array $data
     * @param array $headers
     * @return mixed|null
     */
    private static function DXMB_POST($endpoint, $data=[], $headers=[]) {
        $response = self::_callAPI($endpoint, 'POST', $data, $headers);
        if ($response) {
            $response = json_decode($response, true);
            return $response;
        }
        return null;
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $data
     * @param array $headers
     * @return bool|string
     */
    private static function _callAPI($endpoint, $method, $data = [], $headers = []) {
        $curl = curl_init();
        $url = self::HOST . $endpoint;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: ' . self::JWT;
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            default:
                $url = sprintf("%s?%s", $url,count($data)>0?http_build_query($data):null);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){ return false; };
        curl_close($curl);
        return $result;
    }
}
