<?php

namespace App\Http\Controllers\API;

use App\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log as LogSystem;
use Illuminate\Support\Facades\Auth;
class LogController extends Controller
{
    /**
     * @param array $data
     * @return bool
     */
    public static function saveLogs($data = []) {
        if (!$data || count($data)==0) return false;
        return Log::insert($data);
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logProduct($msg, $type='info', $object=null, $save=true, $api_gate = true) {
        $data = self::generateLog(($api_gate ? '' : 'Web - ').$msg, $type, (int) $object, Log::LOG_OBJECTS['BO_PRODUCT'], AuthController::getCurrentUID($api_gate), $api_gate);
        return $save? Log::create($data) : $data;
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logTransaction($msg, $type = 'info', $object = null, $save = true, $api_gate = true) {
        $data = self::generateLog(($api_gate ? '' : 'Web - ').  $msg, $type, $object, Log::LOG_OBJECTS['BO_TRANSACTION'], AuthController::getCurrentUID($api_gate) , $api_gate);
        return $save? Log::create($data) : $data;
    }


    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logBill($msg, $type = 'info', $object = null, $save = true, $api_gate = true) {
        $data = self::generateLog(($api_gate ? '' : 'Web - ').$msg, $type, $object, Log::LOG_OBJECTS['BO_BILL'], AuthController::getCurrentUID($api_gate), $api_gate);
        return $save? Log::create($data) : $data;
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logPayment($msg, $type = 'info', $object = null, $save = true, $api_gate = true) {
        $data = self::generateLog(($api_gate ? '' : 'Web - ').$msg, $type, $object, Log::LOG_OBJECTS['BO_PAYMENT'], AuthController::getCurrentUID($api_gate), $api_gate);
        return $save? Log::create($data) : $data;
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logUser($msg, $type = 'info', $object = null, $save = true, $api_gate = true) {
        $data = self::generateLog($msg, $type, $object?? AuthController::getCurrentUID($api_gate), Log::LOG_OBJECTS['BO_USER'], AuthController::getCurrentUID($api_gate));
        return $save? Log::create($data) : $data;
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logForum($msg, $type = 'info', $object = null, $save = true, $api_gate = true) {
        $data = self::generateLog($msg, $type, $object?? AuthController::getCurrentUID($api_gate), Log::LOG_OBJECTS['FORUM'], AuthController::getCurrentUID($api_gate));
        return $save? Log::create($data) : $data;
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @param bool $api_gate
     * @return array
     */
    public static function logCustomer($msg, $type = 'info', $object = null, $save = true, $api_gate = true) {
        $data = self::generateLog($msg, $type, $object?? AuthController::getCurrentUID($api_gate), Log::LOG_OBJECTS['BO_CUSTOMER'], AuthController::getCurrentUID($api_gate));
        return $save? Log::create($data) : $data;
    }

    /**
     * @param $msg
     * @param string $type
     * @param null $object
     * @param bool $save
     * @return array
     */
    public static function logUserGroup($msg, $type = 'info', $object = null, $save = true) {
        $data = self::generateLog($msg, $type, $object, Log::LOG_OBJECTS['BO_USER_GROUP'], AuthController::getCurrentUID());
        return $save? Log::create($data) : $data;
    }

    /**
     * @param string $msg
     * @param string $type
     * @param null $object
     * @param null $object_type
     * @param null $user
     * @param bool $api_gate
     * @return array
     */
    private static function generateLog($msg = '', $type = 'info', $object = null, $object_type = null, $user = null , $api_gate = true) {
        switch ($type) {
            case 'info':
                LogSystem::info($msg, ['object' => $object, 'type' => $object_type, 'by' => $user?? AuthController::getCurrentUID($api_gate)]);
                break;
            case 'warning':
                LogSystem::warning($msg, ['object' => $object, 'type' => $object_type, 'by' => $user?? AuthController::getCurrentUID($api_gate)]);
                break;
            case 'error':
                LogSystem::error($msg, ['object' => $object, 'type' => $object_type, 'by' => $user?? AuthController::getCurrentUID($api_gate)]);
                break;
            default:
                LogSystem::notice($msg, ['object' => $object, 'type' => $object_type, 'by' => $user?? AuthController::getCurrentUID($api_gate)]);
        }
        return [
            'bo_content' => $msg,
            'bo_object' => $object? (int) $object : null,
            'bo_type' => Log::LOG_TYPES[strtoupper($type)]?? 0,
            'bo_user' => $user?? AuthController::getCurrentUID($api_gate),
            'bo_object_type' => $object_type,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
