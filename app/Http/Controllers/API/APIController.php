<?php

namespace App\Http\Controllers\API;

use App\BOBill;
use App\BOCategory;
use App\BOProduct;
use App\BOUser;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MailController;
use App\UserCustomerMapping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class APIController extends Controller
{
    const TVC_HOST = 'https://ees.dxmb.vn/directrouter/index';
    const TVC_DTB = 'DXMB';
    const DXMB_HOST = 'https://datxanhmienbac.com.vn';
    const SECURITY_CODE = 'D2EC-BDFF-763B-304D-2710-FBD3-1321-CB40-2445-AC12';
    const BO_PRIORITY_CODE = 'BO.PCN@DXMB!@#';
    const FCM_ACCESS_KEY = 'AAAARJk6EDE:APA91bGM3AC7ChzcOiRJd_TOWMHcWVLXqoXUo1tTy3_qqYGWeQ2jFppDuVg1G6D6MRaSpIbCSF1Kv8l76Skx4KL2dExpb2_KSeQab-gEqVtBQr0g7ppd6vMvAZfw9xHz_1BjrNJgJsW2';
    const TVC_HOST_ERP = 'https://erp.dxmb.vn/directrouter/index';
    /**
     * @param $endpoint
     * @param $jwt
     * @param array $body
     * @return \Illuminate\Http\JsonResponse
     */
    public static function DXMB_POST($endpoint, $jwt, $body = []) {
        $response = self::callAPI(self::DXMB_HOST, 'POST', $endpoint, $body, ['Authorization: '. $jwt]);
        return $response? json_decode($response, true) : self::jsonError("Không thành công", $response);
    }

    /**
     * @param array $body
     * @param string $endpoint
     * @return \Illuminate\Http\JsonResponse
     */
    public static function TVC_POST($body = [], $endpoint = '') {
        $tvcResponse = APIController::callAPI( self::TVC_HOST_ERP, 'POST', $endpoint, $body) ;
        return $tvcResponse? self::jsonSuccess($tvcResponse) : self::jsonError("Không thành công", $body);
    }

    /** Middleware Function - checkTVCLogin
     * @param bool $retry_login
     * @return bool
     */
    public static function _TVC_CHECK_LOGIN($retry_login = true) {
        /** @var $user */
        $user = BOUser::getCurrent('ub_account_tvc');
        if (!$user) return false;
        /** @var $data */
        $data = [
            "action" => "ConnectDB",
            "method" => "checkSession",
            "data"   => [['' => '']],
            "type"   => "rpc",
            "tid"    => rand(1,99)
        ];
        $response = self::TVC_POST_DECODE($data, '', $user);
        if ($response && $response['success'] && $response['data']) {
            if ($response['success']) return true;
        }
        if (!$retry_login) return false;
        /** @var $login_info */
        $login_info = [
            "id" => 1,
            "username" => $user,
            "password" => Auth::guard('api')->payload()->get('__TVC__'),
            "appLog" => "Y",
            "securityKey" => BOUser::getCurrent('security_code')
        ];
        $body = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => rand(1,99),
        );
        $retry = self::TVC_POST_DECODE($body, '', $user, true);
        $success = $retry&&$retry['success']&&$retry['data'];
        Log::debug('[_TVC_CHECK_LOGIN] Response: ', $retry);
        if ($success) {
            LogController::logUser('[_TVC_CHECK_LOGIN] Đăng nhập lại thành công: ' . $user, 'info');
        } else {
            LogController::logUser('[_TVC_CHECK_LOGIN] Đăng nhập lại Tavico không thành công: ' . $user, 'warning');
            Log::critical('[_TVC_CHECK_LOGIN] Đăng nhập lại không thành công: '.$user.' - Code: '.BOUser::getCurrent('security_code'));
        }
        return $success;
    }

    /**
     * @param array $body
     * @param string $endpoint
     * @param null $cookie_file
     * @param bool $new_session
     * @param bool $api_gate
     * @return array
     */
    public static function TVC_POST_DECODE($body=[], $endpoint = '', $cookie_file=null, $new_session = false, $api_gate = true) {
        $tvcResponse = APIController::callAPI( self::TVC_HOST_ERP, 'POST', $endpoint, $body, [], $cookie_file, $new_session , $api_gate) ;
        return $tvcResponse? json_decode($tvcResponse, true)['result'] : ['success'=>false, 'data'=>$body, 'message' => 'Không thể kết nối Tavico'];
    }
    public static function TVC_POST_DECODE_GET_SIGNATURE($body=[], $endpoint = '', $cookie_file=null, $new_session = false) {
        $tvcResponse = APIController::callAPI( self::TVC_HOST, 'POST', $endpoint, $body, [], $cookie_file, $new_session) ;
        return $tvcResponse? json_decode($tvcResponse, true)['result'] : ['success'=>false, 'data'=>$body, 'message' => 'Không thể kết nối Tavico'];
    }

    /**
     * @param array $data
     * @param string $endpoint
     * @return \Illuminate\Http\JsonResponse
     */
    public static function TVC_GET($data = [], $endpoint = '') {
        $tvcResponse = APIController::callAPI(self::TVC_HOST_ERP, 'GET', $endpoint, $data) ;
        return $tvcResponse? self::jsonSuccess($tvcResponse) : self::jsonError("Không thành công");
    }

    /**
     * @param Request $request
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function testAPI(Request $request) {
        $method = $request->input('method');
        $id = $request->input('id', null);
        switch ($method) {
            case 'getCurrentDepartmentHeads':
                return BOUserController::getCurrentDepartmentHeads();
                break;
            case 'getApartmentsByRole':
                $role_group = $request->input('role_group', 'PRODUCT_MANAGER');
                /** @var $projects_involved */
                $projects_involved = BOProductController::getProjectsByUserRole(self::ROLE_GROUPS[$role_group]);
                /** @var $apartments */
                $apartments = BOProductController::getApartmentsByProject($projects_involved);
                return self::jsonSuccess($apartments);
            case 'getCurrentRole':
                $role = 'sale';
                if (AuthController::is_product_manager()) $role = 'Product Manager';
                if (AuthController::is_manager()) $role = 'Manager';
                if (AuthController::is_cs_staff()) $role = 'CS Staff';
                return self::jsonSuccess($role);
            case 'listFCMTokens':
                $tokens = BOUser::select(['id', 'ub_token'])->whereNotNull('ub_token')->where('ub_status', env('STATUS_ACTIVE', 1))
                    ->pluck('ub_token')->toArray();
                return self::jsonSuccess($tokens, 'Done', ['is_array' => is_array($tokens)]);
            case 'getHost':
                return self::jsonSuccess([
                    'url' => url('/'),
                    'root' => \Illuminate\Support\Facades\Request::root()
                ]);
            case 'pushApartmentFCM':
                $apartment = BOProduct::where(BOProduct::ID_KEY, $id)->first();
                $notified = \App\Http\Controllers\NotificationController::notifyApartmentLocked($apartment);
                return self::jsonSuccess([$notified], 'Done!', $request->all());
            case 'sendContractMail':
                $mail = MailController::test($request->input('email', 'cong96bg@gmail.com'));
                return self::jsonSuccess($mail);
            case 'validateEmail':
                $email = $request->input('email', null);
                $data = $request->validate([
                    'email' => 'required|string|email'
                ]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return self::jsonError('Email không hợp lệ!', $data);
                }
                return self::jsonSuccess(compact('email'), 'Email hợp lệ!');
            case 'notifyPostCreated':
                $notified = \App\Http\Controllers\NotificationController::notifyPostCreated(null);
                return self::jsonSuccess($notified);
            default:
                $a = ['key1' => 4, 'key2' => 5];
                return self::jsonSuccess(array_search(5, $a, true), 'Done!', compact('method'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function testVNPay(Request $request) {
        $form_data = $request->all();
        $vnp_TxnRef = $form_data['order_id']; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $form_data['order_desc'];
        $vnp_OrderType = $form_data['order_type'];
        $vnp_Amount = $form_data['amount'] * 100;
        $vnp_Locale = $form_data['language'];
        $vnp_BankCode = $form_data['bank_code'];
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => HomeController::$vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => route('vnpay-response'),
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hash_data = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hash_data .= '&' . $key . "=" . $value;
            } else {
                $hash_data .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = self::$vnp_Url . "?" . $query;
        if (isset(self::$vnp_HashSecret)) {
            $vnpSecureHash = md5(self::$vnp_HashSecret . $hash_data);
            $vnp_Url .= 'vnp_SecureHashType=MD5&vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array(
            'code' => '00',
            'message' => 'success',
            'url' => $vnp_Url
        );
        return self::jsonSuccess($returnData, 'Thành công', $inputData);
    }

    /**
     * @param $host
     * @param $method
     * @param $endpoint
     * @param array $data
     * @param array $headers
     * @param null $cookie_file
     * @param bool $new_session
     * @param bool $api_gate
     * @return bool|mixed|string
     */
    public static function callAPI($host, $method, $endpoint, $data = [], $headers = [], $cookie_file=null, $new_session = false, $api_gate = true){
        $curl = curl_init();
        $url = $host . $endpoint;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            default:
                $url = sprintf("%s?%s", $url,count($data)>0?http_build_query($data):null);
        }
        if($api_gate){
            $file = $cookie_file??  BOUser::getCurrent('ub_account_tvc');
        }else{
            $file = $cookie_file??  Auth::guard("loginfrontend")->user()->ub_account_tvc;
        }
        $file = "/tmp/$file-cookies.txt";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $file);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $file);
        /** Set a new cookie session */
        curl_setopt($curl, CURLOPT_COOKIESESSION, $new_session);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){ return false; };
        curl_close($curl);
        return $result;
    }

    /**
     * @param array $tokens
     * @param string $title
     * @param string $body
     * @param array $data
     * @param string $sound
     * @return array
     */
    public static function pushFCM($tokens=[], $title='', $body='', $data=[], $sound = 'dxmb') {
        /** @var $array_splits */
        $array_splits = [];
        /** @var $results */
        $results = [];
        $data['bo-notification'] = true;
        /** @var $tokens */
        $tokens = is_array($tokens)? array_unique($tokens) : [];
        if(count($tokens) >= 1000){
            $array_splits = array_chunk($tokens, 500);
        }
        else{
            $array_splits[] = $tokens;
        }
        $fcm_endpoint = 'https://fcm.googleapis.com/fcm/send';
        $sound = $sound? $sound : 'dxmb';
        foreach ($array_splits as $item){
            $fcmNotification = [
                'registration_ids' => $item,
                'notification' => [
                    "body" => $body,
                    "title" => $title,
                    "icon" => "myicon",
                    "sound" => $sound . '.wav',
                    'link' => url('/'),
                    'click_action' => url('/'),
                ],
                'data' => $data,
                'webpush' => [
                    'fcm_options' => [
                        'link' => url('/')
                    ]
                ]
            ];
            $headers = array
            (
                'Authorization: key=' . self::FCM_ACCESS_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcm_endpoint);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $results[] = json_decode(curl_exec($ch));
            curl_close($ch);
            Log::notice('Gửi FCM tới '. count($item) . ' người dùng!', [
                'title' => $title,
                'content' => $body,
                'results' => $results
            ]);
        }
        $results['sound'] = $sound;
        return $results;
    }

    /**
     * @param $value
     * @param string $text
     * @param bool $btn_success
     * @param null $extra_role
     * @return array
     */
    public static function generateAppButtons($value, $text = '', $btn_success=true, $extra_role = null) {
        return [
            "TEXT" => $text,
            "VALUE" => $value,
            "SUCCESS" => $btn_success,
            "DANGER" => !$btn_success,
            "ROLE" => $extra_role
        ];
    }

    public static function TVC_ADD_POTENTIAL_CUSTOMER($name, $phone, $id_passport, $email="", $address="", $note="", $cookie = null, $api_gate=true) {
        $tvc_employee_code = BOUser::getCurrent('ub_tvc_code', $api_gate);
        $main = [
            [
                "prospect" => "",
                "swotDetails" => [
                    [
                        "prospect" => "",
                        "linenum" => 1,
                        "analysisdate" => "2018-08-03T00:00:00",
                        "strengths" => "aa",
                        "weaknesses" => "",
                        "opportunities" => "",
                        "threats" => ""
                    ]
                ],
                "qualificationDetails" => [
                    [
                        "prospect" => "",
                        "linenum" => 1,
                        "qualifieddate" => "2018-08-03T15:54:14",
                        "status" => "F",
                        "notes" => "fdsa"
                    ]
                ],
                "idcard" => $id_passport,
                "telephone" => $phone,
                "status" => "F",
                "name" => $name,
                "dagid" => "",
                "lookup" => "",
                "company" => "N",
                "fax" => "",
                "birthday" => 0,
                "birthmonth" => 0,
                "birthyear" => 0,
                "gender" => "M",
                "maritalstatus" => "S",
                "leadsource" => "",
                "profession" => "",
                "employee" => $tvc_employee_code,
                "address" => $address,
                "email" => $email,
                "website" => "",
                "comments" => $note,
                "contactname" => "",
                "contactposition" => "",
                "contactphone" => "",
                "contactemail" => "",
                "copcontactname" => "",
                "copcontactphone" => "",
                "copcontactemail" => "",
                "anal_prs0" => "",
                "anal_prs1" => "",
                "anal_prs2" => "",
                "anal_prs3" => "",
                "anal_prs4" => "",
                "anal_prs5" => "",
                "anal_prs6" => "KT",
                "anal_prs7" => "",
                "anal_prs8" => "",
                "anal_prs9" => "",
                "ward" => "",
                "district" => "",
                "city" => "",
                "country" => "",
                "issueddate" => "1911-01-01T00:00:00",
                "issuedplace" => "Viet Nam",
                "dateofbirth" => "",
                "street" => "",
                "extdate1" => "1911-01-01T00:00:00",
                "extdate2" => "1911-01-01T00:00:00",
                "extdate3" => "1911-01-01T00:00:00",
                "extdate4" => "1911-01-01T00:00:00",
                "extreference1" => "",
                "extreference2" => "",
                "extreference3" => "",
                "extreference4" => "",
                "extnumber1" => 0,
                "extnumber2" => 0,
                "extnumber3" => 0,
                "extnumber4" => 0,
                "extdescription1" => "",
                "extdescription2" => "",
                "extdescription3" => "",
                "extdescription4" => ""
            ]
        ];
        $body = array(
            'action' => 'FrmCrProspect',
            'method' => 'add',
            'data' => [$main],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, "", $cookie, false, $api_gate);
        Log::debug('[TVC_ADD_POTENTIAL_CUSTOMER] Debug: ', $response);
        $account = BOUser::getCurrent('ub_account_tvc', $api_gate);
        if ($response['success'] && $response['data'] && count($response['data']) > 0) {
            LogController::logTransaction('Tạo khách hàng tiềm năng TVC thành công. TK: '.$account, 'info', null, true, $api_gate);
            return $response['data'][0];
        } else {
            $msg = $response&&$response['message']? $response['message'] : json_encode($main);
            LogController::logTransaction('Không thể tạo khách hàng tiềm năng TVC - TK:'.$account.'! Debug: '.$msg, 'warning' , null, true, $api_gate);
            return false;
        }
    }

    /**
     * @param string $name
     * @param string $phone
     * @param null $tvc_employee_code
     * @param null $cookie
     * @param bool $api_gate
     * @return bool|array
     */
    public static function TVC_ADD_POTENTIAL_CUSTOMER_BK($name = '', $phone = '', $tvc_employee_code = null, $cookie = null, $api_gate = true){

        if (!$name || !$phone) {
            LogController::logTransaction('[BO-TVC_ADD_POTENTIAL_CUSTOMER] Thiếu thông tin KH: '.$name."-$phone", 'error', null, true, $api_gate);
            return false;
        }
        $tvc_employee_code = $tvc_employee_code?? BOUser::getCurrent('ub_tvc_code', $api_gate);
        $main = [
            [
                "prospect" => "",
                "swotDetails" => [
                    [
                        "prospect" => "",
                        "linenum" => 1,
                        "analysisdate" => "2018-08-03T00:00:00",
                        "strengths" => "aa",
                        "weaknesses" => "",
                        "opportunities" => "",
                        "threats" => ""
                    ]
                ],
                "qualificationDetails" => [
                    [
                        "prospect" => "",
                        "linenum" => 1,
                        "qualifieddate" => "2018-08-03T15:54:14",
                        "status" => "F",
                        "notes" => "fdsa"
                    ]
                ],
                "idcard" => "",
                "telephone" => $phone,
                "status" => "F",
                "name" => $name,
                "dagid" => "",
                "lookup" => "",
                "company" => "N",
                "fax" => "",
                "birthday" => 0,
                "birthmonth" => 0,
                "birthyear" => 0,
                "gender" => "M",
                "maritalstatus" => "S",
                "leadsource" => "",
                "profession" => "",
                "employee" => $tvc_employee_code,
                "address" => "địa chỉ",
                "email" => "",
                "website" => "",
                "comments" => "",
                "contactname" => "",
                "contactposition" => "",
                "contactphone" => "",
                "contactemail" => "",
                "copcontactname" => "",
                "copcontactphone" => "",
                "copcontactemail" => "",
                "anal_prs0" => "",
                "anal_prs1" => "",
                "anal_prs2" => "",
                "anal_prs3" => "",
                "anal_prs4" => "",
                "anal_prs5" => "",
                "anal_prs6" => "KT",
                "anal_prs7" => "",
                "anal_prs8" => "",
                "anal_prs9" => "",
                "ward" => "",
                "district" => "",
                "city" => "",
                "country" => "",
                "issueddate" => "1911-01-01T00:00:00",
                "issuedplace" => "Viet Nam",
                "dateofbirth" => "",
                "street" => "",
                "extdate1" => "1911-01-01T00:00:00",
                "extdate2" => "1911-01-01T00:00:00",
                "extdate3" => "1911-01-01T00:00:00",
                "extdate4" => "1911-01-01T00:00:00",
                "extreference1" => "",
                "extreference2" => "",
                "extreference3" => "",
                "extreference4" => "",
                "extnumber1" => 0,
                "extnumber2" => 0,
                "extnumber3" => 0,
                "extnumber4" => 0,
                "extdescription1" => "",
                "extdescription2" => "",
                "extdescription3" => "",
                "extdescription4" => ""
            ]
        ];
        $body = array(
            'action' => 'FrmCrProspect',
            'method' => 'add',
            'data' => [$main],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, "", $cookie, false, $api_gate);
        Log::debug('[TVC_ADD_POTENTIAL_CUSTOMER] Debug: ', $response);
        if ($response['success'] && $response['data'] && count($response['data']) > 0) {
            LogController::logTransaction('Tạo khách hàng tiềm năng TVC thành công. TK: '.BOUser::getCurrent('ub_account_tvc', $api_gate), 'info', null, true, $api_gate);
            return $response['data'][0];
        } else {
            $msg = $response&&$response['message']? $response['message'] : json_encode($main);
            LogController::logTransaction('Không thể tạo khách hàng tiềm năng TVC - TK:'.BOUser::getCurrent('ub_account_tvc', $api_gate).'! Debug: '.$msg, 'warning' , null, true, $api_gate);
            return false;
        }
    }

    /**
     * @param string $phone
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE($phone = '', $api_gate = true) {
        if (!$phone) return false;
        $main = [
            "reportcode" => "apiPRS01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name"=> "employee", "value" => ""],
                ["name"=> "telephone", "value" => $phone],
                ["name"=> "dtb", "value"=> "DXMB"],
                ["name"=> "prospect", "value" => ""],
                ["name"=> "idcard", "value" => ""]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$main],
            'type' => 'rpc',
            'tid' => 1,
        );
        // Process Login
        $response = APIController::TVC_POST_DECODE($body, '', null, false, $api_gate);
        if ($response['success']&&$response['data'] && count($response['data'])>0) {
            return $response['data'][0]['prospect'];
        }
        LogController::logTransaction('[TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE] Không tìm thấy KH tiềm năng: '.$phone, 'warning', null, true, $api_gate);
        return false;
    }

    /**
     * @param string $idcard
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_GET_POTENTIAL_CUSTOMER_BY_IDCARD($idcard = '', $api_gate = true) {
        if (!$idcard) return false;
        $main = [
            "reportcode" => "apiPRS01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name"=> "employee", "value" => ""],
                ["name"=> "telephone", "value" => ""],
                ["name"=> "dtb", "value"=> "DXMB"],
                ["name"=> "prospect", "value" => ""],
                ["name"=> "idcard", "value" => $idcard]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$main],
            'type' => 'rpc',
            'tid' => 1,
        );
        // Process Login
        $response = APIController::TVC_POST_DECODE($body, '', null, false, $api_gate);
        if ($response['success']&&$response['data'] && count($response['data'])>0) {
            return $response['data'][0]['prospect'];
        }
        LogController::logTransaction('[TVC_GET_POTENTIAL_CUSTOMER_BY_IDCARD] Không tìm thấy KH tiềm năng: '.$idcard, 'warning', null, true, $api_gate);
        return false;
    }

    /**
     * @param string $phone
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE_RETURN_ALL_DATA($phone = "", $api_gate = true) {
        if (!$phone) return false;
        $main = [
            "reportcode" => "apiPRS01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name"=> "employee", "value" => ""],
                ["name"=> "telephone", "value" => $phone],
                ["name"=> "dtb", "value"=> "DXMB"],
                ["name"=> "prospect", "value" => ""],
                ["name"=> "idcard", "value" => ""]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$main],
            'type' => 'rpc',
            'tid' => 1,
        );
        // Process Login
        $response = APIController::TVC_POST_DECODE($body, '', null, false, $api_gate);
        if ($response['success']&&$response['data'] && count($response['data'])>0) {
            return $response['data'][0];
        }
        LogController::logTransaction('[TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE] Không tìm thấy KH tiềm năng: '.$phone, 'warning', null, true, $api_gate);
        return false;
    }

    /**
     * @param string $customerCode
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_GET_POTENTIAL_CUSTOMER_BY_CUSTOMER_CODE_RETURN_ALL_DATA($customerCode = "", $api_gate = true) {
        if (!$customerCode) return false;
        $main = [
            "reportcode" => "apiPRS01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name"=> "employee", "value" => ""],
                ["name"=> "telephone", "value" => ""],
                ["name"=> "dtb", "value"=> "DXMB"],
                ["name"=> "prospect", "value" => $customerCode],
                ["name"=> "idcard", "value" => ""]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$main],
            'type' => 'rpc',
            'tid' => 1,
        );
        // Process Login
        $response = APIController::TVC_POST_DECODE($body, '', null, false, $api_gate);
        if ($response['success']&&$response['data'] && count($response['data'])>0) {
            return $response['data'][0];
        }
        LogController::logTransaction('[TVC_GET_POTENTIAL_CUSTOMER_BY_CUSTOMER_CODE] Không tìm thấy KH tiềm năng: '.$customerCode, 'warning', null, true, $api_gate);
        return false;
    }

    /**
     * @param string $block
     * @param string $project
     * @return bool|mixed
     */

    public static function TVC_GET_SALE_PROGRAM($block = '', $project = '', $cookie = null, $api_gate = true) {

        if (!$block || !$project) return false;
        $info = [
            "reportcode"=>"apiCMP01",
            "limit"=>25,
            "start"=>0,
            "queryFilters"=> [
                ["name"=> "block", "value"=> $block],
                ["name"=> "project", "value"=> $project],
                ["name"=> "dtb", "value"=> "DXMB"]
            ]
        ];
        $body = array (
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );

        $response = self::TVC_POST_DECODE($body, "", $cookie, false, $api_gate);
        if (!$response['success']||!$response['data']||count($response['data'])==0) return false;
        return $response['data'][0]??false;
    }

    /**
     * @param string $tvc_code
     * @param null $cookie_account
     * @return bool | null
     */
    public static function TVC_GET_EMPLOYEE_GROUP($tvc_code = '', $cookie_account = null, $api_gate = true) {
        if (!$tvc_code) $tvc_code = BOUser::getCurrent('ub_tvc_code' , $api_gate);
        $info = [
            "reportcode"=>"apiEMP01",
            "limit"=> 25,
            "start"=> 0,
            "queryFilters"=> [
                ["name"=> "employee", "value"=> $tvc_code],
                ["name"=> "dtb", "value"=> "DXMB"]
            ]
        ];
        $body = array (
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $cookie_account , false, $api_gate);
        if (!$response['success']||!$response['data']||count($response['data'])==0) return null;
        return $response['data'][0];
    }

    /**
     * @param $tvc_product
     * @param null $cookie_account
     * @return bool
     */
    private static function _TVC_GET_PRODUCT_PRICE($tvc_product, $cookie_account = null, $api_gate = true) {
        $info = [
            "reportcode" => "apiPPB01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name" => "block", "value" => ""],
                ["name" => "property", "value" => $tvc_product],
                ["name" => "project", "value" => ""],
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "pricebookid", "value" => ""]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $cookie_account, false, $api_gate);
        if (!$response['success'] || !$response['data'] || count($response['data']) == 0) return false;
        return $response['data'][0];
    }

    /**
     * @param $product_code
     * @param $potential_customer_code
     * @param int $stage
     * @param null $tvc_staff_code
     * @param string $note
     * @param null $log_transaction
     * @param null $cookie
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_BILL_CREATE($product_code, $potential_customer_code, $stage = 0, $tvc_staff_code = null, $note="", $log_transaction=null, $cookie = null, $api_gate = true) {
        if (!$product_code || !$potential_customer_code) {
            Log::error('[TVC_BILL_CREATE] Không thể tạo phiếu TVC. Sản phẩm: '.$product_code.' - KHTN: '.$potential_customer_code,[
                'note' => $note
            ]);
            return false;
        }
        if ($stage==0) {
            LogController::logTransaction('[TVC_BILL_CREATE] Không thể tạo phiếu do không rõ giai đoạn cọc/chỗ!', 'error', $log_transaction,true, $api_gate);
            return false;
        }
        /** @var $building_reference */
        $building_reference = substr($product_code, 0,6);
        /** @var $project_reference */
        $project_reference = substr($product_code, 0,3);
        /** @var todo: TVC get sale program */
        $sale_program = self::TVC_GET_SALE_PROGRAM($building_reference, $project_reference, $cookie, $api_gate);
        $employee_group = self::TVC_GET_EMPLOYEE_GROUP($tvc_staff_code?? BOUser::getCurrent('ub_tvc_code',$api_gate), $cookie, $api_gate);

        $status = null;
        if($stage == 1){
            $status = 'YCDCO';
        }else if($stage == 2){
            $status = 'YCDCH';
        }else{
            $status = 'XNDCO';
        }
        $data = [
            "systemno"=> "",
            "customer"=> $potential_customer_code,
            "status1"=> $status,
            "bookingdate"=> date("Y-m-d H:i", time()),
            "campaign"=> $sale_program? $sale_program['campaign'] : '',
            "project"=> $project_reference,
            "investor"=> $sale_program? $sale_program['investor'] : '',
            "bookingvalue"=> $sale_program? $sale_program['bookingvalue'] : 0,
            "site"=> $employee_group? $employee_group['site'] : '',
            "bankloan"=> "N",
            "pptvalue"=> null,
            "formid"=> "pctbooking6",
            "sequenceno"=> "",
            "status"=> $status == 'XNDCO' ? "C" : "W",
            "iscustomer"=> "",
            "remarks"=> $note,
            "property"=> $product_code? $product_code : '',
            "haveedit"=> "",
            "employee"=> $tvc_staff_code?? BOUser::getCurrent("ub_tvc_code", $api_gate),
            "orgchartid"=> "",
            "XNDCH"=> "",
            "XNDCO"=> "",
            "isnew"=> "Y",
            "pricebookid"=> "",
            "pymtterm"=> "",
            "contractvalue"=> 0,
            "oldcustomer"=> "",
            "pptother"=> ""

        ];
        $body = array(
            'action' => 'FrmFdUserForm',
            'method' => 'add',
            'data' => [$data],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, "", $cookie, false, $api_gate);

        if (!$response||!$response['success']||!$response['data']||count($response['data'])==0) {
            $msg = $response&&$response['message']? $response['message'] : 'KHTN: ' . $potential_customer_code;
            LogController::logTransaction('Xảy ra lỗi khi tạo phiếu '.$status.' trên TVC. Debug: '.$msg, 'error', $log_transaction , true, $api_gate);
            return false;
        }
        LogController::logTransaction('[TVC] Tạo phiếu yc thành công: '.$response['data'][0]['systemno'], 'info', $log_transaction,  true, $api_gate);
        return $response['data'][0];
    }

    /**
     * @param BOBill $bill
     * @param int $branch_check
     * @param string $branch
     * @param bool $api_gate
     * @return array
     */
    public static function TVC_CONTRACT_CREATE(BOBill $bill, $branch_check = 1, $branch = 'DXMB', $api_gate = true) {
        $cookie = null;
        /** @var $customer */
        if($branch_check == 0){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $user = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $cookie,
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $user->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie, false , $api_gate);
            Log::critical('[TVC_CONTRACT_CREATE] Đăng nhập lại TVC - TK: '. $cookie, [$login_body, $do_login]);
            if (!$do_login || !$do_login['success']) {
                LogController::logBill('[TVC] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $cookie . ' thất bại!', 'error', null, true, $api_gate);
                return ['success' => false, 'msg' => 'Không thể tạo HĐ chéo sàn trên Tavico'];
            }
        }
        /** @var $customer */
        $customer = UserCustomerMapping::select('potential_reference_code')->where([
            'id_user'       => $bill->staff_id,
            'id_customer'   => $bill->customer_id
        ])->first();
        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        if (!$customer||!$customer_info) return ['success' => false, 'msg' => 'Không tìm thấy thông tin khách hàng'];
        /** @var $customer_code */
        $customer_code = $customer->potential_reference_code;
        /** @var $staff */
        $staff = BOUser::select('ub_tvc_code')->where(BOUser::ID_KEY, $bill->staff_id)->first();
        if (!$staff || !$staff->ub_tvc_code) return ['success' => false, 'msg' => 'Không tìm thấy thông tin nhân viên'];

        /** @var $product */
        $product = BOProduct::select('pb_code')->where(BOProduct::ID_KEY, $bill->product_id)->first();
        if (!$product || !$product->pb_code) return ['success' => false, 'msg' => 'Không tìm thấy thông tin căn hộ'];

        /** @var $tvc_official_customer */   
        //check khach hàng chính thức theo idcard
        $tvc_official_customer = self::_TVC_GET_OFFICIAL_CUSTOMER('','', $customer_info->id_passport, $cookie, $api_gate);
        /** @var $tvc_official_customer_code */
        $tvc_official_customer_code = null;
        if ($tvc_official_customer&&$tvc_official_customer['nadcode']) {
                $tvc_official_customer_code = $tvc_official_customer['nadcode'];
                LogController::logBill('[TVC_CONTRACT_CREATE] Mã khách hàng chính thức Vừa lấy được: '.$tvc_official_customer_code, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
        } else {
            /** @var $tvc_convert_customer */            
            $tvc_official_customer = self::_TVC_GET_OFFICIAL_CUSTOMER($customer_info->phone, '', '', $cookie, $api_gate);
            if ($tvc_official_customer&&$tvc_official_customer['nadcode']) {
                $tvc_official_customer_code = $tvc_official_customer['nadcode'];
                LogController::logBill('[TVC_CONTRACT_CREATE] Đã lấy được mã khách hàng chính thức qua SĐT: '.$tvc_official_customer_code. ". Tài khoản: $cookie", 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
            } else {
                if (!$customer_code) {
                    LogController::logBill('[TVC_CONTRACT_CREATE] HĐ chưa có khách hàng tiềm năng! Tài khoản: '.$cookie, 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                    return ['success' => false, 'msg' => '[TVC_CONTRACT_CREATE] Không tìm thấy khách hàng tiềm năng trên Tavico'];
                }
                // cập nhật khách hàng tiềm năng nè config('cmconst.account_CRM')['DXMB']['username'];' 
                $aryUpdatePotential = array();
                $aryUpdatePotential['prospect'] = $customer_code ? $customer_code : '';
                $aryUpdatePotential['idcard'] = $customer_info->id_passport ? $customer_info->id_passport : '';
                $aryUpdatePotential['address'] = $customer_info->permanent_address ? $customer_info->permanent_address : '';
                $aryUpdatePotential['comments'] = $customer_info->address ? $customer_info->address : '';
                $aryUpdatePotential['name'] = $customer_info->name ? $customer_info->name : '';
                $aryUpdatePotential['email'] = $customer_info->email ? $customer_info->email : '';
                self::TVC_UPDATE_POTENTIAL($aryUpdatePotential,config('cmconst.account_CRM')[$branch]['username'], $api_gate);
                $tvc_convert_customer = self::_TVC_CONVERT_POTENTIAL_CUSTOMER($customer_code, $customer_info->phone, $customer_info->name,$customer_info->id_passport,$customer_info->permanent_address,$customer_info->address,config('cmconst.account_CRM')[$branch]['username'], $api_gate);
                if (!$tvc_convert_customer) {
                    LogController::logBill('[TVC_CONTRACT_CREATE] Xảy ra lỗi khi tạo khách hàng chính thức! Tài khoản: '.$cookie, 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                    return ['success' => false, 'msg' => '[TVC] Xảy ra lỗi khi tạo khách hàng chính thức'];
                }
                /** @var $tvc_official_customer */
                $tvc_official_customer = self::_TVC_GET_OFFICIAL_CUSTOMER($customer_info->phone, '', '', $cookie, $api_gate);
                if ($tvc_official_customer && $tvc_official_customer['nadcode']) {
                    $tvc_official_customer_code = $tvc_official_customer['nadcode'];
                    LogController::logBill('[TVC_CONTRACT_CREATE] Mã khách hàng chính thức mới tạo: ' . $tvc_official_customer_code, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
                } else {
                    LogController::logBill('[TVC_CONTRACT_CREATE] Không thể lấy mã khách hàng chính thức mới tạo! Tài khoản: '.$cookie, 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                    return ['success' => false, 'msg' => '[TVC] Xảy ra lỗi khi lấy khách hàng chính thức!'];
                }
            }
        }
        /** todo: On Official Customer retrieved */
        $customer->customer_reference_code = $tvc_official_customer_code;
        $customer->save();
        $tvc_employee = self::TVC_GET_EMPLOYEE_GROUP($staff->ub_tvc_code, $cookie, $api_gate);
        $tvc_price = self::_TVC_GET_PRODUCT_PRICE($product->pb_code, $cookie, $api_gate);
        $info = [
            [
                "contracttype" => substr($product->pb_code,0,3),
                "status" => "CDDCO",
                "dagid" => "",
                "property" => $product->pb_code,
                "clientcode" => $tvc_official_customer_code,
                "site" => $tvc_employee? $tvc_employee['site'] : '',
                "personincharge" => "",
                "orgchartid" => "",
                "pymtterm" => "",
                "pricebookid" => $tvc_price? $tvc_price['pricebookid'] : '',
                "contractvalue" => $tvc_price['propertyvalue'] ? $tvc_price['propertyvalue'] : "", // gia trị hợp đồng
                "effectivedate" => "",
                "signedby" => "",
                "startdate" => date("Y-m-d H:i", time()),
                "enddate" => null,
                "notes" => "",
                "bankloan" => "N",
                "bankref" => "",
                "bankcode" => "",
                "loanvalue" => 0,
                "loandate" => null,
                "loannotes" => "",
                "value0" => 0,
                "value1" => 0,
                "value2" => 0,
                "value3" => 0,
                "value4" => 0,
                "value5" => 0,
                "value6" => 0,
                "value7" => $tvc_price['value7'] ? $tvc_price['value7'] : 0,// gia tri dat coc quy dinh
                "value8" => 0,
                "value9" => 0,
                "anal_pct0" => "",
                "anal_pct1" => "",
                "anal_pct2" => "",
                "anal_pct3" => "",
                "anal_pct4" => "",
                "anal_pct5" => $bill->bill_reference_code,
                "anal_pct6" => "",
                "anal_pct7" => "",
                "anal_pct8" => "",
                "anal_pct9" => "",
                "byconstprogress" => "N",
                "states" => [
                    [
                        "linenum" => 1,
                        "status" => "CDDCO",
                        "statusdate" => date("Y-m-d H:i", time()),
                        "statuscode" => "",
                        "changeby" => "",
                        "remarks" => "",
                        "customerfrom" => "",
                        "customerto" => ""
                    ]
                ],
                "salepersons" => [
                    [
                        "contractid" => "",
                        "saleperson" => "",
                        "site" => "",
                        "orgchartid" => "",
                        "status" => "",
                        "commissionrate" => 0,
                        "remarks" => "",
                        "comagree" => ""
                    ]
                ],
                "promotionapprover" => ""
            ]
        ];
        $body = array(
            'action' => 'FrmPmContract',
            'method' => 'add',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );

        /** @var $response */
        $response = self::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response['success']||!$response['data']||count($response['data'])==0) {
            LogController::logBill('[TVC_CONTRACT_CREATE] Xảy ra lỗi khi tạo hợp đồng bằng tài khoản: '.$cookie.'! TVC msg: '.$response['message'], 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return ['success' => false, 'msg' => 'Xảy ra lỗi khi tạo hợp đồng trên Tavico'];
        }
        $tvc_contract = $response['data'][0]['contractid']?? null;
        LogController::logBill('[TVC_CONTRACT_CREATE] Khởi tạo hợp đồng Tavico thành công: '.$tvc_contract, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
        if (!$tvc_contract) {
            LogController::logBill('[TVC_CONTRACT_CREATE] Không thể truy xuất mã HĐ mới tạo! Tài khoản: '.$cookie, 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
        }

        /** @var $product_data */
        $product_data = [
            'property' => $product->pb_code,
            'status' => 'CDDCO',
        ];
        /** @var $tvc_update_product */
        if($branch_check != 0){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $user = BOUser::where('ub_account_tvc', $cookie)->first();
            $login_info = [
                "id" => 1,
                "username" => $cookie,
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $user->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie, false, $api_gate);
            if (!$do_login || !$do_login['success']) {
                LogController::logBill('[TVC_CONTRACT_CREATE] Không thể đăng nhập tài khoản hệ thống ' . $cookie, 'warning', null, true, $api_gate);
            }
        }
        $tvc_update_product = APIController::TVC_UPDATE_PRODUCT([$product_data],'upd', $cookie, $api_gate);
        if (!$tvc_update_product) {
            LogController::logProduct('[TVC_CONTRACT_CREATE] Không thể cập nhật trạng thái căn hộ! TK: '.$cookie, 'warning', null, true, $api_gate);
        }
        return ['success' => true, 'data' => $tvc_contract];
    }

    /**
     * @param BOBill $bill
     * @param int $branch_check
     * @param string $branch
     * @param null $staffTvcCode
     * @param bool $api_gate
     * @return array
     */
    public static function TVC_CONTRACT_CREATE_F1(BOBill $bill, $branch_check = 1, $branch = 'DXMB', $staffTvcCode = null, $api_gate = true) {
        $cookie = null;
        /** @var $customer */
        if($branch_check == 0){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $user = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $cookie,
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $user->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie, false, $api_gate);
            Log::critical('[TVC_CONTRACT_CREATE_F1] Đăng nhập lại TVC - TK: '. $cookie, [$login_body, $do_login]);
            if (!$do_login || !$do_login['success']) {
                LogController::logBill('[TVC_CONTRACT_CREATE_F1] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $cookie . ' thất bại!', 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                return ['success' => false, 'msg' => 'Không thể tạo HĐ chéo sàn trên Tavico'];
            }
        }
        /** @var $customer */
        $customer = UserCustomerMapping::select(['potential_reference_code','potential_reference_code_F1'])->where([
            'id_user'       => $bill->staff_id,
            'id_customer'   => $bill->customer_id
        ])->first();
        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        if (!$customer||!$customer_info) return ['success' => false, 'msg' => 'Không tìm thấy thông tin khách hàng'];
        /** @var $customer_code */
        $customer_code = $customer->potential_reference_code_F1;

        /** @var $product */
        $product = BOProduct::select('pb_code')->where(BOProduct::ID_KEY, $bill->product_id)->first();
        if (!$product || !$product->pb_code) return ['success' => false, 'msg' => 'Không tìm thấy thông tin căn hộ'];

        /** @var $tvc_official_customer */
        
        //check khach hàng chính thức theo idcard
        $tvc_official_customer = self::_TVC_GET_OFFICIAL_CUSTOMER('','', $customer_info->id_passport, $cookie, $api_gate);
        /** @var $tvc_official_customer_code */
        $tvc_official_customer_code = null;
        if ($tvc_official_customer&&$tvc_official_customer['nadcode']) {
            $tvc_official_customer_code = $tvc_official_customer['nadcode'];            
            LogController::logBill('[TVC_CONTRACT_CREATE_F1] Mã khách hàng chính thức F1 lấy được qua CMND: '.$tvc_official_customer_code, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
        } else {
            if (!$customer_code) {
                LogController::logBill('[TVC_CONTRACT_CREATE_F1] HĐ chưa có khách hàng tiềm năng F1! Tài khoản: '.$cookie, 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                return ['success' => false, 'msg' => '[TVC_CONTRACT_CREATE_F1] Không tìm thấy khách hàng F1 tiềm năng trên Tavico'];
            }
            /** @var $tvc_convert_customer */
            $tvc_official_customer = self::_TVC_GET_OFFICIAL_CUSTOMER($customer_info->phone, '', '', $cookie, $api_gate);
            if ($tvc_official_customer&&$tvc_official_customer['nadcode']) {
                $tvc_official_customer_code = $tvc_official_customer['nadcode'];
                LogController::logBill('[TVC_CONTRACT_CREATE_F1] Đã lấy được mã khách hàng chính thức F1 qua SĐT: '.$tvc_official_customer_code. ". Tài khoản: $cookie", 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
            } else {
                // cập nhật khách hàng tiềm năng nè config('cmconst.account_CRM')['DXMB']['username'];'
                $aryUpdatePotential = array();
                $aryUpdatePotential['prospect'] = $customer_code ? $customer_code : '';
                $aryUpdatePotential['idcard'] = $customer_info->id_passport ? $customer_info->id_passport : '';
                $aryUpdatePotential['address'] = $customer_info->permanent_address ? $customer_info->permanent_address : '';
                $aryUpdatePotential['comments'] = $customer_info->address ? $customer_info->address : '';
                $aryUpdatePotential['name'] = $customer_info->name ? $customer_info->name : '';
                $aryUpdatePotential['email'] = $customer_info->email ? $customer_info->email : '';
                
                self::TVC_UPDATE_POTENTIAL($aryUpdatePotential,$cookie, $api_gate);
                $tvc_convert_customer = self::_TVC_CONVERT_POTENTIAL_CUSTOMER($customer_code, $customer_info->phone, $customer_info->name,$customer_info->id_passport,$customer_info->permanent_address,$customer_info->address, $cookie, $api_gate);
                if (!$tvc_convert_customer) {
                    LogController::logBill('[TVC_CONTRACT_CREATE_F1] Xảy ra lỗi khi tạo khách hàng chính thức F1! Tài khoản: '.$cookie, 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                    return ['success' => false, 'msg' => '[TVC] Xảy ra lỗi khi tạo khách hàng chính thức F1'];
                }
                /** @var $tvc_official_customer */
                $tvc_official_customer = self::_TVC_GET_OFFICIAL_CUSTOMER($customer_info->phone, '', '', $cookie, $api_gate);
                if ($tvc_official_customer && $tvc_official_customer['nadcode']) {
                    $tvc_official_customer_code = $tvc_official_customer['nadcode'];
                    LogController::logBill('[TVC_CONTRACT_CREATE_F1] Mã khách hàng chính thức F1 mới tạo: ' . $tvc_official_customer_code, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
                } else {
                    LogController::logBill('[TVC_CONTRACT_CREATE_F1] Không thể lấy mã khách hàng chính thức F1 mới tạo! Tài khoản: '.$cookie, 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
                    return ['success' => false, 'msg' => '[TVC] Xảy ra lỗi khi lấy khách hàng chính thức F1!'];
                }
            }
        }
        /** todo: On Official Customer retrieved */
        $customer->customer_reference_code_F1 = $tvc_official_customer_code;
        $customer->save();
        $tvc_employee = self::TVC_GET_EMPLOYEE_GROUP($staffTvcCode, $cookie, $api_gate);
        $tvc_price = self::_TVC_GET_PRODUCT_PRICE($product->pb_code, $cookie, $api_gate);
        $info = [
            [
                "contracttype" => substr($product->pb_code,0,3),
                "status" => "CDDCO",
                "dagid" => "",
                "property" => $product->pb_code,
                "clientcode" => $tvc_official_customer_code,
                "site" => $tvc_employee? $tvc_employee['site'] : '',
                "personincharge" => "",
                "orgchartid" => "",
                "pymtterm" => "",
                "pricebookid" => $tvc_price? $tvc_price['pricebookid'] : '',
                "contractvalue" => $tvc_price['propertyvalue'] ? $tvc_price['propertyvalue'] : "", // gia trị hợp đồng
                "effectivedate" => "",
                "signedby" => "",
                "startdate" => date("Y-m-d H:i", time()),
                "enddate" => null,
                "notes" => "",
                "bankloan" => "N",
                "bankref" => "",
                "bankcode" => "",
                "loanvalue" => 0,
                "loandate" => null,
                "loannotes" => "",
                "value0" => 0,
                "value1" => 0,
                "value2" => 0,
                "value3" => 0,
                "value4" => 0,
                "value5" => 0,
                "value6" => 0,
                "value7" => $tvc_price['value7'] ? $tvc_price['value7'] : 0,// gia tri dat coc quy dinh
                "value8" => 0,
                "value9" => 0,
                "anal_pct0" => "",
                "anal_pct1" => "",
                "anal_pct2" => "",
                "anal_pct3" => "",
                "anal_pct4" => "",
                "anal_pct5" => $bill->bill_reference_code_F1,
                "anal_pct6" => "",
                "anal_pct7" => "",
                "anal_pct8" => "",
                "anal_pct9" => "",
                "byconstprogress" => "N",
                "states" => [
                    [
                        "linenum" => 1,
                        "status" => "CDDCO",
                        "statusdate" => date("Y-m-d H:i", time()),
                        "statuscode" => "",
                        "changeby" => "",
                        "remarks" => "",
                        "customerfrom" => "",
                        "customerto" => ""
                    ]
                ],
                "salepersons" => [
                    [
                        "contractid" => "",
                        "saleperson" => "",
                        "site" => "",
                        "orgchartid" => "",
                        "status" => "",
                        "commissionrate" => 0,
                        "remarks" => "",
                        "comagree" => ""
                    ]
                ],
                "promotionapprover" => ""
            ]
        ];
        $body = array(
            'action' => 'FrmPmContract',
            'method' => 'add',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );

        /** @var $response */
        $response = self::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response['success']||!$response['data']||count($response['data'])==0) {
            LogController::logBill('[TVC_CONTRACT_CREATE_F1] Xảy ra lỗi khi tạo hợp đồng F1 bằng tài khoản: '.$cookie.'! TVC msg: '.$response['message'], 'error', $bill->{BOBill::ID_KEY}, true, $api_gate);
            return ['success' => false, 'msg' => 'Xảy ra lỗi khi tạo hợp đồng F1 trên Tavico'];
        }
        $tvc_contract = $response['data'][0]['contractid']?? null;
        LogController::logBill('[TVC_CONTRACT_CREATE_F1] Khởi tạo hợp đồng F1 Tavico thành công: '.$tvc_contract, 'info', $bill->{BOBill::ID_KEY}, true, $api_gate);
        if (!$tvc_contract) {
            LogController::logBill('[TVC_CONTRACT_CREATE_F1] Không thể truy xuất mã HĐ F1 mới tạo! Tài khoản: '.$cookie, 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
        }

        /** @var $product_data */
        $product_data = [
            'property' => $product->pb_code,
            'status' => 'CDDCO',
        ];
        /** @var $tvc_update_product */

        $tvc_update_product = APIController::TVC_UPDATE_PRODUCT([$product_data],'upd', $cookie, $api_gate);
        if (!$tvc_update_product) {
            LogController::logBill('[TVC_CONTRACT_CREATE_F1] Không thể cập nhật trạng thái căn hộ F1! TK: '.$cookie, 'warning', $bill->{BOBill::ID_KEY}, true, $api_gate);
        }
        return ['success' => true, 'data' => $tvc_contract];
    }

    /**
     * @param $potential_code
     * @param string $phone
     * @param string $name
     * @param string $idcard
     * @param string $address
     * @param string $comment
     * @param null $cookie
     * @param bool $api_gate
     * @return bool
     */
    private static function _TVC_CONVERT_POTENTIAL_CUSTOMER($potential_code, $phone = '', $name = '', $idcard='', $address = '', $comment = '', $cookie=null, $api_gate = true) {
        if (!$potential_code) return false;
        $info = [
            "prospect"=> $potential_code,
            "name"=> $name,
            "status"=> "",
            "dagid"=> "",
            "lookup"=> "",
            "telephone"=> $phone,
            "fax"=> "",
            "issueddate"=> null,
            "issuedplace"=> "",
            "birthday"=> 0,
            "birthmonth"=> 0,
            "birthyear"=> 0,
            "idcard"=> $idcard,
            "gender"=> "",
            "maritalstatus"=> "",
            "employee"=> "",
            "address"=> $address,
            "email"=> "",
            "website"=> "",
            "comments"=> $comment,
            "taxcode"=> ""
        ];
        $body = array(
            'action' => 'FrmCrProspect',
            'method' => 'convertToCustomer',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 120,
        );
        $response =  APIController::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response || !$response['success']) {
            $msg = $response&&$response['message']? $response['message'] : json_encode($info);
            Log::error('[_TVC_CONVERT_POTENTIAL_CUSTOMER] Không thể Chuyển khách hàng sang chính thức bằng tài khoản '.$cookie.'! Debug: '.$msg, [
                'phone' => $phone,
                'potential_code' => $potential_code,
                'id_card' => $idcard
            ]);
            return false;
        }
        return true;
    }

    /**
     * @param string $phone
     * @param string $potential_code
     * @param string $idcard
     * @param null $cookie
     * @param bool $api_gate
     * @return bool
     */
    private static function _TVC_GET_OFFICIAL_CUSTOMER($phone = '', $potential_code = '', $idcard = '', $cookie = null, $api_gate = true) {
        if ($phone=='' && $potential_code=='') return false;
        $info = [
            "reportcode" => "apiNAD02",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name" => "saleperson", "value" => ""],
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "prospect", "value" => $potential_code],
                ["name" => "telephone", "value" => $phone],
                ["name" => "idcard", "value" => $idcard],
                ["name" => "nadcode", "value" => ""],
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => 'qry',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response =  APIController::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response['success'] || !$response['data'] || !$response['data'][0]) return false;
        return $response['data'][0];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function testImageUpload(Request $request) {
        $image = $request->input('image', null);
        if (!$image) return self::jsonError('Bạn chưa chọn ảnh!');
        $image = str_replace(' ', '+', $image);
        $image = str_after($image, 'data:image/jpeg;base64,');
        $imageName = 'TEST-' . str_random(2) . '.jpg';
        $root_path = public_path('uploads' . '/' . $imageName);
        File::put($root_path, base64_decode($image));
        $attachment = '/uploads/' . $imageName;
        $optimized = '/uploads/optimized/'.$imageName;

        $optimizerChain = OptimizerChainFactory::create();
        $optimizerChain->optimize(public_path($attachment), public_path($optimized));
        return [
            url($attachment),
            url($optimized)
        ];
    }

    /** Update Booking - Deposit Status Tavico */
    /**
     * @param $systemno
     * @param string $statusName
     * @param int $bill
     * @param int $branch_check
     * @param string $branch
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_UPDATE_BOOKING_STATUS($systemno, $statusName = 'XNDCO', $bill = 0, $branch_check = 1, $branch = 'DXMB', $api_gate = true) {
        $cookie = null;
        if($statusName == 'XNDCO'){
            $status = 'C';
        }else if($statusName == 'XNDCH'){
            $status = 'W';
        }else{
            $status = 'X';
        }

        if($branch_check == 0){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $usercrm = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $config_account[$branch]['username'],
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $usercrm->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie, false, $api_gate);
            if (!$do_login) {
                LogController::logBill('[TVC_UPDATE_BOOKING_STATUS] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account[$branch]['username'] . ' thất bại!', 'error', null, true, $api_gate);
                return false;
            }
        }
        $info = [
            "systemno" => $systemno,
            "status1" => $statusName,
            "formid" => "pctbooking6",
            "status" => $status
        ];
        $body = array(
            'action' => 'FrmFdUserForm',
            'method' => 'upd',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response['success']){
            LogController::logBill('[TVC_UPDATE_BOOKING_STATUS] Xảy ra lỗi khi Thay đổi trạng thái phiếu! TVC msg: '.$response['message'], 'error', $bill, true, $api_gate);
            return false;
        }
        LogController::logBill('[TVC_UPDATE_BOOKING_STATUS] Thay đổi trạng thái phiếu thành công! TVC msg: '.$response['message'], 'info', $bill, true, $api_gate);
        return $response;
    }

    /** NV hoặc Khách huỷ phiếu yêu cầu */
    /**
     * @param $tvc_contract
     * @param int $bo_bill_id
     * @param bool $is_customer
     * @param string $branch
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_CANCEL_BOOKING_REQUEST($tvc_contract, $bo_bill_id = 0, $is_customer = false, $branch = 'DXMB', $api_gate = true) {
        /** @var $cookie */
        $cookie = null;
        if($is_customer){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $usercrm = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $config_account[$branch]['username'],
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $usercrm->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie, false, $api_gate);
            if (!$do_login) {
                LogController::logBill('[TVC_CANCEL_BOOKING_REQUEST] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account[$branch]['username'] . ' thất bại!', 'error', null, true, $api_gate);
                return false;
            }

        }
        $info = [
            "systemno" => $tvc_contract,
            "formid" => "pctbooking6",
            "status" => "X"
        ];
        $body = array(
            'action' => 'FrmFdUserForm',
            'method' => 'upd',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        /** @var $response */
        $response = APIController::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response['success']){
            LogController::logBill('[TVC_CANCEL_BOOKING_REQUEST] Xảy ra lỗi khi Thay đổi trạng thái phiếu yc: '.$tvc_contract.' - Cookie: '.$cookie.'! TVC msg: '.$response['message'], 'error', $bo_bill_id, true, $api_gate);
            return false;
        }
        LogController::logBill('[TVC_CANCEL_BOOKING_REQUEST] Thay đổi trạng thái phiếu yc thành công: '.$tvc_contract.' - Cookie: '.$cookie.'! TVC msg: '.$response['message'], 'info', $bo_bill_id, true, $api_gate);
        return $response;
    }

    /** Chuyển giao dịch sang Đặt cọc */
    /**
     * @param $tvc_contract
     * @param null $log_bill
     * @param bool $check_branch
     * @param string $branch
     * @param string $productCode
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_CONTRACT_DEPOSITED($tvc_contract, $log_bill=null, $check_branch = true, $branch = 'DXMB', $productCode ="", $api_gate = true) {
        $cookie = null;
        if(!$check_branch){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $usercrm = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $config_account[$branch]['username'],
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $usercrm->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie, false, $api_gate);
            if (!$do_login) {
                LogController::logBill('[TVC_CONTRACT_DEPOSITED] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account[$branch]['username'] . ' thất bại!', 'error', $log_bill, true, $api_gate);
                return false;
            }
        }
        if($productCode != ''){
            $dataUpdateProduct[] = [
                'property' => $productCode,
                'status' => 'DCO',
            ];
            APIController::TVC_UPDATE_PRODUCT($dataUpdateProduct,'upd', $cookie, $api_gate);
        }

        return self::_TVC_UPDATE_CONTRACT_STATUS($tvc_contract, 'DCO', $cookie, $log_bill, $api_gate);
    }

    /** Cập nhật tình trạng giao dịch, hợp đồng TVC */
    /**
     * @param $tvc_contract
     * @param $new_status
     * @param null $cookie
     * @param null $log_bill
     * @param bool $api_gate
     * @return array|bool
     */
    private static function _TVC_UPDATE_CONTRACT_STATUS($tvc_contract, $new_status, $cookie = null, $log_bill = null, $api_gate = true) {
        if (!$tvc_contract || !$new_status) {
            LogController::logBill('[_TVC_UPDATE_CONTRACT_STATUS] Không thể cập nhập HĐ TVC do thiếu dữ liệu! '.$tvc_contract.'-'.$new_status, 'error', $log_bill, true, $api_gate);
            return false;
        }
        /** @var $tvc_contract_details */
        $tvc_contract_details = self::_TVC_GET_CONTRACT($tvc_contract, $cookie ,$api_gate);
        /** @var $states */
        $states = [];
        /** @var $line */
        $line = 1;
        if($tvc_contract_details && count($tvc_contract_details) > 0){
            $line += count($tvc_contract_details);
            foreach($tvc_contract_details as $item){
                $states[] = [
                    "contractid" => $tvc_contract,
                    "linenum" => $item['linenum'],
                    "status" => $item['status'],
                    "statusdate" => $item['statusdate'],
                ];
            }
        } else {
            LogController::logBill('[_TVC_UPDATE_CONTRACT_STATUS] Không tìm thấy HĐ: '.$tvc_contract . '. ', 'warning', $log_bill ,true, $api_gate);
        }
        $states[] = [
            "contractid" => $tvc_contract,
            "linenum" => $line,
            "status" => $new_status,
            "statusdate" => date('Y-m-d H:i'),
        ];
        /** @var $info */
        $info = [
            [
                "contractid" => $tvc_contract,
                "status" => $new_status,
                "states" => $states
            ]
        ];
        $body = array(
            'action' => 'FrmPmContract',
            'method' => 'upd',
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response || !$response['success']) {
            $msg = '[_TVC_UPDATE_CONTRACT_STATUS] Cập nhật HĐ '.$tvc_contract.' sang trạng thái '.$new_status.' thất bại: '.$response['message'];
            LogController::logBill($msg, 'error', $log_bill, true, $api_gate);
        } else {
            $msg = '[_TVC_UPDATE_CONTRACT_STATUS] Đã cập nhật HĐ '.$tvc_contract.' sang trạng thái '.$new_status.': '.$response['message'];
            LogController::logBill($msg, 'info', $log_bill, true, $api_gate);
        }
        return $response;
    }

    /**  DVKH thanh lý hợp đồng */
    /**
     * @param $tvc_contract
     * @param null $log_bill
     * @param bool $check_branch
     * @param string $branch
     * @param string $productCode
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_CS_CANCEL_CONTRACT($tvc_contract, $log_bill = null, $check_branch = true, $branch = 'DXMB', $productCode ="", $api_gate = true) {
        $cookie = null;
        if(!$check_branch){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $usercrm = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $config_account[$branch]['username'],
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $usercrm->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie , false , $api_gate);
            if (!$do_login) {
                LogController::logBill('[TVC_CS_CANCEL_CONTRACT] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account[$branch]['username'] . ' thất bại!', 'error', null , true, $api_gate);
                return false;
            }
        }
        if($productCode != ''){
            $dataUpdateProduct[] = [
                'property' => $productCode,
                'status' => 'MBA',
            ];
            APIController::TVC_UPDATE_PRODUCT($dataUpdateProduct,'upd', $cookie, $api_gate);
        }

        return self::_TVC_UPDATE_CONTRACT_STATUS($tvc_contract, 'TLYDCOHC', $cookie, $log_bill, $api_gate);
    }

    /** TKKD chuyển tình trạng: Chờ duyệt Giao dịch thành công */
    /**
     * @param $tvc_contract
     * @param $product_code
     * @param bool $check_branch
     * @param null $log_bill
     * @param string $branch
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_PRODUCT_MANAGER_UPDATE_CONTRACT_STATUS_CDGDTC($tvc_contract, $product_code, $check_branch = true, $log_bill=null, $branch = 'DXMB', $api_gate = true) {
        $cookie = null;
        if(!$check_branch) {
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $usercrm = BOUser::where('ub_account_tvc', $config_account[$branch]['username'])->first();
            $login_info = [
                "id" => 1,
                "username" => $config_account[$branch]['username'],
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $usercrm->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie , false, $api_gate);
            if (!$do_login || !$do_login['success']) {
                LogController::logBill('[TVC_PRODUCT_MANAGER_UPDATE_CONTRACT_STATUS_CDGDTC] Không thể cập nhật HĐ do đăng nhập tài khoản hệ thống ' . $config_account[$branch]['username'] . ' thất bại!', 'error', null , true, $api_gate);
                return false;
            }
        }
        self::_TVC_UPDATE_CONTRACT_STATUS($tvc_contract, 'CDGDTC', $cookie, $log_bill , $api_gate);
        /** @var $product_data */
        $product_data = [
            'property' => $product_code,
            'status' => 'HDO',
        ];
        /** @var $tvc_update_product */
        if($check_branch){
            $config_account = config('cmconst.account_CRM');
            $cookie = $config_account[$branch]['username'];
            $user = BOUser::where('ub_account_tvc', $cookie)->first();
            $login_info = [
                "id" => 1,
                "username" => $cookie,
                "password" => $config_account[$branch]['pwd'],
                "appLog" => "Y",
                "securityKey" => $user->security_code
            ];
            $login_body = array(
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $do_login = self::TVC_POST_DECODE($login_body, '', $cookie , false, $api_gate);
            if (!$do_login || !$do_login['success']) {
                LogController::logBill('[TVC_PRODUCT_MANAGER_UPDATE_CONTRACT_STATUS_CDGDTC] Không thể đăng nhập tài khoản hệ thống ' . $cookie, 'warning', null, true, $api_gate);
            }
        }
        $tvc_update_product = APIController::TVC_UPDATE_PRODUCT([$product_data],'upd', $cookie , $api_gate);
        if (!$tvc_update_product) {
            LogController::logProduct('[TVC_PRODUCT_MANAGER_UPDATE_CONTRACT_STATUS_CDGDTC] Không thể cập nhật trạng thái căn hộ! TK: '.$cookie, 'warning', null, true , $api_gate);
            return false;
        }
        return true;
    }

    /**
     * @param array $info
     * @param string $type
     * @param null $username
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_UPDATE_PRODUCT($info = [], $type='add', $username=null, $api_gate = true) {
        if (!$info) {
            Log::error('[TVC_UPDATE_PRODUCT] Không thể đồng bộ căn hộ do thiếu thông tin!', $info);
            return false;
        }
        $body = array(
            'action' => 'FrmPmProperty',
            'method' => $type,
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );
        $response = APIController::TVC_POST_DECODE($body,'', $username, false, $api_gate);
        if (!$response || !$response['success']) {
            $msg = $response&&$response['message']? $response['message'] : json_encode($info);
            LogController::logProduct('[TVC_UPDATE_PRODUCT] '.$type.' căn hộ không thành công bằng Tài khoản '.$username.'! Debug: '.$msg, 'error', null, true, $api_gate);
            return false;
        }
        return $response;
    }

    // add price
    public static function TVC_ADD_PRICEBOOKING($arrayData, $username=null, $api_gate = true) { //type 1 la dat coc, 0 la dat cho
        $info = $arrayData;
        $body = array(
            'action' => 'FrmPmPriceBook',
            'method' => "add",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );
        $response = APIController::TVC_POST_DECODE($body,'', $username, false, $api_gate);
    }

    /*

     * @auth: Dienct
     * @since: 
     *      */

    public static function TVC_LIST_PRICEBOOKING($product="",$username=null,$project="", $api_gate = true ) {
        $info = [
            "reportcode"=>"apiPPB01",
            "queryFilters"=> [
                ["name"=> "block", "value"=> ""],
                ["name"=> "property", "value"=> $product],
                ["name"=> "project", "value"=> $project],
                ["name"=> "dtb", "value"=> self::TVC_DTB],
                ["name" => "pricebookid", "value" => ""]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body,'', $username, false, $api_gate);
        if (!$response['success'] || !$response['data'] || !$response['data']) return false;
        return $response['data'];

    }

    public static function TVC_UPD_PRICEBOOKING($aryUpdatePrices,$username=null, $api_gate = true) { //type 1 la dat coc, 0 la dat cho
        $body = array(
            'action' => 'FrmPmPriceBook',
            'method' => "upd",
            'data' => [$aryUpdatePrices],
            'type' => 'rpc',
            'tid' => 69,
        );
        $response = APIController::TVC_POST_DECODE($body,'', $username, false, $api_gate);

    }

    /** Chi tiết Giao dịch
     * @param $tvc_contract
     * @param bool $cookie
     * @param bool $api_gate
     * @return bool|mixed
     */
    private static function _TVC_GET_CONTRACT($tvc_contract, $cookie=null, $api_gate = true) {
        $info = [
            "reportcode"=>"apiPCT02",
            "limit"=>25,
            "start"=>0,
            "queryFilters"=> [
                ["name"=> "contractid", "value"=> $tvc_contract],
                ["name"=> "dtb", "value"=> self::TVC_DTB]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
//        $cookie = $cookie? null : Auth::guard('loginfrontend')->user()->ub_account_tvc;
        $response = APIController::TVC_POST_DECODE($body,'', $cookie ,false, $api_gate);
        if (!$response['success'] || !$response['data'] || !$response['data']) return false;
        return $response['data'];
    }

    /*

     * @auth: Dienct
     * @Get Product List
     * 
     *      */
    public static function TVC_CHECK_EXIST_PROJECT($code,$username=null, $api_gate = true ) { //type 1 la dat coc, 0 la dat cho
        $info = [
            "reportcode" => "apiPPJ01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "anal_ppj4", "value" => ""],
                ["name" => "datef", "value" => ""],
                ["name" => "datet", "value" => ""],
                ["name" => "project", "value" => $code]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response['success'])
            return false;
        return $response['totalCount'];
    }
    /*

     * @auth: Dienct
     * @Get Product List
     * 
     *      */
    public static function TVC_LIST_PROJECT($code,$username=null, $api_gate = true ) { //type 1 la dat coc, 0 la dat cho
        $info = [
            "reportcode" => "apiPPJ01",
            "queryFilters" => [
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "anal_ppj4", "value" => ""],
                ["name" => "datef", "value" => ""],
                ["name" => "datet", "value" => ""],
                ["name" => "project", "value" => $code]
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response['success'])
            return false;
        return $response['data'];
    }
    public static function TVC_CHECK_EXIST_BUILDING($code,$username=null, $api_gate = true ) { //type 1 la dat coc, 0 la dat cho
        $info = [
            "reportcode" => "apiBLK01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name" => "project", "value" => ""],
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "block", "value" => $code],
                ["name" => "datef", "value" => ""],
                ["name" => "datet", "value" => ""],
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response['success'])
            return false;
        return $response['totalCount'];
    }

    public static function TVC_CHECK_EXIST_PRODUCT($code,$username=null, $api_gate = true ) { //type 1 la dat coc, 0 la dat cho
        $info = [
            "reportcode" => "apiPPT01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "project", "value" => ""],
                ["name" => "block", "value" => ""],
                ["name" => "property", "value" => $code],
                ["name" => "datef", "value" => ""],
                ["name" => "datet", "value" => ""],
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response || !$response['success']) return false;
        return $response['totalCount'];
    }

    /**
     * @param $data
     * @param null $cookie
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_ADD_PROJECT($data, $cookie=null, $api_gate = true) {
        $info = [
            [
                "project"=> $data['project'],
                "name"=> $data['name'],
                "status"=>"W",
                "projectvalue"=>0,
                "address"=>"",
                "notes"=>"",
                "anal_ppj4"=>"",
                "anal_ppj6"=>"",
                "anal_ppj7"=>"",
                "anal_ppj8"=>"",
                "details"=>[
                    [
                        "name"=>"",
                        "investor"=>"",
                        "project"=>""
                    ]
                ]
            ]
        ];
        $body = array(
            'action' => 'FrmPmProject',
            'method' => "add",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $cookie, false, $api_gate);
        if (!$response || !$response['success']) {
            $msg = $response&&$response['message']? $response['message'] : json_encode($data);
            LogController::logProduct('[TVC_ADD_PROJECT] Không thể thêm dự án. Msg: ' . $msg, 'warning', null, true, $api_gate);
            return false;
        }
        return $response;
    }

    /**
     * @param $data
     * @param null $username
     * @param bool $api_gate
     * @return array|bool
     */
    public static function TVC_ADD_BUILDING($data, $username=null, $api_gate = true ) {
        $info = [
            [
                "project"=> $data['project'],
                "block"=> $data['block'],
                "name"=> $data['name'],
                "status"=>"W",
                "address"=>"",
                "anal_blk4"=>"",
                "anal_blk5"=>"",
                "anal_blk6"=>"",
                "anal_blk7"=>"",
                "anal_blk8"=>""
            ]
        ];
        $body = array(
            'action' => 'FrmPmBlock',
            'method' => "add",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 69,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response || !$response['success']) {
            $msg = $response&&$response['message']? $response['message'] : json_encode($data);
            LogController::logProduct('[TVC_ADD_BUILDING] Không thể thêm toà nhà. Msg: '.$msg, 'warning', null);
            return false;
        }
        return $response;
    }

    public static function TVC_LIST_BUILDING($codeproject,$username=null, $api_gate = true ) {
        $info = [
            "reportcode" => "apiBLK01",
            "limit" => 25,
            "start" => 0,
            "queryFilters" => [
                ["name" => "project", "value" => $codeproject],
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "block", "value" => ""],
                ["name" => "datef", "value" => ""],
                ["name" => "datet", "value" => ""],
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response['success']) return false;
        return $response['data'];
    }

    public static function TVC_LIST_PRODUCT($codeProduct,$username=null, $api_gate = true ) { //type 1 la dat coc, 0 la dat cho
        $info = [
            "reportcode" => "apiPPT01",
            "queryFilters" => [
                ["name" => "dtb", "value" => self::TVC_DTB],
                ["name" => "project", "value" => $codeProduct],
                ["name" => "block", "value" => ""],
                ["name" => "property", "value" => ""],
                ["name" => "datef", "value" => ""],
                ["name" => "datet", "value" => ""],
            ]
        ];
        $body = array(
            'action' => 'FrmFdReport',
            'method' => "qry",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response['success']) return false;
        return $response['data'];
    }

    //Update Potial customer
    public static function TVC_UPDATE_POTENTIAL($aryData, $username = null, $api_gate = true ) { //type 1 la dat coc, 0 la dat cho
        $info = [
            [
                "prospect" => $aryData['prospect'],
                "idcard" => $aryData['idcard'],
                "address"=> $aryData['address'],
                "comments" => $aryData['comments'],
                "name" => $aryData['name'],
                "email" => $aryData['email'],
            ]
        ];
        $body = array(
            'action' => 'FrmCrProspect',
            'method' => "upd",
            'data' => [$info],
            'type' => 'rpc',
            'tid' => 1,
        );
        $response = APIController::TVC_POST_DECODE($body, '', $username, false, $api_gate);
        if (!$response['success']) return false;
        return $response;
    }

    /**
     * @param $branch
     * @param BOProduct $apartment
     * @param null $log_transaction
     * @param bool $api_gate
     * @return bool
     */
    public static function TVC_SYNC_BRANCH_ITEMS($branch, BOProduct $apartment, $log_transaction = null, $api_gate = true) {
        $config_account = config('cmconst.account_CRM');
        if (!$config_account[$branch] ||  !$config_account[$branch]['username'] || !$config_account[$branch]['pwd']) {
            LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Không tìm thấy account config sàn '.$branch, 'error', $log_transaction, true, $api_gate);
            return false;
        }
        $cookie_account = $config_account[$branch]['username'];
        $user = BOUser::where('ub_account_tvc', $cookie_account)->first();
        if (!$user) {
            LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Tài khoản config sàn '.$branch.' không tồn tại trên BO', 'error', $log_transaction, true, $api_gate);
            return false;
        }
        $login_info = [
            "id" => 1,
            "username" => $cookie_account,
            "password" => $config_account[$branch]['pwd'],
            "appLog" => "Y",
            "securityKey" => $user->security_code
        ];
        $login_body = array(
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => 998,
        );
        // Process Login
        $do_login = self::TVC_POST_DECODE($login_body, '', $cookie_account, false, $api_gate);
        Log::critical('[TVC_SYNC_BRANCH_ITEMS] Đăng nhập lại TVC - TK: '. $cookie_account, [$login_body, $do_login]);
        if (!$do_login || !$do_login['success']) {
            $msg = $do_login&&$do_login['message']? $do_login['message'] : json_encode($login_info);
            LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Không thể đăng nhập tài khoản hệ thống: ' . $cookie_account. '. Debug: '.$msg, 'error', $log_transaction, true, $api_gate);
            return false;
        }
        $building = BOCategory::where(BOCategory::ID_KEY, $apartment->category_id)
            ->with('sibling')->first();
        if (!$apartment || !$building || !$building->sibling) {
            LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Không tìm thấy đối tượng căn hộ/toà nhà/dự án', 'error', $log_transaction, true, $api_gate);
            return false;
        }
        /** @var $project */
        $project = $building->sibling;
        $apartment_code = $apartment->pb_code;
        $project_code = substr($apartment_code, 0, 3);
        $building_code = substr($apartment_code, 0, 6);
        /** @var $project_exists */
        $project_exists = APIController::TVC_CHECK_EXIST_PROJECT($project_code, $cookie_account, $api_gate);
        if ($project_exists == 0) {
            $new_project = APIController::TVC_ADD_PROJECT(['project' => $project_code, 'name' => $project->cb_title], $cookie_account, $api_gate);
            if (!$new_project) {
                LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Thêm dự án '.$project->cb_title. ' không thành công. TK: '.$cookie_account, 'error', $log_transaction, true, $api_gate);
                return false;
            }
        }
        /** @var $building_exists */
        $building_exists = APIController::TVC_CHECK_EXIST_BUILDING(substr($apartment_code, 0, 6), $cookie_account, $api_gate);
        if ($building_exists == 0) {
            $new_building = APIController::TVC_ADD_BUILDING(['project'=>$project_code, 'block'=>$building_code, 'name' => $building->cb_title], $cookie_account, $api_gate);
            if (!$new_building) {
                LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Thêm toà nhà '.$building->cb_title. ' không thành công. TK: '.$cookie_account, 'error', $log_transaction, true, $api_gate);
                return false;
            }
        }
        /** @var $product_exists */
        $product_exists = APIController::TVC_CHECK_EXIST_PRODUCT($apartment_code, $cookie_account, $api_gate);
        if ($product_exists == 0) {
            $apartment_data = [
                'property' => $apartment_code,
                'status' => 'MBA',
                "propertynum" => $apartment->pb_code_real ? $apartment->pb_code_real : "",
                "block" => (string) $building_code,
                "area" => $apartment->pb_built_up_s ? $apartment->pb_built_up_s : 0,
                "value1" => $apartment->price_used_s ? $apartment->price_used_s : 0
            ];
            $new_apartment = APIController::TVC_UPDATE_PRODUCT($apartment_data, 'add', $cookie_account, $api_gate);
            if (!$new_apartment) {
                LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Đồng bộ căn hộ '.$apartment_code. ' không thành công. TK: '.$cookie_account, 'error', $log_transaction, true, $api_gate);
                return false;
            } else {
                LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Đã đồng bộ căn hộ '.$apartment_code. '. TK: '.$cookie_account, 'info', $log_transaction, true, $api_gate);
            }
        }
        // PRICE
        $aryInsertPriceBooking = $aryUpdatePriceBooking = array();
        // array update price
        $aryUpdatePriceBooking = [];
        $dataPrice = APIController::TVC_LIST_PRICEBOOKING($apartment->pb_code, $cookie_account,'',$api_gate);
        if (is_array($dataPrice) && count($dataPrice) > 0) {
            foreach ($dataPrice as $val) {
                $aryUpdatePrice['pricebookid'] = $val['pricebookid'];
                $aryUpdatePrice['status'] = 'C';
                $aryUpdatePriceBooking[] = $aryUpdatePrice;
            }
        }

        // add price booking id
        $aryInsertPriceBooking[] = [
            "pricebookid" => "",
            "pymtterm" => "z",
            "status" => "W",
            "property" => $apartment->pb_code,
            "startdate" => "",
            "enddate" => "",
            "notes" => "",
            "propertyvalue" => isset($apartment->pb_price_total) ? $apartment->pb_price_total : 0,
            "value0" => 0,
            "value1" => 0,
            "value2" => isset($apartment->price_maintenance) ? $apartment->price_maintenance: 0,
            "value3" => 0,
            "value4" => "",
            "value5" => 0,
            "value6" => isset($apartment->pb_price_per_s) ? $apartment->pb_price_per_s : 0,
            "value7" => isset($apartment->pb_required_money) ? $apartment->pb_required_money : 0,
            "value8" => isset($apartment->price_min) ? $apartment->price_min : 0,
            "value9" => isset($apartment->price_max) ? $apartment->price_max : 0,
        ];
        if ($aryUpdatePriceBooking && count($aryUpdatePriceBooking) > 0) {
            APIController::TVC_UPD_PRICEBOOKING($aryUpdatePriceBooking, $cookie_account, $api_gate);
        }
        if ($aryInsertPriceBooking && count($aryInsertPriceBooking) > 0) {
            APIController::TVC_ADD_PRICEBOOKING($aryInsertPriceBooking, $cookie_account, $api_gate);
        }

        LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Đồng bộ Căn hộ/Toà nhà/Dự án thành công - sàn: '.$branch, 'info', $log_transaction, true, $api_gate);
        return true;
    }


    /** Auto renew login session TVC */
    /**
     * @param bool $api_gate
     */
    public static function __TVC_RENEW_SESSION($api_gate = true) {
        /** @var $config_account */
        $config_account = config('cmconst.account_CRM');
        foreach ($config_account as $key => $val){
            $data = [
                "action" => "ConnectDB",
                "method" => "checkSession",
                "data"   => [['' => '']],
                "type"   => "rpc",
                "tid"    => rand(1,99)
            ];
            /** @var $renew */
            $renew = self::TVC_POST_DECODE($data, '', $val['username'], false, $api_gate);
            if (!$renew || !$renew['success'] || !$renew['data']) {
                Log::critical('[TVC_RENEW_SESSION] Phiên đăng nhập hết hạn. Tiến hành đăng nhập lại Tài khoản '.$val['username'], $renew);
                $user = BOUser::where('ub_account_tvc', $val['username'])->first();
                $login_info = [
                    "id" => 1,
                    "username" => $val['username'],
                    "password" => $val['pwd'],
                    "appLog" => "Y",
                    "securityKey" => $user->security_code
                ];
                $login_body = array(
                    'action' => 'ConnectDB',
                    'method' => 'getConfig',
                    'data' => [$login_info],
                    'type' => 'rpc',
                    'tid' => 998,
                );
                /** @var $do_login */
                $do_login = self::TVC_POST_DECODE($login_body, '', $val['username'], true, $api_gate);
                if (!$do_login || !$do_login['success'] || !$do_login['data']) {
                    Log::critical('[TVC_RENEW_SESSION] Không thể Đăng nhập lại Tài khoản '.$val['username'], $renew);
                } else {
                    Log::critical('[TVC_RENEW_SESSION] Đã đăng nhập lại Tài khoản '.$val['username'], $renew);
                }
            }
//            else {
//                Log::debug('[TVC_RENEW_SESSION] Gia hạn session thành công: '.$val['username'], $renew);
//            }
        }
    }

}
