<?php

namespace App\Http\Controllers\API;

use App\BOUser;
use App\BOUserGroup;
use App\ForumFeedback;
use App\ForumPost;
use App\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class ForumController extends Controller
{
    public function __construct() {
        $this->middleware("laravel.jwt");
        $this->middleware("CheckStoredJWT");
    }

    /** todo: News Feed
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        $page = $request->input('page', 1);
        $size = $request->input('size', 15);
        /** @var $offset */
        $offset = ($page-1)*$size;
        $where = [
            'status' => env('STATUS_ACTIVE', 1)
        ];
        /** @var $account */
        $account = $request->input('account', null);
        /** @var $keyword */
        $keyword = $request->input('keyword', null);
        if ($account) {
            /** @var $user */
            $user = BOUser::where('ub_account_tvc', trim($account))->first();
            if (!$user) return self::jsonError('Tài khoản không tồn tại!');
            $where['created_by'] = $user->{BOUser::ID_KEY};
            if ($user->{BOUser::ID_KEY} !== AuthController::getCurrentUID()) $where['is_anonymous'] = 0;
        }
        /** @var $data */
        $data = ForumPost::where($where)
                ->orderBy('created_at', 'DESC')
                ->skip($offset)
                ->take($size)
                ->with([
                    'author' => function($query) {
                        $query->select([BOUser::ID_KEY, 'ub_title AS name', 'ub_avatar AS avatar', 'group_ids', 'ub_account_tvc AS account'])
                            ->with('group:gb_title,'.BOUserGroup::ID_KEY);
                    }
                ])
                ->withCount('feedbacks');
        if ($keyword) $data = $data->where('content', 'LIKE', "%$keyword%");
        $data = $data->get();

        if (!$data) return self::jsonError('Không tìm thấy dữ liệu');
        return self::jsonSuccess($data, 'Thành công', [
            'uid' => AuthController::getCurrentUID()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function create(Request $request) {
        $request->validate([
            'content' => 'required|string'
        ]);
        /** @var $content */
        $content = trim($request->input('content'));
        $url = $request->input('url', null);
        $video = $request->input('video', null);
        $tags = $request->input('tags', null);
        $location = $request->input('location', null);
        $is_anonymous = $request->input('is_anonymous', false);
        $images = $request->input('images', []);
        /** @var $data */
        $data = new ForumPost();
        $data->content = $content;
        $data->url = $url;
        $data->video = $video;
        $data->tags = $tags;
        $data->location = $location;
        $data->is_anonymous = $is_anonymous;
        $data->status = env('STATUS_ACTIVE', 1);
        $data->created_by = AuthController::getCurrentUID();
        $data->updated_by = AuthController::getCurrentUID();
        if (!file_exists(public_path('uploads/forum/'))) File::makeDirectory(public_path('uploads/forum/'));
        $time = time();
        if (count($images) > 0) {
            foreach ($images as $key => &$image) {
                $image = str_replace(' ', '+', $image);
                $image = preg_replace('/data:image\/[\w]+;base64,/', '', $image);
                $image_name = AuthController::getCurrentUID() . '-' . $time . '-' . $key . '.jpg';
                $path = 'uploads/forum/' . $image_name;
                File::put(public_path($path), base64_decode($image));
                $optimizerChain = OptimizerChainFactory::create();
                $optimizerChain->optimize(public_path($path));
                $image = $path;
            }
            $data->images = $images;
        }
        /** @var $log */
        $log = LogController::logForum('Chuẩn bị đăng bài Forum', 'info', null, false);
        if (!$data->save()) return self::jsonError('Xảy ra lỗi khi đăng nội dung!', $data, $log);

        return self::jsonSuccess($data);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id=0) {
        $item = ForumPost::find($id);
        if (!$item) return self::jsonError('Không tìm thấy bài đăng!');
        if ($item->updated_by !== AuthController::getCurrentUID()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
        $request->validate([
            'content'   => 'required|string'
        ]);
        /** @var $content */
        $content = trim($request->input('content'));
        $url = $request->input('url', null);
        $video = $request->input('video', null);
        $tags = $request->input('tags', null);
        $location = $request->input('location', null);
        $is_anonymous = $request->input('is_anonymous', null);
        $images = $request->input('images', []);
        $time = time();
        /** todo: Handle Images */
        if (count($images) > 0) {
            foreach ($images as $key => &$image) {
                if (starts_with($image, url('/'))) {
                    $image = str_after($image, url('/'));
                } else {
                    $image = str_replace(' ', '+', $image);
                    $image = preg_replace('/data:image\/[\w]+;base64,/', '', $image);
                    $image_name = AuthController::getCurrentUID() . '-' . $time . '-' . $key . '.jpg';
                    $path = 'uploads/forum/' . $image_name;
                    File::put(public_path($path), base64_decode($image));
                    $optimizerChain = OptimizerChainFactory::create();
                    $optimizerChain->optimize(public_path($path));
                    $image = $path;
                }

            }
            $item->images = $images;
        }
        /** todo: Assign data */
        $item->content = $content;
        $item->url = $url?? $item->url;
        $item->video = $video?? $item->video;
        $item->tags = $tags?? $item->tags;
        $item->location = $location?? $item->location;
        $item->is_anonymous = $is_anonymous!==null? $is_anonymous : $item->is_anonymous;
        $item->updated_by = AuthController::getCurrentUID();
        if (!$item->save()) return self::jsonError('Cập nhật bài đăng không thành công!');
        return self::jsonSuccess($item,'Cập nhật bài đăng thành công!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function remove($id) {
        $item = ForumPost::find($id);
        if (!$item) return self::jsonError('Không tìm thấy bài đăng!');
        if ($item->updated_by !== AuthController::getCurrentUID()) return self::jsonError('Bạn không có quyền thực hiện thao tác này!');
        $item->status = env('STATUS_DELETE', -1);
        if (!$item->save()) return self::jsonError('Xóa bài đăng không thành công!');
        return self::jsonSuccess($item,'Xóa bài đăng thành công!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function react(Request $request, $id) {
        if (!$id) return self::jsonError('Yêu cầu không hợp lệ!');
        /** @var $item */
        $item = ForumPost::find($id);
        if (!$item) return self::jsonError('Không tìm thấy bài đăng');
        $likes = $item->likes?? [];
        $dislikes = $item->dislikes?? [];
        /** @var $type */
        $type = $request->input('type', 1);
        $uid = AuthController::getCurrentUID();
        if ($type==1) {
            //** Toggle Like status */
            if (in_array($uid, $likes)) {
                $likes = Util::unsetArrayValue($uid, $likes);
            } else {
                $likes[] = (int) $uid;
            }
            //** Unset dislike */
            $dislikes = Util::unsetArrayValue($uid, $dislikes);
        } else {
            //** Toggle Dislike status */
            if (in_array($uid, $dislikes)) {
                $dislikes = Util::unsetArrayValue($uid, $dislikes);
            } else {
                $dislikes[] = (int) $uid;
            }
            //** Unset like */
            $likes = Util::unsetArrayValue($uid, $likes);
        }
        $item->likes = array_unique($likes);
        $item->dislikes = array_unique($dislikes);
        if (!$item->save()) return self::jsonError('Thao tác không thành công!');
        return self::jsonSuccess([
            'likes' => $likes,
            'dislikes' => $dislikes
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendFeedback(Request $request, $id) {
        $request->validate([
            'comment' => 'required|string'
        ]);
        /** @var $comment */
        $comment = $request->input('comment');
        $post = ForumPost::find($id);
        if (!$post) return self::jsonError('Bài đăng không tồn tại!');
        $query = new ForumFeedback();
        $query->comment = trim($comment);
        $query->is_anonymous = $request->input('is_anonymous', false);
        $query->forum_post_id = (int) $id;
        $query->created_by = AuthController::getCurrentUID();
        $query->reply_to = $request->input('reply_to', null);
        if (!$query->save()) return self::jsonError('Bình luận không thành công');
        return self::jsonSuccess($query);
    }

    /**
     * @param Request $request
     * @param $post
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listComment(Request $request, $post) {
        /** @var $item */
        $item = ForumPost::find($post);
        if (!$item) return self::jsonError('Bài viết không tồn tại!');
        $page = $request->input('page', 1);
        $size = $request->input('size', 10);
        /** @var $offset */
        $offset = ($page-1)*$size;
        /** @var $data */
        $data = ForumFeedback::where([
            'forum_post_id' => $post,
            'reply_to'      => $request->input('reply_to', null)
        ])
            ->orderBy('updated_at', 'DESC')
            ->skip($offset)
            ->take($size)
            ->with([
                'replies' => function($reply) {
                    $reply->with([
                        'created_by' => function($query) {
                            $query->select([
                                'ub_title AS name',
                                'ub_account_tvc AS account',
                                'ub_avatar AS avatar',
                                BOUser::ID_KEY
                            ]);
                        }
                    ]);
                },
                'created_by' => function($query) {
                    $query->select([
                        'ub_title AS name',
                        'ub_account_tvc AS account',
                        'ub_avatar AS avatar',
                        BOUser::ID_KEY
                    ]);
                }
            ])
            ->get();
        if (!$data) return self::jsonError('Không tìm thấy bình luận!');
        return self::jsonSuccess($data, 'Done', $request->all());
    }
}
