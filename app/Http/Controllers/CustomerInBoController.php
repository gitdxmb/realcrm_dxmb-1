<?php

namespace App\Http\Controllers;

use App\BOProduct;
use App\Http\Controllers\API\BOProductController;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BOCustomer;
use App\BOUser;
use App\BOBill;
use Illuminate\Support\Facades\Auth;

class CustomerInBoController extends Controller
{
    private $BOCustomer;
    public function __construct() {
        $this->BOCustomer = new BOCustomer();
    }
    public function getAllCustomer(){
        $a_Data = $this->BOCustomer->getAllSearch();
        
        $Data_view['a_Customer'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];
        
        $Data_view['sourceCustomer'] = config('cmconst.source_id');
        $Data_view['a_Saffs'] = BOUser::getAllUserInBo();        

        return view('customerInBo.index',$Data_view);
    }
    
    public function getAllOfficalCustomer(){
        $a_Data = $this->BOCustomer->getAllOfficalSearch();
        
        $Data_view['a_Customer'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];
        
        $Data_view['sourceCustomer'] = config('cmconst.source_id');
        $Data_view['a_Saffs'] = BOUser::getAllUserInBo();

        return view('customerInBo.offical',$Data_view);
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit customer
     *      */
    public function addEditCustomer(){
        $a_DataView = array();
        $idMapping = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $idMapping;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $res = $this->BOCustomer->AddEditCustomerMapping($idMapping);
            return redirect('customer')->with('status', 'Cập nhật thành công!');
        }
        if($idMapping != 0){
            $a_DataView['customerData'] = $a_Data = DB::table('user_customer_mappings')->where('id', $idMapping)->first();            
        }else{
            $a_DataView['customerData'] = array();
        }
        return view('customerInBo.edit', $a_DataView );
    }
    
    public function EditOfficalCustomer(){
        $a_DataView = array();
        $a_DataView['sourceCustomer'] = config('cmconst.source_id');
        
        $a_DataView['a_Saffs'] = BOUser::getAllUserInBo();
        $customerId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $customerId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            
            $res = $this->BOCustomer->AddEditCustomer($customerId);
            if($res){
                echo"<script>alert('{$res['msg']}]');</script>";
                return redirect('offical_customer')->with('status', 'Cập nhật thành công!');
            }else{
                echo"<script>alert('{$res['msg']}]');</script>";
            } 
        }
        if($customerId != 0){
            $a_DataView['customerData'] = $this->BOCustomer->getCustomerById($customerId);
            $strStaffTranfer = $a_DataView['customerData']->cb_staff_id;
            $a_DataView['strStaffTranfer'] = explode(',',$strStaffTranfer);
        }else{
            $a_DataView['customerData'] = array();
            $a_DataView['strStaffTranfer'] = array();
        }
        return view('customerInBo.editoffical', $a_DataView );
    }
    
    /**
     * @auth: Dienct
     * @since: 19/09/2018
     * @Des: tranfer customer recent
     *      */
    public function tranferCustomerRecent(){
        $a_Data = $this->BOCustomer->getAllSearchCusRecent();

        $Data_view['a_Customer'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];
        $Data_view['a_Saffs'] = BOUser::getAllUserInBo();

        return view('customerInBo.tmpCus',$Data_view);
    }

    /**
     * @auth: HuyNN
     * @since: 10/10/2018
     * @Des: Customer confirm via email
     *      */

    public function Confirm(Request $request){
        $request = $request->input();
        $noti = '';
        if(isset($request['customer']) && isset($request['bill_id']) && isset($request['status'])){
            $bill = BOBill::where(['bill_id' => $request['bill_id']])->first();
            if($bill){
                if($bill->status_id != BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
                    $noti = 'Không thể xác nhận hợp đồng này!';
                }else{
                    if(isset(BOBill::STATUSES[$request['status']]) && ($request['status'] == 'STATUS_APPROVED_BY_CUSTOMER' || $request['status'] == 'STATUS_DISAPPROVED_BY_CUSTOMER')){
                        $status_id = BOBill::STATUSES[$request['status']];
                        $customer = BOCustomer::where([BOCustomer::ID_KEY => $request['customer']])->first();
                        if($customer){
                            $current_log = $bill->bill_approval_log;
                            $current_log[] = BOBill::generateApprovalLog(BOBill::STATUSES[$request['status']],$request['customer']);
                            $bill->bill_approval_log = $current_log;
                            $bill->status_id = $status_id;
                            if($bill->save()){
                                if($request['status'] == 'STATUS_APPROVED_BY_CUSTOMER'){
                                    BOProductController::changeStatus($bill->product_id, BOProduct::STATUSES['CUSTOMER_CONFIRM'], true);
                                    $noti = 'Xác nhận đồng ý hợp đồng thành công!';
                                }else{
                                    $noti = 'Xác nhận từ chối hợp đồng thành công!';
//                                    BOProductController::releaseApartment($bill->product_id);
                                }
                            }
                        }else{
                            $noti = 'Khách hàng không tồn tại trong hệ thống!';
                        }
                    }else{
                        $noti = 'Trạng thái xác nhận hợp đồng không hợp lệ!';
                    }
                }
            }else{
                $noti = 'Không tồn tại hợp đồng này! Vui lòng kiểm tra lại!';
            }
        }else{
            $noti = 'Dữ liệu không dầy đủ!';
        }
        $Data_view['noti'] = $noti;
        return view('customerInBo.confirm',$Data_view);
    }
    
    /**
     * @auth: Dienct
     * @since: 19/01/2019
     * @Des: tranfer customer recent
     *      */
    
    public function listFileManagement(){
        
        $a_Data = $this->BOCustomer->getAllFileSearch();

        $Data_view['a_Customer'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];
        
        $Data_view['sourceCustomer'] = config('cmconst.source_id');
        $Data_view['a_Saffs'] = BOUser::getAllUserInBo();

        return view('customerInBo.file_management',$Data_view);
    }
    
}
