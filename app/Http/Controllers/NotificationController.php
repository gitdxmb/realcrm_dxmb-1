<?php

namespace App\Http\Controllers;

use App\BOBill;
use App\BOCustomer;
use App\BOPayment;
use App\BOProduct;
use App\BOTransaction;
use App\BOUser;
use App\BOUserGroup;
use App\Http\Controllers\API\APIController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\BOProductController;
use App\Http\Controllers\API\BOUserController;
use App\Notifications\ApartmentLocked;
use App\Notifications\ApartmentLockRequest;
use App\Notifications\ApartmentUnlocked;
use App\Notifications\BillCreated;
use App\Notifications\BillStatusUpdated;
use App\Notifications\PaymentCreated;
use App\Notifications\PaymentStatusUpdated;
use App\Notifications\PostCreated;
use App\Notifications\TransactionCustomerAdded;
use App\Notifications\Customer\BillStatusUpdated as BillStatusUpdatedByCustomer;
use App\Post;
use App\Util;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\API\NotificationController as APINotificationController;

class NotificationController extends Controller
{
    /**
     * @param $targets
     * @param $title
     * @param $body
     * @param int $apartment_id
     * @param string $sound
     * @return array
     */
    private static function pushApartmentFCM($targets, $title, $body, $apartment_id = 0, $sound = '') {
        $data = [
            'screen' => $apartment_id? 'ApartmentDetails' : 'Project',
            'id'     => $apartment_id
        ];
        return APIController::pushFCM($targets, $title, $body, $data, $sound);
    }

    /**
     * @param $targets
     * @param $title
     * @param $body
     * @param int $transaction_id
     * @param string $sound
     * @return array
     */
    private static function pushTransactionFCM($targets, $title, $body, $transaction_id = 0, $sound = '') {
        $data = [
            'screen' => $transaction_id? 'TransactionDetails' : 'TransactionList',
            'id'     => $transaction_id,
        ];
        return APIController::pushFCM($targets, $title, $body, $data, $sound);
    }

    /**
     * @param $targets
     * @param $title
     * @param $body
     * @param int $bill_id
     * @param string $sound
     * @return array
     */
    private static function pushBillFCM($targets, $title, $body, $bill_id = 0, $sound = '') {
        $data = [
            'screen' => $bill_id? 'ContractDetails' : 'ContractList',
            'id'     => $bill_id,
            'tab'    => 1
        ];
        return APIController::pushFCM($targets, $title, $body, $data, $sound);
    }

    /**
     * @param $targets
     * @param $title
     * @param $body
     * @param $post_id
     * @param string $sound
     * @return array
     */
    private static function pushPostFCM($targets, $title, $body, $post_id, $sound = '') {
        $data = [
            'screen' => 'NewsDetails',
            'id'     => $post_id,
        ];
        return APIController::pushFCM($targets, $title, $body, $data, $sound);
    }
    /**
     * @param $targets
     * @param $title
     * @param $body
     * @param int $bill_id
     * @param string $sound
     * @return array
     */
    private static function pushPaymentFCM($targets, $title, $body, $bill_id = 0, $sound = '') {
        $data = [
            'screen' => $bill_id? 'ContractDetails' : 'ContractList',
            'id'     => $bill_id,
            'tab'    => 2
        ];
        return APIController::pushFCM($targets, $title, $body, $data, $sound);
    }

    /**
     * @param BOTransaction $transaction
     * @param BOProduct $apartment
     * @param array $targets
     * @param bool $stored
     * @return bool
     */
    public static function notifyLockRequest(BOTransaction $transaction, BOProduct $apartment, $targets = [], $stored = true) {
        if (!$targets) {
            $targets = BOProductController::getProductManagersByApartment($apartment->{BOProduct::ID_KEY})?? [];
            $targets[] = AuthController::getCurrentUID();
        }
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        /** @var $department_name */
        $department_name = BOUser::getCurrentDepartment('gb_title');
        /** @var $project */
        $project = BOProductController::findProjectByApartment($apartment->{BOProduct::ID_KEY}, ['id', 'alias', 'cb_title']);
        $project_name = $project? ($project->alias??$project->cb_title) : '';
        /** @var $tokens */
        $tokens = [];
        foreach ($users as $user) {
            $tokens[] = $user->ub_token;
        }
        /** @var $fcm */
        $fcm = [
            'title' => 'Yêu cầu Lock căn ' . ($apartment->pb_title?? $apartment->pb_code) . ' - ' . $project_name,
            'body'  => 'NV: ' . BOUser::getCurrent('ub_title') . ' - Sàn: ' . $department_name,
            'sound' => 'lock_1'
        ];
        self::pushTransactionFCM($tokens, $fcm['title'], $fcm['body'], $transaction->{BOTransaction::ID_KEY}, $fcm['sound']);
        if ($stored) {
            Notification::send($users, new ApartmentLockRequest($transaction));
        }
        return true;
    }

    /**
     * @param BOProduct $apartment
     * @param null|int $staff_id
     * @param bool $stored
     * @return bool
     */
    public static function notifyApartmentLocked(BOProduct $apartment, $staff_id = null, $stored = true) {
        /** @var $targets */
        $targets = BOProductController::getProductManagersByApartment($apartment->{BOProduct::ID_KEY})?? [];
        $targets[] = AuthController::getCurrentUID();

        if ($staff_id>0) {
            $targets[] = $staff_id;
            $staff = BOUser::select(['ub_title', 'group_ids'])->where(BOUser::ID_KEY, $staff_id)->with('group')->first();
            $department = $staff->group;
        } else {
            /** @var $staff_name */
            $staff = BOUser::getCurrent();
            $department = BOUser::getCurrentDepartment();
        }
        $staff_name = $staff->ub_title;
        $department_name = $department->gb_description?? $department->gb_title;

        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
                ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        /** @var $tokens */
        $tokens = BOUser::select(['id', 'ub_token'])->whereNotNull('ub_token')->where('ub_status', env('STATUS_ACTIVE', 1))
                ->pluck('ub_token')->toArray();
        /** @var $project */
        $project = BOProductController::findProjectByApartment($apartment->{BOProduct::ID_KEY}, ['id', 'alias', 'cb_title']);
        $project_name = $project? ($project->alias??$project->cb_title) : '';

        /** @var $fcm */
        $fcm = [
            'title' => 'Lock: '. ($apartment->pb_title?? $apartment->pb_code) . ' - ' . $project_name,
            'body'  => 'NV: '. $staff_name . ' - ' . $department_name,
            'sound' => 'lock_1'
        ];
        self::pushApartmentFCM($tokens, $fcm['title'], $fcm['body'], $apartment->pb_id, $fcm['sound']);
        if ($stored) {
            Notification::send($users, new ApartmentLocked($apartment));
        }
        return true;
    }

    /**
     * @param BOProduct $apartment
     * @param array $targets
     * @param bool $stored
     * @return bool
     */
    public static function notifyApartmentUnlocked(BOProduct $apartment, $targets = [], $stored = true) {
        if (!$targets) {
            $targets = BOProductController::getProductManagersByApartment($apartment->{BOProduct::ID_KEY})?? [];
        }
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        /** @var $tokens */
        $tokens = BOUser::select(['id', 'ub_token'])->whereNotNull('ub_token')->where('ub_status', env('STATUS_ACTIVE', 1))
            ->pluck('ub_token')->toArray();
        /** @var $project */
        $project = BOProductController::findProjectByApartment($apartment->{BOProduct::ID_KEY}, ['id', 'alias', 'cb_title']);
        $project_name = $project? ($project->alias??$project->cb_title) : '';
        $apartment_name = $apartment->pb_title?? $apartment->pb_code;
        /** @var $fcm */
        $fcm = [
            'title' => 'Bung lock căn '. $apartment_name . ' - ' . $project_name,
            'body'  => 'Căn ' . $apartment_name . ' đã trở về trạng thái Mở bán',
            'sound' => 'unlock_2'
        ];
        self::pushApartmentFCM($tokens, $fcm['title'], $fcm['body'], $apartment->pb_id, $fcm['sound']);
        if ($stored) {
            Notification::send($users, new ApartmentUnlocked($apartment));
        }
        return true;
    }

    /**
     * @param BOTransaction $transaction
     * @param BOProduct $apartment
     * @param array $targets
     * @param bool $stored
     * @return bool
     */
    public static function notifyTransactionCustomerAdded(BOTransaction $transaction, BOProduct $apartment, $targets = [], $stored = true) {
        $targets[] = $transaction->staff_code;
        $product_managers = BOProductController::getProductManagersByApartment($transaction->product_id)?? [];
        $targets = array_merge($targets, $product_managers);
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        /** @var $tokens */
        $tokens = [];
        foreach ($users as $user) {
            $tokens[] = $user->ub_token;
        }
        /** @var $project */
        $project = BOProductController::findProjectByApartment($apartment->{BOProduct::ID_KEY}, ['id', 'alias', 'cb_title']);
        $project_name = $project? ($project->alias??$project->cb_title) : '';
        $apartment_name = $apartment->pb_title?? $apartment->pb_code;
        /** @var $department */
        $department = BOUser::getCurrentDepartment('gb_description');

        /** @var $fcm */
        $fcm = [
            'title' => 'Thêm KH cho căn: '. $apartment_name . ' - ' . $project_name,
            'body'  => 'NV: ' . BOUser::getCurrent('ub_title') . ' - Sàn ' . $department,
            'sound' => 'lock_3'
        ];
//        APIController::pushFCM($tokens, $fcm['title'], $fcm['body'], ['event' => 'add-transaction-customer', 'id' => $transaction->{BOTransaction::ID_KEY}], $fcm['sound']);
        self::pushTransactionFCM($tokens, $fcm['title'], $fcm['body'], $transaction->{BOTransaction::ID_KEY}, $fcm['sound']);
        if ($stored) {
            Notification::send($users, new TransactionCustomerAdded($transaction));
        }
        return true;
    }

    /**
     * @param BOBill $bill
     * @param BOProduct $apartment
     * @param array $targets
     * @param bool $stored
     * @return bool
     */
    public static function notifyBillCreated(BOBill $bill, BOProduct $apartment, $targets = [], $stored = true) {
        /** @var $targets */
        $targets[] = $bill->staff_id;
        /** @var $product_managers */
        $product_managers = BOProductController::getProductManagersByApartment($bill->product_id)?? [];
        /** @var $department_head */
        $department_head = BOUserController::getCurrentDepartmentHeads()?? [];
        $targets = array_merge($targets, array_wrap($product_managers));
        $targets = array_merge($targets, array_wrap($department_head));
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        /** @var $tokens */
        $tokens = [];
        foreach ($users as $user) {
            $tokens[] = $user->ub_token;
        }
        /** @var $project */
        $project = BOProductController::findProjectByApartment($apartment->{BOProduct::ID_KEY}, ['id', 'alias', 'cb_title']);
        $project_name = $project? ($project->alias??$project->cb_title) : '';
        $apartment_name = $apartment->pb_title?? $apartment->pb_code;
        /** @var $department */
        $department = BOUser::getCurrentDepartment('gb_description');
        /** @var $fcm */
        $fcm = [
            'title' => 'Hợp đồng mới cho căn: '. $apartment_name . ' - ' . $project_name,
            'body'  => 'NV: ' . BOUser::getCurrent('ub_title') . ' - Sàn: ' . $department,
            'sound' => 'success_1'
        ];
//        APIController::pushFCM($tokens, $fcm['title'], $fcm['body'], ['event' => 'add-bill', 'id' => $bill->{BOBill::ID_KEY}], $fcm['sound']);
        self::pushBillFCM($tokens, $fcm['title'], $fcm['body'], $bill->{BOBill::ID_KEY}, $fcm['sound']);
        if ($stored) {
            Notification::send($users, new BillCreated($bill));
        }
        return true;
    }

    /**
     * @param BOBill $bill
     * @param array $targets
     * @param bool $stored
     * @param bool $api_gate
     * @return bool
     */
    public static function notifyBillStatusUpdated(BOBill $bill, $targets = [], $stored = true, $api_gate = true) {
        $targets[] = $bill->staff_id;
        $staff = BOUser::where(BOUser::ID_KEY, $bill->staff_id)->with('group')->first();
        /** @var $product_managers */
        $product_managers = BOProductController::getProductManagersByApartment($bill->product_id)?? [];
        /** @var $department_head */
        $department_head = $staff? BOUserController::getDepartmentHeads($staff) : [];
        $targets = array_merge($targets, array_wrap($product_managers));
        $targets = array_merge($targets, array_wrap($department_head));
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        $customer = BOCustomer::select(['id', 'device_token'])->where(BOCustomer::ID_KEY, $bill->customer_id)->first();
        /** @var $tokens */
        $tokens = [];
        foreach ($users as $user) {
            $tokens[] = $user->ub_token;
        }
        /** @var $user_name */
        $user_name = BOUser::getCurrent('ub_title', $api_gate);
        $staff_name = $staff->ub_title;
        $department_name = $staff->group? ($staff->group->gb_description?? $staff->group->gb_title) : '';
        $bill_code = $bill->bill_code;
        /** @var $apartment */
        $apartment = BOProduct::where(BOProduct::ID_KEY, $bill->product_id)->first();
        $apartment_name = $apartment->pb_title?? $apartment->pb_code;
        /** @var $project */
        $project = BOProductController::findProjectByApartment($bill->product_id, ['id', 'alias', 'cb_title']);
        $project_name = $project? ($project->alias??$project->cb_title) : '';
        $fcm_data = ['event' => 'update-bill', 'id' => $bill->{BOBill::ID_KEY}];
        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        $sms_content = '';
        /** @var $fcm */
        switch ($bill->status_id) {
            case BOBill::STATUSES['STATUS_APPROVED']:
                $fcm = [
                    'title' => 'TKKD duyệt hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'TKKD ' . $user_name . ' đã xác nhận hợp đồng ' . $bill_code,
                    'sound' => 'success_1'
                ];
                break;
            case BOBill::STATUSES['STATUS_DISAPPROVED']:
                $fcm = [
                    'title' => 'TKKD từ chối hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'TKKD ' . $user_name . ' đã từ chối hợp đồng ' . $bill_code,
                    'sound' => 'unlock_2'
                ];
                break;
            case BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']:
                $fcm = [
                    'title' => 'Giám đốc sàn xác nhận hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'QLKD ' . $user_name . ' đã xác nhận hợp đồng ' . $bill_code,
                    'sound' => 'success_2'
                ];
                if ($customer) APIController::pushFCM($customer->device_token, "X/N Hợp đồng ĐXMB: $bill_code", 'Khách hàng vui lòng xác nhận hợp đồng '.$bill_code, $fcm_data, 'dxmb');
                break;
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER']:
                $fcm = [
                    'title' => 'Giám đốc sàn từ chối hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'QLKD ' . $user_name . ' đã từ chối hợp đồng ' . $bill_code,
                    'sound' => 'unlock_2'
                ];
                break;
            case BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER']:
                $fcm = [
                    'title' => 'Khách hàng xác nhận hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'Hợp đồng ' . $bill_code . ' và các điều khoản đã được khách hàng chấp nhận',
                    'sound' => 'success_3'
                ];
                if ($customer) APIController::pushFCM($customer->device_token, "Đã tạo Hợp đồng ĐXMB: $bill_code", 'Hợp đồng cho căn '.$apartment_name.' đã được khởi tạo thành công!', $fcm_data, 'dxmb');
                if ($stored&&$customer) $customer->notify(new BillStatusUpdatedByCustomer($bill));
                break;
            case BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER']:
                $fcm = [
                    'title' => 'Khách hàng từ chối hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'Hợp đồng ' . $bill_code . ' đã bị khách hàng từ chối',
                    'sound' => 'fail_1'
                ];
                if ($customer) APIController::pushFCM($customer->device_token, "Đã huỷ Hợp đồng $bill_code", '', $fcm_data, 'dxmb');
                if ($stored&&$customer) $customer->notify(new BillStatusUpdatedByCustomer($bill));
                break;
            case BOBill::STATUSES['STATUS_CANCELED']:
                $fcm = [
                    'title' => 'Huỷ hợp đồng - Căn: '. $apartment_name,
                    'body'  => 'NVKD '. $staff_name .' đã huỷ hợp đồng ' . $bill_code,
                    'sound' => 'fail_1'
                ];
                break;
            case BOBill::STATUSES['STATUS_DEPOSITED']:
                $fcm = [
                    'title' => 'Chúc mừng đặt cọc thành công, căn: ' . $apartment_name . ' - ' . $project_name,
                    'body'  => 'NV: ' . $staff_name . ' - Sàn: ' . $department_name,
                    'sound' => 'success_2'
                ];
                if ($customer) APIController::pushFCM($customer->device_token, "ĐXMB chúc mừng Quý khách đã đặt cọc căn hộ thành công!", $fcm['body'], $fcm_data, 'dxmb');
                if ($stored&&$customer) $customer->notify(new BillStatusUpdatedByCustomer($bill));
                $sms_content = 'Chuc mung khach hang '.strtoupper(Util::stripVN($customer_info->name));
                $sms_content .= ' dat mua thanh cong can ho ' . Util::stripVN($apartment_name);
                $sms_content .= ' tai ' . Util::stripVN($project_name). '. Tran trong!';
                break;
            case BOBill::STATUSES['STATUS_BOOKED']:
                $fcm = [
                    'title' => 'Chúc mừng đặt chỗ thành công, căn: ' . $apartment_name . ' - ' . $project_name,
                    'body'  => 'NV: ' . $staff_name . ' - Sàn: ' . $department_name,
                    'sound' => 'success_2'
                ];
                if ($customer) APIController::pushFCM($customer->device_token, "ĐXMB chúc mừng Quý khách đã đặt chỗ căn hộ thành công!", $fcm['body'], $fcm_data, 'dxmb');
                if ($stored&&$customer) $customer->notify(new BillStatusUpdatedByCustomer($bill));
                $sms_content = 'Chuc mung khach hang '.strtoupper(Util::stripVN($customer_info->name));
                $sms_content .= ' dat cho thanh cong can ho ' . Util::stripVN($apartment_name);
                $sms_content .= ' tai ' . Util::stripVN($project_name). '. Tran trong!';
                break;
            case BOBill::STATUSES['STATUS_SUCCESS']:
                $fcm = [
                    'title' => "Chúc mừng Giao dịch " . ($bill->bill_total_money>=10000000000?'bom tấn!' : 'thành công!'),
                    'body'  => 'NV: ' . $staff_name . ' - Sàn: ' . $department_name,
                    'sound' => 'success_3'
                ];
                if ($customer) APIController::pushFCM($customer->device_token, "ĐXMB chúc mừng khách hàng ký hợp đồng thành công!", $fcm['body'], $fcm_data, 'dxmb');
                if ($stored&&$customer) $customer->notify(new BillStatusUpdatedByCustomer($bill));
                $sms_content = 'Chuc mung KH '.strtoupper(Util::stripVN($customer_info->name));
                $sms_content .= ' tro thanh tan chu nhan can ho ' . Util::stripVN($apartment_name);
                $sms_content .= ' tai ' . Util::stripVN($project_name). '. DXMB cam on su tin tuong cua quy khach hang!';
                break;
            default:
                $fcm = [];
        }
        if (count($fcm)>0) {
//            APIController::pushFCM($tokens, $fcm['title'], $fcm['body'], [], $fcm['sound']);
            self::pushBillFCM($tokens, $fcm['title'], $fcm['body'], $bill->{BOBill::ID_KEY}, $fcm['sound']);
        }
        if ($stored) {
            Notification::send($users, new BillStatusUpdated($bill, false));
        }
        if ($sms_content!=='') APINotificationController::sendSMS($sms_content, $customer_info->phone);
        return true;
    }


    /**
     * @param BOPayment $payment
     * @param int $apartment_id
     * @param bool $stored
     * @return bool
     */
    public static function notifyPaymentCreated(BOPayment $payment, $apartment_id=0, $stored = true) {
        if (!$payment) return false;
        $targets = [$payment->request_staff_id];
        $product_managers = BOProductController::getProductManagersByApartment($apartment_id);
        $targets = array_merge($targets, array_wrap($product_managers));
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
        if (!$users) return false;
        /** @var $tokens */
        $tokens = [];
        foreach ($users as $user) {
            $tokens[] = $user->ub_token;
        }
        /** @var $customer*/
        $customer = BOCustomer::select(['id','device_token','cb_name'])->where(BOCustomer::ID_KEY, $payment->customer_id)->first();
        if($customer){
            $tokens[] = $customer->device_token;
        }

        $user_name = BOUser::getCurrent('ub_title');
        /** @var $bill_code */
        $bill_code = $payment->bill_code;
        /** @var $bill */
        $bill = BOBill::select([BOBill::ID_KEY, 'bill_customer_info'])->where('bill_code', $bill_code)->first();
        $customer_info = $bill->bill_customer_info;
        $bill_id = $bill? $bill->{BOBill::ID_KEY} : 0;

        $product = BOProduct::select([
            "pb_id", "pb_code", "category_id", "pb_title"
        ])
            ->where(BOProduct::ID_KEY, $apartment_id)
            ->with([ "category" => function($building) {
                return $building->select([
                    "id", "cb_id", "parent_id",
                ])
                    ->where("cb_level", 2)
                    ->with(["sibling" => function($project) {
                        return $project->select(["id", "cb_id", "parent_id", "cb_title", "alias"])
                            ->where("cb_level", 1);
                    }]);
            }])->first();
        $product_name = $product->pb_title?? $product->pb_code;
        $project = $product->category->sibling;
        $project_name = $project->alias?? $project->cb_title;

        /** @var $fcm */
        $fcm = [
            'title' => 'Xác nhận thông tin chuyển tiền từ Đất Xanh Miền Bắc!',
            'body'  => 'DXMB đã nhận được thông tin chuyển tiền của KH ' . $customer->cb_name . ' cho căn hộ ' . $product_name .' tại dự án '.$project_name.' .Trân trọng!',
            'sound' => 'dxmb'
        ];
        if (count($tokens)>0) {
//            APIController::pushFCM($tokens, $fcm['title'], $fcm['body'], [], $fcm['sound']);
            self::pushPaymentFCM($tokens, $fcm['title'], $fcm['body'], $bill_id, $fcm['sound']);
        }
        if ($stored) {
            Notification::send($users, new PaymentCreated($payment));
        }
        $sms_content = 'DXMB da nhan duoc thong tin chuyen tien cho Can ho '.
            Util::stripVN($product_name)
            . ' - Du an '
            . Util::stripVN($project_name)
            .' cua KH '
            . strtoupper(Util::stripVN($customer_info->name))
            .'. Tran trong!';
        APINotificationController::sendSMS($sms_content, $customer_info->phone);
        return true;
    }

    /**
     * @param BOPayment $payment
     * @param BOBill|null $bill
     * @param bool $stored
     * @return bool
     */
    public static function notifyPaymentStatusUpdated(BOPayment $payment, BOBill $bill = null, $stored=true) {
        if (!$payment || !$bill) return false;
        /** @var $targets */
        $targets = [$bill->staff_id];
        /** @var $product_managers */
        $product_managers = BOProductController::getProductManagersByApartment($bill->product_id);
        if ($product_managers) $targets = array_merge($targets, array_wrap($product_managers));
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($targets))->get();
//        if (!$users) return false;

        /** @var $tokens */
        $tokens = [];
        if($users){
            foreach ($users as $user) {
                $tokens[] = $user->ub_token;
            }
        }
        /** @var $customer*/
        $customer = BOCustomer::select(['id','device_token','cb_name'])->where(BOCustomer::ID_KEY, $bill->customer_id)->first();
        if($customer){
            $tokens[] = $customer->device_token;
        }
        /** @var $customer_info */
        $customer_info = $bill->bill_customer_info;
        /** @var $product */
        $product = BOProduct::select([
            "pb_id", "pb_code", "category_id", "pb_title"
        ])
            ->where(BOProduct::ID_KEY, $bill->product_id)
            ->with([ "category" => function($building) {
                return $building->select([
                    "id", "cb_id", "parent_id",
                ])
                    ->where("cb_level", 2)
                    ->with(["sibling" => function($project) {
                        return $project->select(["id", "cb_id", "parent_id", "cb_title", "alias"])
                            ->where("cb_level", 1);
                    }]);
            }])->first();
        $product_name = $product->pb_title?? $product->pb_code;
        $project = $product->category->sibling;
        $project_name = $project->alias?? $project->cb_title;
        /** @var $response_msg */
        $response_msg = number_format($payment->pb_response_money) . ' VNĐ của KH '.$customer_info->name.' cho căn hộ '. $product_name .' tại dự án '.$project_name.' .Trân trọng!';
        /** @var $fcm */
        $fcm = $payment->pb_status == BOPayment::STATUS['APPROVED']? [
            'title' => 'Xác nhận chuyển tiền thành công từ Đất Xanh Miền Bắc!',
            'body'  => 'DXMB đã nhận ' . $response_msg,
            'sound' => 'dxmb'
        ]
        :
        [
            'title' => 'Xác nhận từ chối thanh toán từ Đất Xanh Miền Bắc!',
            'body'  => 'DXMB từ chối thanh toán ' . $response_msg,
            'sound' => 'fail_1'
        ];
        if (count($tokens)>0) {
            self::pushPaymentFCM($tokens, $fcm['title'], $fcm['body'], $bill->{BOBill::ID_KEY}, $fcm['sound']);
        }
        if ($stored) {
            Notification::send($users, new PaymentStatusUpdated($payment, false));
        }
        $sms_content = 'DXMB da nhan '
            . number_format($payment->pb_response_money) . ' VND'
            .' cua KH ' . strtoupper(Util::stripVN($customer_info->name))
            . ' cho can ho ' . Util::stripVN($product_name)
            . ' tai du an ' . Util::stripVN($project_name)
            .'. Tran trong!';
        APINotificationController::sendSMS($sms_content, $customer_info->phone);
        return true;
    }

    /**
     * @param Post $post
     * @param string $targets
     * @param string $target_type
     * @param bool $stored
     * @param bool $api_gate
     * @return bool
     */
    public static function notifyPostCreated(Post $post, $targets = 'all', $target_type = BOUser::class, $stored=true, $api_gate=false) {
        if (!$post) return false;
        /** @var $users */
        $users = BOUser::select(['id', 'ub_token'])->where('ub_status', env('STATUS_ACTIVE', 1));
        if ($targets === 'all') {
            $target_type = BOUserGroup::class;
            /** @var $group */
            $group = BOUserGroup::findGroupOneForAll(false);
            if (!$group) {
                Log::notice('[Post FCM] Không tìm thấy nhóm ALL để gửi FCM', [
                    'id' => $post->id,
                    'title' => $post->title,
                    'created_by' => $post->created_by,
                    'updated_by' => $post->updated_by
                ]);
                return false;
            }
            /** todo: Store TO-ALL notification */
            if ($stored) Notification::send($group, new PostCreated($post, $api_gate));
        } else {
            if (!is_array($targets)) return false;
            /** @var $targets */
            $targets = array_unique($targets);
            if ($target_type == BOUserGroup::class) {
                $users = $users->whereIn('group_ids', $targets);
                /** @var $groups */
                $groups = BOUserGroup::whereIn(BOUserGroup::ID_KEY, $targets)->get();
                /** todo: Store TO-GROUP notification */
                if ($stored) Notification::send($groups, new PostCreated($post, $api_gate));
            } else {
                $users = $users->whereIn(BOUser::ID_KEY, $targets);
            }
        }
        $users = $users->get();

        if (is_array($targets) && $target_type==BOUser::class && $stored) {
            /** todo: Store TO-USER notification */
            Notification::send($users, new PostCreated($post, $api_gate));
        }
        /** @var $tokens */
        $tokens = [];
        foreach ($users as $user) {
            $tokens[] = $user->ub_token;
        }
        $tokens = array_values(array_unique($tokens));
        /** @var $fcm */
        $fcm = [
            'title' => 'Tin nội bộ DXMB',
            'body'  => $post->title,
            'sound' => 'dxmb'
        ];
        self::pushPostFCM($tokens, $fcm['title'], $fcm['body'], $post->id, $fcm['sound']);
        return true;
    }

    /**
     * @param $instance
     * @param $model
     * @param bool $api_gate
     * @return bool
     */
    public static function notifyApprovalReminded($instance, $model, $api_gate=true) {
        if (!$instance) return false;
        /** @var $title */
        $title = 'Nhắc duyệt ';
        switch ($model) {
            case BOTransaction::class:
                $title .= 'Yêu cầu lock';
                break;
            case BOBill::class:
                $title .= 'Hợp đồng';
                break;
            default:
                return false;
        }
        /** @var $product */
        $product = $instance->product;
        $product_name = $product->pb_title?? $product->pb_code;
        /** @var $user */
        $user = BOUser::getCurrent('ub_title', $api_gate);
        $body = "Căn: $product_name - NV: $user";
        /** @var $product_managers */
        $product_managers = BOProductController::getProductManagersByApartment($product->{BOProduct::ID_KEY})?? [];
        if ($model===BOBill::class && $instance->status_id===BOBill::STATUSES['STATUS_APPROVED']) {
            $managers = BOUserController::getCurrentDepartmentHeads($api_gate);
            $product_managers = $managers? array_merge($product_managers, $managers) : $product_managers;
        }
        /** @var $tokens */
        $tokens = BOUser::where('ub_status', env('STATUS_ACTIVE', 1))
            ->whereIn(BOUser::ID_KEY, array_unique($product_managers))
            ->pluck('ub_token')
            ->toArray();
        switch ($model) {
            case BOTransaction::class:
                self::pushTransactionFCM($tokens, $title, $body, $instance->{BOTransaction::ID_KEY});
                break;
            case BOBill::class:
                self::pushBillFCM($tokens, $title, $body, $instance->{BOBill::ID_KEY});
                break;
            default:
        }
        return true;
    }

}
