<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\PaymentMethod;

class PaymentMethodController extends Controller
{
    private $o_PayMentMethod;
    public function __construct() {
        $this->o_PayMentMethod = new PaymentMethod();
    }
    public function getAllPayMentMethod(){
        $a_Data = $this->o_PayMentMethod->getAllSearch();
        
        $Data_view['a_PayMentMethod'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];        
        return view('paymentMethod.index',$Data_view);
        
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit customer
     */
    public function addEditPayMentMethod(){
        $a_DataView = array();
        $PayMentMethodId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $PayMentMethodId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->o_PayMentMethod->AddEditPayMentMethod($PayMentMethodId);
                return redirect('payment_medthod')->with('status', 'Cập nhật thành công!');
        }
        $a_DataView['PayMentMTData'] = $PayMentMethodId != 0 ? $this->o_PayMentMethod->getPayMentMethodById($PayMentMethodId) : array();

        return view('paymentMethod.edit', $a_DataView );
    }
}
