<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BankInfo;
use App\PaymentMethod;
use Maatwebsite\Excel\Facades\Excel;
use App\ImportExcel;
use App\Category;
use App\BOUser;


class ImportExcelController extends Controller
{
    private $o_ImportExcel;
    public function __construct() {
        $this->o_ImportExcel = new ImportExcel();
    }
    
    public function ImportProject(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_ImportExcel->ImportExcelProject($sz_FileDir);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }

            return view('import.import',['a_Res'=>$strRes]);

        }else{
            return view('import.import');
        }
        
    }
    public function ImportBuilding(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_ImportExcel->ImportExcelBuilding($sz_FileDir);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }

            return view('import.import',['a_Res'=>$strRes]);

        }else{
            return view('import.import');
        }
        
    }
    
    public function ImportProduct(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = Input::file('excel')->getClientOriginalName();
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_ImportExcel->ImportExcelProduct($sz_FileDir);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }

            return view('import.import',['a_Res'=>$strRes]);

        }else{
            return view('import.import');
        }
        
    }
    
    /**

     * @auth: Dienct
     * @des: import Customer In BO
     * @since: 14/09/2018
     *      */
    public function ImportCustomer(){
        
        $a_Res = array();
        if (Input::hasFile('excel')) {
            
            $filename = time().'_'.Input::file('excel')->getClientOriginalName();            
            $extension = Input::file('excel')->getClientOriginalExtension();
            
            if($extension == 'xlsx' || $extension == 'xls'){
                Input::file('excel')->move('uploads/', $filename);
                $sz_FileDir = 'uploads'."/".$filename;
                $a_Res = $this->o_ImportExcel->ImportExcelCustomer($sz_FileDir,$filename);
                $strRes = "";
                foreach ($a_Res as $key => $val){
                    $strRes .=" ".$val;
                }
            }else{
                $strRes = "Cần nhập đúng định dạng file (xls, xlsx)!!!!";
            }
            $a_Project = DB::table('b_o_categories')->where('cb_status', 1)->where('cb_level', 1)->get();
            $dataView['a_projects'] = $a_Project;
            $dataView['a_Saffs'] = BOUser::getAllUserInBo();
            $dataView['a_Res'] = $strRes;

            return view('import.importUser',$dataView);

        }else{
            $a_Project = DB::table('b_o_categories')->where('cb_status', 1)->where('cb_level', 1)->get();
            $dataView['a_projects'] = $a_Project;
            $dataView['a_Saffs'] = BOUser::getAllUserInBo();
            
            return view('import.importUser', $dataView);
        }
        
    }

    
}
