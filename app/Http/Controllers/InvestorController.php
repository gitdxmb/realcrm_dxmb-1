<?php
namespace App\Http\Controllers;
use App\BOBill;
use App\BOInvestor;
use Illuminate\Http\Request;
class InvestorController extends Controller
{
    public function index() {

        /** @var $query */
//        $query = BOBill::where([BOBill::ID_KEY => '1544608997'])
//            ->with([
//                'staff' => function($staff) {
//                    $staff->select([
//                        'ub_id',
//                        'ub_title AS name',
//                        'ub_account_name AS account',
//                        'ub_email AS email',
//                        'ub_avatar AS avatar',
//                        'signature',
//                        'ub_tvc_code',
//                        "group_ids",
//                        'ub_account_tvc'
//                    ])
//                        ->with(["group" => function($group) {
//                            return $group->select(["gb_title"]);
//                        }]);
//                },
//                'transaction' => function($transaction) {
//                    $transaction->select([
//                        'trans_id', 'trans_deposit AS deposit_amount', 'staff_code'
//                    ]);
//                },
//                'product' => function($apartment) {
//                    return $apartment->select([
//                        "pb_id",
//                        "pb_title AS apartment", "pb_code AS code",
//                        "category_id", "pb_required_money",
//                        "pb_required_money AS required_money",
//                        "pb_used_s", "pb_built_up_s", "pb_code_real", "pb_price_total",
//                        "price_used_s","pb_price_per_s",
//                        "stage", "book_type"
//                    ]);
//                },
//            ])->first();
//        MailController::customerContract($query, BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']);
//die('dang gui mail day');
        $investors = BOInvestor::paginate(30);
        $a_DataView['investors'] = $investors;
        return view('investor.index',$a_DataView);
    }

    public function addEdit(Request $request) {
        $investorId = $request->input('id', 0);
        $submit = $request->input('submit');
        $a_DataView['investorId'] = $investorId;
        if($investorId != 0){
            $investor = BOInvestor::find($investorId);
            if($investor) $a_DataView['investor'] = $investor;
        }else{
            $investor = new BOInvestor();
        }
        if (isset($submit) && $submit != "") {
            $investor->code = $request->input('code');
            $investor->name = $request->input('name');
            if($investor->save()){
                return redirect('investor')->with('status', 'Cập nhật thành công!');
            }
        }
        return view('investor.addedit',$a_DataView);
    }
}
