<?php

namespace App\Http\Middleware;

use App\Http\Controllers\API\AuthController;
use Closure;

class CheckStoredJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!AuthController::checkJWT()) {
            return response()->json(['msg' => 'Phiên đăng nhập hết hạn!', 'success' => false, 'status' => 401], 401);
        }
        return $next($request);
    }
}
