<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Util;

class BORole extends Model
{
    const ID_KEY = 'ro_id';
    protected $casts = [
        'ro_object_ids' => 'array',
        'ro_group_ids' => 'array',
        'ro_role_detail' => 'array',
    ];

    /**

     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditRole($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['ro_title'] = Input::get('ro_title');
        
        $a_DataUpdate['ro_description'] = Input::get('ro_description');
        $a_DataUpdate['ro_key'] = Input::get('ro_key');
        $a_objectId = Input::get('ro_object_ids');
        if(count($a_objectId) >0){
            foreach($a_objectId as $val){
                $a_Data = DB::table('b_o_categories')->where('cb_id', $val)->first();
                if($a_Data->cb_level == 2){
                    $a_objectId[] = DB::table('b_o_categories')->where('id', $a_Data->parent_id)->first()->cb_id;
                }
                if($a_Data->cb_level == 3){
                    $a_oblv2 = DB::table('b_o_categories')->where('id', $a_Data->parent_id)->first();
                    $a_objectId[] = $a_oblv2->cb_id;
                    $a_oblv1 = DB::table('b_o_categories')->where('id', $a_oblv2->parent_id)->first();
                    $a_objectId[] = $a_oblv1->cb_id;
                }
            }
            $a_objectId = array_unique($a_objectId);
            $CatidCB = array();
            foreach($a_objectId as $val){
                $CatidCB[$val] = $val;
            }
            
            $a_DataUpdate['ro_object_ids'] = json_encode($CatidCB);
        }
        
        
        if(count(Input::get('ro_group_ids')) > 0){
            $groupRgId = array();
            foreach(Input::get('ro_group_ids') as $val1){
                $groupRgId[$val1] = $val1;
            }
            $a_DataUpdate['ro_group_ids'] = json_encode($groupRgId);
        }
        
        if(count(Input::get('ro_role_detail')) > 0){
            $roleRoId = array();
            foreach(Input::get('ro_role_detail') as $val3){
                $roleRoId[$val3] = $val3;
            }
            $a_DataUpdate['ro_role_detail'] = json_encode($roleRoId);
        }
        
        
                        
        $a_DataUpdate['ro_status'] = Input::get('ro_status') == 'on' ? 1 : 0;
        
        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['ro_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_roles')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['ro_id'] = time();
            $a_DataUpdate['ro_created_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['ro_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_roles')->insert($a_DataUpdate);
        }
    }
    /**

     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditRoleMenu($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['mb_title'] = Input::get('mb_title');
        
        $a_DataUpdate['mb_note'] = Input::get('mb_note');
        
        if(count ( (array)Input::get('role_group_id')) > 0){
            $groupRgId = array();
            foreach(Input::get('role_group_id') as $val1){
                $groupRgId[$val1] = $val1;
            }
            $a_DataUpdate['role_group_id'] = json_encode($groupRgId);
        }
        
        if(count(Input::get('mb_menu_detail')) > 0){
            $roleRoId = array();
            foreach(Input::get('mb_menu_detail') as $val3){
                $roleRoId[$val3] = $val3;
            }
            $a_DataUpdate['mb_menu_detail'] = json_encode($roleRoId);
        }
        
        
                        
        $a_DataUpdate['mb_status'] = Input::get('mb_status') == 'on' ? 1 : 0;
        
        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['mb_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_role_menu')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['mb_id'] = time();
            $a_DataUpdate['mb_updated_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['mb_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('b_o_role_menu')->insert($a_DataUpdate);
        }
    }
    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getRoleById($id) {
        
        $a_Data = DB::table('b_o_roles')->where('id', $id)->first();
        
        if ($a_Data){
            $a_Data->mb_created_time = Util::sz_DateTimeFormat($a_Data->ro_created_time);
            $a_Data->mb_updated_time = Util::sz_DateTimeFormat($a_Data->ro_updated_time);
        }

        return $a_Data;
    }
    public function getRoleMenuById($id) {
        
        $a_Data = DB::table('b_o_role_menu')->where('id', $id)->first();
        
        if ($a_Data){
            $a_Data->mb_created_time = Util::sz_DateTimeFormat($a_Data->mb_created_time);
            $a_Data->mb_updated_time = Util::sz_DateTimeFormat($a_Data->mb_updated_time);
        }

        return $a_Data;
    }
    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('b_o_roles')->select('*');
        $a_data = $o_Db->where('ro_status', '!=', -1);
        $a_search = array();
        //search
        
        $sz_ro_title = Input::get('ro_title','');
        if($sz_ro_title != '') {
            $a_search['ro_title'] = $sz_ro_title;
            $a_data = $o_Db->where('ro_title', 'like', '%'.$sz_ro_title.'%');
        }
                
        $a_data = $o_Db->orderBy('ro_updated_time', 'desc')->paginate(30);
                
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    
    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearchMenuRole() {
        $a_data = array();
        $o_Db = DB::table('b_o_role_menu')->select('*');
        $a_data = $o_Db->where('mb_status', '!=', -1);
        $a_search = array();
        //search
        
        $sz_mb_title = Input::get('mb_title','');
        if($sz_mb_title != '') {
            $a_search['mb_title'] = $sz_mb_title;
            $a_data = $o_Db->where('mb_title', 'like', '%'.$sz_mb_title.'%');
        }
                
        $a_data = $o_Db->orderBy('mb_updated_time', 'desc')->paginate(30);
                
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    
    public function a_GetAllRoleByRoleGroupIDFilter($i_RoleGroupId){
        if(isset($i_RoleGroupId) && $i_RoleGroupId > 0) {
            $a_DataRole = array();
            $a_DataRole = DB::table('role_menu_web')->select('*')->where('id', $i_RoleGroupId)->first();
            $a_DataRole->role_menu = json_decode($a_DataRole->role_menu);
            
            return $a_DataRole;
        }
    }
    public function AddEditRoleMenuWeb($id){        
        $a_Data =  Input::all();        
        unset($a_Data["_token"]);
        unset($a_Data["submit"]);
        unset($a_Data["id"]);
        
        $a_DataUpdate['role_group_ids'] = json_encode($a_Data['role_group_id']);
        $a_DataUpdate['title'] = $a_Data['title'];
        unset($a_Data["role_group_id"]);
        unset($a_Data["title"]);
        
        $a_DataUpdate['role_menu'] = json_encode($a_Data);
        
        $a_DataUpdate['updated_at'] = date('Y-m-d H:i:s', time());
        $a_DataUpdate['update_by'] = Auth::guard('loginfrontend')->user()->ub_id; // ke toan thu tien
        
        if (is_numeric($id) == true && $id != 0) {
            DB::table('role_menu_web')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['created_at'] = date('Y-m-d H:i:s', time());
            DB::table('role_menu_web')->insert($a_DataUpdate);
        }
        

    }
    
}
