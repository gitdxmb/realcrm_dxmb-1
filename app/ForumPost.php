<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumPost extends Model
{
    public function getImagesAttribute($value) {
        if ($value) {
            $value = json_decode($value)->value;
            foreach ($value as &$item) {
                $item = asset($item);
            }
        }
        return $value;
    }
    public function getLikesAttribute($value) {
        return $value? json_decode($value)->value : null;
    }
    public function getDislikesAttribute($value) {
        return $value? json_decode($value)->value : null;
    }
    public function getTagsAttribute($value) {
        return $value? json_decode($value)->value : null;
    }
    public function getCreatedAtAttribute($value) {
        return $value? Util::timeAgo($value) : $value;
    }
    public function getCreatedByAttribute($value) {
        return $this->is_anonymous? null : $value;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case 'images':
            case 'likes':
            case 'dislikes':
            case 'tags':
                $this->attributes[$key] = $value? json_encode(['value' => array_values($value)]) : null;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    public function author() {
        return $this->belongsTo(BOUser::class, 'created_by', BOUser::ID_KEY);
    }

    public function feedbacks() {
        return $this->hasMany(ForumFeedback::class, 'forum_post_id');
    }
}
