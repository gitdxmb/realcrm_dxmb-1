<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BOCustomerDiary extends Model
{
    const ID_KEY = 'cd_id';
    public $timestamps = false;

    protected $casts = [
        'date' => 'datetime:d-m-Y',
        'created_at' => 'datetime:H:i d-m-Y'
    ];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case self::ID_KEY:
                $this->attributes[$key] = $value?? time();
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    public function project() {
        return $this->belongsTo(BOCategory::class, 'cd_category', BOCategory::ID_KEY);
    }
}
