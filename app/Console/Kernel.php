<?php

namespace App\Console;

use App\Http\Controllers\API\APIController;
use App\Log;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        /** Auto clear log db */
        $schedule->call(function () {
            Log::where('updated_at', '<=', Carbon::now()->subWeek(1))->delete();
        })->dailyAt("23:00");
        /** Auto renew TVC session every 3 minutes */
        $schedule->call(function () {
            APIController::__TVC_RENEW_SESSION();
        })->cron('*/3 * * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
