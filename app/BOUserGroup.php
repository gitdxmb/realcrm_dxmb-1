<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Notifications\Notifiable;

class BOUserGroup extends Model
{
    use Notifiable;

    public $timestamps = false;
    const ID_KEY = 'gb_id';
    protected $fillable = ['gb_id', 'reference_code', 'gb_status', 'gb_title', 'level_id', 'gb_created_time', 'gb_updated_time'];
    /**
     * Group levels
     */
    const LEVELS = [
        'COMPANY'       => 1,
        'DEPARTMENT'    => 2,
        'TEAM'          => 3,
        'SUB_TEAM'      => 4
    ];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case self::ID_KEY:
                $this->attributes[$key] = $value?? time();
                break;
            case 'gb_created_time':
            case 'gb_updated_time':
                $this->attributes[$key] = $value?? now();
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /**
     * @param bool $get_id
     * @return int | self
     */
    public static function findGroupOneForAll($get_id = true) {
        $group = self::where('gb_code', 'ALL')->first();
        if (!$group) return null;
        return $get_id? $group->id : $group;
    }

    public static function getAllBranch(){
        $a_Data = DB::table('b_o_user_groups')->where('gb_status', 1)->get();
        return $a_Data;
    }
       public static function getSanCCN(){
        $a_Data = BOUserGroup::where('gb_status','!=','-1')
                                ->where('level_id',BOUserGroup::LEVELS['DEPARTMENT'])
                                ->where(function ($q){
                                    $q->orWhere('reference_code','LIKE','I%')->orWhere('reference_code','LIKE','SA%');}
                                )->orderBy('gb_title','ASC')->get();
        return $a_Data;
    }

}
