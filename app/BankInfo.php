<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Util;

class BankInfo extends Model {

    public $timestamps = false;
    const ID_KEY = 'bank_id';

    /**
     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditBank($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['bank_title'] = Input::get('bank_title');

        $a_DataUpdate['bank_code'] = Input::get('bank_code');
        $a_DataUpdate['bank_holder'] = Input::get('bank_holder');
        $a_DataUpdate['bank_status'] = Input::get('bank_status') == 'on' ? 1 : 0;
        $a_DataUpdate['bank_number'] = Input::get('bank_number');
        $a_DataUpdate['branch_id'] = Input::get('branch_id');
        if(count(Input::get('payment_method_ids')) > 0){
            $payment_method_id = array();
            foreach(Input::get('payment_method_ids') as $val1){
                $payment_method_id[$val1] = $val1;
            }
            $a_DataUpdate['payment_method_ids'] = json_encode($payment_method_id);
        }

        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['bank_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('bank_infos')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['bank_id'] = time();
            $a_DataUpdate['bank_created_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['bank_updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('bank_infos')->insert($a_DataUpdate);
        }
    }

    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getBankInfoById($id) {

        $a_Data = DB::table('bank_infos')->where('id', $id)->first();

        if ($a_Data) {
            $a_Data->bank_created_time = Util::sz_DateTimeFormat($a_Data->bank_created_time);
            $a_Data->bank_updated_time = Util::sz_DateTimeFormat($a_Data->bank_updated_time);
        }

        return $a_Data;
    }

    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('bank_infos')->select('*');
//        $a_data = $o_Db->where('bank_status', '!=', -1);
        $a_search = array();
        //search

        $sz_bank_title = Input::get('bank_title', '');
        if ($sz_bank_title != '') {
            $a_search['bank_title'] = $sz_bank_title;
            $a_data = $o_Db->where('bank_title', 'like', '%' . $sz_bank_title . '%');
        }
        $a_data = $o_Db->orderBy('bank_title', 'asc')->get();


        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    /*
     * @auth: Dienct
     * @Des: get all Payment Method
     * @Since: 06/09/2018
     * 
     * **/
   public static function getAllBank(){
       $o_Db = DB::table('bank_infos')->select('*')->where('bank_status', '!=', -1)->orderBy('bank_title', 'asc')->get();
       return $o_Db;
   }

}
