<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'department',
        'project',
        'created_by',
        'value',
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;
}
