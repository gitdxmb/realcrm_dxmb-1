@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == 0 ? 'Thêm khách hàng' : 'Sửa khách hàng' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="user_customer_mappings">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="name" class="col-xs-12 col-sm-3 control-label text-left">Tên Khách Hàng</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="c_title" name="c_title" field-name="Tên" type="text" value="<?php echo isset($customerData->c_title) ? $customerData->c_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Khách Hàng" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="c_phone" class="col-xs-12 col-sm-3 control-label text-left">Số điện thoại</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="c_phone" name="c_phone" field-name="Số điện thoại" type="text" value="<?php echo isset($customerData->c_phone) ? $customerData->c_phone : "" ?>" class="form-control check-duplicate" placeholder="Số Điện Thoại" required readonly/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="email" class="col-xs-12 col-sm-3 control-label text-left">Email</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="c_phone" name="email" field-name="email" type="text" value="<?php echo isset($customerData->email) ? $customerData->email : "" ?>" class="form-control check-duplicate" placeholder="Email" required />
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
