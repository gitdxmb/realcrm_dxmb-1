@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Khách Hàng Vừa Tải Nhập</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="tc_name" name="tc_name" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['tc_name'])?$a_search['tc_name']:''?>">
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>
    <br/>
    <br/>
    <div class="form-group">
        <select class="form-control input-sm js-select2" id="staff_id" name="staff_id">
                    <option value=""><span class="text-center">Chọn Nhân Viên Phân Bổ</span></option>
                    @if(count($a_Saffs) > 0)
                        @foreach($a_Saffs as $key => $valStaff )
                        <option value="{{$valStaff->ub_id}}" <?php echo isset($customerData->tc_staff_id) && $customerData->tc_staff_id == $valStaff->ub_id ? 'selected' : '' ?> > {{$valStaff->ub_account_tvc}}</option>
                        @endforeach
                    @endif
                    
        </select>
    </div>
    <div class="form-group">
        <input type="button" class="btn btn-warning btn-sm" value="Phân Bổ" onclick="GLOBAL_JS.v_fTransferDataTMP()">
    </div>
    <br/>
    <br/>
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <th class="bg-success"><input type="checkbox" id="check_all" class="checkAll"></th>
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên Khách</strong></td>
                <td class="bg-success"><strong>Số Điện Thoại</strong></td>
                <td class="bg-success"><strong>Email</strong></td>
                <td class="bg-success"><strong>Tên File</strong></td>
                <td class="bg-success"><strong>Ngày Tạo</strong></td>
                <td class="bg-success"><strong>Phẩn Hồi</strong></td>
                <td class="bg-success"><strong>Thư Ký</strong></td>
                <td class="bg-success"><strong>Nhân viên được phân bổ</strong></td>
            </tr>
            @foreach ($a_Customer as $a_val)
            <tr>
                <td><input type="checkbox" class="chk_item" value="<?php echo $a_val->id?>" name="check[]"/></td>
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->tc_name }}</td>
                <td>    {{ $a_val->tc_phone }}</td>
                <td>    {{ $a_val->tc_email }}</td>
                <td>    {{ $a_val->tc_file_name }}</td>
                <td>    {{ $a_val->tc_create_time }}</td>
                <td>    Phản Hồi</td>
                <td>{{ $a_val->cf_assign_name }}</td>
                <td>{{ $a_val->tc_employ_name }}</td>
            </tr>
        @endforeach
        
        </table>
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="tmp_customer_data">
<?php echo (empty($a_search)) ? $a_Customer->render(): $a_Customer->appends($a_search)->render();?>

@endsection