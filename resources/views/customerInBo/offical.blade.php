@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách Khách Hàng</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="cb_name" name="cb_name" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['cb_name'])?$a_search['cb_name']:''?>">
    </div>
    
    <div class="form-group">
        
        <select class="form-control input-sm " id="source_id" name="source_id">
                    <option value=""><span class="text-center">Chọn Nguồn</span></option>
                    @if(count($sourceCustomer) > 0)
                        @foreach($sourceCustomer as $key => $val )
                        <option value="{{$key}}" <?php echo isset($a_search['source_id']) && $a_search['source_id'] == $key ? 'selected':''?> > {{$val}}</option>
                        @endforeach
                    @endif
        </select>
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-warning btn-sm" value="Phân Bổ" onclick="GLOBAL_JS.v_fTransferData()">
    </div>
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <th class="bg-success"><input type="checkbox" id="check_all" class="checkAll"></th>
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên Khách</strong></td>
                <td class="bg-success"><strong>Số Điện Thoại</strong></td>
                <td class="bg-success"><strong>Email</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_Customer as $a_val)
            <tr>
                <td><input type="checkbox" class="chk_item" value="<?php echo $a_val->id?>" name="check[]"/></td>
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->cb_name }}</td>
                <td>    {{ $a_val->cb_phone }}</td>
                <td>    {{ $a_val->cb_email }}</td>
                <td>
                    <?php
                        if($a_val->cb_status == 1){
                    ?>
                    <a title="Edit" href="<?php echo Request::root().'/offical_customer/edit?id='.$a_val->id;?>" title="Edit" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'cb_status')" title="Cho vào thùng rác" class="not-underline">
                    <i class="fa fa-trash fa-fw text-danger"></i>
                    </a>
                    <?php }else if($a_val->cb_status == 0){ ?>
                    <a title="Khôi phục user" href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'cb_status')"  title="Edit" class="not-underline">
                        <i class="fa fa-upload fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'cb_status')" title="Xóa vĩnh viễn" class="not-underline">
                        <i class="fa fa-trash-o fa-fw text-danger"></i>
                    </a>
                    <?php }?>
                </td>
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="b_o_customers">
<?php echo (empty($a_search)) ? $a_Customer->render(): $a_Customer->appends($a_search)->render();?>

@endsection