@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách PTTT</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="pm_title" name="pm_title" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['pm_title'])?$a_search['pm_title']:''?>">
    </div>
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>    
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tiêu Đề</strong></td>
                <td class="bg-success"><strong>Code</strong></td>
                <td class="bg-success"><strong>Mô Tả</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_PayMentMethod as $a_val)
            <tr>
                
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->pm_title }}</td>
                <td>    {{ $a_val->pm_code }}</td>
                <td>    {{ $a_val->pm_note }}</td>
                <td>
                    <?php
                        if($a_val->pm_status == 1){
                    ?>
                    <a title="Edit" href="<?php echo Request::root().'/payment_medthod/addedit?id='.$a_val->id;?>" title="Edit" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'pm_status')" title="Cho vào thùng rác" class="not-underline">
                    <i class="fa fa-trash fa-fw text-danger"></i>
                    </a>
                    <?php }else if($a_val->pm_status == 0){ ?>
                    <a title="Khôi phục user" href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'pm_status')"  title="Edit" class="not-underline">
                        <i class="fa fa-upload fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'pm_status')" title="Xóa vĩnh viễn" class="not-underline">
                        <i class="fa fa-trash-o fa-fw text-danger"></i>
                    </a>
                    <?php }?>
                </td>
            </tr>
        @endforeach
        </table>
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="payment_methods">
<?php echo (empty($a_search)) ? $a_PayMentMethod->render(): $a_PayMentMethod->appends($a_search)->render();?>

@endsection