@extends('layouts.admin.layoutAdmin')
@section ('cssfile')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<style>
.alert {
    padding: 3px !important;}
.pagination {margin: 0 !important;
}
    
</style>

@stop
@section('content')
    
    <form method="get" action="" id="frmFilter" name="frmFilter" class="row">
        <div class='col-lg-12'>
            <h3 class="no-padding text-uppercase">Danh sách sản phẩm</h3>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="form-group col-md-3">
                    <input class="form-control" value = "{{isset($request['r_text'])?$request['r_text']:''}}" name = 'r_text' id='r_text' placeholder="Nhập mã sp, tên sp, dự án..." onchange="this.form.submit()">
                </div>
                <div class="form-group col-md-4">
                    <select class="form-control js-select2" id="r_project_id" name="r_project_id" onchange="this.form.submit()">
                        <option value="">Tìm Dự án</option>
                        @foreach ($r_project as $o_Project)
                            <option @if(isset($request['r_project_id'])&&$request['r_project_id']==$o_Project->id) selected @endif value="{{ $o_Project->id }}">{{ $o_Project->reference_code }}-{{ $o_Project->cb_title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <select class="form-control input-sm js-select2" name="status_id" onchange="this.form.submit()">
                        <option value=""><span class="text-center">Tìm trạng thái</span></option>
                        @foreach (\App\BOProduct::STATUS_DISPLAY as $id => $text)
                            <option @if(isset($request['status_id'])&&$request['status_id']==$id) selected @endif value="{{ $id }}"><span class="text-center">{{ $text['text'] }}</span></option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
                    <input type="submit" class="btn btn-success btn-sm submit hide">
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <div class='row' >
                <div class="col-lg-2">
                    <div class="alert alert-danger">
                        <p>Tổng số: <b> {{ $list_product->total() }}</b></p>
                    </div>
                </div>
                <div class="col-lg-2">
                    <input hidden name = 'export_excel' id='export_excel'>

                    <label class="checkbox-inline btn btn-primary btn-sm" onclick="exportExcel(this.form);"><i class="fa fa-download" >Xuất file Excel</i></label>
                </div>
                <div class="form-group col-lg-2">
                    <select class="form-control js-select2" id="itemperpage" name="itemperpage" onchange="this.form.submit()">
                        <option value="20" @if(isset($request['itemperpage'])&&$request['itemperpage']==20) selected @endif >20 kết quả /trang</option>
                        <option value="50" @if(isset($request['itemperpage'])&&$request['itemperpage']==50) selected @endif >50 kết quả /trang</option>
                        <option value="0" @if(isset($request['itemperpage'])&&$request['itemperpage']==0) selected @endif >Tất cả </option>

                    </select>
                </div>
                <div class='col-lg-6 text-right'>
                    {{ $list_product->links() }}
                </div>
            </div>
        </div>
        <input hidden name = 'kind' value="{{isset($request['kind'])?$request['kind']:'bill'}}">
    </form>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-bordered" id = 'example-export'>
            <thead class="header-tr">
                <th class="bg-success"><strong>STT</strong></th>               
                 <th class="bg-success"><strong>Mã căn hộ</strong></th>
                <th class="bg-success"><strong>Dự án</strong></th>
                <th class="bg-success"><strong>Tòa/khu vực</strong></th>
                <th class="bg-success"><strong>Tổng giá (max)</strong></th>
                <th class="bg-success"><strong>T.Tường / T.Thủy ($/m2)</strong></th>
                <th class="bg-success"><strong>Giá trị yêu cầu</strong></th>               
                <th class="bg-success" align="center"><strong>Giới hạn lock</strong></th>
                <th class="bg-success"><strong>Đặt chỗ/cọc</strong></th>
                <!-- <th class="bg-success"><strong>Thư ký cầm</strong></th> -->
                <th class="bg-success"><strong>TT lock</strong></th>
                <th class="bg-success"><strong>TT giao dịch</strong></th>
                <th class="bg-success"><strong>Last update</strong></th>


            </thead>     
           
            @foreach ($list_product as $index =>  $product)
                <tbody>
                    <tr>
                        <td align="center">{{ $index + 1 }}</td>
                        <td>{{ $product->pb_code }}</td>
                         <td>{{ isset($product->category->sibling)?$product->category->sibling->cb_code:'-' }}</td>
                        <td>{{ isset($product->category)?$product->category->cb_code:'-' }}</td>
                       <td align='right'>{{ $product->pb_price_total?number_format($product->pb_price_total):'-' }}</td>
                       <td align="center"><b>{{ $product->pb_built_up_s }}</b> / <b>{{ $product->pb_used_s }}</b></td>
                       <td align='right'>{{ $product->pb_required_money?number_format($product->pb_required_money):'-' }}</td>
                       <td align="center"><span class="label label-warning">{{ $product->number_lock }}</span></td>
                        <td>{{ $product->stage!=1?'Đặt chỗ':'Đặt cọc' }}</td>
                       
                        <td bgcolor="{{\App\BOProduct::STATUS_DISPLAY[$product->status_id]['background-color']}}"
                            style='color:{{\App\BOProduct::STATUS_DISPLAY[$product->status_id]['color']}};'>
                            {{ \App\BOProduct::STATUS_DISPLAY[$product->status_id]['text']  }}
                        </td>

                        <td>
                            <span class="badge">{{ array_search($product->status_id, \App\BOProduct::STATUSES)?? '(Unknown)' }}</span>
                        </td>

                        <td>
                            {!! $product->updated_by? ($product->updated_by->ub_title.'<br/>(' . $product->updated_by->ub_account_tvc . ')<br/>') : '' !!}
                            {{ $product->ub_updated_time?? '' }}
                        </td>
                    </tr>
                </tbody>
            @endforeach
            
        </table>
        {{ $list_product->links() }}
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">
@endsection
@section('jsfile')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> 

<script type="text/javascript">
    $('.js-select2').select2();
    $('.input-daterange').datetimepicker({ format: 'DD-MM-YYYY' });
      function exportExcel(){
          alert('Hoàn thiện sau');
        // $('#export_excel').val(1);
        form = $('form[name="frmFilter"]');
        // form.submit();
    }
</script>

@endsection
