@extends('layouts.admin.layoutAdmin')
@section ('cssfile')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />

<style>
.alert {
    padding: 3px !important;}
.pagination {margin: 0 !important;
}
    
</style>

@stop
@section('content')
    
     <div class="row">
    <form method="get" action="" id="frmFilter" name="frmFilter"  >
        <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
        <div class='col-lg-12'>
            <div class="col-lg-4" style="text-align:left;">
                <h3 class="no-padding text-uppercase">Danh sách nhân viên </h3>

            </div>
           <div class="col-lg-4"></div>
        </div>
        <div class='row'>
      
          <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="r_project_id" name="department_id" onchange="this.form.submit()">
                <option value="">Tìm theo sàn</option>
                @foreach ($r_departments as $department)
                    <option @if(isset($request['department_id'])&&$request['department_id']==$department->gb_id) selected @endif value="{{ $department->gb_id }}">{{ $department->gb_title }}</option>
                @endforeach
            </select>
        </div>
      <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="staff_id" name="staff_id" onchange="this.form.submit()">
                <option value="">Nhân viên</option>
                @foreach ($staffList as $staff)
                    <option @if(isset($request['staff_id'])&&$request['staff_id']==$staff['ub_id']) selected @endif value="{{ $staff['ub_id'] }}">{{$staff['ub_staff_code']}}-{{ $staff['ub_title'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="q_type" name="q_type" onchange="this.form.submit()">
                <option value="">Theo trạng thái</option>
                <option value="1"@if(isset($request['q_type'])&&$request['q_type']==1) selected @endif>Chưa có chữ ký</option>                
              
               
            </select>
        </div>
        
        <div class="form-group col-lg-3">
          <div class="input-group" id="datepicker">
           
             <input type="text" class="input-sm form-control input-daterange" name="start_date" value="{{isset($request['start_date'])&&$request['start_date']?$request['start_date']:''}}" placeholder="từ ngày"  />                    
            <span class="input-group-addon">tới</span>
            <input type="text" class="input-sm form-control input-daterange" name="end_date" value="{{isset($request['end_date'])&&$request['end_date']?$request['end_date']:date('dd-mm-YYYY')}}" />
        </div>
        </div>
        
        <div class="form-group col-lg-1">
                  
            <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
    </div>
    <div class='row' >
        <div class='col-lg-2'>
            <div class="alert alert-danger">
                Tổng số: <b> {{ $list_staffs->total() }}</b>
            </div> 
        </div>
        <div class="col-lg-2">
            <input hidden name = 'export_excel' id='export_excel'>
          
            <label class="checkbox-inline btn btn-primary btn-sm" onclick="exportExcel(this.form);"><i class="fa fa-download" >Xuất file Excel</i></label>
        </div>
        <div class='col-lg-8'>
            <div class="row">
                <div class="form-group col-lg-3">
                    <select class="form-control js-select2" id="itemperpage" name="itemperpage" onchange="this.form.submit()">
                        <option value="20" @if(isset($request['itemperpage'])&&$request['itemperpage']==20) selected @endif >20 kết quả /trang</option>
                        <option value="50" @if(isset($request['itemperpage'])&&$request['itemperpage']==50) selected @endif >50 kết quả /trang</option>
                        <option value="0" @if(isset($request['itemperpage'])&&$request['itemperpage']==0) selected @endif >Tất cả </option>

                    </select>
                </div>
                <div class='col-lg-9'>{{ $list_staffs->links() }}</div>
            </div>
             <input hidden name = 'kind' value="{{isset($request['kind'])?$request['kind']:'bill'}}">
        </div>
    </div>
     </form>
   </div>
    <div class="row table table-responsive">
        <table class="table-hover table-striped table-bordered" id = 'example-export'>
            <thead class="header-tr">
            <tr class="text-center">
                <th class="bg-success text-center"><strong>STT</strong></th>
                <th class="bg-success text-center"><strong>Avatar</strong></th>
                <th class="bg-success text-center"><strong>Họ tên</strong></th>
                 <th class="bg-success text-center"><strong>Sàn</strong></th>
                <th class="bg-success text-center"><strong>Mã DXMB</strong></th>
                <th class="bg-success text-center"><strong>Mã Tavico</strong></th>
                <th class="bg-success text-center"><strong>Tài khoản Tavico</strong></th>
                <th class="bg-success text-center"><strong>Chữ ký</strong></th>
                <th class="bg-success text-center"><strong>LastLogin</strong></th>
                <th class="bg-success text-center"><strong>Ngày tạo</strong></th>
                <th class="bg-success text-center"><strong>Thao tác</strong></th>
            </tr>
            </thead>
           
            @foreach ($list_staffs as $index =>  $staff)
                <tbody>
                    <tr class="text-center">
                        <td align="center">{{ $index + 1 }}</td>
                        <td>
                            @if($staff->ub_avatar)
                                <div style="padding: 5px;">
                                <img class="img-responsive img-circle" width="60px" src="{{ url($staff->ub_avatar) }}" />
                                </div>
                            @endif
                        </td>
                        <td>{{ $staff->ub_title }}</td>
                        <td>{{ isset($staff->group)?$staff->group->gb_title:'-' }}</td>
                        <td>{{ $staff->ub_staff_code }}</td>
                        <td>{{ $staff->ub_tvc_code }}</td>
                        <td>{{ $staff->ub_account_tvc }}</td>
                        <td>
                            @if($staff->signature)
                                <a href="{{ url($staff->signature).'?v='.time()}}" class="fancybox">
                                    <img class="img-responsive" src="{{ url($staff->signature).'?v='.time()}}" />
                                </a>
                            @endif </td>
                        <td>{{ $staff->ub_last_logged_time? date('d-m-Y h:i',strtotime($staff->ub_last_logged_time)):'-' }}</td>
                        <td>{{ $staff->ub_created_time? date('d-m-Y h:i',strtotime($staff->ub_created_time)):'-' }}</td>
                        <td>
                            <button type="button" class="btn btn-danger btn-sm" onclick="resetToken('{{ $staff->ub_id }}')">Logout App</button>
                        </td>               
                    </tr>
                </tbody>
            @endforeach
            <input type="hidden" id='urlReset' value="{{url('report/1/resetToken')}}">
            <tfoot>
            <tr>
                <td colspan="11">
                    <div class="text-center">
                    {{ $list_staffs->links() }}
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">
@endsection
@section('jsfile')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

<script type="text/javascript">
    $(function () {
        $(".fancybox").fancybox();
        $('.js-select2').select2();
        $('.input-daterange').datetimepicker({ format: 'DD-MM-YYYY' });
    });

    function exportToExcel(){
        alert('Chuẩn bị exportToExcel');
    }
    function resetToken(id){  
        // alert('HHHH');
        let _token = $('meta[name="csrf-token"]').attr("content");
        let urlReset = $('#urlReset').val() + '?id='+id+'&_token='+_token;    
        $.ajax({
            type: 'GET',
            url: urlReset,           
            success: function (msg) {
                alert(msg);
            },
            error: function () {

                alert('Loi');
            }
        });
    }
    function exportExcel(){
          alert('Hoàn thiện sau');
        // $('#export_excel').val(1);
        form = $('form[name="frmFilter"]');
        // form.submit();
    }
  
</script>

@endsection
