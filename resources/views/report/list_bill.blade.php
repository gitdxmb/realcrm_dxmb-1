@extends('layouts.admin.layoutAdmin')
@section('cssfile')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<style>
    .alert {
        padding: 3px !important;
    }
    .pagination {
        margin: 0 !important;
    }
</style>

@endsection
@section('content')
    
     <div class="row">
    <form method="get" action="" id="frmFilter" name="frmFilter"  >
        <div class='col-lg-12'>
            <div class="col-lg-4" style="text-align:left;">
                <h3 class="no-padding text-uppercase">Danh sách Bill <a class="btn btn-primary btn-xs" href="{{url('report/1/list-action/?type_report=2')}}">
                                <i class="fa fa fa-bar-chart-o"></i> Xem biểu đồ
                            </a></h3>

            </div>
           <div class="col-lg-4"></div>
        </div>
        <div class='row'>
         <div class="form-group col-lg-2">
        <input  class="form-control" value = "{{isset($request['r_text'])?$request['r_text']:''}}" name = 'r_text' id='r_text' placeholder="Nhập mã sản phẩm.." onchange="this.form.submit()">
    </div>
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="r_project_id" name="r_project_id" onchange="this.form.submit()">
                <option value="">Tìm Dự án</option>
                @foreach ($r_project as $o_Project)
                    <option @if(isset($request['r_project_id'])&&$request['r_project_id']==$o_Project->id) selected @endif value="{{ $o_Project->id }}">{{ $o_Project->reference_code }}-{{ $o_Project->cb_title }}</option>
                @endforeach
            </select>
        </div>

          <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="r_department_id" name="department_id" onchange="this.form.submit()">
                <option value="">Tìm theo sàn</option>
                @foreach ($r_departments as $department)
                    <option @if(isset($request['department_id'])&&$request['department_id']==$department->gb_id) selected @endif value="{{ $department->gb_id }}">{{ $department->gb_title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="staff_id" name="staff_id" onchange="this.form.submit()">
                <option value="">Tìm nhân viên</option>
                @foreach ($staffList as $key =>$staff)
                    <option @if(isset($request['staff_id'])&&$request['staff_id']==$key) selected @endif value="{{ $key }}">{{ $staff['ub_account_tvc'] }}-{{ $staff['ub_title'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control input-sm js-select2" name="status_id" onchange="this.form.submit()">
                <option value=""><span class="text-center">Tìm trạng thái</span></option>
                @foreach (\App\BOBill::STATUS_TEXTS_WEB as $id => $text)
                    <option @if(isset($request['status_id'])&&$request['status_id']==$id) selected @endif value="{{ $id }}"><span class="text-center">{{ $text }}</span></option>
                @endforeach
            </select>
        </div>
        
        
    </div>
    <div class='row' >

        <div class="alert alert-danger col-lg-1 " text-align="right" style="margin-left:20px">
        Tổng số: <b> {{ $list_bills->total() }}</b>
        </div> 
         <div class="col-lg-1">
            <input hidden name = 'export_excel' id='export_excel'>
          
            <label class="checkbox-inline btn btn-primary btn-sm" onclick="exportExcel(this.form);"><i class="fa fa-download" >Xuất file Excel</i></label>
        </div>
        <div class='col-lg-9'> 
     
            <div class="form-group col-lg-2"> 
            <select class="form-control js-select2" id="itemperpage" name="itemperpage" onchange="this.form.submit()">
                <option value="20" @if(isset($request['itemperpage'])&&$request['itemperpage']==20) selected @endif >20 kết quả /trang</option>
                <option value="50" @if(isset($request['itemperpage'])&&$request['itemperpage']==50) selected @endif >50 kết quả /trang</option>
                <option value="0" @if(isset($request['itemperpage'])&&$request['itemperpage']==0) selected @endif >Tất cả </option>
               
            </select> 
            </div>
            <div class='col-lg-6'>  {{ $list_bills->links() }}</div>
            <div class="form-group col-lg-3">
          <div class="input-group" id="datepicker">
           
             <input type="text" class="input-sm form-control input-daterange" name="start_date" value="{{isset($request['start_date'])&&$request['start_date']?$request['start_date']:''}}" placeholder="từ ngày"  />                    
            <span class="input-group-addon">tới</span>
            <input type="text" class="input-sm form-control input-daterange" name="end_date" value="{{isset($request['end_date'])&&$request['end_date']?$request['end_date']:date('dd-mm-YYYY')}}" />
        </div>
        </div>
        
        <div class="form-group col-lg-1">
                  
            <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
            
    <input hidden name = 'kind' value="{{isset($request['kind'])?$request['kind']:'bill'}}">
    </div>
    </div>
     </form>
   </div>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered" id = 'example-export'>
            <thead class="header-tr">
                <th class="bg-success"><strong>STT</strong></th>
                <th class="bg-success"><strong>Mã hợp đồng</strong></th>              
                <th class="bg-success"><strong>Sản phẩm</strong></th>
                <th class="bg-success"><strong>Khách hàng</strong></th>
                <th class="bg-success"><strong>Nhân viên</strong></th>
                <th class="bg-success"><strong>Yêu cầu / Tổng giá trị</strong></th>
                <th class="bg-success text-center"><strong>Hình thức</strong></th>
                <th class="bg-success"><strong>Ảnh</strong></th>
                <th class="bg-success"><strong>Ngày tạo</strong></th>
                <th class="bg-success text-center"><strong>Trạng thái</strong></th>
            </thead>     
           
            @foreach ($list_bills as $index =>  $bill)
                <tbody>
                    <tr bgcolor="{{\App\BOBill::STATUS_COLORS[$bill->status_id]}}">
                        <td align="center">{{ $index + 1 }}</td>
                        <td>
                            <p><b> {{ $bill->bill_code }}</b></p>
                            <ul style="padding-left: 10px" class="small">
                                <li class="list-item">Phiếu F1: <b>{{ $bill->bill_reference_code_F1?? '-' }}</b></li>
                                <li>GD F1: <b>{{ $bill->contract_reference_code_F1?? '-' }}</b></li>
                                <li>Phiếu F2: <b>{{ $bill->bill_reference_code?? '-' }}</b></li>
                                <li>GD F2: <b>{{ $bill->contract_reference_code?? '-' }}</b></li>
                            </ul>
                        </td>
                        <td><b>{{ isset($bill->product->pb_code)?$bill->product->pb_code:'-' }}</b></td>

                        <td>
                            <dl>
                                <dt><b>{{ isset($bill->bill_customer_info)?$bill->bill_customer_info->name:'-' }}</b></dt>
                                <dd>
                                    <small><i class="fa fa-envelope"></i> {!!  isset($bill->bill_customer_info)&&filter_var($bill->bill_customer_info->email, FILTER_VALIDATE_EMAIL)?'Hợp lệ' : '<i class="text-danger" title="'.$bill->bill_customer_info->email.'">KHÔNG hợp lệ</i>' !!}</small>
                                </dd>
                                <dd style="word-break: break-all">
                                    <small><i class="fa fa-credit-card"></i> {{ isset($bill->bill_customer_info)?$bill->bill_customer_info->id_passport:'-' }}</small>
                                </dd>
                                <dd><small><i class="fa fa-map-marker"></i> {{ isset($bill->bill_customer_info)?$bill->bill_customer_info->address:'-' }}</small></dd>
                            </dl>
                        </td>
                         <td>
                             <b>{{ isset($bill->staff)?$bill->staff->ub_title:'-' }}</b>
                             <p><small>{{ isset($bill->staff->group)?$bill->staff->group->gb_title:'-' }}</small></p>
                         </td>
                        <td style="text-align:right;">
                            {{ number_format($bill->bill_required_money) }} / {{ number_format($bill->bill_total_money) }}
                        </td>
                        <td class="text-center">
                            @if($bill->type==\App\BOBill::TYPE_DEPOSIT)
                                <span class="label label-success">Đặt Cọc</span>
                            @elseif($bill->type==\App\BOBill::TYPE_BOOK_ONLY)
                                <span class="label label-primary">Đặt Chỗ</span>
                                <br>
                                <span class="label label-primary">Cố định</span>
                            @else
                                <span class="label label-warning">Đặt Chỗ</span>
                                <br>
                                <span class="label label-warning">Tạm thời</span>
                            @endif
                        </td>
                         <td width="70px;">
                             @if(is_array($bill->media))
                                 @foreach($bill->media as $image)
                                     <a target="_blank" href="{{ url($image) }}"><img class="img-responsive" src="{{ url($image) }}" /></a>
                                 @endforeach
                             @endif
                         </td>
                        <td>{{ date('d-m-Y h:s:i', strtotime($bill->bill_created_time)) }}</td>

                        <td style="min-width: 100px;">
                            <p class="text-center">{!! \App\BOBill::STATUS_TEXTS_WEB_COLOR[$bill->status_id]  !!}</p>
                            <button data-bill="{{ $bill->bill_code }}" data-role="log-bill-modal" class="btn btn-xs btn-danger">
                                <i class="fa fa-sticky-note-o"></i> Xem nhật ký
                            </button>
                            <div class="log-content hidden">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="text-center text-success"><i class="fa fa-exchange"></i> Nhật ký duyệt giao dịch</h4>
                                        <dl class="small">
                                            @foreach($bill->bill_approval_log as $key => $logs)
                                                @if($logs)
                                                    <dt class="text-primary" style="margin-top: 10px;">{{ $key+1 . '. ' . $logs['NOTE'] }}</dt>
                                                    <dd style="margin-left: 10px;"><i class="fa fa-angle-right"></i> {{ isset($staffList[$logs['CREATED_BY']])? $staffList[$logs['CREATED_BY']]['ub_title'] : 'Vô danh' }}</dd>
                                                    <dd style="margin-left: 10px;"><i class="fa fa-angle-right"></i> Giá trị: {{$logs['VALUE']}}</dd>
                                                    <dd style="margin-left: 10px;"><i class="fa fa-clock-o"></i> {{ date('d-m-Y H:i',$logs['TIME']) }}</dd>
                                                @endif
                                            @endforeach
                                        </dl>
                                    </div>
                                    <div class="col-md-6" style="border-left: 1px solid grey;">
                                        <h4 class="text-center text-success"><i class="fa fa-database"></i> Nhật ký CSDL hợp đồng</h4>
                                        @if(is_array($bill->bill_log_edit))
                                        <dl class="small">
                                            @foreach($bill->bill_log_edit as $key => $log)
                                                <dt class="text-primary" style="margin-top: 10px;">{{ $key+1 . '. ' . $log['LABEL'] }}</dt>
                                                <dd style="margin-left: 10px;"><i class="fa fa-angle-right"></i> Cũ: {{ $log['FROM'] . ' ('.($log['OLD']?? 'N.A').')'}}</dd>
                                                <dd style="margin-left: 10px;"><i class="fa fa-angle-right"></i> Mới: {{$log['TO'] . ' ('. $log['VALUE'] .')'}}</dd>
                                                <dd style="margin-left: 10px;"><i class="fa fa-angle-right"></i> {{ isset($staffList[$log['BY']])? $staffList[$log['BY']]['ub_title'] : 'Vô danh' }}</dd>
                                                <dd style="margin-left: 10px;"><i class="fa fa-clock-o"></i> {{ $log['TIME'] }}</dd>
                                            @endforeach
                                        </dl>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            @endforeach
            
        </table>
        {{ $list_bills->links() }}
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">


     <!-- Log Modal -->
     <div id="billLogModal" class="modal fade" role="dialog">
         <div class="modal-dialog">
             <!-- Modal content-->
             <div class="modal-content">
                 <div class="modal-header bg-secondary">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Nhật ký Hợp đồng</h4>
                 </div>
                 <div class="modal-body">
                     <p>Không tìm thấy dữ liệu.</p>
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                 </div>
             </div>

         </div>
     </div>

@endsection
@section('jsfile')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> 

<script type="text/javascript">
    $('.js-select2').select2();
    $('.input-daterange').datetimepicker({ format: 'DD-MM-YYYY' });
    function exportExcel(){
        alert('Hoàn thiện sau');
        // $('#export_excel').val(1);
        // let form = $('form[name="frmFilter"]');
        // form.submit();
    }

    $(function () {
       $('[data-role="log-bill-modal"]').on('click', function () {
           let billLogModal = $('#billLogModal'), bill =  $(this).attr('data-bill');
           let content = $(this).parent().find('.log-content').html();
           console.log(content);
           billLogModal.find('.modal-body').html(content);
           billLogModal.find('.modal-title').text('Nhật ký Hợp đồng: ' + bill);
           billLogModal.modal('show');
       });
    });

</script>

@endsection
