@extends('layouts.admin.layoutAdmin')
@section ('cssfile')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<style>
.alert {
    padding: 3px !important;}
.pagination {margin: 0 !important;
}
    
</style>

@stop
@section('content')
    
     <div class="row">
    <form method="get" action="" id="frmFilter" name="frmFilter"  >
        <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
        <div class='col-lg-12'>
            <div class="col-lg-4" style="text-align:left;">
                <h3 class="no-padding text-uppercase">Danh sách yêu cầu        <a class="btn btn-primary btn-xs" href="{{url('report/1/list-action/?type_report=2')}}">
                                <i class="fa fa fa-bar-chart-o"></i> Xem biểu đồ
                            </a></h3>

            </div>
           <div class="col-lg-4"></div>
        </div>
        <div class='row'>
             <div class="form-group col-lg-2">
            <input  class="form-control" value = "{{isset($request['r_text'])?$request['r_text']:''}}" name = 'r_text' id='r_text' placeholder="Nhập mã sản phẩm.." onchange="this.form.submit()">
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="r_project_id" name="r_project_id" onchange="this.form.submit()">
                <option value="">Tìm Dự án</option>
                @foreach ($r_project as $o_Project)
                    <option @if(isset($request['r_project_id'])&&$request['r_project_id']==$o_Project->id) selected @endif value="{{ $o_Project->id }}">{{ $o_Project->reference_code }}-{{ $o_Project->cb_title }}</option>
                @endforeach
            </select>
        </div>
          <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="r_department_id" name="department_id" onchange="this.form.submit()">
                <option value="">Tìm theo sàn</option>
                @foreach ($r_departments as $department)
                    <option @if(isset($request['department_id'])&&$request['department_id']==$department->gb_id) selected @endif value="{{ $department->gb_id }}">{{ $department->gb_title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="staff_id" name="staff_id" onchange="this.form.submit()">
                <option value="">Nhân viên</option>
                @foreach ($staffList as $staff)
                    <option @if(isset($request['staff_id'])&&$request['staff_id']==$staff['ub_id']) selected @endif value="{{ $staff['ub_id'] }}">{{$staff['ub_staff_code']}}-{{ $staff['ub_title'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control input-sm js-select2" name="status_id" onchange="this.form.submit()">
                <option value=""><span class="text-center">Tìm trạng thái</span></option>
                @foreach (\App\BOTransaction::STATUSES as $id => $text)
                    <option @if(isset($request['status_id'])&&$request['status_id']==$id) selected @endif value="{{ $id }}"><span class="text-center">{{ $text }}</span></option>
                @endforeach
            </select>
        </div>
        
       
    </div>
    <div class='row' >

        <div class="alert alert-danger col-lg-1 " text-align="right" style="margin-left:20px">
        Tổng số: <b> {{ $list_trans->total() }}</b>
        </div> 
          <div class="col-lg-1">
            <input hidden name = 'export_excel' id='export_excel'>
          
            <label class="checkbox-inline btn btn-primary btn-sm" onclick="exportExcel(this.form);"><i class="fa fa-download" >Xuất file Excel</i></label>
        </div>
        <div class='col-lg-9'> 
     
            <div class="form-group col-lg-2"> 
            <select class="form-control js-select2" id="itemperpage" name="itemperpage" onchange="this.form.submit()">
                <option value="20" @if(isset($request['itemperpage'])&&$request['itemperpage']==20) selected @endif >20 kết quả /trang</option>
                <option value="50" @if(isset($request['itemperpage'])&&$request['itemperpage']==50) selected @endif >50 kết quả /trang</option>
                <option value="0" @if(isset($request['itemperpage'])&&$request['itemperpage']==0) selected @endif >Tất cả </option>
               
            </select> 
            </div>
            <div class='col-lg-6'>  {{ $list_trans->links() }}</div>
             <div class="form-group col-lg-3">
          <div class="input-group" id="datepicker">
           
             <input type="text" class="input-sm form-control input-daterange" name="start_date" value="{{isset($request['start_date'])&&$request['start_date']?$request['start_date']:''}}" placeholder="từ ngày"  />                    
            <span class="input-group-addon">tới</span>
            <input type="text" class="input-sm form-control input-daterange" name="end_date" value="{{isset($request['end_date'])&&$request['end_date']?$request['end_date']:date('dd-mm-YYYY')}}" />
        </div>
        </div>
        
        <div class="form-group col-lg-1">
                  
            <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
        <input hidden name = 'kind' value="{{isset($request['kind'])?$request['kind']:'bill'}}">    
   
    </div>
     </form>
   </div>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered" id = 'example-export'>
            <thead class="header-tr">
                <th class="bg-success"><strong>STT</strong></th>               
                 <th class="bg-success"><strong>Nội dung</strong></th>
                <th class="bg-success"><strong>Nhân viên</strong></th>            
                <th class="bg-success"><strong>Sản phẩm</strong></th>
                <th class="bg-success"><strong>Giá trị Y/c</strong></th>               
                <th class="bg-success"><strong>Nhật ký</strong></th>
                <th class="bg-success"><strong>Tạo lúc</strong></th>
                <th class="bg-success"><strong>Lần cuối</strong></th>
                <th class="bg-success"><strong>Trạng thái</strong></th>
                
            </thead>     
           
            @foreach ($list_trans as $index =>  $trans)
                <tbody>
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $trans->trans_title }}</td>
                        <td>{{ isset($trans->staff)?$trans->staff->ub_account_tvc:'-' }} - {{ isset($trans->staff)?$trans->staff->ub_title:'-' }}<br/>Sàn<b> {{ isset($trans->staff->group)?$trans->staff->group->gb_title:'-' }}</b></td>
                        <td>{{ isset($trans->product->pb_code)?$trans->product->pb_code:'-' }}</td>
                        <td align="right">{{ $trans->trans_deposit?$trans->trans_deposit:'-'}}</td>
                         <td>@foreach($trans->trans_status_log as $logs) 
                            @if($logs)
                            {!! isset($staffList[$logs['CREATED_BY']])? '<b>'.$staffList[$logs['CREATED_BY']]['ub_title'].'</b>':'-' !!}:
                            {{ $logs['NOTE'] }} lúc: 
                            <b>{{ date('d-m-Y h:i',$logs['TIME']) }}</b> Giá trị: <b>{{$logs['VALUE']}}</b>
                            @endif <br/>
                         @endforeach</td>
                        <td>{{ date('d-m-Y h:s:i', strtotime($trans->trans_created_time)) }}</td>
                        <td>{{ date('d-m-Y h:s:i', strtotime($trans->trans_updated_time)) }}</td>
                        <td>{!! \App\BOTransaction::STATUSES_COLOR[$trans->trans_code]  !!}</td>                       
                    </tr>
                </tbody>
            @endforeach
            
        </table>
        {{ $list_trans->links() }}
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">
@endsection
@section('jsfile')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> 

<script type="text/javascript">
    $('.js-select2').select2();
    $('.input-daterange').datetimepicker({ format: 'DD-MM-YYYY' });
    function exportExcel(){
          alert('Hoàn thiện sau');
        // $('#export_excel').val(1);
        form = $('form[name="frmFilter"]');
        // form.submit();
    }
</script>

@endsection