@if(isset($list_bills)&&is_array($list_bills))
@foreach ($list_bills as $index =>  $bill)
    <tbody>
        <tr>
            <td>{{ $index + 1 }}</td>
            <td>{{ $bill->bill_code }}</td>
            <td>{{ explode($bill->bill_customer_info)['name'] }}</td>
            <td>{{ $bill->bill_total_money }}</td>
            <td>{{ $bill->bill_required_money }}</td>
            <td>{{ $bill->bill_approval_log }}</td>
            <td>{{ $bill->bill_created_time }}</td>

            <td>{{ \App\BOBill::STATUS_TEXTS_WEB[$bill->status_id]  }}</td>
           
        </tr>
    </tbody>
@endforeach
@endif