<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2019-03-12
 * Time: 11:28 AM
 */
?>
<!doctype html>
<html lang="vi">
<head>
    <style>
        table thead tr {
            background-color: #0f74a8;
        }
        table thead tr th {
            color: #fff;
            height: auto;
            text-align: center;
        }
        table tbody tr {
            text-align: center;
        }
    </style>
</head>
<body>
    <table>
        <thead>
        <tr style="height: 30px;">
            <th>STT</th>
            <th width="40">Đơn vị</th>
            <th>Mã CRM</th>
            @if(isset($departments))
                @foreach($departments as $department)
                    <th width="20" style="padding: 20px 2px;text-align:left;">{{ $department }}</th>
                @endforeach
            @endif
        </tr>
        </thead>
        @if(isset($projects))
        <tbody>
            @foreach($projects as $key => $project)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <th scope="row" style="text-align:left;">
                        {{ $project->title }}
                    </th>
                    <th scope="row">
                        {{ $project->code }}
                    </th>
                    {{--@foreach($departments as $department)--}}
                        {{--<td style="font-style:italic;font-weight: bold;"></td>--}}
                    {{--@endforeach--}}
                </tr>
            @endforeach
        </tbody>
        @endif
    </table>
</body>
</html>
