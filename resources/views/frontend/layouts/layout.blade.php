<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from swlabs.co/madmin/code/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Aug 2015 17:13:58 GMT -->

<head>
    <title>Dashboard | Dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Thu, 19 Nov 1900 08:52:00 GMT">
    <link rel="shortcut icon" href="<?php echo URL::to('/');?>/frontend/images/icons/favicon.html">
    <link rel="apple-touch-icon" href="<?php echo URL::to('/');?>/frontend/images/icons/favicon-2.html">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo URL::to('/');?>/frontend/images/icons/favicon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo URL::to('/');?>/frontend/images/icons/favicon-114x114.html">
    <!--BEGIN CSS-->
    @include('frontend.layouts.css')
    <!--END CSS-->
</head>

<body class=" ">
<div>
    <!--BEGIN BACK TO TOP-->
    <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
    <!--END BACK TO TOP-->


    <!--BEGIN TOPBAR-->
    @include('frontend.layouts.top_menu')
    <!--END TOPBAR-->
    <div id="wrapper">
        <!--BEGIN SIDEBAR MENU-->
        @include('frontend.layouts.sidebar')
        <!--END SIDEBAR MENU-->

        <!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper">
            <!--BEGIN TITLE & BREADCRUMB PAGE-->
            @include('frontend.layouts.breadcrumb')
            <!--END TITLE & BREADCRUMB PAGE-->

            <!--BEGIN CONTENT-->
            <div class="page-content">
                <!--BEGIN CONTENT PAGE-->
                @yield('content')
                <!--END CONTENT PAGE-->
            </div>
            <!--END CONTENT-->
        </div>
        <!--BEGIN FOOTER-->
        @include('frontend.layouts.foot')
        <!--END FOOTER-->
        <!--END PAGE WRAPPER-->
    </div>
</div>
<!--BEGIN JS PAGE-->
@include('frontend.layouts.js')
<!--END JS PAGE-->
</body>
</html>