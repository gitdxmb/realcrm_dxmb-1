@extends('frontend.layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12 blog-page">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 blog-articles">
                        <h1>TIN THEO PHÒNG BAN</h1>
                        <div class="row">
                            <div class="col-md-4 blog-img">
                                <img src="images/blogs/1.jpg" alt="" class="img-responsive"/>
                                <ul class="list-inline blog-date">
                                    <li>
                                        <i class="fa fa-calendar fa-fw"></i>
                                        <a href="#">May 10, 2013</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-comments fa-fw"></i>
                                        <a href="#">41 Comments</a>
                                    </li>
                                </ul>
                                <ul class="list-inline blog-tags">
                                    <li>
                                        <i class="fa fa-tags fa-fw"></i>
                                        <a href="#" class="label label-default">Design</a>
                                        <a href="#" class="label label-default">Development</a>
                                        <a href="#" class="label label-default">Html</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-8 blog-article">
                                <h3>
                                    <a href="page-blog-item.html">Sed ut perspiciatis unde omnis iste natus.</a>
                                </h3>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                                <a href="page-blog-item.html" class="btn btn-primary">Chi tiết
                                    &nbsp;
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-4 blog-img">
                                <img src="images/blogs/2.jpg" alt="" class="img-responsive"/>
                                <ul class="list-inline blog-date">
                                    <li>
                                        <i class="fa fa-calendar fa-fw"></i>
                                        <a href="#">May 10, 2013</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-comments fa-fw"></i>
                                        <a href="#">41 Comments</a>
                                    </li>
                                </ul>
                                <ul class="list-inline blog-tags">
                                    <li>
                                        <i class="fa fa-tags fa-fw"></i>
                                        <a href="#" class="label label-default">Design</a>
                                        <a href="#" class="label label-default">Development</a>
                                        <a href="#" class="label label-default">Html</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-8 blog-article">
                                <h3>
                                    <a href="page-blog-item.html">Sed ut perspiciatis unde omnis iste natus.</a>
                                </h3>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                                <a href="page-blog-item.html" class="btn btn-primary">Chi tiết
                                    &nbsp;
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-4 blog-img">
                                <img src="images/blogs/3.jpg" alt="" class="img-responsive"/>
                                <ul class="list-inline blog-date">
                                    <li>
                                        <i class="fa fa-calendar fa-fw"></i>
                                        <a href="#">May 10, 2013</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-comments fa-fw"></i>
                                        <a href="#">41 Comments</a>
                                    </li>
                                </ul>
                                <ul class="list-inline blog-tags">
                                    <li>
                                        <i class="fa fa-tags fa-fw"></i>
                                        <a href="#" class="label label-default">Design</a>
                                        <a href="#" class="label label-default">Development</a>
                                        <a href="#" class="label label-default">Html</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-8 blog-article">
                                <h3>
                                    <a href="page-blog-item.html">Sed ut perspiciatis unde omnis iste natus.</a>
                                </h3>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                                <a href="page-blog-item.html" class="btn btn-primary">Chi tiết
                                    &nbsp;
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-4 blog-img">
                                <img src="images/blogs/4.jpg" alt="" class="img-responsive"/>
                                <ul class="list-inline blog-date">
                                    <li>
                                        <i class="fa fa-calendar fa-fw"></i>
                                        <a href="#">May 10, 2013</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-comments fa-fw"></i>
                                        <a href="#">41 Comments</a>
                                    </li>
                                </ul>
                                <ul class="list-inline blog-tags">
                                    <li>
                                        <i class="fa fa-tags fa-fw"></i>
                                        <a href="#" class="label label-default">Design</a>
                                        <a href="#" class="label label-default">Development</a>
                                        <a href="#" class="label label-default">Html</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-8 blog-article">
                                <h3>
                                    <a href="page-blog-item.html">Sed ut perspiciatis unde omnis iste natus.</a>
                                </h3>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                                <a href="page-blog-item.html" class="btn btn-primary">Chi tiết
                                    &nbsp;
                                    <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 blog-sidebar">
                        <h3>Tìm kiếm</h3>
                        <div class="blog-search">
                            <div class="input-group">
                                <input type="text" placeholder="Nhập từ khóa..." class="form-control"/>
                                <span class="input-group-btn">
                                                                                                                                        <button type="button" class="btn btn-default">
                                                                                                                                            <i class="fa fa-search"></i>
                                                                                                                                        </button>
                                                                                                                                    </span>
                            </div>
                        </div>
                        <div class="mbl"></div>
                        <h3>Tabs</h3>
                        <div class="tabbable tabbable-custom">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab_1_1">Section 1</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab_1_2">Section 2</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab_1_1" class="tab-pane active">
                                    <p>I'm in Section 1.</p>
                                    <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                </div>
                                <div id="tab_1_2" class="tab-pane">
                                    <p>Howdy, I'm in Section 2.</p>
                                    <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
                                </div>
                            </div>
                        </div>
                        <div class="mbl"></div>
                        <h3>Flickr</h3>
                        <ul class="list-inline blog-images">
                            <li>
                                <a href="images/blogs/1.jpg" data-lightbox="image-1" data-title="Image 1">
                                    <img src="images/blogs/1.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/2.jpg" data-lightbox="image-2" data-title="Image 2">
                                    <img src="images/blogs/2.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/3.jpg" data-lightbox="image-3" data-title="Image 3">
                                    <img src="images/blogs/3.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/4.jpg" data-lightbox="image-4" data-title="Image 4">
                                    <img src="images/blogs/4.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/5.jpg" data-lightbox="image-5" data-title="Image 5">
                                    <img src="images/blogs/5.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/6.jpg" data-lightbox="image-6" data-title="Image 6">
                                    <img src="images/blogs/6.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/7.jpg" data-lightbox="image-7" data-title="Image 7">
                                    <img src="images/blogs/7.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/8.jpg" data-lightbox="image-8" data-title="Image 8">
                                    <img src="images/blogs/8.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/9.jpg" data-lightbox="image-9" data-title="Image 9">
                                    <img src="images/blogs/9.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/10.jpg" data-lightbox="image-10" data-title="Image 10">
                                    <img src="images/blogs/10.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/11.jpg" data-lightbox="image-11" data-title="Image 11">
                                    <img src="images/blogs/11.jpg" alt=""/>
                                </a>
                            </li>
                            <li>
                                <a href="images/blogs/12.jpg" data-lightbox="image-12" data-title="Image 12">
                                    <img src="images/blogs/12.jpg" alt=""/>
                                </a>
                            </li>
                        </ul>
                        <div class="mbl"></div>
                        <h3>Archives</h3>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-right mrm"></i>March 2012
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-right mrm"></i>September 2011
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-right mrm"></i>July 2011
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-right mrm"></i>June 2011
                                </a>
                            </li>
                        </ul>
                        <div class="mbl"></div>
                    </div>
                </div>
                <ul class="pagination pull-right">
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-left"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">1</a>
                    </li>
                    <li>
                        <a href="#">2</a>
                    </li>
                    <li>
                        <a href="#">3</a>
                    </li>
                    <li>
                        <a href="#">4</a>
                    </li>
                    <li>
                        <a href="#">5</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection