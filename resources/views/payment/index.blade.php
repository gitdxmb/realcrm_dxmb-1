@extends('layouts.admin.layoutAdmin')
@section('content')
    <div class="page-header">
        <h2 class="text-success"><i class="fa fa-money"></i> Quản lý Thanh toán</h2>
    </div>

    <form class="filterForm row" method="GET" autocomplete="off">
        {{--<div class="col-md-3">
            <input type="text" name="code" class="form-control input-sm" placeholder="Nhập mã thanh toán (OBP...)"
                   value="{{ $filter['code']?? '' }}"
            />
        </div>--}}
        <div class="col-md-3">
            <select name="product" class="form-control input-sm" id="productSelect2">
            </select>
        </div>
        <div class="col-md-2">
            <select name="status" class="form-control input-sm">
                <option value="" selected>Tình trạng</option>
                <option value="0" {{ $filter['status']===0? 'selected' : '' }}>Chờ xử lý</option>
                <option value="1" {{ $filter['status']==1? 'selected' : '' }}>Đã xử lý</option>
                <option value="-1 {{ $filter['status']==2? 'selected' : '' }}">Từ chối</option>
            </select>
        </div>
        <div class="col-md-3">
            <select name="bill_code" class="form-control js-select2" data-allow-clear="true" data-placeholder="Chọn hợp đồng">
                <option value="" selected>Chọn hợp đồng</option>
                @if($bills)
                    @foreach($bills as $bill)
                        <option {{ $filter['bill_code']==$bill? 'selected' : '' }} value="{{ $bill }}">{{ $bill }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Tìm kiếm</button>
        </div>
    </form>
    <br>

    <div>
        @if($errors->all())
            <div class="alert alert-danger backend">
                @foreach($errors->all() as $error)
                    <p><i class="fa fa-exclamation-triangle"></i> {{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(session()->get('message'))
            <div class="alert alert-success">
                <p>{{ session()->get('message') }}</p>
            </div>
        @endif
    </div>

    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Danh sách</div>
        <div class="panel-body">
        @if(isset($data))
            <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="payment_table">
                        <thead>
                        <tr>
                            <th class="text-center"><strong>#</strong></th>
                            <th class="text-center"><strong>Mã TT</strong></th>
                            <th class="text-center"><strong>TT sản phẩm</strong></th>
                            <th class="text-center"><strong>TT hợp đồng</strong></th>
                            <th class="text-center"><strong>Số tiền yêu cầu</strong></th>
                            <th class="text-center"><strong>Thực Thu/Chi</strong></th>
                            <th class="text-center"><strong>Ghi chú</strong></th>
                            <th class="text-center"><strong>Mã Tavico</strong></th>
                            <th class="text-center"><strong>Ngày tạo</strong></th>
                            <th class="text-center"><strong>Tình trạng</strong></th>
                            <th class="text-center"><strong>Thao tác</strong></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $item)
                            <tr class="text-center">
                                <td>{{ $data->total() - (($data->currentPage()-1) * $data->perPage() + $key) }}</td>
                                <td>
                                    <p class="small">
                                        <a href="{{ route('show-payment', ['id' => $item->id]) }}"><i><b>{{ $item->pb_code }}</b></i></a>
                                    </p>
                                </td>
                                <td class="text-left">
                                    @if(isset($apartments) && isset($apartments[$item->bill->product_id]))
                                    <dl>
                                        <?php $apartment = $apartments[$item->bill->product_id]; ?>
                                        <dt><p>{{ $apartment->code }}</p></dt>
                                        <dd class="small">Tòa: <i>{{ $apartment->category? $apartment->category->title : '(N/A)' }}</i></dd>
                                        <dd class="small">Dự án: <i>{{ $apartment->category&&$apartment->category->sibling? $apartment->category->sibling->title : '(N/A)' }}</i></dd>
                                    </dl>
                                    @else
                                        <p class="text-danger">Không có TT</p>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <p><b>{{ $item->bill_code?? '(Không có HĐ)' }}</b></p>
                                    @if($item->bill)
                                        <span style="font-size: smaller">
                                            {!! \App\BOBill::STATUS_TEXTS_WEB_COLOR[$item->bill->status_id] !!}
                                        </span>
                                        <dl class="small">
                                            <dt>Giá trị HĐ:</dt>
                                            <dd>{{ $item->bill->total_money }}</dd>
                                            <dt style="margin-top: 4px;">Khách hàng:</dt>
                                            <dd>{{ $item->bill->bill_customer_info->name }}</dd>
                                            <dt style="margin-top: 4px;">NVKD:</dt>
                                            <dd>{{ $item->requestStaff? $item->requestStaff->ub_account_tvc : '(Không rõ)' }}</dd>
                                        </dl>
                                    @endif
                                </td>
                                <td>
                                    <span class="label label-default">{{ number_format($item->pb_request_money,0,'.',',') }}</span>
                                </td>
                                <td>
                                    <span class="label label-{{ $item->type_of_payment == 'C' ? 'primary' : 'danger' }}">
                                        {{ $item->type_of_payment == 'C' ? '' : '-'  }} {{ number_format($item->pb_response_money,0,'.',',') }}
                                    </span>
                                </td>
                                <td class="text-left">
                                    <p><i>{{ $item->pb_note2 }}</i></p>
                                </td>
                                <td>
                                    <p class="small text-danger">
                                        <i>Thanh toán:</i>
                                        <br>
                                        <b>{{ $item->reference_code? $item->reference_code : '(N.A)' }}</b>
                                    </p>
                                    <p class="small text-info">
                                        <i>Giao dịch F1:</i>
                                        <br>
                                        <b>{{ $item->bill&&$item->bill->contract_reference_code_F1? $item->bill->contract_reference_code_F1 : '(N.A)' }}</b>
                                        <br>
                                        <i>Giao dịch F2:</i>
                                        <br>
                                        <b>{{ $item->bill&&$item->bill->contract_reference_code? $item->bill->contract_reference_code : '(N.A)' }}</b>
                                    </p>
                                </td>
                                <td>
                                    {{ date('d-m-Y H:i', strtotime($item->created_at)) }}
                                </td>
                                <td>
                                    @if($item->pb_status == -1)
                                        <span class="label label-danger">Từ chối</span>
                                    @elseif ($item->pb_status == 0)
                                        <span class="label label-warning">Chờ Xử Lý</span>
                                    @else
                                        <span class="label label-success">Đã Xử Lý</span>
                                    @endif
                                    @if($item->responseStaff)
                                    <p><i>bởi:</i> {{ $item->responseStaff->ub_title }}</p>
                                    @endif
                                </td>
                                <td style="width:90px;">
                                    <div class="btn-group">
                                        <a href="{{ route('show-payment', ['id' => $item->id]) }}"
                                           title="Cập nhật" class="btn btn-xs btn-danger">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @if($item->pb_status != -1)
                                            <a class="btn btn-xs btn-info" target="_blank" href="{{ url('/payment/print?pb_id='.$item->pb_id) }}" title="In phiếu thu">
                                                <i class="fa fa-print"></i>
                                            </a>&nbsp
                                            @if(isset($item->pdf))
                                            <a href="javascript:void(0);"
                                                title="In hợp đồng"
                                                class="btn btn-default btn-xs"
                                                onclick="window.open('{{ asset($item->pdf) }}').print(); return false;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>&nbsp;
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4" style="vertical-align:middle;">
                                <p>{{ 'Xem: '.$data->count().' / Tổng số: ' . $data->total() }}</p>
                            </td>
                            <td colspan="7" class="text-right">
                                {{ $data->links() }}
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            @else
                <div class="alert alert-warning">
                    <p>Không tìm thấy dữ liệu</p>
                </div>
            @endif
        </div>
    </div>
@endsection
