@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Thêm Đợt Thanh Toán</h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="tbl" value="b_o_payments">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="project" class="col-xs-12 col-sm-3 control-label text-left">Chọn Dự án</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" id="project" class="js-select2" onchange="GLOBAL_JS.v_fLoadBuilding(this)">
                    <option value=""><span class="text-center">Chọn dự án</span></option>
                    @foreach ($projects as $project)
                        <option value="{{ $project->id }}">{{ $project->cb_title }} ({{ $project->cb_code }})</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="building" class="col-xs-12 col-sm-3 control-label text-left">Chọn Tòa</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" id="building" class="js-select2" onchange="GLOBAL_JS.v_fLoadApartment(this)">
                    <option value=""><span class="text-center">Chọn tòa</span></option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="apartment" class="col-xs-12 col-sm-3 control-label text-left">Chọn Căn hộ</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" id="apartment" class="js-select2" onchange="GLOBAL_JS.v_fLoadBillByApartment(this)">
                    <option value="">Chọn căn hộ</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="type" class="col-xs-12 col-sm-3 control-label text-left">Loại phiếu</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="radio">
                    <label><input type="radio" class="type" id="typeOfPayment" name="typeOfPayment" value="C" onclick="GLOBAL_JS.v_fLoadBillByTypePayment(this)" checked>Phiếu Thu</label>
                </div>
                <div class="radio">
                    <label><input type="radio" class="type" id="typeOfPayment" name="typeOfPayment" value="D" onclick="GLOBAL_JS.v_fLoadBillByTypePayment(this)">Phiếu Chi</label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bill_code" class="col-xs-12 col-sm-3 control-label text-left">Chọn Bill</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" id="bill_id" name="bill_id" class="js-select2" onchange="GLOBAL_JS.v_fInfoBill()" >
                    <option value=""><span class="text-center">Chọn Bill</span></option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="customer_id" class="col-xs-12 col-sm-3 control-label text-left">Khách Hàng</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" id="customer_id" class="js-select2" name="customer_id" >
                    <option value=""> Khách Hàng </option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="request_staff_id" class="col-xs-12 col-sm-3 control-label text-left">Nhân Viên </label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select required style="width:300px" id="request_staff_id" class="js-select2" name="request_staff_id" >
                    <option value=""> Nhân Viên </option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_note" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control pb_note" name="pb_note" rows="5" id="pb_note" placeholder="Mô Tả..."></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_note" class="col-xs-12 col-sm-3 control-label text-left">Ghi chú</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control" name="pb_note2" rows="5" id="pb_note2" placeholder="Ghi chú..."></textarea>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_request_money" class="col-xs-12 col-sm-3 control-label text-left">Số Tiền yêu cầu</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input type="text" class="form-control formatMoney" name="pb_request_money" required/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_response_money" class="col-xs-12 col-sm-3 control-label text-left">Số Tiền Thu/Chi</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="pb_response_money" name="pb_response_money" field-name="Tên" type="text" class="form-control formatMoney" placeholder="Số Tiền" required/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="method_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn Phương Thức TT</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control" name="method_id" id="methodPayment" required>
                    <option value=""> Chọn Phương Thức TT</option>
                    @foreach ($a_PaymentMethod as $valPMMT)
                    <option code="{{$valPMMT->pm_code}}" value="{{$valPMMT->pm_id}}">{{$valPMMT->pm_title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bank_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn ngân hàng</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select style="width:300px" class="form-control js-select2" name="bank_id" id="bankCode">
                    <option value="">Chọn Ngân hàng</option>
                    @foreach ($a_Bank as $bank)
                        <option value="{{$bank->id}}">{{$bank->bank_code}} - {{ $bank->bank_title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="pb_bank_number_id" class="col-xs-12 col-sm-3 control-label text-left">Số Tài Khoản</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="pb_bank_number_id" name="pb_bank_number_id" type="text" class="form-control" readonly placeholder="Số Tài Khoản" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="reference_code" class="col-xs-12 col-sm-3 control-label text-left">Code trên tavico</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="reference_code" name="reference_code" type="text" class="form-control"/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding text-right">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <a href="/payment" class="btn btn-danger">Hủy bỏ</a>
            <input type="submit" name="submit" value="Cập nhật" class="btn btn-primary btn-sm submit">
        </div>
    </div>
</form>
@endsection
