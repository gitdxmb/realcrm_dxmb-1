@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase">Bảng hàng dự án</h3>
    <div class="alert alert-success">
        <strong>Tổng số căn hộ: {{ $count }}</strong>
    </div>
    <form method="get" action="" id="frmFilter" name="frmFilter" class="form-inline">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <select class="form-control input-sm js-select2" id="project" name="project" required onchange="GLOBAL_JS.v_fLoadBuilding(this)" style="width: 450px;">
                <option value=""><span class="text-center">Chọn dự án</span></option>
                @foreach ($a_Project as $o_Project)
                    <option value="{{ $o_Project->id }}" <?php echo isset($a_search['project']) && $a_search['project'] == $o_Project->id ? 'selected':''?> >{{ $o_Project->cb_title }} ({{ $o_Project->cb_code }})</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="form-control input-sm js-select2" id="building" name="building" required style="width: 250px;">
                <option value=""><span class="text-center">Chọn tòa</span></option>
                <?php if (isset($a_Building)) { ?>
                    @foreach ($a_Building as $building)
                    <option value="{{ $building->cb_id }}" <?php echo $a_search['building'] == $building->cb_id ? 'selected':''?>><span class="text-center">{{ $building->cb_title }} ({{ $building->cb_code }})</span></option>
                    @endforeach
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            {{--<input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">--}}
            <input type="submit" class="btn btn-success btn-sm submit" value="Tìm kiếm">
            <a href="/apartment/export{{ !empty($a_search) ? '?project='.$a_search['project'].'&building='.$a_search['building'] : '' }}" class="btn btn-primary btn-sm">Export</a>
        </div>
    </form>

    <div class="">
        <table class="table table-responsive table-hover table-bordered">
            <tr class="header-tr">
                <td class="bg-primary"><strong>Căn số</strong></td>
            <?php if (count($a_numberOfType)) { ?>
                @foreach ($a_numberOfType as $numberType => $infoType)
                    <td class="bg-primary"><strong>{{ $numberType  }}</strong></td>
                @endforeach
            </tr>
            <tr>
                <td class="bg-success"><strong>Hướng Cửa</strong></td>
                @foreach ($a_numberOfType as $infoType)
                    <td class="bg-success"><strong>{{ $infoType['pb_door_direction']  }}</strong></td>
                @endforeach
            </tr>
            <tr>
                <td class="bg-success"><strong>Hướng BC</strong></td>
                @foreach ($a_numberOfType as $infoType)
                    <td class="bg-success"><strong>{{ $infoType['pb_balcony_direction']  }}</strong></td>
                @endforeach
            </tr>
            @foreach ($a_numberOfFloor as $floor)
                <tr>
                    <td>  {{ $floor }}</td>
                    @foreach ($a_numberOfType as $numberType => $infoType)
                        <?php
                            $apartment = $floor . $numberType;
                            $apartment = isset($arrApartment[$apartment]) ? $apartment: '';
                            $tdId =  $apartment != '' ? $arrApartment[$apartment]['id'] : '';
                            $tdStyle = $apartment != '' ? 'background: '.$statusDisplay[$arrApartment[$apartment]['status']]['background-color'].'; color: '.$statusDisplay[$arrApartment[$apartment]['status']]['color'] : '';
                            $iconId = $apartment != '' ? 'icon-'.$arrApartment[$apartment]['id'] : '';
                            $classIcon = $apartment != '' ? $statusDisplay[$arrApartment[$apartment]['status']]['icon-web'] : '';
                            $price = $apartment != '' ? substr(ceil($arrApartment[$apartment]['price']), 0,-6) : '';
                        ?>

                        <td class="showDetailApartment" id="{{ $tdId }}" style="{{ $tdStyle }}">
                            <span>{{ $apartment  }}</span>
                            <span><i id="{{ $iconId }}" class="{{ $classIcon }}"></i></span>
                            <br>
                            <span>
                                {{ $price }}
                            </span>
                        </td>
                    @endforeach
                </tr>
            @endforeach
            <?php } ?>
        </table>
    </div>

    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_product">
@endsection

<!-- Modal -->
<div class="modal fade" id="detailApartmentPopup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>THÔNG TIN CĂN HỘ</h4>
            </div>

            <div class="modal-body">
                <h4 id="titleTransLog"></h4>
                <table class="table table-responsive table-hover table-striped table-bordered hide" id="tblTransLog">
                    <tr class="header-tr">
                        <td class="bg-success"><strong>Nhân viên</strong></td>
                        <td class="bg-success"><strong>Phòng ban</strong></td>
                        <td class="bg-success"><strong>Đến hạn bung Lock</strong></td>
                    </tr>
                    <tbody id="listTransLog">
                        <tr>
                            <td>dsff</td>
                            <td>vdvdfv</td>
                            <td>vdvdv</td>
                        </tr>
                    </tbody>
                </table>

                <!-- The form is placed inside the body of modal -->
                <form id="" name="" class="form-horizontal" enctype="multipart/form-data" method="post" action="">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="status" class="col-xs-12 col-sm-4 control-label text-left">Tình trạng</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <select id="status" class="form-control">
                                    <option value="">Chọn Tình trạng</option>
                                    <option value="0">Mở bán</option>
                                    <option value="-1">Đang chờ duyệt Lock</option>
                                    <option value="1">Lock thành công</option>
                                    <option value="2">Đã thêm thông tin KH</option>
                                    <option value="3">Khách hàng đã xác nhận</option>
                                    <option value="-4">Đặt chỗ</option>
                                    <option value="4">Chờ duyệt đặt cọc</option>
                                    <option value="5">Đặt cọc</option>
                                    <option value="6">Hợp đồng mua bán</option>
                                    <option value="7">Chủ đầu tư thu hồi</option>
                                    <option value="-2">Khác</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="status" class="col-xs-12 col-sm-4 control-label text-left">Trạng thái</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <select id="pb_status" class="form-control">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                    <option value="-1">Xóa</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_code" class="col-xs-12 col-sm-4 control-label text-left">Mã căn</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_code" type="text" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="stage" class="col-xs-12 col-sm-4 control-label text-left">Giai đoạn</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <select id="stage" class="form-control">
                                    <option value="">Chọn giai đoạn bán</option>
                                    <option value="1">Đặt cọc</option>
                                    <option value="2">Chuyển cọc trực tiếp</option>
                                    <option value="3">Hủy chỗ, cọc mới</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_used_s" class="col-xs-12 col-sm-4 control-label text-left">DT thông thủy</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_used_s" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_built_up_s" class="col-xs-12 col-sm-4 control-label text-left">DT tim tường</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_built_up_s" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_door_direction" class="col-xs-12 col-sm-4 control-label text-left">Hướng cửa chính</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_door_direction" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_balcony_direction" class="col-xs-12 col-sm-4 control-label text-left">Hướng ban công</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_balcony_direction" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="view" class="col-xs-12 col-sm-4 control-label text-left">View</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="view" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="book_type" class="col-xs-12 col-sm-4 control-label text-left">Loại đặt chỗ</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <select id="book_type" class="form-control">
                                    <option value="">Chọn loại</option>
                                    <option value="1">Đặt chỗ thưởng phạt</option>
                                    <option value="2">Đặt chỗ không thưởng phạt</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="price_used_s" class="col-xs-12 col-sm-4 control-label text-left">Giá thông thủy</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="price_used_s" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_price_per_s" class="col-xs-12 col-sm-4 control-label text-left">Giá tim tường</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_price_per_s" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="price_min" class="col-xs-12 col-sm-4 control-label text-left">Giá sàn</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="price_min" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="price_max" class="col-xs-12 col-sm-4 control-label text-left">Giá trần</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="price_max" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="price_maintenance" class="col-xs-12 col-sm-4 control-label text-left">Phí bảo trì</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="price_maintenance" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_price_total" class="col-xs-12 col-sm-4 control-label text-left">Tổng giá bán</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_price_total" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="number_lock" class="col-xs-12 col-sm-4 control-label text-left">Số người cho lock</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="number_lock" type="number" min="0" step="1" class="form-control formatMoney">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 no-padding">
                            <label for="pb_required_money" class="col-xs-12 col-sm-4 control-label text-left">Số tiền yêu cầu</label>
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <input id="pb_required_money" type="text" class="form-control formatMoney">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 no-padding">
                            <label for="number_lock" class="col-xs-12 col-sm-2 control-label text-left">Ghi chú</label>
                            <div class="col-xs-12 col-sm-10 no-padding">
                                <textarea id="note" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-5">
                            <button type="button" class="btn btn-primary" id="updateApartment">Cập nhật</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="closeModalApartment">Hủy bỏ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<style>
    .modal-dialog {
        width: 800px !important;
        margin: 30px auto !important;
    }
    .showDetailApartment{
        cursor: pointer;
    }
</style>