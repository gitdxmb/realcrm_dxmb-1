@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding">Tải Nhâp bảng hàng</h3>
    <div class="alert alert-success">
        <p><strong>File mẫu import:</strong> <a href="/mau-cap-nhat-bang-hang.xlsx" download >Download File</a></p>
    </div>
    <div class="alert alert-success">
        <p><h3>Các chú ý về File Import:</h3></p>
        <p><strong>Mã BĐS: chỉ chứa 12 kí tự quy ước theo mã dự án, tòa nhà, vị trí</strong></p>
        <p><strong>Cột "Tầng" có thể để trống, hệ thống tự xác định được số tầng dựa theo mã căn hộ.</strong>
        <p><strong>Cột "Phòng ngủ" có thể để trống hoặc chỉ được điền số.</strong>

        <p><strong>Cột "Trạng thái" có thể để trống hoặc chỉ được điền 1 trong 3 số:</strong>
        <p>Số 0: Căn sẽ bị ẩn đi và không hiển thị ở trên App, nhưng vẫn hiển thị trong phần quản trị</p>
        <p>Số 1 (Nếu để trống mặc định là 1): Căn hiển thị bình thường</p>
        <p>Số -1: Căn coi như bị xóa, không hiển thị cả trên App và phần quản trị bảng hàng</p>

        <p><strong>Cột "Góc" có thể để trống hoặc chỉ được điền 1 trong 2 số:</strong>
        <p>Số 0 (Hoặc để trống): Nếu không là căn góc</p>
        <p>Số 1: Nếu là căn góc</p>

        <p><strong>Cột "Giai đoạn" chỉ được điền 1 trong 3 số:</strong>
        <p>Số 1: Đặt cọc</p>
        <p>Số 2: Đặt chỗ, chuyển cọc trực tiếp</p>
        <p>Số 3: Đặt chỗ, phải hủy chỗ mới lên được cọc</p>

        <p><strong>Cột "Loại đặt chỗ" chỉ được điền 1 trong 2 số (Hoặc để trống nếu căn hộ không thuộc loại đặt chỗ):</strong>
        <p>Số 1: Đặt chỗ thưởng phạt</p>
        <p>Số 2: Đặt chỗ không thưởng phạt</p>

        <p><strong>Cột "Số người lock" (Số người cho phép lock căn này) chỉ được điền số.</strong>

        <p><strong>Các cột liên quan đến giá tiền (Giá tim tường, giá thông thủy, tổng giá...) cần định dạng như sau:</strong>
        <p>Bước 1:Chọn cột cần định dạng -> Chuột phải chọn <strong>"Format Cells".</strong> </p>
        <p>Bước 2: Tại tab <strong>"Number"</strong>, mục <strong>"Category"</strong> chọn <strong>"Number"</strong>.</p>
        <p>Bước 3: Tại khung bên phải:  Ô <strong>"Decimal places"</strong> chỉnh về số 0, tích chọn vào <strong>"Use 1000 Separator (,)"</strong>, tại khung <strong>"Negative numbers"</strong> chọn dòng thứ 3 -> OK</p>
    </div>
    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
        @if(isset($res))
            <div class="form-group">
                <div class="col-xs-12 col-sm-12 no-padding">
                    <label class="alert alert-warning">{!! $res !!}</label>
                </div>
            </div>
        @endif
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        
        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="ro_object_ids" class="col-xs-12 col-sm-3 control-label text-left">Chọn Tòa Nhà</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <select id="building" name="building"  size="15" class="js-select2">
                        <option value=""> Chọn</option>
                        @foreach ($a_projects as $key => $val)
                        <?php
                        if ($val['cb_level'] == 1) {
                            $beautiFomat = '';
                        } else if ($val['cb_level'] == 2) {
                            $beautiFomat = '--';
                        } else if ($val['cb_level'] == 3) {
                            $beautiFomat = '----';
                        }
                        ?>

                        <option value="{{$key}}" <?php if($val['cb_level'] == 1) echo 'disabled'; ?> >{{$beautiFomat}}{{$val['cb_title']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="ro_key" class="col-xs-12 col-sm-3 control-label text-left">Chọn File</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <input type="file" name="excel" id="excel" />
                </div>
            </div>
        </div>


        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <button type="reset" class="btn btn-default">Nhập lại</button>
                <input type="submit" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm ">
            </div>
        </div>
    </form>

@endsection
