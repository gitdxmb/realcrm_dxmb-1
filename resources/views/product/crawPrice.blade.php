@extends('layouts.admin.layoutAdmin')
@section('content')

    <h3 class="col-xs-12 no-padding text-uppercase">Chuyển Nhượng</h3>
    <div class="alert alert-danger hide backend">form</div>
    <form id="fileupload" class="form-horizontal" method="post" action="">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="product_code" class="col-xs-12 col-sm-3 control-label text-left">Chọn Dự Án</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <select style="width:300px" class="form-control js-select2" name="product_code" id="product_code" required>
                        <option value="">Chọn Dự Án</option>
                        @foreach ($a_Projects as $a_Project)
                        <option value="{{$a_Project->reference_code}}">{{$a_Project->cb_title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6 col-sm-3 no-padding text-right">
                <input type="submit" name="submit" class="btn btn-primary" value="Cập nhật">
            </div>
        </div>
    </form>
@endsection
