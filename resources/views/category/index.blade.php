@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase">Danh sách dự án</h3>
    <form method="get" action="" id="frmFilter" name="frmFilter" class="form-inline">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <select class="form-control input-sm js-select2" id="project" name="search_project" onchange="GLOBAL_JS.v_fLoadBuilding(this)" style="width: 450px;">
                <option value=""><span class="text-center">Chọn dự án</span></option>
                @foreach ($arrProject as $id => $arrInfo)
                    <option value="{{ $id }}" <?php echo isset($a_search['search_project']) && $a_search['search_project'] == $id ? 'selected':''?> >{{ $arrInfo['title'] }} ({{ $arrInfo['code'] }})</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="form-control input-sm js-select2" id="building" name="search_building" style="width: 250px;">
                <option value=""><span class="text-center">Chọn tòa</span></option>
                <?php if (isset($a_Building)) { ?>
                @foreach ($a_Building as $building)
                    <option value="{{ $building->cb_id }}" <?php echo $a_search['search_building'] == $building->cb_id ? 'selected':''?>><span class="text-center">{{ $building->cb_title }} ({{ $building->cb_code }})</span></option>
                @endforeach
                <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
    </form>
    <br>
    <div>
        <a class="btn btn-primary" href="/category/addedit">Thêm mới</a>
    </div>
    <div class="">
        <table class="table table-responsive table-hover table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Loại danh mục</strong></td>
                <td class="bg-success"><strong>Tiêu đề</strong></td>
                <td class="bg-success"><strong>Kiểu</strong></td>
                <td class="bg-success"><strong>Mã</strong></td>
                <td class="bg-success"><strong>Thuộc dự án</strong></td>
                <td class="bg-success"><strong>Kiểu bảng hàng</strong></td>
                <td class="bg-success"><strong>Mở Lock</strong></td>
                <td class="bg-success"><strong>Chủ đầu tư</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($cats as $index => $cat)
                <tr>
                    <td>  {{ $index + 1 }}</td>
                    <td>  {{ $cat->cb_level == 1 ? 'Dự án' : 'Tòa' }}</td>
                    <td>  {{ $cat->cb_title }}</td>
                    <td>  {{ $cat->cb_level == 1 ? ($cat->apartment_grid == 1 ? 'Chung cư' : 'Liền kề - Biệt thự - Đất nền' ) : '' }}</td>
                    <td>  {{ $cat->reference_code }}</td>
                    <td>  {{ $cat->cb_level == 2 && isset($arrProject[$cat->parent_id]) ? $arrProject[$cat->parent_id]['title'] : ''  }}</td>
                    <td>  {{ $cat->type == 'private' ? 'Riêng' : ($cat->cb_level == 1 ? '' : 'Chung') }}</td>
                    <td>  {{ $cat->active_booking == 1 ? 'Cho Lock' : ($cat->cb_level == 1 ? '' : 'Không cho Lock') }}</td>
                    <td>  {{ $cat->cb_level == 1 && isset($arrInvestor[$cat->investor_id]) ? $arrInvestor[$cat->investor_id]['name'] : ''  }}</td>
                    <td>
                        <a title="Edit" href="<?php echo Request::root().'/category/addedit?id='.$cat->id;?>" title="Edit" class="not-underline">
                            <i class="fa fa-edit fw"></i>
                        </a>
                        <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $cat->id }},1,'cb_status')" title="Cho vào thùng rác" class="not-underline">
                            <i class="fa fa-trash fa-fw text-danger"></i>
                        </a>&nbsp;
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_categories">
    <?php echo (empty($a_search)) ? $cats->render(): $cats->appends($a_search)->render();?>

@endsection