@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase"><?php echo $catId == 0 ? 'Thêm mới' : 'Sửa' ?></h3>
    <div class="alert alert-danger hide backend"></div>
    <form id="fileupload" class="form-horizontal" method="post" action="">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <input type="hidden" id="id" value="<?php echo $catId ?>">
        <input type="hidden" id="tbl" value="b_o_payments">

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="bill_code" class="col-xs-12 col-sm-3 control-label text-left">Loại</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <select style="width:300px" id="selectTypeCate" name="level" class="form-control" required>
                        <option value="">Chọn loại</option>
                        <option value="1" {{ $cat->cb_level == 1 ? 'selected' : '' }}>Dự án</option>
                        <option value="2" {{ $cat->cb_level == 2 ? 'selected' : '' }}>Tòa nhà</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="type" class="col-xs-12 col-sm-3 control-label text-left">Loại dự án</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <div class="radio">
                        <label><input type="radio" class="apartmentGrid" name="apartmentGrid" value="1" {{ $cat->apartment_grid == 1 ? 'checked' : '' }} {{ $cat->cb_level == 2 ? 'disabled' : '' }} >Chung cư</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="apartmentGrid" name="apartmentGrid" value="0" {{ $catId != 0 && $cat->apartment_grid == 0 ? 'checked' : '' }} {{ $cat->cb_level == 2 ? 'disabled' : '' }}>Liền kề, biệt thự, đất nền</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="type" class="col-xs-12 col-sm-3 control-label text-left">Kiểu bảng hàng</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <div class="radio">
                        <label><input type="radio" class="type" name="type" value="private" {{ $cat->type == 'private' ? 'checked' : '' }} {{ $cat->cb_level == 1 ? 'disabled' : '' }}>Riêng</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="type" name="type" value="share" {{ $cat->type == 'share' ? 'checked' : '' }} {{ $cat->cb_level == 1 ? 'disabled' : '' }}>Chung</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="active_booking" class="col-xs-12 col-sm-3 control-label text-left">Mở Lock</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <div class="radio">
                        <label><input type="radio" class="active_booking" name="active_booking" value="1" {{ $cat->active_booking == 1 ? 'checked' : '' }} {{ $cat->cb_level == 1 ? 'disabled' : ''  }}>Cho Lock</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" class="active_booking" name="active_booking" value="0" {{ $cat->active_booking === 0 ? 'checked' : '' }} {{ $cat->cb_level == 1 ? 'disabled' : ''  }}>Không cho Lock</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group {{ $cat->cb_level == 1 ? 'hide' : ''}}">
            <div class="col-xs-12 col-sm-3 no-padding">
                <label for="ub_status" class="col-xs-6 control-label text-left">Mở gửi Email</label>
                <div class="col-xs-6 no-left-padding">
                    <input id="active_send_mail" name="send_mail" type="checkbox" class="form-control" {{ $cat->cb_level == 1 ? 'disabled' : ''}}  {{ $catId == 0 ? 'checked' : ($cat->send_mail == 1 ? 'checked' : '') }} >
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="investor" class="col-xs-12 col-sm-3 control-label text-left">Chọn Chủ đầu tư</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <select style="width:300px" id="investor" name="investor" class="form-control js-select2" {{ $cat->cb_level == 2 ? 'disabled' : '' }} required>
                        <option value="">Chọn chủ đầu tư</option>
                        @foreach ($investors as $investor)
                            <option value="{{$investor->id}}" {{ $cat->investor_id == $investor->id ? 'selected' : '' }}>{{$investor->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="bill_code" class="col-xs-12 col-sm-3 control-label text-left">Chọn dự án</label>
                <div class="col-xs-12 col-sm-6 no-padding">
                    <select style="width:300px" id="project" name="project" class="js-select2" {{ $cat->cb_level == 1 ? 'disabled' : '' }} required>
                        <option value="">Chọn dự án</option>
                        @foreach ($projects as $project)
                            <option value="{{$project->id}}" {{ $cat->parent_id == $project->id ? 'selected' : '' }}>{{$project->cb_title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="reference_code" class="col-xs-12 col-sm-3 control-label text-left">Mã</label>
                <div class="col-xs-12 col-sm-9 no-padding">
                    <input id="reference_code" name="reference_code" type="text" value="<?php echo $cat->reference_code ?>" class="form-control check-duplicate" required/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="cb_title" class="col-xs-12 col-sm-3 control-label text-left">Tiêu đề</label>
                <div class="col-xs-12 col-sm-9 no-padding">
                    <input id="cb_title" name="cb_title" type="text" value="<?php echo $cat->cb_title ?>" class="form-control" required />
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <label for="cb_title" class="col-xs-12 col-sm-3 control-label text-left">Thời gian mở bảng hàng </label><i>(Để trống mặc định là luôn mở)</i>
                <div class="input-group date datetimepicker">
                    <input name="enableListPrice" type="text" class="form-control" value="{{ $cat->enable_list_price != Null ? date('d-m-Y H:i',$cat->enable_list_price) : '' }} "/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-6 col-sm-3 no-padding text-right">
                <input type="submit" name="submit" class="btn btn-primary" value="Cập nhật">
            </div>
        </div>
    </form>

@endsection
