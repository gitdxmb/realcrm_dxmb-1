@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách nhân viên</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="ub_title" name="ub_title" type="text" class="form-control input-sm" placeholder="tên" value="<?php echo isset($a_search['ub_title'])?$a_search['ub_title']:''?>">
    </div>
    <div class="form-group">
        <select class="form-control input-sm js-select2" id="branch_id" name="branch_id">
                    <option value=""><span class="text-center">Chọn Sàn</span></option>
                    @if(count($a_Branch) > 0)
                        @foreach($a_Branch as $key => $val )
                        <option value="{{$key}}" <?php    echo isset($a_search['branch_id']) && $a_search['branch_id'] == $key ? 'selected':''?> > {{$val}}</option>
                        @endforeach
                    @endif
        </select>
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên</strong></td>
                <td class="bg-success"><strong>Email</strong></td>
                <td class="bg-success"><strong>Phòng ban</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_Users as $a_val)
            <tr>
                <td> {{ $a_val->stt }}</td>
                <td> {{ $a_val->ub_title }}</td>
                <td> {{ $a_val->ub_email }}</td>
                <td><?php echo isset($a_Branch[$a_val->group_ids]) ? $a_Branch[$a_val->group_ids] : 'Chưa có phòng ban'; ?></td>
                <td>
                    <?php
                        if($a_val->ub_status == 1){
                    ?>
                    <a title="Edit" href="<?php echo Request::root().'/userBO/addedit?id='.$a_val->id;?>" title="Edit" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'ub_status')" title="Cho vào thùng rác" class="not-underline">
                    <i class="fa fa-trash fa-fw text-danger"></i>
                    </a>&nbsp;

                    <?php }else if($a_val->ub_status == 0){ ?>
                    <a title="Khôi phục user" href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'ub_status')"  title="Edit" class="not-underline">
                        <i class="fa fa-upload fw"></i>
                    </a>&nbsp;
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'ub_status')" title="Xóa vĩnh viễn" class="not-underline">
                        <i class="fa fa-trash-o fa-fw text-danger"></i>
                    </a>&nbsp;

                    <?php }?>
                </td>
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="b_o_users">
<?php echo (empty($a_search)) ? $a_Users->render(): $a_Users->appends($a_search)->render();?>

@endsection