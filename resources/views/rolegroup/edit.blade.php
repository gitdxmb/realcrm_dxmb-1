@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm Nhóm Quyền' : 'Sửa Nhóm Quyền' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="b_o_role_groups">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="name" class="col-xs-12 col-sm-3 control-label text-left">Tên Nhóm Quyền</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="rg_title" name="rg_title" field-name="Tên" type="text" value="<?php echo isset($RoleGroupData->rg_title) ? $RoleGroupData->rg_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Nhóm Quyền" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="rg_description" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control rg_description" name="rg_description" rows="5" id="rg_description" placeholder="Mô Tả..."><?php echo isset($RoleGroupData->rg_description) ? $RoleGroupData->rg_description : "" ?></textarea>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="rg_staff_ids" class="col-xs-12 col-sm-3 control-label text-left">Chọn nhân viên thuộc nhóm</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="rg_staff_ids" name="rg_staff_ids[]" multiple="multiple" class="js-select2">
                    @foreach ($a_Users as $a_User)
                    <option value="{{$a_User->ub_id}}" <?php if(isset($RoleGroupData->rg_staff_ids) &&  in_array($a_User->ub_id, (array) json_decode($RoleGroupData->rg_staff_ids))) echo"selected"; ?>>{{$a_User->ub_account_tvc}}</option>
                    @endforeach
                    
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="rg_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="rg_status" name="rg_status" type="checkbox" class="form-control" <?php if (isset($RoleGroupData->rg_status) && $RoleGroupData->rg_status): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
