<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-182">
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>{{ env('APP_NAME', 'Booking Online - DXMB') }}</title>

        <link href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>


        <!-- MetisMenu CSS -->
        <link href="<?php echo URL::to('/');?>/css/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo URL::to('/');?>/css/sb-admin-2.css" rel="stylesheet">
        <link href="<?php echo URL::to('/');?>/js/select2.min.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<?php echo URL::to('/');?>/css/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo URL::to('/');?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        @yield('cssfile')

        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>

    <body>

        <div id="wrapper backend" class="backend">

            <!-- Navigation -->
            @include('layouts.admin.head')

            <div id="page-wrapper" class="container-fluid">
                <!-- /.row -->
                @yield('content')
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--}}
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo URL::to('/');?>/js/metisMenu.min.js"></script>
        <!-- Morris Charts JavaScript -->
        <script src="<?php echo URL::to('/');?>/js/raphael.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo URL::to('/');?>/js/sb-admin-2.js"></script>
        <script src="<?php echo URL::to('/');?>/js/select2.min.js"></script>
        <script src="<?php echo URL::to('/');?>/js/global.js"></script>
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
        <script src="<?php echo URL::to('/');?>/js/realtime.js"></script>
        <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo URL::to('/');?>/plugin/tinymce/js/tinymce/tinymce.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

        <script src="{{ asset('js/custom.js') }}"></script>

        @yield('jsfile')
    </body>

</html>
