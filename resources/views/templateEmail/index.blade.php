@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase">Danh sách Template</h3>
    <form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <input id="pm_title" name="pm_title" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['pm_title'])?$a_search['pm_title']:''?>">
        </div>
        <div class="form-group">
            <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
    </form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Code</strong></td>
                <td class="bg-success"><strong>Tiêu Đề</strong></td>
                <td class="bg-success"><strong>Loại</strong></td>
                <td class="bg-success"><strong>Loại template</strong></td>
                <td class="bg-success"><strong>Thuộc danh mục</strong></td>
                <td class="bg-success"><strong>Dự án</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_Tmp as $index => $o_tmp)
                <tr>
                    <td>    {{ $index + 1 }}</td>
                    <td>    {{ $o_tmp->code }}</td>
                    <td>    {{ $o_tmp->title }}</td>
                    <td>    {{ $o_tmp->is_default == 1 ? 'Mặc định' : 'Khác' }}</td>
                    <td>    {{ \App\TemplateEmail::TYPE[$o_tmp->type] }}</td>
                    <td>
                        @if ($o_tmp->is_default == true)
                        {{ $o_tmp->is_apartment == true ? 'Chung cư' : 'Liền kề - biệt thự' }}
                        @endif
                    </td>
                    <td>    {{ isset($a_Project[$o_tmp->project]) ? $a_Project[$o_tmp->project]  : ''  }}</td>
                    <td>
                        <a title="Edit" href="<?php echo Request::root().'/template_email/addedit?is_default='.$o_tmp->is_default.'&id='.$o_tmp->id;?>" title="Edit" class="not-underline">
                            <i class="fa fa-edit fw"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="payment_methods">


@endsection