<?php
/**
 * Created by PhpStorm.
 * User: congnt
 * Date: 2018-11-05
 * Time: 3:44 PM
 */
?>
<div id="mail-content">{!! $content !!}</div>
<?php if($status == 3) { ?>
<div class="ahbJ5">
    <p>Click theo link dưới đ&acirc;y để x&aacute;c nhận:&nbsp;
        <a href="{{ $url }}/customer/confirm?customer={{ $customer }}&bill_id={{ $bill }}&status=STATUS_APPROVED_BY_CUSTOMER">TÔI XÁC NHẬN ĐÃ ĐỌC KỸ, HIỂU VÀ ĐỒNG Ý TOÀN BỘ NỘI DUNG TRÊN</a>
    </p>
</div>
<div class="ahbJ5">
    <p>Click theo link dưới đ&acirc;y để hủy:&nbsp;
        <a href="{{ $url }}/customer/confirm?customer={{ $customer }}&bill_id={{ $bill }}&status=STATUS_DISAPPROVED_BY_CUSTOMER">TÔI KHÔNG ĐỒNG Ý VÀ HỦY GIAO DỊCH NÀY</a>
    </p>
</div>
<?php } ?>
