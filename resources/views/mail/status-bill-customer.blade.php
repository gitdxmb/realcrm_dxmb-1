<h3>Kính chào Quý khách!</h3>
<?php if($statusBill == \App\BOBill::STATUSES['STATUS_DEPOSITED']) { ?>
<p>Chúc mừng Quý khách đặt mua thành công căn hộ {{ $productCode }} tại {{ $projectName }} và trở
    thành khách hàng thân thiết (KHTT) của Đất Xanh Miền Bắc. Mọi thông tin về giao
    dịch và chính sách ưu đãi dành cho KHTT, khách hàng có thể lên ứng dụng Đất Xanh
    Miền Bắc để tìm hiểu và cập nhật thêm.</p>
<p>Trong vòng 7 ngày kể từ ngày đặt mua thành công, Quý khách vui lòng hoàn tất hợp đồng mua bán căn hộ {{ $productCode }}. </p>
<?php } else if($statusBill == \App\BOBill::STATUSES['STATUS_SUCCESS']) { ?>
<p>Chúc mừng Quý khách đã chính thức trở thành tân chủ nhân căn hộ
    {{ $productCode }} tại dự án {{ $projectName }} do Công ty Cổ phần Dịch vụ và Địa ốc Đất Xanh
    Miền Bắc phân phối. </p>
<p> Đất Xanh Miền Bắc cảm ơn Quý khách hàng đã tin tưởng vào dịch vụ của chúng tôi. </p>
<?php } else if($statusBill == \App\BOBill::STATUSES['STATUS_BOOKED']) { ?>
<p>Chúc mừng Quý khách đặt chỗ thành công căn hộ {{ $productCode }} tại {{ $projectName }}.</p>
<?php } else if($statusBill == \App\BOBill::STATUSES['STATUS_CANCELED']) { ?>
        <?php if($typeBill == \App\BOBill::TYPE_DEPOSIT) { ?>
            <p>Đất Xanh Miền Bắc xác nhận Quý khách đã thanh lý đặt mua thành công cho căn hộ
                {{ $productCode }} tại dự án {{ $projectName }}. </p>
            <p>Quý khách vui lòng liên hệ với nhân viên kinh doanh hoặc hotline CSKH: 18006366
                để hoàn tất thủ tục.</p>
            <p>Đất Xanh Miền Bắc hi vọng tiếp tục đồng hành cùng Quý khách hàng trong các giao dịch tiếp theo. </p>
        <?php } else { ?>
            <p>Đất Xanh Miền Bắc xác nhận Quý khách đã thanh lý đặt chỗ thành công cho căn hộ
                {{ $productCode }} tại dự án {{ $projectName }}. </p>
            <p>Quý khách vui lòng liên hệ với nhân viên kinh doanh hoặc hotline CSKH: 18006366
                để hoàn tất thủ tục.</p>
            <p>Đất Xanh Miền Bắc hi vọng tiếp tục đồng hành cùng Quý khách hàng trong các giao dịch tiếp theo. </p>
        <?php } ?>
<?php } ?>
<p><strong>Mọi thông tin thêm về dự án Quý khách vui lòng liên hệ nhân viên kinh doanh hoặc hotline CSKH: 18006366</strong></p>
<p><strong>TRÂN TRỌNG!</strong></p>

