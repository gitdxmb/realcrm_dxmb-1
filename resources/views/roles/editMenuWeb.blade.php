@extends('layouts.admin.layoutAdmin')
@section('content')

<form class="form-horizontal" method="post" action="{{ route('web-menu-config') }}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="role_menu_web">
    <br/><br/>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="title" class="col-xs-12 col-sm-3 control-label text-left">Tên Quyền</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="title" name="title" field-name="Tên" type="text" value="<?php echo isset($a_RoleActive->title) ? $a_RoleActive->title : "" ?>" class="form-control check-duplicate" placeholder="Tên Quyền" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding ">
            <label for="role_group_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn Nhóm Quyền</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="role_group_id" name="role_group_id[]" multiple="multiple" class="js-select2" required>
                    @foreach ($a_RoleGroups as $a_RoleGroup)
                    <option value="{{$a_RoleGroup->rg_id}}" <?php if(isset($a_RoleActive->role_group_ids) &&  in_array($a_RoleGroup->rg_id, (array) json_decode($a_RoleActive->role_group_ids))) echo"selected"; ?>>{{$a_RoleGroup->rg_title}}</option>
                    @endforeach
                    
                </select>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Controller/Action</th>
                <th>ACTIVE</th>
            </tr>
        </thead>        
        <tbody>
            @foreach ($a_AllRole as $sz_Controller => $a_Action)
            
                <tr>
                    <td colspan="4"><strong>{{ $a_NameController[$sz_Controller] }}</strong>&nbsp &nbsp<input type="checkbox" id="{{ $sz_Controller }}" onclick="GLOBAL_JS.v_fCheckAllRoleGroup('{{ $sz_Controller }}')"></td>
                </tr>
                @foreach($a_Action as $key => $sz_ActionName)
                    <tr>
                        <td>&nbsp &nbsp &nbsp {{ $sz_ActionName }} </td>
                        <td>
                            <input type="checkbox" value="{{$key}}" name="{{$sz_Controller}}[]" id="" class="{{ $sz_Controller }}" <?php if(isset($a_RoleMenu[$sz_Controller])  && in_array($key, $a_RoleMenu[$sz_Controller])) echo "checked"; ?>/>
                        </td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <input class="btn btn-warning btn-sm" type="reset" value="Nhập lại" >
            <input class="btn btn-primary btn-sm" id="btn_submit_data" name="submit" value="Cập nhật" type="submit">
        </div>
    </div>
</form>

@endsection
