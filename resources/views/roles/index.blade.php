@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách Quyền</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="ro_title" name="ro_title" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['ro_title'])?$a_search['ro_title']:''?>">
    </div>        
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>    
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên</strong></td>
                <td class="bg-success"><strong>Mô Tả</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_Role as $a_val)
            <tr>
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->ro_title }}</td>
                <td>    {{ $a_val->ro_description }}</td>
                <td>
                    <?php
                        if($a_val->ro_status == 1){
                    ?>
                    <a title="Edit" href="{{ route('role-form') . '?id=' . $a_val->id }}" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'ro_status')" title="Cho vào thùng rác" class="not-underline">
                    <i class="fa fa-trash fa-fw text-danger"></i>
                    </a>&nbsp;

                    <?php }else if($a_val->ro_status == 0){ ?>
                    <a title="Khôi phục user" href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'ro_status')"  title="Edit" class="not-underline">
                        <i class="fa fa-upload fw"></i>
                    </a>&nbsp;
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'ro_status')" title="Xóa vĩnh viễn" class="not-underline">
                        <i class="fa fa-trash-o fa-fw text-danger"></i>
                    </a>&nbsp;

                    <?php }?>
                </td>
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="b_o_roles">
<?php echo (empty($a_search)) ? $a_Role->render(): $a_Role->appends($a_search)->render();?>

@endsection
