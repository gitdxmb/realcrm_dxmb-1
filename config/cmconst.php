<?php
return [    
    'source_id'=>[
        1 => 'Nguồn MKT',
        2 => 'nguồn NV tự tìm',
        3 => 'nguồn khác'
    ],
//    'name_controller'=>[
//        'UserController'=>'Quản lý user',
//        'ProjectsController'=>'Quản lý dự án',
//        'SupplierController'=>'Quản lý nhà cung cấp',
//        'ChannelController'=>'Quản lý kênh',
//        'BranchController'=>'Quản lý chi nhánh',
//        'JobController'=>'Quản lý tác vụ',
//        'RoleController'=>'Quản lý phân quyền',
//        'BudgetController'=>'Quản lý ngân sách'
//    ],
    'role_key'=>[
        'product'  => 'product',
        'transaction' => 'transaction',
        'bill' => 'bill',
        'customer' => 'customer',
        'project'=> 'project',
        'payment' => 'payment'
    ],
    'role_action' => [
        'view' => 'view', // Xem
        'approve' => 'approve', // Duyệt lock
        'book' => 'book', // Yêu cầu lock
        'update' => 'update', // Sửa
        'export' => 'export', // Xuất excel
        'mod' => 'mod',
        'admin'=> 'admin', // Full
        'confirm' => 'confirm', // Quyền DVKH
        'manage' => 'manage', // Quyền QLKD
        'accounting' => 'accounting', // Quyền Kế toán,
        'report'    => 'report' // Xuất, nhập Báo cáo
    ],
    'app_menu' =>  [
        'personal' => 'Cá nhân và thiết lập',
        'customer' => 'Khách hàng của tôi',
        'vehicleBooking' => 'Đặt xe của nhân viên',
        'vehicleBookingDriver' => 'QL Đặt xe của tài xế',
        'vehicleBookingReceptionist' => 'QL Đặt xe của Lễ tân',
        'project' => 'Dự án',
        'projectNews' => 'Tin dự án',
//        'billCheckManager' => 'GĐS duyệt Yêu cầu của TKKD',
//        'paymentCheck' => 'Duyệt thanh toán',
        'requestCheck' => 'Duyệt yêu cầu',
        'requestPersonal' => 'QL Yêu cầu lock cá nhân',
        'contract' => 'DS Hợp đồng',
        'contractCheck' => 'Duyệt hợp đồng',
        'report' => 'Báo cáo giao dịch',
        'forum' => 'Diễn đàn',
        'internalNews' => 'Tin nội bộ',
        'qrScanner' => 'Quét mã QR'
    ],
    'web_menu' => [
     
        'report_admin' => [
              'r_dashboard' => 'Dashboard',
              'r_transactions' => 'Thống kê yêu cầu',
              'r_bills'=>'Thống kê hợp đồng',
              'r_products'=>'Thống kê sản phẩm',
              'r_customers'=>'Thống kê khách hàng',
              'r_staffs'=>'Thống kê nhân viên'
          ],
        'management_customer' => [
            'list' => 'Danh sách khách hàng',
            'import'=>'Tải nhập khách hàng',
            'file_manager'=>'QL File Tải Nhập',
            'transfer'=>'Phân bổ khách hàng vừa tải nhập',
            'offical_customer'=>'Khách Hàng Chính Thức'
        ],
        'management_bank'=>[
            'list'=> 'Danh sách ngân hàng',
            'sync'=> 'Đồng bộ ngân hàng',            
            'addedit'=> 'Thêm sửa ngân hàng',
        ],
        'management_payment_method'=>[
            'list'=> 'Danh sách PTTT',
            'add' => 'Thêm mới PTTT'
        ],
        'management_role_group'=>[
            'list'=> 'Danh sách nhóm quyền',
            'add' => 'Thêm mới nhóm quyền'
        ],
        'management_role'=>[
            'list' => 'Danh sách quyền',
            'add' => 'Thêm mới quyền',
            'config_menu' =>'Cấu hình menu hiển thị',
            'list_menu' => 'danh sách quyền menu',
            'list_menu_web' => 'danh sách menu web',
            'config_menu_web' =>'Cấu hình menu web'
        ],
        'payment'=>[
            'list' => 'Danh sách đợt thanh toán',
            'add' => 'Thêm mới đợt thanh toán'
        ],
        'user' => [
            'list' => 'danh sách nhân viên',
            'add' => 'thêm mới nhân viên'
        ],
        'product_list' => [
            'import' => 'tải nhập bảng hàng',
            'list' => 'danh sách căn hộ',
            'tranfer' => 'Chuyển nhượng bảng hàng',
            'crawprice' => 'Kéo giá từ TVC về BO',
            'crawproduct' => 'Kéo BH từ TVC về BO'
        ],
        'booking_online' =>[
            'request_lock' => 'quản lý yeu cầu lock',
            'management_bill' => 'quản lý bill'
        ],
        'management_template'=>[
            'list'=> 'Danh sách template',
            'add' => 'thêm mới template'
        ],
        'customer_notice'=>[
            'list'=> 'Danh Sách Thông báo',
            'add' => 'thêm mới Thông báo'
        ],
        'category'=>[
            'list'=> 'Danh sách',
            'add' => 'Thêm mới'
        ],
        'investor'=>[
            'list'=> 'Danh sách',
            'add' => 'Thêm mới'
        ],
        'post'=>[
            'list'=> 'Danh sách',
            'add' => 'Thêm mới',
            'report'    => 'Báo cáo GD'
        ],
    ],
    'name_module'=>[

        'report_admin'=>'Siêu thống kê',

        'management_customer'=>'Quản lý khách hàng',
        'management_bank'=>'Quản lý ngân hàng',
        'management_payment_method'=>'Quản lý phương thức thanh toán',
        'management_role_group'=>'Quản lý nhóm quyền',
        'management_role'=>'Quản lý quyền',
        'payment'=>'Thanh toán',
        'user'=>'Nhân viên',
        'product_list'=>'Quản lý bảng hàng',
        'booking_online'=>'Booking online',
        'management_template'=>'Template email',
        'customer_notice'=>'Thông báo khách hàng',
        'category' => 'Dự án',
        'investor' => 'Chủ đầu tư',
        'post' => 'Tin tức & Báo cáo'
    ],
    'type_news'=>[
        1 => 'Ảnh/Slide',
        2 => 'Video',
        3 => 'Quote'
    ],
    'category_news' => [
        1 => 'Danh Mục 1',
        2 => 'Danh Mục 2',
        3 => 'Danh Mục 3'
    ],
    'account_CRM' => [
        'DXMB' => [
            'username' => 'PDTBO@DXMB',
            'pwd' => '123456',
            'db' => 'DXMB',
            'label' => 'ĐẤT XANH MIỀN BẮC',
            'key' => 'FCF2-112E-E9FD-A442-B1FE-36CC-83D6-7ABC-8B3E-9875'
        ],
        'DXNT' => [
            'username' => 'PDT.BO@DXNT',
            'pwd' => '123456',
            'db' => '',
            'label' => 'ĐẤT XANH NHA TRANG',
            'key' => 'D81C-B24C-673C-21CC-7CA1-7CEF-7AB4-33DC-DCFA-FF2C'
        ],
        'DXQN' => [
            'username' => 'PDT.BO@DXQN',
            'pwd' => '123456',
            'label' => 'ĐẤT XANH QUẢNG NINH',
            'db' => '',
            'key'   => '5418-86D7-D5CF-E4D7-0937-3926-5C95-B2ED-84D3-90E5'
        ],
        'CNNA' => [
            'username' => 'PDT.BO@CNNA',
            'pwd' => '123456',
            'label' => 'ĐẤT XANH NGHỆ AN',
            'db' => '',
            'key'   => '5098-15D5-EA50-EF1B-894D-EDBF-3774-88FC-822B-4756'
        ],
        'VEH' => [
            'username' => 'PDT.BO@VEH',
            'pwd' => '123456',
            'label' => 'VIETHOMES',
            'db' => '',
            'key'   => '3E45-531C-1EE6-69CD-CDAC-AAE4-FD11-9576-8206-2F13'
        ],
        'VAH' => [
            'username' => 'PDT.BO1@VAH',
            'pwd' => '123456',
            'label' => 'VINAHOMES',
            'db' => '',
            'key'   => '2992-56DB-1084-9172-1336-500C-9A19-729C-D195-B7EB'
        ],
        'DXMBT' => [
            'username' => 'TKKDFULL@DXMBT',
            'pwd' => '123456',
            'db' => 'DXMBT',
            'label' => 'ĐẤT XANH MIỀN BẮC',
            'key'   => '64B2-8DBF-C9B1-0CDB-5DF0-8703-3A80-1F09-861B-4E92'
        ],
        
    ],
    'account_CRM_F1' => [
        'DXMB' => [
            'DXNT' => 'NVCTYCDXNT',
            'DXQN' => 'NVCTYCDXQN',
            'CNNA' => 'NVCTYCCNNA',
            'VEH' => 'NVCTYCVEH',
            'VAH' => 'NVCTYCVAH'
        ],
        'DXNT' => [
            'DXMB' => 'NVCTYCDXMB',
            'DXQN' => 'NVCTYCDXQN',
            'CNNA' => 'NVCTYCCNNA',
            'VEH' => 'NVCTYCVEH',
            'VAH' => 'NVCTYCVAH'
        ],
        'DXQN' => [
            'DXMB' => 'NVCTYCDXMB',
            'DXNT' => 'NVCTYCDXNT',
            'CNNA' => 'NVCTYCCNNA',
            'VEH' => 'NVCTYCVEH',
            'VAH' => 'NVCTYCVAH'
        ],
        'CNNA' => [
            'DXMB' => 'NVCTYCDXMB',
            'DXNT' => 'NVCTYCDXNT',
            'DXQN' => 'NVCTYCDXQN',
            'VEH' => 'NVCTYCVEH',
            'VAH' => 'NVCTYCVAH'
        ],
        'VEH' => [
            'DXMB' => 'NVCTYCDXMB',
            'DXNT' => 'NVCTYCDXNT',
            'DXQN' => 'NVCTYCDXQN',
            'CNNA' => 'NVCTYCCNNA',
            'VAH' => 'NVCTYCVAH'
        ],
        'VAH' => [
            'DXMB' => 'NVCTYCDXMB',
            'DXNT' => 'NVCTYCDXNT',
            'DXQN' => 'NVCTYCDXQN',
            'CNNA' => 'NVCTYCCNNA',
            'VEH' => 'NVCTYCVEH'
        ],
        'DXMBT' => [
            'DXNT' => 'NVCTYCDXNT',
            'DXQN' => 'NVCTYCDXQN',
            'CNNA' => 'NVCTYCCNNA',
            'VEH' => 'NVCTYCVEH',
            'VAH' => 'NVCTYCVAH'
        ],
        
    ],
    
    
    
];
