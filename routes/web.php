<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Test route group
 */
Route::group(['prefix' => 'test'], function () {
    Route::get('/vnpay', 'HomeController@vnpay')->name('vnpay-test');
    Route::post('/vnpay', 'HomeController@vnpay_submit');
    Route::get('/vnpay-response', function () {
        return view('test.vnpay-response');
    })->name('vnpay-response');
    Route::get('/host', function () {
        return view('welcome');
    });
});

Route::get('sign-in', 'UserInBOController@loginUser');
Route::post('sign-in', 'UserInBOController@loginUser');
Route::get('sign-out', 'UserInBOController@logoutUser');
Route::post('sign-out', 'UserInBOController@logoutUser');
Route::get('ajax', 'AjaxController@SetProcess');
Route::post('ajax', 'AjaxController@SetProcess');

Route::get('customer/confirm', 'CustomerInBoController@Confirm');

Route::group(['middleware' => 'guest'], function () {
    Route::group(['prefix' => 'my-report'], function () {
        Route::get('/bill', 'ReportController@getBillReport')->name('report-bill');
        Route::post('/bill', 'ReportController@submitBillReport');
        Route::get('/bill-template', 'ReportController@downloadBillTemplate')->name('bill-report-template');
        Route::post('/bill-import', 'ReportController@importBillReport')->name('import-bill-report');
        Route::get('/compose/{from}/{to}', 'ReportController@composeReportPost')->name('compose-report');
    });


    Route::get('report/{type}/{action}', 'ReportController@index');
    Route::get('customer', 'CustomerInBoController@getAllCustomer');
    Route::get('customer/addedit', 'CustomerInBoController@addEditCustomer');
    Route::post('customer/addedit', 'CustomerInBoController@addEditCustomer');
    Route::get('customer/import', 'ImportExcelController@ImportCustomer');
    Route::post('customer/import', 'ImportExcelController@ImportCustomer');
    Route::get('customer/tranfer-customer', 'CustomerInBoController@tranferCustomerRecent');
    Route::post('customer/tranfer-customer', 'CustomerInBoController@tranferCustomerRecent');
    Route::get('offical_customer', 'CustomerInBoController@getAllOfficalCustomer');
    Route::get('offical_customer/edit', 'CustomerInBoController@EditOfficalCustomer');
    Route::post('offical_customer/edit', 'CustomerInBoController@EditOfficalCustomer');
    Route::get('customer/file_management', 'CustomerInBoController@listFileManagement');
    Route::post('customer/file_management', 'CustomerInBoController@listFileManagement');

    //bank
    Route::get('bank', 'BankInfoController@getAllBankInfo');
    Route::get('bank/addedit', 'BankInfoController@addEditBankInfo');
    Route::post('bank/addedit', 'BankInfoController@addEditBankInfo');

    //paymentMethod
    Route::get('payment_medthod', 'PaymentMethodController@getAllPayMentMethod');
    Route::get('payment_medthod/addedit', 'PaymentMethodController@addEditPayMentMethod');
    Route::post('payment_medthod/addedit', 'PaymentMethodController@addEditPayMentMethod');
    Route::get('payment/print', 'PaymentController@print');

    //role group
    Route::get('role_groups', 'RoleGroupController@getAllRoleGroup');
    Route::get('role_group/addedit', 'RoleGroupController@addEditRoleGroup');
    Route::post('role_group/addedit', 'RoleGroupController@addEditRoleGroup');

    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', 'RoleController@index')->name('role-list');
        Route::get('/form', 'RoleController@addEditRole')->name('role-form');
        Route::post('/form', 'RoleController@addEditRole');

        Route::get('config-app-menu', 'RoleController@ConfigRoleMenu')->name('app-menu-config');
        Route::post('config-app-menu', 'RoleController@ConfigRoleMenu');
        Route::get('app-menu', 'RoleController@getAllRoleMenu')->name('app-menu');
        Route::post('app-menu', 'RoleController@getAllRoleMenu');

        Route::get('web-menu', 'RoleController@getAllRoleMenuWeb')->name('web-menu');
        Route::get('config-web-menu', 'RoleController@editRoleMenuWeb')->name('web-menu-config');
        Route::post('config-web-menu', 'RoleController@editRoleMenuWeb');
    });
    Route::get('getRolebyUserId', 'RoleController@getRoleMenuByUserId');

    Route::get('payment/addnew', 'PaymentController@addNewPayment');
    Route::post('payment/addnew', 'PaymentController@addNewPayment');

    /** Payment Group */
    Route::group(['prefix' => 'payment'], function () {
        Route::get('/', 'PaymentController@index')->name('list-payment');
        Route::get('/form/{id}', 'PaymentController@showForm')->name('show-payment')->where('id', '[0-9]+');
        Route::post('/form/{id}', 'PaymentController@submitForm')->name('submit-payment')->where('id', '[0-9]+');
    });
    /** End Payment Group */

    // UserInBO
    Route::get('userBO', 'UserInBOController@getAllUserInBO');
    Route::get('userBO/addedit', 'UserInBOController@addEditUserInBO')->name('user-form');
    Route::post('userBO/addedit', 'UserInBOController@addEditUserInBO');

    //import UserInBo

    Route::get('importUser', 'UserInBOController@ImportUser');
    Route::post('importUser', 'UserInBOController@ImportUser');

    //import UserInBo
    Route::get('importUserChild', 'UserInBOController@ImportUserChild');
    Route::post('importUserChild', 'UserInBOController@ImportUserChild');

    Route::get('importGroups', 'UserInBOController@ImportUserGroups');
    Route::post('importGroups', 'UserInBOController@ImportUserGroups');

    Route::get('importProject', 'ImportExcelController@ImportProject');
    Route::post('importProject', 'ImportExcelController@ImportProject');
    Route::get('importBuilding', 'ImportExcelController@ImportBuilding');
    Route::post('importBuilding', 'ImportExcelController@ImportBuilding');
    Route::get('importProduct', 'ImportExcelController@ImportProduct');
    Route::post('importProduct', 'ImportExcelController@ImportProduct');

    ///Bảng hàng///
    Route::get('apartment', 'ProductController@getAllApartment');
    Route::get('apartment/import', 'ProductController@importApartment');
    Route::post('apartment/import', 'ProductController@importApartment');
    Route::get('apartment/export', 'ProductController@exportApartment');

    ///Book online///
    Route::get('transaction', 'TransactionController@getAllTransaction')->name('list-transaction');
    Route::get('transaction/confirm', 'TransactionController@confirm');
    Route::get('transaction/changeStatus', 'TransactionController@changeStatus');

    Route::group(['prefix' => 'bill'], function () {
        Route::get('/', 'BillController@getAllBill')->name('list-bill');
        Route::get('/confirm', 'BillController@confirm');
        Route::get('/changeStatus', 'BillController@changeStatus');
    });

    Route::get('template_email', 'TemplateEmailController@index');
    Route::get('template_email/addedit', 'TemplateEmailController@addedit');
    Route::post('template_email/addedit', 'TemplateEmailController@addedit');

    Route::get('pdf/index', 'PdfController@index');

//        Route::get('chat/index', 'ChatController@index');

    //customer notice
    Route::get('customer_notice/list', 'CustomerNoticeController@getAllCustomerNotice');
    Route::get('customer_notice/addedit', 'CustomerNoticeController@addEditCustomerNotice');
    Route::post('customer_notice/addedit', 'CustomerNoticeController@addEditCustomerNotice');

    // POLL
    Route::get('poll/add', 'PollController@addEditPoll');
    Route::post('poll/add', 'PollController@addEditPoll');

    ///Category//
    Route::get('category', 'CategoryController@index');
    Route::get('category/addedit', 'CategoryController@addEdit');
    Route::post('category/addedit', 'CategoryController@addEdit');

    ///Investor//
    Route::get('investor', 'InvestorController@index');
    Route::get('investor/addedit', 'InvestorController@addEdit');
    Route::post('investor/addedit', 'InvestorController@addEdit');

    // sync All Project
    Route::get('syncProduct', 'ProductController@syncAllProduct');
    Route::post('syncProduct', 'ProductController@syncAllProduct');

    // sync Price List
    Route::get('syncPriceList', 'ProductController@syncPriceList');
    Route::get('udpateCategory', 'ProductController@updatecategoryId');

    // Craw price form TVC to BO
    Route::get('crawPrice', 'ProductController@crawPrice');
    Route::post('crawPrice', 'ProductController@crawPrice');
    Route::get('crawProduct', 'ProductController@crawProductFromTvcToBO');
    Route::post('crawProduct', 'ProductController@crawProductFromTvcToBO');
    Route::get('processcrawProduct', 'ProductController@ProcessCrawProduct');

    // Post
    Route::group(['prefix' => 'post'], function () {
        Route::get('/', 'PostController@index')->name('post-list');
        Route::get('/form/{id?}', 'PostController@postForm')->name('post-form');
        Route::post('/form/{id?}', 'PostController@submitForm');
        Route::post('/{id}/delete', 'PostController@delete')->name('post-delete');
        Route::get('/{id}/feedbacks', 'PostController@getFeedbacks')->name('post-feedbacks')->where(['id' => '[0-9]+']);
    });
});

Route::group(['prefix' => 'noi-bo'], function () {
    Route::get('trang-chu', 'Frontend\HomeController@index');
});

Route::get('renewSession', 'API\APIController@__TVC_RENEW_SESSION');
// SYNC DATA TVC
Route::get('syncProjectTVC', 'CategoryController@syncAPIProjectTVC');
Route::post('syncProjectTVC', 'CategoryController@syncAPIProjectTVC');
Route::get('syncBuildingTVC', 'CategoryController@syncAPIBuildingTVC');
Route::post('syncBuildingTVC', 'CategoryController@syncAPIBuildingTVC');
Route::get('syncProductTVC', 'ProductController@syncAPIProductTVC');
Route::post('syncProductTVC', 'ProductController@syncAPIProductTVC');

//Sync bank
Route::get('syncBankTVC', 'BankInfoController@syncAPIBankTVC');
Route::post('syncBankTVC', 'BankInfoController@syncAPIBankTVC');

//Route::get('abcd', 'RoleController@getRoleMenuByUserId');

Route::get('/home', 'HomeController@index')->name('home');
// get image

Route::get('get_singnature', 'UserInBOController@getSignature');

Route::get('admin', function () {
    return redirect('/sign-in');
});
Route::get('/{path?}', function () {
    return view('APP.index');
})->where(['path' => '.+']);

Auth::routes();
